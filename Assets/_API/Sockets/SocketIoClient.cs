﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Quobject.SocketIoClientDotNet.Client;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEssentials;
using UnityEssentials.ExtensionMethods;

public class SocketIoClient : Singleton<SocketIoClient>
{
    #region Delegates
    public delegate void LogHandler (string message);
    public delegate void ConnectHandler ();
    public delegate void DisconnectHandler ();
    public delegate void RoomUpdateHandler (string message);
    #endregion

    #region Fields
    [SerializeField] private string _serverUrl = "ws://128.199.215.246:3000";
    [SerializeField] private bool _autoJoinGame = true;
    [SerializeField] private List<string> _logs = new List<string>();

    public string theUsername;
    public string roomId;

    private bool _initialized;
    private bool _authenticated;
    public string _roomJoined;
    private string _username = string.Empty;
    private string _password = string.Empty;

    protected Socket socket = null;

    public LogHandler On_Log;
    public ConnectHandler On_Connect;
    public DisconnectHandler On_Disconnect;
    public RoomUpdateHandler On_RoomUpdate;

    public Text label;
    public string labelQueue;
    #endregion

    #region Properties
    public string ServerUrl
    {
        get { return _serverUrl; }
    }

    public string Username
    {
        get { return _username; }
    }

    public bool Initialized
    {
        get { return _initialized; }
    }

    public bool Authenticated
    {
        get { return _authenticated; }
    }

    public bool InRoom
    {
        get { return !_roomJoined.IsNullOrEmpty(); }
    }
    #endregion

    #region Methods
    public void Initialize (string username, string password)
    {
        if (username.IsNullOrEmpty() ||
            password.IsNullOrEmpty())
            return;

        if (Initialized || socket != null)
            return;

        _username = username;
        _password = password;

        socket = IO.Socket(_serverUrl);

        socket.On(Socket.EVENT_CONNECT, OnConnect);
        socket.On(Socket.EVENT_CONNECT_ERROR, OnConnectError);
        socket.On(Socket.EVENT_CONNECT_TIMEOUT, OnConnectTimeout);
        socket.On(Socket.EVENT_DISCONNECT, OnDisconnect);
        socket.On(Socket.EVENT_ERROR, OnError);
        socket.On(Socket.EVENT_MESSAGE, OnMessage);
        socket.On(Socket.EVENT_RECONNECT, OnReconnect);
        socket.On(Socket.EVENT_RECONNECTING, OnReconnecting);
        socket.On(Socket.EVENT_RECONNECT_ATTEMPT, OnReconnectAttempt);
        socket.On(Socket.EVENT_RECONNECT_ERROR, OnReconnectError);
        socket.On(Socket.EVENT_RECONNECT_FAILED, OnReconnectFailed);

        InitializeLegendsOfThePitListeners();

        _initialized = true;
    }

    private void InitializeLegendsOfThePitListeners ()
    {
        if (socket != null)
        {
            socket.On("new_bet", OnNewBet);

            socket.On("authFailed", () =>
            {
                Log("authFailed");

                _authenticated = false;
            });

            socket.On("authSuccess", () =>
            {
                Log("authSuccess");

                _authenticated = true;
            });

            socket.On("join_game", () =>
            {
                Log("join_game");
            });

            socket.On("join_match", () =>
            {
                Log("join_match");
            });

            socket.On("joinMatchSuccess", () =>
            {
                Log("joinMatchSuccess");
            });

            socket.On("alreadyInMatch", () =>
            {
                Log("alreadyInMatch");
            });

            socket.On("match_update", () =>
            {
                Log("match_update");

                Test_RoomUpdate();
            });
        }        
    }

    public void Disconnect ()
    {
        On_Log = null;
        On_Connect = null;
        On_Disconnect = null;
        On_RoomUpdate = null;

        if (socket != null)
        {
            socket.Disconnect();
            socket = null;
        }
    }

    private void Log (string message)
    {
        _logs.Add(message);

        if (label != null)
            labelQueue += "\n" + message;

        if (On_Log != null)
            On_Log(message);
    }

    private void LogFormat (string format, params object[] args)
    {
        Log(string.Format(format, args));
    }
    #endregion

    #region Socket Event Methods
    private void OnConnect ()
    {
        Log("Connected.");

        if (On_Connect != null)
            On_Connect();

        if (_autoJoinGame)
            DoJoinGame();
    }

    private void OnConnectError ()
    {
        Log("Error Connecting.");
    }

    private void OnConnectTimeout ()
    {
        Log("Connection Timeout.");
    }

    private void OnDisconnect ()
    {
        Log("Disconnected.");

        if (On_Disconnect != null)
            On_Disconnect();
    }

    private void OnError ()
    {
        Log("Error.");
    }

    private void OnMessage ()
    {
        Log("Message.");
    }

    private void OnReconnect ()
    {
        Log("Reconnect.");
    }

    private void OnReconnecting ()
    {
        Log("Reconnecting.");
    }

    private void OnReconnectAttempt ()
    {
        Log("Attempting to Reconnect.");
    }

    private void OnReconnectError ()
    {
        Log("Reconnection Error.");
    }

    private void OnReconnectFailed ()
    {
        Log("Reconnection Failed.");
    }
    #endregion

    #region Socket Event LotP Methods
    private void DoJoinGame ()
    {
        if (socket == null)
            return;

        JObject jJoinGame = new JObject(new JProperty("username", _username));

        socket.Emit("join_game", jJoinGame);

        LogFormat("Joining Game: {0}.", _username);
    }

    private void DoJoinMatch (string eventMatchId, string eventId, string startTime, string endTime, string duration, RoomUpdateHandler callback = null)
    {
        if (socket == null)
            return;

        if (!Authenticated)
            return;

        if (InRoom)
            return;

        JObject jMatch = new JObject(new JProperty("EventMatchID", eventMatchId),
                                     new JProperty("EventID", eventId),
                                     new JProperty("StartTime", startTime),
                                     new JProperty("EndTime", endTime),
                                     new JProperty("Duration", duration));

        socket.Emit("join_match", jMatch);

        // TODO
        _roomJoined = eventMatchId;

        if (callback != null)
            On_RoomUpdate = callback;

        LogFormat("Joining Match: #{0}.", eventMatchId);
    }

    private void DoLeaveMatch (string eventMatchId)
    {
        if (socket == null)
            return;

        if (!Authenticated)
            return;

        if (!InRoom)
            return;

        socket.Emit("leave_match");

        // TODO
        _roomJoined = string.Empty;

        On_RoomUpdate = null;

        LogFormat("Leaving Match: #{0}.", eventMatchId);
    }

    private void DoNewBet (string eventMatchId, string amount, string cockId)
    {
        if (socket == null)
            return;

        if (!Authenticated)
            return;

        if (!InRoom)
            return;

        JObject jNewBet = new JObject(new JProperty("EventMatchID", eventMatchId),
                                      new JProperty("amount", amount),
                                      new JProperty("cockId", cockId));

        socket.Emit("new_bet", jNewBet);

        LogFormat("Betting: ${0} on Cock #{1} on Match #{2}.",
                  amount,
                  cockId,
                  eventMatchId);
    }

    private void Test_RoomUpdate ()
    {
        // TODO
        //if (_roomJoined != _roomJoined)
        //    return;

        if (On_RoomUpdate != null)
            On_RoomUpdate(UERandom.Range(0, 100).ToString());
    }

    private void OnNewBet (object data)
    {
        socket.Off("new_bet");

        Emit_NewBet newBet = JsonConvert.DeserializeObject<Emit_NewBet>(data.ToString());

        LogFormat("QQ'{0}' betted ${1} on Cock #{2}.",
                  newBet.username,
                  newBet.betAmount,
                  newBet.cockId);

        socket.On("new_bet", OnNewBet);
    }
    #endregion

    #region MonoBehaviour Methods
    private void Start ()
    {
        On_Log = Debug.Log;

        Initialize(theUsername, "password1");

        // TODO
        //InvokeRepeating("Test_RoomUpdate", 1.0f, 0.5f);
    }

    // TODO
    private void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Q))
            DoJoinMatch(roomId,
                        "1",
                        "2017-11-20 12:00:00",
                        "2017-11-20 12:00:150",
                        "15",
                        (m) => { Debug.LogWarning(m); });

        if (Input.GetKeyDown(KeyCode.W))
            DoNewBet(roomId,
                     "100",
                     "1");

        if (Input.GetKeyDown(KeyCode.E))
            DoLeaveMatch(roomId);

        if (Input.GetKeyDown(KeyCode.Z))
            Test_RoomUpdate();

        if (Input.GetKeyDown(KeyCode.Backspace))
            if (label != null)
                label.text = string.Empty;

        if (Input.GetKeyDown(KeyCode.Escape))
            Disconnect();

        if (!labelQueue.IsNullOrEmpty())
        {
            if (label != null)
                label.text += labelQueue;

            labelQueue = string.Empty;
        }
    }

    protected override void OnDestroy ()
    {
        base.OnDestroy();

        Disconnect();
    }
    #endregion

    #region Sub-Classes
    public class Emit_NewBet
    {
        public string username;
        public string betAmount;
        public string cockId;
    }
    #endregion
}
