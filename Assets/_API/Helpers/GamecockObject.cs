﻿using FullInspector;
using System;
using System.Collections.Generic;
using System.Linq;

public class GamecockObject : BaseScriptableObject
{
    public Gamecock Gamecock;
    public List<GCMoveCardObject> MoveCards;

    [InspectorButton, InspectorOrder(1.0)]
    public void Autofill ()
    {
        Gamecock.UniqueId = Guid.NewGuid().ToString();
        Gamecock.BreedId = "BREEDID";
        Gamecock.Name = Gamecock.UniqueId;
        Gamecock.BreedName = "BREED";
        Gamecock.BreederName = "BREEDER";
        Gamecock.Style.LegStation = "LEGSTATION";
        Gamecock.Style.Comb = "COMB";
        Gamecock.Style.Hackle = "HACKLE";
        Gamecock.Style.Wing = "WING";
        Gamecock.Style.Saddle = "SADDLE";
        Gamecock.Style.Shank = "SHANK";
        Gamecock.Style.Tail = "TAIL";
        Gamecock.Attributes.FightingPeak = 10;
        Gamecock.Attributes.Gameness = 10;
        Gamecock.Attributes.Intellect = 10;
        Gamecock.Attributes.Power = 10;
        Gamecock.Attributes.Cut = 10;
        Gamecock.Attributes.SpeedAerial = 10;
        Gamecock.Attributes.SpeedGround = 10;
        Gamecock.Attributes.Pedigree = 10;
        Gamecock.Attributes.Instinct = 10;
        Gamecock.Attributes.BaseWeight = 10;

        Gamecock.MoveCards = new GCMoveCard[]
        {
            new GCMoveCard
            {
                Name = "MOVECARD",
                Weight = 10,
                Move1 = new GCMove
                {
                    UniqueId = 1,
                    Name = "MOVESUPERIOR",
                    AnimationId = 1,
                    Verticality = 5,
                    BaseInitiative = 5,
                    BaseDamage = 25,
                    BaseAccuracy = 60,
                    BaseDodge = 10
                },
                Move2 = new GCMove
                {
                    UniqueId = 2,
                    Name = "MOVEINFERIOR",
                    AnimationId = 2,
                    Verticality = 4,
                    BaseInitiative = 0,
                    BaseDamage = 20,
                    BaseAccuracy = 60,
                    BaseDodge = 10
                }
            }
        };
    }

    [InspectorButton, InspectorOrder(1.1)]
    public void LoadMoveCards ()
    {
        List<GCMoveCard> moveCards = new List<GCMoveCard> { };

        foreach (GCMoveCardObject mco in MoveCards)
        {
            moveCards.Add(new GCMoveCard
            {
                Name = mco.Name,
                Weight = mco.Weight,
                Move1 = GCMove.DeepCopy(mco.Move1.Move),
                Move2 = GCMove.DeepCopy(mco.Move2.Move)
            });
        }

        Gamecock.MoveCards = moveCards.ToArray();
    }

    [InspectorButton, InspectorOrder(1.2)]
    public void GenerateNewGUIDS()
    {
        Gamecock.UniqueId = Guid.NewGuid().ToString();
        Gamecock.MoveCards.ToList().ForEach(mc => mc.UniqueId = Guid.NewGuid().ToString());
    }
}

public class GCMoveCardObject : BaseScriptableObject
{
    public string Name;
    public int Weight;
    public GCMoveObject Move1;
    public GCMoveObject Move2;
}

public class GCMoveObject : BaseScriptableObject
{
    public GCMove Move;
}