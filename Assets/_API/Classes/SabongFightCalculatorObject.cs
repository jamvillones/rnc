﻿using FullInspector;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEssentials;
using UnityEssentials.ExtensionMethods;

public class SabongFightCalculatorObject : BaseScriptableObject
{
    public Gamecock gamecockBlue;
    public Gamecock gamecockRed;

    [InspectorTextArea, InspectorOrder(1.1), InspectorShowIf("ResultIsNotEmpty")]
    public string result;
    
    private bool ResultIsNotEmpty { get { return !result.IsNullOrEmpty(); } }

    [InspectorButton, InspectorOrder(1.0)]
    public void Simulate ()
    {
        result = Simulate1(gamecockBlue, gamecockRed);
    }

    /// <summary>
    /// This is an exact copy of the Simulate1( ) method defined in the PHP-base project.
    /// This was not written with performance in mind prioritizing instead conformity with the PHP-base project.
    /// </summary>
    /// <param name="gc1"></param>
    /// <param name="gc2"></param>
    /// <returns></returns>
    public string Simulate1 (Gamecock gc1, Gamecock gc2)
    {
        string result = string.Empty;

        /* One-off variables */
        int rounds = 0;
        string id1 = gc1.UniqueId;
        string id2 = gc2.UniqueId;
        string name1 = gc1.Name;
        string name2 = gc2.Name;

        /* Re-usable variables */
        int distance = 0;
        int gc1Health = 0, gc2Health = 0;
        List<GCAilment> gc1Ailments = new List<GCAilment>();
        List<GCAilment> gc2Ailments = new List<GCAilment>();
        bool gcAilmentCheck = false;
        List<int> gcAilmentDelete = new List<int>();
        GCMoveCard gc1MoveCard = null, gc2MoveCard = null;
        int gc1InitiativeRoll = 0, gc2InitiativeRoll = 0;
        int gc1InitiativeBonus = 0, gc2InitiativeBonus = 0;
        int gc1InitiativeFinal = 0, gc2InitiativeFinal = 0;
        int gc1Damage = 0, gc2Damage = 0;
        int gc1Accuracy = 0, gc2Accuracy = 0;
        int gc1Dodge = 0, gc2Dodge = 0;
        GCMove gc1Move = null, gc2Move = null;
        int gc1MoveRoll = 0, gc2MoveRoll = 0;
        int gc1MoveOdds = 0, gc2MoveOdds = 0;
        bool gc1MoveSuccess = false, gc2MoveSuccess = false;
        int gc1MoveAilmentRoll = 0, gc2MoveAilmentRoll = 0;
        List<int> gc1AilmentIds = new List<int>();
        List<int> gc2AilmentIds = new List<int>();
        GCFightMove gc1FMove = null, gc2FMove = null;
        List<GCFightEvent> gcFEvents = new List<GCFightEvent>();

        /* Initialize variables */
        gc1Health = 100;
        gc2Health = 100;
        List<GCFightRound> fRounds = new List<GCFightRound>();
        GCFightData data = new GCFightData()
        {
            FightId = "0",
            BlueGamecockId = id1,
            RedGamecockId = id2
        };

        /* Loop until one or both gamecocks are down. */
        while (gc1Health > 0 && gc2Health > 0)
        {
            // Clear variable values.
            gc1MoveCard = gc2MoveCard = null;
            gc1Move = gc2Move = null;
            gc1FMove = gc2FMove = null;
            gc1AilmentIds.Clear();
            gc2AilmentIds.Clear();
            gcFEvents.Clear();

            // Increment round counter.
            rounds += 1;

            // Random a move card for both gamecocks.
            gc1MoveCard = gc1.RandomMoveCard();
            gc2MoveCard = gc2.RandomMoveCard();

            // Roll for initiative for both gamecocks.
            gc1InitiativeRoll = UERandom.Range(0, 100);
            gc2InitiativeRoll = UERandom.Range(0, 100);

            // Total initiative for both gamecocks.
            gc1InitiativeFinal = gc1InitiativeRoll + gc1InitiativeBonus;
            gc2InitiativeFinal = gc2InitiativeRoll + gc2InitiativeBonus;

            // Determine the move card for both gamecocks.
            gc1Move = (gc1InitiativeFinal < gc2InitiativeFinal) ?
                gc1MoveCard.Move2 :
                gc1MoveCard.Move1;

            gc2Move = (gc1InitiativeFinal > gc2InitiativeFinal) ?
                gc2MoveCard.Move2 :
                gc2MoveCard.Move1;

            // Check selected move distances.
            distance = Mathf.Abs(gc1Move.Verticality - gc2Move.Verticality);

            if (distance <= 2)
            {
                // Calculate damage for both gamecocks.
                gc1Damage = gc1Move.BaseDamage - gc2Move.EnemyDamage;
                gc2Damage = gc2Move.BaseDamage - gc1Move.EnemyDamage;

                // Calculate accuracy for both gamecocks.
                gc1Accuracy = gc1Move.BaseAccuracy - gc2Move.EnemyAccuracy;
                gc2Accuracy = gc2Move.BaseAccuracy - gc1Move.EnemyAccuracy;

                // Calculate dodge for both gamecocks.
                gc1Dodge = gc1Move.BaseDodge - gc2Move.EnemyDodge;
                gc2Dodge = gc2Move.BaseDodge - gc1Move.EnemyDodge;

                // Roll for move for both gamecocks.
                gc1MoveRoll = UERandom.Range(0, 100);
                gc2MoveRoll = UERandom.Range(0, 100);

                // Calculate move success odds.
                gc1MoveOdds = 100 - (gc1Accuracy - gc2Dodge);
                gc2MoveOdds = 100 - (gc2Accuracy - gc1Dodge);

                // Calculate move success.
                gc1MoveSuccess = (gc1MoveRoll >= gc1MoveOdds);
                gc2MoveSuccess = (gc2MoveRoll >= gc2MoveOdds);

                // Resolve move for both gamecocks.
                if (gc1MoveSuccess && (gc1Damage > 0))
                    gc2Health -= gc1Damage;

                if (gc2MoveSuccess && (gc2Damage > 0))
                    gc1Health -= gc2Damage;

                // Resolve move ailments for gamecock1.
                if (gc1MoveSuccess)
                {
                    // For each possible ailment in gamecock1's move roll for proc.
                    foreach (GCAilment pAilment in gc1Move.PossibleAilments)
                    {
                        gc1MoveAilmentRoll = UERandom.Range(0, 100);

                        // On success:
                        if (gc1MoveAilmentRoll >= (100 - pAilment.Weight))
                        {
                            // Add to GCFightMove for gamecock1.
                            gc1AilmentIds.Add(pAilment.UniqueId);

                            // Check if proc'd ailment already exists in gamecock2's stack.
                            gcAilmentCheck = false;
                            foreach (GCAilment gcAilment in gc2Ailments)
                            {
                                // If ailment already exists reset.
                                if (gcAilment.UniqueId == pAilment.UniqueId)
                                {
                                    gcAilment.Reset();
                                    gcAilmentCheck = true;
                                    break;
                                }
                            }

                            // If ailment doesn't exist add it to gamecock2's stack.
                            if (!gcAilmentCheck)
                                gc2Ailments.Add(pAilment);
                        }
                    }
                }

                // Resolve move ailments for gamecock2.
                if (gc2MoveSuccess)
                {
                    // For each possible ailment in gamecock2's move roll for proc.
                    foreach (GCAilment pAilment in gc2Move.PossibleAilments)
                    {
                        gc2MoveAilmentRoll = UERandom.Range(0, 100);

                        // On success:
                        if (gc2MoveAilmentRoll >= (100 - pAilment.Weight))
                        {
                            // Add to GCFightMove for gamecock2.
                            gc2AilmentIds.Add(pAilment.UniqueId);
                            
                            // Check if proc'd ailment already exists in gamecock1's stack.
                            gcAilmentCheck = false;
                            foreach (GCAilment gcAilment in gc1Ailments)
                            {
                                // If ailment already exists reset.
                                if (gcAilment.UniqueId == pAilment.UniqueId)
                                {
                                    gcAilment.Reset();
                                    gcAilmentCheck = true;
                                    break;
                                }
                            }

                            // If ailment doesn't exist add it to gamecock1's stack.
                            if (!gcAilmentCheck)
                                gc1Ailments.Add(pAilment);
                        }
                    }
                }
            }
            else
            {
                gc1MoveSuccess = gc2MoveSuccess = false;
                gc1Damage = gc2Damage = 0;
                gcFEvents.Clear();
            }

            // Create fight move for both gamecocks.
            gc1FMove = new GCFightMove ()
            {
                MoveId = gc1Move.UniqueId,
                Success = gc1MoveSuccess,
                Critical = false,
                Damage = gc1Damage,
                Height = gc1Move.Verticality,
                AilmentIds = gc1AilmentIds.ToArray()
            };

            gc2FMove = new GCFightMove ()
            {
                MoveId = gc2Move.UniqueId,
                Success = gc2MoveSuccess,
                Critical = false,
                Damage = gc2Damage,
                Height = gc2Move.Verticality,
                AilmentIds = gc2AilmentIds.ToArray()
            };

            // Resolve active ailments for gamecock1.
            if (gc1Ailments != null &&
                gc1Ailments.Count > 0)
            {
                gcAilmentDelete.Clear();
                for (int i = 0; i < gc1Ailments.Count; i++)
                {
                    // Deal damage.
                    gc1Health -= gc1Ailments[i].DamagePerRound;

                    // Add to GCFightRound as GCFightEvent.
                    gcFEvents.Add(new GCFightEvent ()
                    {
                        GamecockId = id1,
                        Type = "ailment/health",
                        Payload = new GCFightPayload ()
                        {
                            Id = gc1Ailments[i].UniqueId.ToString(),
                            Duration = gc1Ailments[i].DurationCounter,
                            Damage = gc1Ailments[i].DamagePerRound
                        }
                    });

                    // Increment counter.
                    gc1Ailments[i].IncrementCounter();

                    // If ailment has expired, add it to the delete stack.
                    if (!gc1Ailments[i].IsActive())
                    {
                        gc1Ailments[i].Reset();
                        gcAilmentDelete.Add(i);
                    }
                }

                // Remove all expired ailments.
                if (gcAilmentDelete != null && gcAilmentDelete.Count > 0)
                {
                    while (gcAilmentDelete.Count > 0)
                    {
                        gc1Ailments.RemoveAt(gcAilmentDelete[gcAilmentDelete.Count - 1]);
                        gcAilmentDelete.RemoveAt(gcAilmentDelete.Count - 1);
                    }
                }
            }

            // Resolve active ailments for gamecock2.
            if (gc2Ailments != null &&
                gc2Ailments.Count > 0)
            {
                gcAilmentDelete.Clear();
                for (int i = 0; i < gc2Ailments.Count; i++)
                {
                    // Deal damage.
                    gc2Health -= gc2Ailments[i].DamagePerRound;

                    // Add to GCFightRound as GCFightEvent.
                    gcFEvents.Add(new GCFightEvent ()
                    {
                        GamecockId = id2,
                        Type = "ailment/health",
                        Payload = new GCFightPayload ()
                        {
                            Id = gc2Ailments[i].UniqueId.ToString(),
                            Duration = gc2Ailments[i].DurationCounter,
                            Damage = gc2Ailments[i].DamagePerRound
                        }
                    });

                    // Increment counter.
                    gc2Ailments[i].IncrementCounter();

                    // If ailment has expired, add it to the delete stack.
                    if (!gc2Ailments[i].IsActive())
                    {
                        gc2Ailments[i].Reset();
                        gcAilmentDelete.Add(i);
                    }
                }

                // Remove all expired ailments.
                if (gcAilmentDelete != null && gcAilmentDelete.Count > 0)
                {
                    while (gcAilmentDelete.Count > 0)
                    {
                        gc2Ailments.RemoveAt(gcAilmentDelete[gcAilmentDelete.Count - 1]);
                        gcAilmentDelete.RemoveAt(gcAilmentDelete.Count - 1);
                    }
                }
            }

            fRounds.Add(new GCFightRound ()
            {
                BlueGamecockHealth = gc1Health,
                RedGamecockHealth = gc2Health,
                BlueFightMove = gc1FMove,
                RedFightMove = gc2FMove,
                Events = gcFEvents.ToArray()
            });
        }

        data.Rounds = fRounds.ToArray();

        if (gc1Health <= 0 && gc2Health <= 0)
            data.WinnerGamecockId = "-1";
        else if (gc1Health <= 0)
            data.WinnerGamecockId = id2;
        else
            data.WinnerGamecockId = id1;

        result = data.ToJson(true);

        return result;
    }
}

public partial class Gamecock
{
    public GCMoveCard RandomMoveCard ()
    {
        return UERandom.WeightedChoice(MoveCards, MoveCards.Select(x => x.Weight).ToArray());
    }
}

public partial class GCAilment
{
    public int DurationCounter { get; set; }

    public void IncrementCounter ()
    {
        DurationCounter -= 1;
    }

    public bool IsActive ()
    {
        return (DurationCounter > 0);
    }

    public void Reset ()
    {
        DurationCounter = Duration;
    }
}
