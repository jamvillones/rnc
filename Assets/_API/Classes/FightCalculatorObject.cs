﻿using FullInspector;
using LegendsOfThePit;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FightCalculatorObject : BaseScriptableObject
{
    public GamecockObject blue;
    public GamecockObject red;

    public bool VerticalityTest
    {
        get
        {
            bool verticality = false;

            int[] blueVerticality = blue.Gamecock.MoveCards.Select(x => x.Move1.Verticality).Union(blue.Gamecock.MoveCards.Select(x => x.Move2.Verticality)).ToArray();
            int[] redVerticality = red.Gamecock.MoveCards.Select(x => x.Move1.Verticality).Union(red.Gamecock.MoveCards.Select(x => x.Move2.Verticality)).ToArray();

            foreach (int b in blueVerticality)
                foreach (int r in redVerticality)
                    if (Mathf.Abs(b - r) <= 2)
                        return true;

            return verticality;
        }
    }

    [InspectorButton, InspectorOrder(1.1), InspectorShowIf("VerticalityTest")]
    public void Simulate()
    {
        FightResult result = FightResult.Simulate_(blue.Gamecock, red.Gamecock);
      //  FightResult result = FightResult.Simulate(blue.Gamecock, red.Gamecock);
        Debug.Log(string.Join("\n\n", result.Logs.Union(new string[]{ "\n\n\n" }).ToArray()));
        Debug.Log(JsonConvert.SerializeObject(result, Formatting.Indented));
    }

    [InspectorButton, InspectorOrder(1.2), InspectorShowIf("VerticalityTest")]
    public void Simulate1000 ()
    {
        List<FightResult> list = new List<FightResult>();

        for (int i = 0; i < 1000; i++)
            list.Add(FightResult.Simulate_(blue.Gamecock, red.Gamecock));

        List<string> statistics = new List<string> { };

        statistics.Add(string.Format("Average number of Rounds: {0}", list.Average(x => x.Clashes.Length)));

        statistics.Add(string.Format("Number of times Blue Won: {0}", list.SelectMany(x => x.Decisions).Where(y => y.Decision == blue.Gamecock.UniqueId).ToArray().Length));

        statistics.Add(string.Format("Number of times Red Won: {0}", list.SelectMany(x => x.Decisions).Where(y => y.Decision == red.Gamecock.UniqueId).ToArray().Length));

        statistics.Add(string.Format("Number of times fight ended with a Draw: {0}", list.SelectMany(x => x.Decisions).Where(y => y.Decision == "Draw").ToArray().Length));

        statistics.Add(string.Format("Number of times Blue gained Initiative: {0}", list.SelectMany(x => x.Clashes).Where(y => y.BlueInitiative).ToArray().Length));

        statistics.Add(string.Format("Number of times Red gained Initiative: {0}", list.SelectMany(x => x.Clashes).Where(y => y.RedInitiative).ToArray().Length));

        statistics.Add(string.Format("Average Initiative Value of Blue: {0}", list.SelectMany(x => x.Clashes).Average(y => y.BlueInitiativeValue)));

        statistics.Add(string.Format("Average Initiative Value of Red: {0}", list.SelectMany(x => x.Clashes).Average(y => y.RedInitiativeValue)));

        statistics.Add(string.Format("Average Hit Rate of Blue: {0}", (float) list.SelectMany(x => x.Clashes).Where(y => y.BlueSuccess).ToArray().Length / list.SelectMany(x => x.Clashes).ToArray().Length));

        statistics.Add(string.Format("Average Hit Rate of Red: {0}", (float) list.SelectMany(x => x.Clashes).Where(y => y.RedSuccess).ToArray().Length / list.SelectMany(x => x.Clashes).ToArray().Length));

        statistics.Add(string.Format("Total Damage Dealt by Blue: {0}", list.SelectMany(x => x.Clashes).Where(y => y.BlueSuccess).Sum(z => z.BlueDamage)));

        statistics.Add(string.Format("Total Damage Dealt by Red: {0}", list.SelectMany(x => x.Clashes).Where(y => y.RedSuccess).Sum(z => z.RedDamage)));

        FightRound_Clash highestBlueDamage = list.SelectMany(x => x.Clashes).Where(y => y.BlueSuccess).OrderByDescending(z => z.BlueDamage).First();
        statistics.Add(string.Format("Highest Damaging Move of Blue: {0} Damage '{1}'", highestBlueDamage.BlueDamage, highestBlueDamage.BlueMove));

        FightRound_Clash highestRedDamage = list.SelectMany(x => x.Clashes).Where(y => y.RedSuccess).OrderByDescending(z => z.RedDamage).First();
        statistics.Add(string.Format("Highest Damaging Move of Red: {0} Damage '{1}'", highestRedDamage.RedDamage, highestRedDamage.RedMove));

        statistics.Add(string.Format("Total Damage Mitigated by Blue: {0}", list.SelectMany(x => x.Clashes).Where(y => y.RedSuccess).Sum(z => z.BlueDamageMitigated)));

        statistics.Add(string.Format("Total Damage Mitigated by Red: {0}", list.SelectMany(x => x.Clashes).Where(y => y.BlueSuccess).Sum(z => z.RedDamageMitigated)));

        FightRound_Clash highestBlueDamageMitigated = list.SelectMany(x => x.Clashes).Where(y => y.RedSuccess).OrderByDescending(z => z.BlueDamageMitigated).First();
        statistics.Add(string.Format("Highest Damage Mitigating Move of Blue: {0} Damage Mitigated '{1}'", highestBlueDamageMitigated.BlueDamageMitigated, highestBlueDamageMitigated.BlueMove));

        FightRound_Clash highestRedDamageMitigated = list.SelectMany(x => x.Clashes).Where(y => y.BlueSuccess).OrderByDescending(z => z.RedDamageMitigated).First();
        statistics.Add(string.Format("Highest Damage Mitigating Move of Red: {0} Damage Mitigated '{1}'", highestRedDamageMitigated.RedDamageMitigated, highestRedDamageMitigated.RedMove));

        int blueMovesCount = list.SelectMany(x => x.Clashes).ToArray().Length;
        string blueMC = "Moves by Blue:";
        string[] blueMoves = blue.Gamecock.MoveCards.Select(x => x.Move1.Name).Union(blue.Gamecock.MoveCards.Select(x => x.Move2.Name)).ToArray();
        foreach (string move in blueMoves)
        {
            int moveUsage = list.SelectMany(x => x.Clashes).Where(y => y.BlueMove == move).ToArray().Length;
            int moveUsageSuccess = list.SelectMany(x => x.Clashes).Where(y => y.BlueMove == move && y.BlueSuccess).ToArray().Length;
            blueMC += string.Format("\n\t{0}: {1} with Success Rate of: {2}",
                move,
                (float) moveUsage / blueMovesCount,
                (float) moveUsageSuccess / moveUsage);
        }
        statistics.Add(blueMC);

        int redMovesCount = list.SelectMany(x => x.Clashes).ToArray().Length;
        string redMC = "Moves by Red:";
        string[] redMoves = red.Gamecock.MoveCards.Select(x => x.Move1.Name).Union(red.Gamecock.MoveCards.Select(x => x.Move2.Name)).ToArray();
        foreach (string move in redMoves)
        {
            int moveUsage = list.SelectMany(x => x.Clashes).Where(y => y.RedMove == move).ToArray().Length;
            int moveUsageSuccess = list.SelectMany(x => x.Clashes).Where(y => y.RedMove == move && y.RedSuccess).ToArray().Length;
            redMC += string.Format("\n\t{0}: {1} with Success Rate of: {2}",
                move,
                (float)moveUsage / redMovesCount,
                (float)moveUsageSuccess / moveUsage);
        }
        statistics.Add(redMC);

        statistics.Add("\n\n\n");

        Debug.Log(string.Join("\n", statistics.ToArray()));
    } 
}
