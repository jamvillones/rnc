﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEssentials;
using FSP = FullSerializer.fsPropertyAttribute;
using J = Newtonsoft.Json.JsonPropertyAttribute;
using JI = Newtonsoft.Json.JsonIgnoreAttribute;

namespace LegendsOfThePit
{
    [Serializable]
    public partial class FightResult
    {
        [FSP, J("AId")] public string ArenaId { get; set; }
        [FSP, J("DId")] public string DerbyId { get; set; }
        [FSP, J("FId")] public string FightId { get; set; }
        [FSP, J("BId")] public string BlueId { get; set; }
        [FSP, J("RId")] public string RedId { get; set; }
        [FSP, J("StT")] public string StartTime { get; set; }
        [FSP, J("EnT")] public string EndTime { get; set; }
        [FSP, J("RIx")] public FightRoundIndex[] Indexes { get; set; }
        [FSP, J("RAp")] public FightRound_Approach[] Approaches { get; set; }
        [FSP, J("RCl")] public FightRound_Clash[] Clashes { get; set; }
        [FSP, J("RDe")] public FightRound_Decision[] Decisions { get; set; }
        [FSP, J("RSt")] public FightRound_Standstill[] Standstills { get; set; }
        [JI] public string[] Logs { get; set; }

        public string GetFightRoundType (int index)
        {
            FightRoundIndex fightRoundIndex = null;

            if (Indexes != null)
            {
                fightRoundIndex = Indexes.FirstOrDefault(x => x.Index == index);

                if (fightRoundIndex != null)
                    return fightRoundIndex.Type;
            }

            return string.Empty;
        }

        public FightRound_Approach GetFightRoundApproach (int index)
        {
            if (Approaches != null)
                return Approaches.FirstOrDefault(x => x.Index == index);

            return null;
        }

        public FightRound_Clash GetFightRoundClash (int index)
        {
            if (Clashes != null)
                return Clashes.FirstOrDefault(x => x.Index == index);

            return null;
        }

        public FightRound_Decision GetFightRoundDecision (int index)
        {
            if (Decisions != null)
                return Decisions.FirstOrDefault(x => x.Index == index);

            return null;
        }

        public FightRound_Standstill GetFightRoundStandstill (int index)
        {
            if (Standstills != null)
                return Standstills.FirstOrDefault(x => x.Index == index);

            return null;
        }

        #region Statics
        public static FightResult Simulate (Gamecock gc1, Gamecock gc2)
        {
            bool fighting = true;
            int rounds = 0;
            int blueHp = 100;
            int redHp = 100;
            int decisionsCounter = 0;
            float blueKOChance = 0;
            float redKOChance = 0;
            string roundType = string.Empty;
            float[] roundTypesWeight = new float[4] { 40.0f, 30.0f, 0.0f, 30.0f };
            string[] roundTypes = new string[4] { "Approach", "Clash", "Decision", "Standstill" };
            string[] approachTypes = new string[4] { "Normal", "Stealthy", "Alert", "Limping" };
            string[] clashAttackTypes = new string[5] { "Heavy Right Kick", "Heavy Left Kick", "Light Right Kick", "Light Left Kick", "Peck" };
            string[] clashHitTypes = new string[5] { "Heavy Right Hit", "Heavy Left Hit", "Light Right Hit", "Light Left Hit", "Dodge" };
            string[] standstillTypes = new string[4] { "Stare", "Flap", "Screech", "Kaw" };
            Vector3 fightRoundVPosition = Vector3.zero;
            FightRoundPosition fightRoundPosition = new FightRoundPosition ()
            {
                Angle = UERandom.Range(0, 360),
                Height = 0.0f,
                Radius = UERandom.Range(0.0f, 1.0f)
            };
            List<string> logs = new List<string>();
            List<FightRoundIndex> indexes = new List<FightRoundIndex>();
            List<FightRound_Approach> approaches = new List<FightRound_Approach>();
            List<FightRound_Clash> clashes = new List<FightRound_Clash>();
            List<FightRound_Decision> decisions = new List<FightRound_Decision>();
            List<FightRound_Standstill> standstills = new List<FightRound_Standstill>();

            while (fighting)
            {
                roundType = UERandom.WeightedChoice(roundTypes, roundTypesWeight);

                switch (roundType)
                {
                    case "Approach":

                        // Update Position.
                        fightRoundPosition.Angle = UEMath.Wrap(fightRoundPosition.Angle + UERandom.Range(0, 31), 0, 360);
                        fightRoundPosition.Height = 0.0f;
                        fightRoundPosition.Radius = Mathf.Clamp(fightRoundPosition.Radius + UERandom.Range(-0.2f, 0.2f), 0.0f, 1.0f);
                        fightRoundVPosition = Spherical.SphericalToCartesian(fightRoundPosition.Radius, 0.0f, fightRoundPosition.Angle);
                        fightRoundVPosition *= 10.0f;
                        fightRoundVPosition.y = fightRoundPosition.Height;

                        // Build Round Data.
                        FightRound_Approach fightRound_Approach = new FightRound_Approach ()
                        {
                            Index = rounds,
                            Position = new FightRoundPosition ()
                            {
                                Angle = fightRoundPosition.Angle,
                                Height = fightRoundPosition.Height,
                                Radius = fightRoundPosition.Radius
                            },
                            BlueAction = UERandom.Choice(approachTypes),
                            RedAction = UERandom.Choice(approachTypes)
                        };

                        // Register Round Data.
                        approaches.Add(fightRound_Approach);

                        // Recalibrate Round Type Weights.
                        roundTypesWeight[0] -= 8;
                        roundTypesWeight[1] += 10;
                        roundTypesWeight[2] += 0;
                        roundTypesWeight[3] -= 2;

                        logs.Add(string.Format(
                            "Round #{0}:\n" +
                            "Position: {1}\n" +
                            "Blue Action: {2}\n" +
                            "Red Action: {3}\n" +
                            "Next Round Weights (Approach: {4}% | Clash: {5}% | Decision: {6}% | Standstill: {7}%)",
                            rounds + 1,
                            fightRoundVPosition,
                            fightRound_Approach.BlueAction,
                            fightRound_Approach.RedAction,
                            roundTypesWeight[0], roundTypesWeight[1], roundTypesWeight[2], roundTypesWeight[3]
                            ));

                        break;

                    default:
                    case "Clash":

                        // Update Position.
                        fightRoundPosition.Angle = UEMath.Wrap(fightRoundPosition.Angle + UERandom.Range(0, 11), 0, 360);
                        fightRoundPosition.Height = UERandom.Range(0.0f, 4.0f);
                        fightRoundPosition.Radius = Mathf.Clamp(fightRoundPosition.Radius + UERandom.Range(-0.1f, 0.1f), 0.0f, 1.0f);
                        fightRoundVPosition = Spherical.SphericalToCartesian(fightRoundPosition.Radius, 0.0f, fightRoundPosition.Angle);
                        fightRoundVPosition *= 10.0f;
                        fightRoundVPosition.y = fightRoundPosition.Height;

                        // Build Round Data.
                        bool blueAdvantage = UERandom.SplitChance();
                        int counterAdvantageValue = UERandom.Range(0, 41);
                        int blueAttackWeight = blueAdvantage ? (100 - counterAdvantageValue) : counterAdvantageValue;
                        int redAttackWeight = blueAdvantage ? counterAdvantageValue : (100 - counterAdvantageValue);
                        int blueDamage = (int) (20 * (blueAttackWeight * 0.01f));
                        int redDamage = (int) (20 * (redAttackWeight * 0.01f));

                        blueHp -= redDamage;
                        redHp -= blueDamage;
                        blueKOChance = (decisionsCounter / 2) + ((100 - blueHp) / 5);
                        redKOChance = (decisionsCounter / 2) + ((100 - redHp) / 5);

                        FightRound_Clash fightRound_Clash = new FightRound_Clash ()
                        {
                            Index = rounds,
                            Position = new FightRoundPosition ()
                            {
                                Angle = fightRoundPosition.Angle,
                                Height = fightRoundPosition.Height,
                                Radius = fightRoundPosition.Radius
                            },
                            BluePre = "PRE" + UERandom.Range(0, 5),
                            BlueAttack = UERandom.Choice(clashAttackTypes),
                            BlueHit = UERandom.Choice(clashHitTypes),
                            BlueAttackWeight = blueAttackWeight,
                            BlueDamage = blueDamage,
                            BlueHp = blueHp,
                            BluePost = "POST" + UERandom.Range(0, 5),
                            RedPre = "PRE" + UERandom.Range(0, 5),
                            RedAttack = UERandom.Choice(clashAttackTypes),
                            RedHit = UERandom.Choice(clashHitTypes),
                            RedAttackWeight = redAttackWeight,
                            RedDamage = redDamage,
                            RedHp = redHp,
                            RedPost = "POST" + UERandom.Range(0, 5)
                        };

                        // Register Round Data.
                        clashes.Add(fightRound_Clash);

                        // Recalibrate Round Type Weights.
                        if (blueHp <= 0 || redHp <= 0)
                        {
                            roundTypesWeight[0] = 0;
                            roundTypesWeight[1] = 0;
                            roundTypesWeight[2] = 100;
                            roundTypesWeight[3] = 0;
                        }
                        else
                        {
                            float bonusToDecisionWeight = (200 - blueHp - redHp) / 3;
                            float tempPool = bonusToDecisionWeight;
                            float bonusToApproachWeight = UERandom.Range(0.0f, tempPool);
                            tempPool -= bonusToApproachWeight;
                            float bonusToClashWeight = UERandom.Range(0.0f, tempPool);
                            tempPool -= bonusToClashWeight;
                            float bonusToStandStill = tempPool;

                            roundTypesWeight[0] -= 2 + bonusToApproachWeight;
                            roundTypesWeight[1] += 3 + bonusToClashWeight;
                            roundTypesWeight[2] += 0 + bonusToDecisionWeight;
                            roundTypesWeight[3] -= 1 + bonusToStandStill;
                        }                        

                        logs.Add(string.Format(
                            "Round #{0}:\n" +
                            "Position: {1}\n" +
                            "Blue Action (Pre: {2} | Attack: {3} at {4}% | Hit: {5} at {6}% | Post: {7} | Damage: {8} | HP: {9} | Sudden-KO: {10}%)\n" +
                            "Red Action (Pre: {11} | Attack: {12} at {13}% | Hit: {14} at {15}% | Post: {16} | Damage: {17} | HP: {18} | Sudden-KO: {19}%)\n" +
                            "Next Round Weights (Approach: {20}% | Clash: {21}% | Decision: {22}% | Standstill: {23}%)",
                            rounds + 1,
                            fightRoundVPosition,
                            fightRound_Clash.BluePre, fightRound_Clash.BlueAttack, fightRound_Clash.BlueAttackWeight, fightRound_Clash.BlueHit, (100 - fightRound_Clash.BlueAttackWeight), fightRound_Clash.BluePost, fightRound_Clash.BlueDamage, blueHp, blueKOChance,
                            fightRound_Clash.RedPre, fightRound_Clash.RedAttack, fightRound_Clash.RedAttackWeight, fightRound_Clash.RedHit, (100 - fightRound_Clash.RedAttackWeight), fightRound_Clash.RedPost, fightRound_Clash.RedDamage, redHp, redKOChance,
                            roundTypesWeight[0], roundTypesWeight[1], roundTypesWeight[2], roundTypesWeight[3]
                            ));

                        break;

                    case "Decision":

                        // Build Round Data.
                        decisionsCounter += 1;
                        string decision = "Proceed";
                        bool blueKO = UERandom.Chance(blueKOChance, 100.0f);
                        bool redKO = UERandom.Chance(redKOChance, 100.0f);

                        if (blueHp <= 0 && redHp <= 0)
                            decision = "Draw";
                        else if (blueHp <= 0)
                            decision = "(By Kill) " + gc2.UniqueId;
                        else if (redHp <= 0)
                            decision = "(By Kill) " + gc1.UniqueId;
                        else if (blueKO && redKO)
                            decision = "Draw KO";
                        else if (blueKO)
                            decision = "(By KO) " + gc2.UniqueId;
                        else if (redKO)
                            decision = "(By KO) " + gc1.UniqueId;

                        FightRound_Decision fightRound_Decision = new FightRound_Decision ()
                        {
                            Index = rounds,
                            Decision = decision
                        };

                        // Register Round Data.
                        decisions.Add(fightRound_Decision);

                        // Recalibrate Round Type Weights.
                        if (decision == "Proceed")
                        {
                            roundTypesWeight[0] = 30;
                            roundTypesWeight[1] = 40;
                            roundTypesWeight[2] = 0;
                            roundTypesWeight[3] = 30;
                        }
                        else
                        {
                            roundTypesWeight[0] = 0;
                            roundTypesWeight[1] = 0;
                            roundTypesWeight[2] = 0;
                            roundTypesWeight[3] = 0;

                            fighting = false;
                        }

                        logs.Add(string.Format(
                            "Round #{0}:\n" +
                            "Position: {1}\n" +
                            "Decision: {2}\n" +
                            "Next Round Weights (Approach: {3}% | Clash: {4}% | Decision: {5}% | Standstill: {6}%)",
                            rounds + 1,
                            fightRoundVPosition,
                            decision,
                            roundTypesWeight[0], roundTypesWeight[1], roundTypesWeight[2], roundTypesWeight[3]
                            ));

                        break;

                    case "Standstill":

                        // Build Round Data.
                        FightRound_Standstill fightRound_Standstill = new FightRound_Standstill ()
                        {
                            Index = rounds,
                            Position = new FightRoundPosition()
                            {
                                Angle = fightRoundPosition.Angle,
                                Height = fightRoundPosition.Height,
                                Radius = fightRoundPosition.Radius
                            },
                            BlueAction = UERandom.Choice(standstillTypes),
                            RedAction = UERandom.Choice(standstillTypes)
                        };

                        // Register Round Data.
                        standstills.Add(fightRound_Standstill);

                        // Recalibrate Round Type Weights.
                        roundTypesWeight[0] += 2;
                        roundTypesWeight[1] += 4;
                        roundTypesWeight[2] += 0;
                        roundTypesWeight[3] -= 6;

                        logs.Add(string.Format(
                            "Round #{0}:\n" +
                            "Position: {1}\n" +
                            "Blue Action: {2}\n" +
                            "Red Action: {3}\n" +
                            "Next Round Weights (Approach: {4}% | Clash: {5}% | Decision: {6}% | Standstill: {7}%)",
                            rounds + 1,
                            fightRoundVPosition,
                            fightRound_Standstill.BlueAction,
                            fightRound_Standstill.RedAction,
                            roundTypesWeight[0], roundTypesWeight[1], roundTypesWeight[2], roundTypesWeight[3]
                            ));

                        break;
                }

                // Register Round.
                indexes.Add(new FightRoundIndex ()
                {
                    Index = rounds,
                    Type = roundType
                });

                rounds += 1;
            }

            Debug.Log(string.Join("\n\n", logs.ToArray()));

            FightResult result = new FightResult ()
            {
                ArenaId = "ArenaId",
                DerbyId = "DerbyId",
                FightId = "FightId",
                BlueId = gc1.UniqueId,
                RedId = gc2.UniqueId,
                StartTime = "2018-01-01 00:00:00",
                EndTime = "2018-01-01 00:15:00",
                Indexes = indexes.ToArray(),
                Approaches = approaches.ToArray(),
                Clashes = clashes.ToArray(),
                Decisions = decisions.ToArray(),
                Standstills = standstills.ToArray()
            };

            Debug.Log(JsonConvert.SerializeObject(result, Formatting.Indented));

            return result;
        }
        
        public static int GamecockStartingHp = 100;
        public static FightResult Simulate_ (Gamecock gc1, Gamecock gc2)
        {
            bool isFighting = true;
            int roundsCounter = 0;
            FightRoundType roundType = FightRoundType.Null;
            FightingGamecock blue = new FightingGamecock(GamecockStartingHp, 0, Spherical.SphericalToCartesian(0.5f, 0.0f, 0.0f), gc1);
            FightingGamecock red = new FightingGamecock(GamecockStartingHp, 0, Spherical.SphericalToCartesian(0.5f, 0.0f, 180.0f), gc2);
            List<string> logs = new List<string>();
            List<FightRoundIndex> indexes = new List<FightRoundIndex>();
            List<FightRound_Approach> approaches = new List<FightRound_Approach>();
            List<FightRound_Clash> clashes = new List<FightRound_Clash>();
            List<FightRound_Decision> decisions = new List<FightRound_Decision>();
            List<FightRound_Standstill> standstills = new List<FightRound_Standstill>();

            while (isFighting)
            {
                roundType = DetermineRoundType(blue, red, roundType);

                switch (roundType)
                {
                    #region Clash
                    case FightRoundType.Clash:

                        // Build Round Data.
                        FightRound_Clash clash = new FightRound_Clash
                        {
                            Index = roundsCounter
                        };

                        // Random a move card for both gamecocks.
                        GCMoveCard blueMoveCard = blue.Gamecock.RandomMoveCard();
                        GCMoveCard redMoveCard = red.Gamecock.RandomMoveCard();

                        // Roll for initiative for both gamecocks. D20 + Bonuses.
                        int blueInitiative = UERandom.Range(1, 21) + blue.InitiativeBonus;
                        int redInitiative = UERandom.Range(1, 21) + red.InitiativeBonus;

                        clash.BlueInitiative = (blueInitiative > redInitiative);
                        clash.RedInitiative = (blueInitiative < redInitiative);
                        clash.BlueInitiativeValue = blueInitiative;
                        clash.RedInitiativeValue = redInitiative;

                        // Determine move for both gamecocks.
                        GCMove blueMove = !clash.BlueInitiative ? blueMoveCard.Move2 : blueMoveCard.Move1;
                        GCMove redMove = !clash.RedInitiative ? redMoveCard.Move2 : redMoveCard.Move1;

                        clash.BlueMove = blueMove.Name;
                        clash.RedMove = redMove.Name;

                        int distance = Mathf.Abs(blueMove.Verticality - redMove.Verticality);

                        if (distance > 2)
                        {
                            clash.BluePre = "TBD";
                            clash.BlueAttack = "TBD";
                            clash.BlueHit = "TBD";
                            clash.BluePost = "TBD";

                            clash.RedPre = "TBD";
                            clash.RedAttack = "TBD";
                            clash.RedHit = "TBD";
                            clash.RedPost = "TBD";
                        }
                        else
                        {
                            // Calculate damage for both gamecocks.
                            int blueDamage = blueMove.BaseDamage - redMove.EnemyDamage;
                            int redDamage = redMove.BaseDamage - blueMove.EnemyDamage;

                            clash.BlueDamageMitigated = blueMove.EnemyDamage;
                            clash.RedDamageMitigated = redMove.EnemyDamage;

                            // Calculate accuracy for both gamecocks.
                            int blueAccuracy = blueMove.BaseAccuracy - redMove.EnemyAccuracy;
                            int blueDodge = blueMove.BaseDodge - redMove.EnemyDodge;

                            // Calculate dodge for both gamecocks.
                            int redAccuracy = redMove.BaseAccuracy - blueMove.EnemyAccuracy;
                            int redDodge = redMove.BaseDodge - blueMove.EnemyDodge;

                            // Roll for move for both gamecocks.
                            int blueRoll = UERandom.Range(1, 101);
                            int redRoll = UERandom.Range(1, 101);

                            // Calculate move success odds.
                            int blueOdds = 100 - (blueAccuracy - redDodge);
                            int redOdds = 100 - (redAccuracy - blueDodge);

                            // Calculate move success.
                            bool blueSuccess = (blueRoll >= blueOdds);
                            bool redSuccess = (redRoll >= redOdds);

                            // Resolve damage for both gamecocks.
                            if (blueSuccess && (blueDamage > 0)) red.Hp -= blueDamage;
                            if (redSuccess && (redDamage > 0)) blue.Hp -= redDamage;

                            // Resolve bonus initiative for next round for both gamecocks.
                            blue.InitiativeBonus = blueSuccess ? blueMove.BaseInitiative : 0;
                            red.InitiativeBonus = redSuccess ? redMove.BaseInitiative : 0;

                            clash.BlueSuccess = blueSuccess;
                            clash.BlueHp = blue.Hp;
                            clash.BlueDamage = blueDamage;
                            clash.BlueAttackWeight = !clash.BlueInitiative ? 0.3f : 0.7f;
                            clash.BluePre = "TBD";
                            clash.BlueAttack = "TBD";
                            clash.BlueHit = "TBD";
                            clash.BluePost = "TBD";

                            clash.RedSuccess = redSuccess;
                            clash.RedHp = red.Hp;
                            clash.RedDamage = redDamage;
                            clash.RedAttackWeight = !clash.RedInitiative ? 0.3f : 0.7f; ;
                            clash.RedPre = "TBD";
                            clash.RedAttack = "TBD";
                            clash.RedHit = "TBD";
                            clash.RedPost = "TBD";
                        }

                        // Add to Log.
                        logs.Add(string.Format(
                            "Round #{0}:\n" +
                            "Blue Action (Success: {1} | Pre: {2} | Attack: {3} at {4}% | Hit: {5} at {6}% | Post: {7} | Damage: {8} | HP: {9})\n" +
                            "Red Action (Success: {10} | Pre: {11} | Attack: {12} at {13}% | Hit: {14} at {15}% | Post: {16} | Damage: {17} | HP: {18})",
                            roundsCounter + 1,
                            clash.BlueSuccess, clash.BluePre, clash.BlueAttack, clash.BlueAttackWeight, clash.BlueHit, (100 - clash.BlueAttackWeight), clash.BluePost, clash.BlueDamage, clash.BlueHp,
                            clash.RedSuccess, clash.RedPre, clash.RedAttack, clash.RedAttackWeight, clash.RedHit, (100 - clash.RedAttackWeight), clash.RedPost, clash.RedDamage, clash.RedHp
                            ));

                        // Register Round Data.
                        clashes.Add(clash);

                        break;
                    #endregion

                    #region Decision
                    case FightRoundType.Decision:

                        string decisionResult = string.Empty;

                        if (blue.Hp <= 0 && red.Hp <= 0)
                            decisionResult = "Draw";
                        else if (blue.Hp <= 0)
                            decisionResult = red.Gamecock.UniqueId;
                        else if (red.Hp <= 0)
                            decisionResult = blue.Gamecock.UniqueId;
                        else
                            decisionResult = "Proceed";

                        if (decisionResult != "Proceed")
                            isFighting = false;

                        // Build Round Data.
                        FightRound_Decision decision = new FightRound_Decision
                        {
                            Index = roundsCounter,
                            Decision = decisionResult
                        };

                        // Add to Log.
                        logs.Add(string.Format(
                            "Round #{0}:\n" +
                            "Decision: {1}",
                            roundsCounter + 1,
                            decision.Decision
                            ));

                        // Register Round Data.
                        decisions.Add(decision);

                        break;
                    #endregion
                }

                // Register Rounds.
                indexes.Add(new FightRoundIndex { Index = roundsCounter, Type = roundType.ToString() });

                // Increment rounds counter.
                roundsCounter++;
            }

            FightResult result = new FightResult
            {
                ArenaId = "ArenaId",
                DerbyId = "DerbyId",
                FightId = "FightId",
                BlueId = blue.Gamecock.UniqueId,
                RedId = red.Gamecock.UniqueId,
                StartTime = "2018-01-01 00:00:00",
                EndTime = "2018-01-01 00:15:00",
                Indexes = indexes.ToArray(),
                Approaches = approaches.ToArray(),
                Clashes = clashes.ToArray(),
                Decisions = decisions.ToArray(),
                Standstills = standstills.ToArray(),

                Logs = logs.ToArray()
            };

            return result;
        }

        private static FightRoundType DetermineRoundType (FightingGamecock blue, FightingGamecock red, FightRoundType previousRoundType = FightRoundType.Null)
        {
            if (blue.Hp <= 0 || red.Hp <= 0)
                return FightRoundType.Decision;

            return FightRoundType.Clash;
        }
        #endregion
    }

    public class FightingGamecock
    {
        #region Fields & Properties
        public int Hp { get; set; }

        public int InitiativeBonus { get; set; }

        public Vector3 Position { get; set; }

        private Gamecock _gamecock;
        public Gamecock Gamecock
        {
            get { return _gamecock; }
        }

        public bool IsAlive
        {
            get { return (Hp > 0); }
        }
        #endregion

        #region Constructor
        public FightingGamecock (int startingHp, int startingInitiativeBonus, Vector3 startingPosition, Gamecock gamecock)
        {
            Hp = startingHp;
            InitiativeBonus = startingInitiativeBonus;
            Position = startingPosition;

            _gamecock = gamecock;
        }
        #endregion
    }

    [Serializable]
    public partial class FightRoundIndex
    {
        [FSP, J("Idx")] public int Index { get; set; }
        [FSP, J("Typ")] public string Type { get; set; }
    }

    [Serializable]
    public partial class FightRoundPosition
    {
        [FSP, J("Agl")] public int Angle { get; set; }
        [FSP, J("Hgt")] public float Height { get; set; }
        [FSP, J("Rad")] public float Radius { get; set; }
    }

    [Serializable]
    public partial class FightRound_Approach
    {
        [FSP, J("Idx")] public int Index { get; set; }
        [FSP, J("Pos")] public FightRoundPosition Position { get; set; }
        [FSP, J("BAc")] public string BlueAction { get; set; }
        [FSP, J("RAc")] public string RedAction { get; set; }
    }

    [Serializable]
    public partial class FightRound_Clash
    {
        [FSP, J("Idx")] public int Index { get; set; }
        [FSP, J("Pos")] public FightRoundPosition Position { get; set; }
        [FSP, JI] public bool BlueInitiative { get; set; }
        [FSP, J("BSu")] public bool BlueSuccess { get; set; }
        [FSP, J("BHp")] public int BlueHp { get; set; }
        [FSP, J("BDa")] public int BlueDamage { get; set; }
        [FSP, JI] public int BlueDamageMitigated { get; set; }
        [FSP, JI] public int BlueInitiativeValue { get; set; }
        [FSP, J("BAW")] public float BlueAttackWeight { get; set; }
        [FSP, JI] public string BlueMove { get; set; }
        [FSP, J("BPr")] public string BluePre { get; set; }
        [FSP, J("BAt")] public string BlueAttack { get; set; }
        [FSP, J("BHi")] public string BlueHit { get; set; }
        [FSP, J("BPo")] public string BluePost { get; set; }
        [FSP, JI] public bool RedInitiative { get; set; }
        [FSP, J("RSu")] public bool RedSuccess { get; set; }
        [FSP, J("RHp")] public int RedHp { get; set; }
        [FSP, J("RDa")] public int RedDamage { get; set; }
        [FSP, JI] public int RedDamageMitigated { get; set; }
        [FSP, JI] public int RedInitiativeValue { get; set; }
        [FSP, J("RAW")] public float RedAttackWeight { get; set; }
        [FSP, JI] public string RedMove { get; set; }
        [FSP, J("RPr")] public string RedPre { get; set; }
        [FSP, J("RAt")] public string RedAttack { get; set; }
        [FSP, J("RHi")] public string RedHit { get; set; }
        [FSP, J("RPo")] public string RedPost { get; set; }
    }

    [Serializable]
    public partial class FightRound_Decision
    {
        [FSP, J("Idx")] public int Index { get; set; }
        [FSP, J("Dec")] public string Decision { get; set; }
    }

    [Serializable]
    public partial class FightRound_Standstill
    {
        [FSP, J("Idx")] public int Index { get; set; }
        [FSP, J("Pos")] public FightRoundPosition Position { get; set; }
        [FSP, J("BAc")] public string BlueAction { get; set; }
        [FSP, J("RAc")] public string RedAction { get; set; }
    }

    public enum FightRoundType
    {
        Null,
        Clash,
        Decision
    }
}