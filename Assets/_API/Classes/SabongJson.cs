﻿using FullSerializer;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using J = Newtonsoft.Json.JsonPropertyAttribute;

/// <summary>
/// TODO: Someone has to clean up the return values from the backend. Only relevant values should be passed.
/// </summary>

[Serializable]
public partial class Arena
{
    [fsProperty] [J("ArenaID")] public string ArenaId { get; set; }
    [fsProperty] [J("Name")] public string Name { get; set; }
    [fsProperty] [J("loc")] public string Location { get; set; }
    [fsProperty] [J("LocationX")] public float MapCoordinateX { get; set; }
    [fsProperty] [J("LocationY")] public float MapCoordinateY { get; set; }

    public static Arena FromJson(string json)
    {
        return JsonConvert.DeserializeObject<Arena>(json);
    }
}

[Serializable]
public partial class EventDerby
{
    [fsProperty] [J("EventID")] public string DerbyId;
    [fsProperty] [J("ArenaID")] public string ArenaId;
    [fsProperty] [J("EventName")] public string Name;
    [fsProperty] [J("Category")] public string Category;
    [fsProperty] [J("LevelRestriction")] public string Restriction;
    [fsProperty] [J("EntryCount")] public string EntryCount;
    [fsProperty] [J("StartDate")] public string DateStart;
    [fsProperty] [J("EndDate")] public string DateEnd;
    [fsProperty] [J("EntranceFee")] public int EntranceFee;
    [fsProperty] [J("MinBetAmount")] public int MinBetAmount;
    [fsProperty] [J("MaxBetAmount")] public int MaxBetAmount;

    public static EventDerby FromJson(string json)
    {
        return JsonConvert.DeserializeObject<EventDerby>(json);
    }
}

[Serializable]
public partial class Derby
{
    [fsProperty] [J("EventID")] public string DerbyId;
    [fsProperty] [J("ArenaID")] public string ArenaId;
    [fsProperty] [J("EventName")] public string Name;
    [fsProperty] [J("Category")] public string Category;
    [fsProperty] [J("LevelRestriction")] public string Restriction;
    [fsProperty] [J("EntryCount")] public string EntryCount;
    [fsProperty] [J("StartDate")] public string DateStart;
    [fsProperty] [J("EndDate")] public string DateEnd;
    [fsProperty] [J("EntranceFee")] public int EntranceFee;
    [fsProperty] [J("MinBetAmount")] public int MinBetAmount;
    [fsProperty] [J("MaxBetAmount")] public int MaxBetAmount;

    [fsProperty] [J("arena")] public Arena Arena;
    [fsProperty] [J("fts")] public GCFight[] Fights;

    public static Derby FromJson(string json)
    {
        return JsonConvert.DeserializeObject<Derby>(json);
    }
}

[Serializable]
public partial class Bet
{
    [fsProperty] [J("uId")] public string UserId { get; set; }
    [fsProperty] [J("fId")] public string FightId { get; set; }
    [fsProperty] [J("mId")] public string Gamecock1Id { get; set; }
    [fsProperty] [J("wId")] public string Gamecock2Id { get; set; }
    [fsProperty] [J("mvl")] public int Gamecock1Value { get; set; }
    [fsProperty] [J("wvl")] public int Gamecock2Value { get; set; }

    public static Bet FromJson(string json)
    {
        return JsonConvert.DeserializeObject<Bet>(json);
    }
}

public partial class User
{
    [fsProperty] [J("uId")] public string UserId { get; set; }
    [fsProperty] [J("fnm")] public string FirstName { get; set; }
    [fsProperty] [J("lnm")] public string LastName { get; set; }
    [fsProperty] [J("mny")] public int Money { get; set; }

    public string FullName { get { return FirstName + " " + LastName; } }

    public static User FromJson(string json)
    {
        return JsonConvert.DeserializeObject<User>(json);
    }
}

[Serializable]
public partial class ArenaSetting
{
    public string ArenaSettingID;
    public string AdvertisementID;
    public string Material;
    public string Type;
}

[Serializable]
public partial class Advertisement
{
    public string AdvertisementID;
    public string AdvertiserName;
    public string BannerSml;
    public string BannerMid;
    public string BannerLrg;
    public string BannerLong;
    public string Poster;
    public string Flag;
    public string Scroll;
    public string Stageside;
    public string StageCnt;
    public string Others;
}

[Serializable]
public partial class GCFight
{
    [fsProperty] [J("EventMatchID")] public string FightId { get; set; }
    [fsProperty] [J("aId")] public string ArenaId { get; set; }
    [fsProperty] [J("EventID")] public string DerbyId { get; set; }
    [fsProperty] [J("CreatedDate")] public string CreatedDate { get; set; }
    [fsProperty] [J("ModifiedDate")] public string ModifiedDate { get; set; }
    [fsProperty] [J("FirstRoosterID")] public string Gamecock1Id
    {
        get { return _firstRoosterId; }
        set
        {
            _firstRoosterId = value;

            _gamecock1.UniqueId = _firstRoosterId;
        }
    }
    [fsProperty] [J("SecondRoosterID")] public string Gamecock2Id
    {
        get { return _secondRoosterId; }
        set
        {
            _secondRoosterId = value;

            _gamecock2.UniqueId = _secondRoosterId;
        }
    }
    [fsProperty] [J("MatchName")] public string MatchName { get; set; }
    [fsProperty] [J("gc1")] public Gamecock Gamecock1
    {
        get { return _gamecock1; }
        set { _gamecock1 = value; }
    }
    [fsProperty] [J("gc2")] public Gamecock Gamecock2
    {
        get { return _gamecock2; }
        set { _gamecock2 = value; }
    }
    [fsProperty] [J("FirstRoosterTrait")] public string FirstRoosterTrait
    {
        get { return _firstRoosterTrait; }
        set
        {
            _firstRoosterTrait = value;
            _gamecock1.Attributes = JsonConvert.DeserializeObject<GCAttributes>(_firstRoosterTrait);
        }
    }
    [fsProperty] [J("SecondRoosterTrait")] public string SecondRoosterTrait
    {
        get { return _secondRoosterTrait; }
        set
        {
            _secondRoosterTrait = value;
            _gamecock2.Attributes = JsonConvert.DeserializeObject<GCAttributes>(_secondRoosterTrait);
        }
    }
    [fsProperty] [J("MatchWinner")] public string WinnerID { get; set; }
    [fsProperty] [J("gb1")] public int Gamecock1Bet { get; set; }
    [fsProperty] [J("gb2")] public int Gamecock2Bet { get; set; }
    [fsProperty] [J("StartTime")] public string DateStart { get; set; }
    //[fsProperty] [J("EndTime")] public string DateEnd { get; set; }
    [fsProperty] [J("FightNo")] public string FightNumber { get; set; }

    public int Duration;
    public GCRooster first_rooster;
    public GCRooster second_rooster;

    private string _firstRoosterId;
    private string _secondRoosterId;
    private string _firstRoosterTrait;
    private string _secondRoosterTrait;
    private Gamecock _gamecock1 = new Gamecock();
    private Gamecock _gamecock2 = new Gamecock();
    private DateTime startTime = DateTime.MinValue;
    private DateTime endTime = DateTime.MinValue;

    [fsProperty] [J("event")] public EventDerby derby;
    public DateTime DateStartValue
    {
        get
        {
            if (startTime == DateTime.MinValue)
                startTime = DateTime.Parse(DateStart);

            return startTime;
        }
    }

    //public DateTime DateEndValue
    //{
    //    get
    //    {
    //        if (endTime == DateTime.MinValue)
    //            endTime = DateTime.Parse(DateEnd);

    //        return endTime;
    //    }
    //}

    public static GCFight FromJson(string json)
    {
        return JsonConvert.DeserializeObject<GCFight>(json);
    }
}

[Serializable]
public partial class GCFightData
{
    [fsProperty] [J("EventMatchID")] public string FightId { get; set; }
    [fsProperty] [J("FirstRoosterID")] public string BlueGamecockId { get; set; }
    [fsProperty] [J("SecondRoosterID")] public string RedGamecockId { get; set; }
    [fsProperty] [J("MatchWinner")] public string WinnerGamecockId { get; set; }
    [fsProperty] [J("FightScript")] public string FightScriptString
    {
        get { return _fightScriptString; }
        set
        {
            _fightScriptString = value;

            //Rounds = JsonConvert.DeserializeObject<GCFightRound[]>(_fightScriptString);
            RoundsA = JsonConvert.DeserializeObject<GCFightRoundA[]>(_fightScriptString);
        }
    }
    [fsProperty] [J("log")] public GCFightRound[] Rounds { get; set; }
    [fsProperty] [J("logA")] public GCFightRoundA[] RoundsA { get; set; }

    private string _fightScriptString;

    public static GCFightData FromJson (string json)
    {
        return JsonConvert.DeserializeObject<GCFightData>(json);
    }
}

[Serializable]
public partial class GCFightRound
{
    [fsProperty] [J("gc1Health")] public int BlueGamecockHealth { get; set; }
    [fsProperty] [J("gc2Health")] public int RedGamecockHealth { get; set; }
    [fsProperty] [J("gc1FM")] public GCFightMove BlueFightMove { get; set; }
    [fsProperty] [J("gc2FM")] public GCFightMove RedFightMove { get; set; }
    [fsProperty] [J("gcFEvents")] public GCFightEvent[] Events { get; set; }

    public static GCFightRound FromJson(string json)
    {
        return JsonConvert.DeserializeObject<GCFightRound>(json);
    }
}

[Serializable]
public partial class GCRooster
{
    public string RoosterID;
    public string Name;
    public string BreederID;
    public string RoosterBreederID;
    public string RoosterTrait;
    public GCBreeder breeder;
}

[Serializable]
public partial class GCBreeder
{
    public string BreederID;
    public string PlayerID;
    public string Name;
    public int Level;
    public int BreederExperience;
}

[Serializable]
public partial class GCFightRoundA
{
    [fsProperty] [J("pos")] public GCPosition Position { get; set; }
    [fsProperty] [J("bhp")] public int BlueHealth { get; set; }
    [fsProperty] [J("bdmg")] public int BlueDamage { get; set; }
    [fsProperty] [J("bwt")] public int BlueWeight { get; set; }
    [fsProperty] [J("bpa")] public string BluePreAnimation { get; set; }
    [fsProperty] [J("boff")] public string BlueOffenseAnimation { get; set; }
    [fsProperty] [J("bdef")] public string BlueDefenseAnimation { get; set; }
    [fsProperty] [J("blan")] public string BlueLandingAnimation { get; set; }
    [fsProperty] [J("rhp")] public int RedHealth { get; set; }
    [fsProperty] [J("rdmg")] public int RedDamage { get; set; }
    [fsProperty] [J("rwt")] public int RedWeight { get; set; }
    [fsProperty] [J("rpa")] public string RedPreAnimation { get; set; }
    [fsProperty] [J("roff")] public string RedOffenseAnimation { get; set; }
    [fsProperty] [J("rdef")] public string RedDefenseAnimation { get; set; }
    [fsProperty] [J("rlan")] public string RedLandingAnimation { get; set; }

    public static GCFightRoundA FromJson (string json)
    {
        return JsonConvert.DeserializeObject<GCFightRoundA>(json);
    }
}

[Serializable]
public partial class GCPosition
{
    [fsProperty] [J("x")] public int X { get; set; }
    [fsProperty] [J("y")] public int Y { get; set; }
    [fsProperty] [J("z")] public int Z { get; set; }

    public static GCPosition FromJson (string json)
    {
        return JsonConvert.DeserializeObject<GCPosition>(json);
    }
}

[Serializable]
public partial class GCFightMove
{
    [fsProperty] [J("moveId")] public int MoveId { get; set; }
    [fsProperty] [J("success")] public bool Success { get; set; }
    [fsProperty] [J("critical")] public bool Critical { get; set; }
    [fsProperty] [J("damage")] public int Damage { get; set; }
    [fsProperty] [J("height")] public int Height { get; set; }
    [fsProperty] [J("ailmentIds")] public int[] AilmentIds { get; set; }

    public static GCFightMove FromJson(string json)
    {
        return JsonConvert.DeserializeObject<GCFightMove>(json);
    }
}

[Serializable]
public partial class GCFightEvent
{
    [fsProperty] [J("gcId")] public string GamecockId { get; set; }
    [fsProperty] [J("type")] public string Type { get; set; }
    [fsProperty] [J("payload")] public GCFightPayload Payload { get; set; }

    public static GCFightEvent FromJson(string json)
    {
        return JsonConvert.DeserializeObject<GCFightEvent>(json);
    }
}

[Serializable]
public partial class GCFightPayload
{
    [fsProperty] [J("id")] public string Id { get; set; }
    [fsProperty] [J("duration")] public int Duration { get; set; }
    [fsProperty] [J("damage")] public int Damage { get; set; }

    public static GCFightPayload FromJson(string json)
    {
        return JsonConvert.DeserializeObject<GCFightPayload>(json);
    }
}

[Serializable]
public partial class Gamecock
{
    [fsProperty] [J("uId")] public string UniqueId { get; set; }
    [fsProperty] [J("bId")] public string BreedId { get; set; }
    [fsProperty] [J("nam")] public string Name { get; set; }
    [fsProperty] [J("brn")] public string BreedName { get; set; }
    [fsProperty] [J("brd")] public string BreederName { get; set; }
    [fsProperty] [J("sty")] public GCStyle Style { get; set; }
    [fsProperty] [J("att")] public GCAttributes Attributes { get; set; }
    [fsProperty] [J("mvc")] public GCMoveCard[] MoveCards { get; set; }

    public static Gamecock FromJson(string json)
    {
        return JsonConvert.DeserializeObject<Gamecock>(json);
    }
}

[Serializable]
public partial class GCBreed
{
    [fsProperty] [J("uId")] public string UniqueId { get; set; }
    [fsProperty] [J("nam")] public string Name { get; set; }
    [fsProperty] [J("sty")] public GCStyle Style { get; set; }
    [fsProperty] [J("att")] public GCAttributes Attributes { get; set; }
    [fsProperty] [J("mvc")] public GCMoveCard[] MoveCards { get; set; }

    public static GCBreed FromJson(string json)
    {
        return JsonConvert.DeserializeObject<GCBreed>(json);
    }
}

[Serializable]
public partial class GCStyle
{
    [fsProperty] [J("leg")] public string LegStation { get; set; }
    [fsProperty] [J("cmb")] public string Comb { get; set; }
    [fsProperty] [J("hck")] public string Hackle { get; set; }
    [fsProperty] [J("wng")] public string Wing { get; set; }
    [fsProperty] [J("sdd")] public string Saddle { get; set; }
    [fsProperty] [J("shk")] public string Shank { get; set; }
    [fsProperty] [J("tai")] public string Tail { get; set; }

    public static GCStyle FromJson(string json)
    {
        return JsonConvert.DeserializeObject<GCStyle>(json);
    }
}

[Serializable]
public partial class GCAttributes
{
    [fsProperty] [J("fpk")] public int FightingPeak { get; set; }
    [fsProperty] [J("gam")] public int Gameness { get; set; }
    [fsProperty] [J("int")] public int Intellect { get; set; }
    [fsProperty] [J("pow")] public int Power { get; set; }
    [fsProperty] [J("cut")] public int Cut { get; set; }
    [fsProperty] [J("spa")] public int SpeedAerial { get; set; }
    [fsProperty] [J("spg")] public int SpeedGround { get; set; }
    [fsProperty] [J("ped")] public int Pedigree { get; set; }
    [fsProperty] [J("ins")] public int Instinct { get; set; }
    [fsProperty] [J("bwg")] public int BaseWeight { get; set; }

    public static GCAttributes FromJson(string json)
    {
        return JsonConvert.DeserializeObject<GCAttributes>(json);
    }
}

[Serializable]
public partial class GCMoveCard
{
    [fsProperty] [J("uId")] public string UniqueId { get; set; }
    [fsProperty] [J("nam")] public string Name { get; set; }
    [fsProperty] [J("wgt")] public int Weight { get; set; }
    [fsProperty] [J("mv1")] public GCMove Move1 { get; set; }
    [fsProperty] [J("mv2")] public GCMove Move2 { get; set; }

    public static GCMoveCard FromJson(string json)
    {
        return JsonConvert.DeserializeObject<GCMoveCard>(json);
    }
}

[Serializable]
public partial class GCMove
{
    [fsProperty] [J("uId")] public int UniqueId { get; set; }
    [fsProperty] [J("nam")] public string Name { get; set; }
    [fsProperty] [J("aId")] public int AnimationId { get; set; }
    [fsProperty] [J("ver")] public int Verticality { get; set; }
    [fsProperty] [J("bin")] public int BaseInitiative { get; set; }
    [fsProperty] [J("bda")] public int BaseDamage { get; set; }
    [fsProperty] [J("bac")] public int BaseAccuracy { get; set; }
    [fsProperty] [J("bdg")] public int BaseDodge { get; set; }
    [fsProperty] [J("ein")] public int EnemyInitiative { get; set; }
    [fsProperty] [J("eda")] public int EnemyDamage { get; set; }
    [fsProperty] [J("eac")] public int EnemyAccuracy { get; set; }
    [fsProperty] [J("edg")] public int EnemyDodge { get; set; }
    [fsProperty] [J("pai")] public GCAilment[] PossibleAilments { get; set; }

    public static GCMove FromJson (string json)
    {
        return JsonConvert.DeserializeObject<GCMove>(json);
    }

    public static GCMove DeepCopy (GCMove move)
    {
        return new GCMove
        {
            UniqueId = move.UniqueId,
            Name = move.Name,
            AnimationId = move.AnimationId,
            Verticality = move.Verticality,
            BaseInitiative = move.BaseInitiative,
            BaseDamage = move.BaseDamage,
            BaseAccuracy = move.BaseAccuracy,
            BaseDodge = move.BaseDodge,
            EnemyInitiative = move.EnemyInitiative,
            EnemyDamage = move.EnemyDamage,
            EnemyAccuracy = move.EnemyAccuracy,
            EnemyDodge = move.EnemyDodge,
            PossibleAilments = move.PossibleAilments.Select(x => GCAilment.DeepCopy(x)).ToArray()
        };
    }
}

[Serializable]
public partial class GCAilment
{
    [fsProperty] [J("uId")] public int UniqueId { get; set; }
    [fsProperty] [J("nam")] public string Name { get; set; }
    [fsProperty] [J("wgt")] public int Weight { get; set; }
    [fsProperty] [J("dur")] public int Duration { get; set; }
    [fsProperty] [J("dpr")] public int DamagePerRound { get; set; }

    public static GCAilment FromJson (string json)
    {
        return JsonConvert.DeserializeObject<GCAilment>(json);
    }

    public static GCAilment DeepCopy (GCAilment ailment)
    {
        return new GCAilment
        {
            UniqueId = ailment.UniqueId,
            Name = ailment.Name,
            Weight = ailment.Weight,
            Duration = ailment.Duration,
            DamagePerRound = ailment.DamagePerRound
        };
    }
}

[Serializable]
public partial class LoginResponse
{
    public string success;
    public string error;
    public XToken data;
    public XUser user;

    public bool IsSuccessful
    {
        get { return (success != "0") ? true : false; }
    }

    public string Token
    {
        get
        {
            if (data != null)
                return data.token;
            else
                return string.Empty;
        }
    }
}

[Serializable]
public partial class XToken
{
    public string token;
}

[Serializable]
public partial class XSimpleResponse
{
    public string ErrorMessage;
    public string success;
    public string status;
    public string message;
    public string error;
    public string Result;
}

[Serializable]
public partial class XPayoutResponse : XSimpleResponse
{
    public string Message;
    public string PlayerPayoutID;
}

[Serializable]
public partial class XUser
{
    public string AccountID;
    public string FbID;
    public string FbApiToken;
    public string username;
    public string Email;
    public string FirstName;
    public string MiddleName;
    public string LastName;
    public string BirthDate;
    public string PhoneNumber;
    public string SignUpDate;
    public int LoginCount;
    public string remember_token;
    public string CreatedBy;
    public string CreatedDate;
    public string ModifiedBy;
    public string ModifiedDate;
    public int Active;
    public int Deleted;
    public Player player;
}

[Serializable]
public partial class Player
{
    public string PlayerID;
    public string AccountID;
    public string LastLoginTime;
    public int CoinAmount;
    public string Languange;
    public int TotalDepositCount;
    public int TotalDepositAmount;
    public int TotalBonusCount;
    public int TotalBonusAmount;
    public int FirstTimeDepositAmount;
    public string FirstTimeDepositDate;
    public int Level;
    public int PlayerExperience;
    public int TotalEarnings;
    public int TotalLoss;
}

[Serializable]
public partial class PlayerHistory : XSimpleResponse
{
    public string PlayerHistoryID;
    public string MatchID;
    public string PlayerID;
    public int BlueOdds;
    public int RedOdds;
    public int BlueBet;
    public int RedBet;
    public int BlueWinnings;
    public int RedWinnings;
    public int ExpGained;
    public string TStamp;
    public Player player;
    public GCFight match;
}

[Serializable]
public partial class PlayerPayout
{
    public string PlayerPayoutID;
    public string PlayerID;
    public string MatchID;
    public int Amount;
    public string TStamp;
    public int isClaimed;
    public Player player;
    public GCFight match;
}

[Serializable]
public partial class SocketBet
{
    [fsProperty] public string username;
    [fsProperty] public string betAmount;
    [fsProperty] public string cockId;
}

[Serializable]
public partial class SocketBettingUser
{
    [fsProperty] [J("blueBet")] public int blueBet;
    [fsProperty] [J("redBet")] public int redBet;
    [fsProperty] [J("user")] public User user = new User();
}

[Serializable]
public partial class SocketMatch
{
    [fsProperty] [J("EventMatchID")] public string EventMatchID;
    [fsProperty] [J("EventID")] public string EventID;
    [fsProperty] [J("BlueID")] public string BlueID;
    [fsProperty] [J("RedID")] public string RedID;
    [fsProperty] [J("StartTime")] public string StartTime;
    [fsProperty] [J("EndTime")] public string EndTime;
    [fsProperty] [J("Duration")] public string Duration;
    [fsProperty] [J("BettingUsers")] public List<SocketBettingUser> BettingUsers = new List<SocketBettingUser>();

    public int TotalBlueBet { get { return BettingUsers.Sum(x => x.blueBet); } }
    public int TotalRedBet { get { return BettingUsers.Sum(x => x.redBet); } }
    public int TotalBets { get { return BettingUsers.Sum(x => x.blueBet + x.redBet); } }

    public int HouseTotal { get { return TotalBets - ((int)(TotalBets * 0.064f)); } }
    public float BlueOdds { get { return ((float) HouseTotal) / TotalBlueBet; }}
    public float RedOdds { get { return ((float)HouseTotal) / TotalRedBet; } }
}

[Serializable]
public partial class SocketIOUser
{
    public string username { get; set; }
    public string Token { get; set; }
}

[Serializable]
public partial class SocketIOMatch
{
    public string EventMatchID { get; set; }
    public string EventID { get; set; }
    public string MatchName { get; set; }
    public string FirstRoosterID { get; set; }
    public string SecondRoosterID { get; set; }
    public string StartTime { get; set; }
    public string EndTime { get; set; }
    public string Duration { get; set; }
    public string MatchWinner { get; set; }
    public string CreatedDate { get; set; }
    public string ModifiedDate { get; set; }
    public int Active { get; set; }
    public int Deleted { get; set; }
    public int isFinished { get; set; }
}

[Serializable]
public partial class SocketIOMatchUpdate
{
    public string EventMatchID { get; set; }
    public SocketIOStake[] totals { get; set; }
    public SocketIOStake[] matchTotals { get; set; }
    public SocketIOPayout payout { get; set; }

    public int GetPlayerMeronBet ()
    {
        int result = 0;

        if (totals != null)
            foreach (SocketIOStake stake in totals)
                if (stake.rooster == "1")
                {
                    result = stake.total;
                    break;
                }

        return result;
    }

    public int GetPlayerWalaBet ()
    {
        int result = 0;

        if (totals != null)
            foreach (SocketIOStake stake in totals)
                if (stake.rooster == "2")
                {
                    result = stake.total;
                    break;
                }

        return result;
    }

    public int GetAllPlayersMeronBet ()
    {
        int result = 0;

        if (matchTotals != null)
            foreach (SocketIOStake stake in matchTotals)
                if (stake.rooster == "1")
                {
                    result = stake.total;
                    break;
                }

        return result;
    }

    public int GetAllPlayersWalaBet ()
    {
        int result = 0;

        if (matchTotals != null)
            foreach (SocketIOStake stake in matchTotals)
                if (stake.rooster == "2")
                {
                    result = stake.total;
                    break;
                }

        return result;
    }

    public int GetPayoutMeron ()
    {
        int result = 0;

        if (payout != null)
            result = payout.r1;

        return result;
    }

    public int GetPayoutWala ()
    {
        int result = 0;

        if (payout != null)
            result = payout.r2;

        return result;
    }
    //HACK
    public int GetPlayerPayoutMeron ()
    {
        return (int) (((float) GetPlayerMeronBet() / 100) * GetPayoutWala());
    }
    //HACK
    public int GetPlayerPayoutWala ()
    {
        return (int)(((float) GetPlayerWalaBet() / 100) * GetPayoutMeron());
    }

    public string DisplayMatchUpdate()
    {
        string result;

        result = "EventMatchID: " + EventMatchID + " PlayerMeronBet: " + GetPlayerMeronBet().ToString() + " PlayerWalaBet: " + GetPlayerWalaBet().ToString() + " MeronTotal: "
            + GetAllPlayersMeronBet().ToString() + " WalaTotal: " + GetAllPlayersWalaBet().ToString() + " MeronNet: " + GetPayoutMeron().ToString() + " WalaNet: " + GetPayoutWala().ToString();

        return result;
    }
}

[Serializable]
public partial class SocketIOStake
{
    public string rooster { get; set; }
    public int total { get; set; }
}

[Serializable]
public partial class SocketIOPayout
{
    public int r1 { get; set; }
    public int r2 { get; set; }
}

[Serializable]
public partial class SocketIOBet
{
    public string EventMatchID { get; set; }
    public int FirstRooster { get; set; }
    public int SecondRooster { get; set; }
    public int Phase { get; set; }
}

[Serializable]
public partial class SocketIOMatchBets
{
    public string EventMatchID { get; set; }
    public int Amount { get; set; }
    public string CockId { get; set; }
}