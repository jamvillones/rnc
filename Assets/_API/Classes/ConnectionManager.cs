
//﻿using FullInspector;
//using FullSerializer;
//using Newtonsoft.Json;
//using Newtonsoft.Json.Linq;
//using System;
//using System.Linq;
//using UnityEngine;
//using UnityEssentials;
//using UnityEssentials.ExtensionMethods;
//using System.Collections.Generic;

//public class ConnectionManager : FSSingleton<ConnectionManager>
//{
//    #region Fields
//    [InspectorHeader("Game Settings")]
//    [fsProperty] private User _user;
//    [fsProperty] private string _matchId;

//    [InspectorHeader("API Settings")]
//    [fsProperty] private ISabongApiHandler _apiHandler;
//    [fsProperty] private string _apiUrl = "http://api.legendsofthepit.com/api/v1";

//    [InspectorHeader("Socket.IO Settings")]
//    [fsProperty] private BaseSocketIoClient _socketHandler;
//    [fsProperty] private string _socketUrl = "ws://128.199.215.246:3000";
//    [fsProperty] private bool _socketConnected = false;
//    [fsProperty] private bool _socketInMatch = false;
//    [fsProperty] private string _socketInMatchId = string.Empty;
//    public MockSocketDataObject SocketDataObject;
//    private SocketMatch _socketMatch = null;

//    private bool _initialized = false;
//    private Action<bool> _socket_On_JoinGameFinish;
//    private Action<bool> _socket_On_JoinMatchFinish;

//    private int _socket_blueBet = 0;
//    private int _socket_redBet = 0;
//    #endregion

//    int tempBet;

//    #region Properties
//    public bool Initialized
//    {
//        get { return _initialized; }
//    }

//    public User User
//    {
//        get { return _user; }
//    }

//    public bool SocketConnected
//    {
//        get { return _socketConnected; }
//    }

//    public bool SocketInMatch
//    {
//        get { return _socketInMatch; }
//    }
//    #endregion

//    #region Methods
//    private void Initialize ()
//    {
//        if (_initialized)
//            return;

//        // Temporary
//        //RandomizeUser();
//        //MenuController.Instance.Datas.CheckPlayerPrefs();

//        _matchId = UERandom.Range(1, 1000001).ToString();
//        // END

//        // Initialize API Handler.
//        if (_apiHandler != null)
//        {
//            if (_apiHandler is MarkSabongApiHandler)
//            {
//                MarkSabongApiHandler msah = (_apiHandler as MarkSabongApiHandler);
//                msah.BaseUrl = _apiUrl;
//                msah.UserId = _user.UserId;
//                msah.AccessToken = "helloworld";
//                msah.MonoBehaviour = this;
//            }
//            //else if (_apiHandler is MockSabongApiHandler)
//            //{
//            //    MockSabongApiHandler msah = (_apiHandler as MockSabongApiHandler);
//            //    msah.MockApiDataObject = Resources.Load("MockApiDataObject") as MockApiDataObject;
//            //}
//        }
//        else
//        {
//            Debug.Log("Api Handler is null. Setting value to MarkSabongApiHandler.");

//            MarkSabongApiHandler msah = new MarkSabongApiHandler();
//            msah.BaseUrl = _apiUrl;
//            msah.UserId = _user.UserId;
//            msah.AccessToken = "helloworld";
//            msah.MonoBehaviour = this;

//            _apiHandler = msah;
//        }

//        // Initialize Socket Handler.
//        if (_socketHandler != null &&
//            !_socketUrl.IsNullOrEmpty())
//        {
//            _socketHandler.BaseUrl = _socketUrl;
//            _socketHandler.On_Connect = Socket_SubscribeSabongEvents;
//            _socketHandler.On_Disconnect = Socket_UnsubscribeSabongEvents;

//            _socketHandler.Connect();
//        }
//        else
//        {
//            Debug.Log("Socket Handler is null. Setting value to QuobjectSocketIoClient.");

//            _socketHandler = new QuobjectSocketIoClient (_socketUrl)
//            {
//                On_Connect = Socket_SubscribeSabongEvents,
//                On_Disconnect = Socket_UnsubscribeSabongEvents
//            };

//            _socketHandler.Connect();
//        }

//        _initialized = true;
//    }

//    // Temporary
//    public void RandomizeUser ()
//    {
//        _user.UserId = UERandom.Range(1, 1000001).ToString();
//        _user.FirstName = UERandom.Choice("Andrew", "Bobson", "Charlie", "Dean", "Eric",
//            "Alice", "Betty", "Christine", "Dianne", "Elaine");
//        _user.LastName = UERandom.Choice("Atlas", "Butcher", "Cook", "Diner", "Enger",
//            "Filibuster", "Garfield", "Holland", "Il", "Jackson");
//        _user.Money = 3000;
//    }
//    #endregion

//    #region ISabongApiHandler Methods
//    public void GetAllArenas (Action<bool, Arena[]> callback)
//    {
//        if (_apiHandler == null)
//        {
//            Debug.LogError("Api not handled.");
//            return;
//        }

//        _apiHandler.GetAllArenas(callback);
//    }

//    public void GetAllDerbies (Action<bool, Derby[]> callback)
//    {
//        if (_apiHandler == null)
//        {
//            Debug.LogError("Api not handled.");
//            return;
//        }

//        _apiHandler.GetAllDerbies(callback);
//    }

//    public void GetArena (string arenaId, Action<bool, Arena> callback)
//    {
//        if (_apiHandler == null)
//        {
//            Debug.LogError("Api not handled.");
//            return;
//        }

//        _apiHandler.GetArena(arenaId, callback);
//    }

//    public void GetDerby (string derbyId, Action<bool, Derby> callback)
//    {
//        if (_apiHandler == null)
//        {
//            Debug.LogError("Api not handled.");
//            return;
//        }

//        _apiHandler.GetDerby(derbyId, callback);
//    }

//    public void GetFight (string fightId, Action<bool, GCFight> callback)
//    {
//        if (_apiHandler == null)
//        {
//            Debug.LogError("Api not handled.");
//            return;
//        }

//        _apiHandler.GetFight(fightId, callback);
//    }

//    public void GetFightResult (string fightId, Action<bool, GCFightData> callback)
//    {
//        if (_apiHandler == null)
//        {
//            Debug.LogError("Api not handled.");
//            return;
//        }

//        _apiHandler.GetFightResult(fightId, callback);
//    }

//    public void GetActiveDerby (string arenaId, Action<bool, Derby> callback)
//    {
//        if (_apiHandler == null)
//        {
//            Debug.LogError("Api not handled.");
//            return;
//        }

//        _apiHandler.GetActiveDerby(arenaId, callback);
//    }

//    public void GetDerbyFights (string derbyId, Action<bool, GCFight[]> callback)
//    {
//        if (_apiHandler == null)
//        {
//            Debug.LogError("Api not handled.");
//            return;
//        }

//        _apiHandler.GetDerbyFights(derbyId, callback);
//    }

//    public void PostFightBet (string fightId, int gamecock1Bet, int gamecock2Bet, Action<bool> callback)
//    {
//        if (_apiHandler == null)
//        {
//            Debug.LogError("Api not handled.");
//            return;
//        }

//        _apiHandler.PostFightBet(fightId, gamecock1Bet, gamecock2Bet, callback);
//    }
//    #endregion

//    #region BaseSocketIoClient Subscription Methods
//    private void Socket_SubscribeSabongEvents (object data)
//    {
//        if (_socketHandler != null)
//        {
//            _socketHandler.On("join_game", Socket_On_JoinGame);
//            _socketHandler.On("authSuccess", Socket_On_AuthSuccess);
//            _socketHandler.On("authFailed", Socket_On_AuthFailed);
//            _socketHandler.On("join_match", Socket_On_JoinMatch);
//            _socketHandler.On("joinMatchSuccess", Socket_On_JoinMatchSuccess);
//            _socketHandler.On("alreadyInMatch", Socket_On_AlreadyInMatch);
//            _socketHandler.On("new_bet", Socket_On_NewBet);

//            Socket_Emit_JoinGame(_user.UserId, (b) => _socketConnected = b);
//        }
//    }

//    private void Socket_UnsubscribeSabongEvents (object data)
//    {
//        if (_socketHandler != null)
//        {
//            _socketHandler.Off("join_game");
//            _socketHandler.Off("authSuccess");
//            _socketHandler.Off("authFailed");
//            _socketHandler.Off("join_match");
//            _socketHandler.Off("joinMatchSuccess");
//            _socketHandler.Off("alreadyInMatch");
//            _socketHandler.Off("new_bet");
//        }
//    }

//    private void Socket_On_JoinGame ()
//    {
//        Debug.Log("join_game");
//    }

//    private void Socket_On_AuthSuccess ()
//    {
//        Debug.Log("authSuccess");

//        if (_socket_On_JoinGameFinish != null)
//        {
//            _socket_On_JoinGameFinish(true);
//            _socket_On_JoinGameFinish = null;
//        }
//    }

//    private void Socket_On_AuthFailed ()
//    {
//        Debug.Log("authFailed");

//        if (_socket_On_JoinGameFinish != null)
//        {
//            _socket_On_JoinGameFinish(false);
//            _socket_On_JoinGameFinish = null;
//        }
//    }

//    private void Socket_On_JoinMatch ()
//    {
//        Debug.Log("join_match");
//    }

//    private void Socket_On_JoinMatchSuccess ()
//    {
//        Debug.Log("joinMatchSuccess");

//        _socketInMatch = true;

//        // TODO
//        MockSocket_AddMatch(_user.UserId, _socketMatch);

//        if (_socket_On_JoinMatchFinish != null)
//        {
//            _socket_On_JoinMatchFinish(true);
//            _socket_On_JoinMatchFinish = null;
//        }
//    }

//    private void Socket_On_AlreadyInMatch ()
//    {
//        Debug.Log("alreadyInMatch");

//        _socketInMatch = true;

//        if (_socket_On_JoinMatchFinish != null)
//        {
//            _socket_On_JoinMatchFinish(true);
//            _socket_On_JoinMatchFinish = null;
//        }
//    }

//    private void Socket_On_NewBet (object data)
//    {
//        SocketBet bet = JsonConvert.DeserializeObject<SocketBet>(data.ToString());

//        // TODO
//        MockSocket_UpdateBet(_socketInMatchId, bet.username, bet.betAmount, bet.cockId);

//        Debug.LogFormat("'{0}' betted ${1} on Cock #{2}.",
//            bet.username,
//            bet.betAmount,
//            bet.cockId);
//    }

//    public bool Socket_Emit_JoinGame (string username, Action<bool> callback = null)
//    {
//        if (_socketHandler == null)
//            return false;

//        bool result = _socketHandler.Emit("join_game",
//            new JObject(
//                new JProperty("username", username)));

//        if (!result)
//        {
//            if (callback != null)
//                callback(false);

//            return false;
//        }

//        if (callback != null)
//            _socket_On_JoinGameFinish = callback;

//        return true;
//    }

//    public bool Socket_Emit_JoinMatch (string eventMatchId, string eventId, string blueId, string redId, string startTime, string endTime, string duration, Action<bool> callback = null)
//    {
//        if (_socketHandler == null)
//            return false;

//        bool result = _socketHandler.Emit("join_match",
//            new JObject(
//                new JProperty("EventMatchID", eventMatchId),
//                new JProperty("EventID", eventId),
//                new JProperty("StartTime", startTime),
//                new JProperty("EndTime", endTime),
//                new JProperty("Duration", duration)));

//        if (!result)
//        {
//            if (callback != null)
//                callback(false);

//            return false;
//        }

//        _socketInMatchId = eventMatchId;
//        _socket_blueBet = UERandom.Range(0, 100001);
//        _socket_redBet = _socket_blueBet + UERandom.Range(-5000, 5001);

//        _socketMatch = new SocketMatch()
//        {
//            EventMatchID = eventMatchId,
//            EventID = eventId,
//            BlueID = blueId,
//            RedID = redId,
//            StartTime = startTime,
//            EndTime = endTime,
//            Duration = duration
//        };

//        if (callback != null)
//            _socket_On_JoinMatchFinish = callback;

//        return true;
//    }

//    public bool Socket_Emit_LeaveMatch (string eventMatchId)
//    {
//        _socketInMatch = false;
//        _socketInMatchId = string.Empty;

//        Debug.LogFormat("'{0}' left the match #{1}.",
//            _user.FullName,
//            _socketInMatchId);

//        return true;
//    }

//    public bool Socket_Emit_NewBet (string eventMatchId, string amount, string cockId)
//    {
//        if (_socketHandler == null)
//            return false;

//        // TODO
//        if (SocketDataObject.Matches.First(x => x.EventMatchID == eventMatchId) != null)
//        {
//            if (cockId == SocketDataObject.Matches.First(x => x.EventMatchID == eventMatchId).BlueID)
//                tempBet = SocketDataObject.Matches.First(x => x.EventMatchID == eventMatchId).BettingUsers.First(x => x.user.UserId == User.UserId).blueBet;
//            else if (cockId == SocketDataObject.Matches.First(x => x.EventMatchID == eventMatchId).RedID)
//                tempBet = SocketDataObject.Matches.First(x => x.EventMatchID == eventMatchId).BettingUsers.First(x => x.user.UserId == User.UserId).redBet;
//        }

//        _user.Money = _user.Money - Mathf.Abs(tempBet - int.Parse(amount));

//        //Debug.Log("User Money: " + _user.Money);

//        MockSocket_UpdateBet(eventMatchId, _user.UserId, amount, cockId);

//        Debug.LogFormat("'{0}' betted ${1} on Cock #{2}.",
//            "You",
//            amount,
//            cockId);

//        return _socketHandler.Emit("new_bet",
//            new JObject(
//                new JProperty("EventMatchID", eventMatchId),
//                new JProperty("amount", amount),
//                new JProperty("cockId", cockId)));
//    }

//    public void MockSocket_AddMatch (string userId, SocketMatch newSocketMatch)
//    {
//        if (SocketDataObject != null)
//        {
//            if (SocketDataObject.Matches == null)
//                SocketDataObject.Matches = new List<SocketMatch>();

//            if (SocketDataObject.Matches.Count == 0 ||
//                !SocketDataObject.Matches.Any(x => x.EventMatchID == newSocketMatch.EventMatchID))
//            {
//                SocketDataObject.Matches.Add(new SocketMatch()
//                {
//                    EventMatchID = newSocketMatch.EventMatchID,
//                    EventID = newSocketMatch.EventID,
//                    BlueID = newSocketMatch.BlueID,
//                    RedID = newSocketMatch.RedID,
//                    StartTime = newSocketMatch.StartTime,
//                    EndTime = newSocketMatch.EndTime,
//                    Duration = newSocketMatch.Duration
//                });
//            }

//            SocketMatch socketMatch = SocketDataObject.Matches.First(x => x.EventMatchID == newSocketMatch.EventMatchID);

//            if (socketMatch != null)
//            {
//                socketMatch.BettingUsers.Add(new SocketBettingUser()
//                {
//                    blueBet = _socket_blueBet,
//                    redBet = _socket_redBet,
//                    user = new User() { UserId = "All" }
//                });

//                socketMatch.BettingUsers.Add(new SocketBettingUser() { blueBet = 0, redBet = 0,
//                    user = new User() { UserId = userId }
//                });
//            }
//        }
//    }

//    public void MockSocket_UpdateBet (string eventMatchId, string userId, string amount, string cockId)
//    {
//        if (SocketDataObject != null)
//        {
//            SocketMatch socketMatch = SocketDataObject.Matches.First(x => x.EventMatchID == eventMatchId);

//            if (socketMatch != null)
//            {
//                if (socketMatch.BettingUsers == null)
//                    socketMatch.BettingUsers = new List<SocketBettingUser>();

//                if (socketMatch.BettingUsers.Count == 0 ||
//                    !socketMatch.BettingUsers.Any(x => x.user.UserId == userId))
//                {
//                    socketMatch.BettingUsers.Add(new SocketBettingUser()
//                    {
//                        blueBet = (socketMatch.BlueID == cockId ? int.Parse(amount) : 0),
//                        redBet = (socketMatch.RedID == cockId ? int.Parse(amount) : 0),
//                        user = new User()
//                        {
//                            UserId = userId
//                        }
//                    });
//                }
//                else
//                {
//                    SocketBettingUser socketBettingUser = socketMatch.BettingUsers.First(x => x.user.UserId == userId);

//                    if (socketBettingUser != null)
//                    {
//                        if (socketMatch.BlueID == cockId)
//                            socketBettingUser.blueBet = int.Parse(amount);

//                        else if (socketMatch.RedID == cockId)
//                            socketBettingUser.redBet = int.Parse(amount);
//                    }
//                }
//            }
//        }
//    }
//    #endregion

//    #region MonoBehaviour Methods
//    protected override void Awake ()
//    {
//        base.Awake();

//        Initialize();
//    }

//    private void Update ()
//    {
//        if (Input.GetKeyDown(KeyCode.Q))
//            Socket_Emit_JoinGame(_user.UserId);

//        if (Input.GetKeyDown(KeyCode.W))
//            Socket_Emit_JoinMatch(_matchId, _matchId, "1", "2", "0", "10", "10");

//        if (Input.GetKeyDown(KeyCode.E))
//            Socket_Emit_NewBet(_matchId, UERandom.Range(1, 101).ToString(), UERandom.Choice("1", "2"));

//        if (Input.GetKeyDown(KeyCode.R))
//            Socket_Emit_LeaveMatch(_matchId);

//        if (Input.GetKeyDown(KeyCode.Escape))
//            if (_socketHandler != null)
//                _socketHandler.Disconnect();
//    }

//    protected override void OnDestroy ()
//    {
//        base.OnDestroy();

//        if (_socketHandler != null)
//            _socketHandler.Disconnect();
//    }
//    #endregion
//}
//﻿//using FullInspector;
////using FullSerializer;
////using Newtonsoft.Json;
////using Newtonsoft.Json.Linq;
////using System;
////using System.Linq;
////using UnityEngine;
////using UnityEssentials;
////using UnityEssentials.ExtensionMethods;
////using System.Collections.Generic;

////public class ConnectionManager : FSSingleton<ConnectionManager>
////{
////    #region Fields
////    [InspectorHeader("Game Settings")]
////    [fsProperty] private User _user;
////    [fsProperty] private string _matchId;

////    [InspectorHeader("API Settings")]
////    [fsProperty] private ISabongApiHandler _apiHandler;
////    [fsProperty] private string _apiUrl = "http://api.legendsofthepit.com/api/v1";

////    [InspectorHeader("Socket.IO Settings")]
////    [fsProperty] private BaseSocketIoClient _socketHandler;
////    [fsProperty] private string _socketUrl = "ws://128.199.215.246:3000";
////    [fsProperty] private bool _socketConnected = false;
////    [fsProperty] private bool _socketInMatch = false;
////    [fsProperty] private string _socketInMatchId = string.Empty;
////    public MockSocketDataObject SocketDataObject;
////    private SocketMatch _socketMatch = null;

////    private bool _initialized = false;
////    private Action<bool> _socket_On_JoinGameFinish;
////    private Action<bool> _socket_On_JoinMatchFinish;

////    private int _socket_blueBet = 0;
////    private int _socket_redBet = 0;
////    #endregion

////    int tempBet;

////    #region Properties
////    public bool Initialized
////    {
////        get { return _initialized; }
////    }

////    public User User
////    {
////        get { return _user; }
////    }

////    public bool SocketConnected
////    {
////        get { return _socketConnected; }
////    }

////    public bool SocketInMatch
////    {
////        get { return _socketInMatch; }
////    }
////    #endregion

////    #region Methods
////    private void Initialize ()
////    {
////        if (_initialized)
////            return;

////        // Temporary
////        //RandomizeUser();
////        //MenuController.Instance.Datas.CheckPlayerPrefs();

////        _matchId = UERandom.Range(1, 1000001).ToString();
////        // END

////        // Initialize API Handler.
////        if (_apiHandler != null)
////        {
////            if (_apiHandler is MarkSabongApiHandler)
////            {
////                MarkSabongApiHandler msah = (_apiHandler as MarkSabongApiHandler);
////                msah.BaseUrl = _apiUrl;
////                msah.UserId = _user.UserId;
////                msah.AccessToken = "helloworld";
////                msah.MonoBehaviour = this;
////            }
////            else if (_apiHandler is MockSabongApiHandler)
////            {
////                MockSabongApiHandler msah = (_apiHandler as MockSabongApiHandler);
////                msah.MockApiDataObject = Resources.Load("MockApiDataObject") as MockApiDataObject;
////            }
////        }
////        else
////        {
////            Debug.Log("Api Handler is null. Setting value to MarkSabongApiHandler.");

////            MarkSabongApiHandler msah = new MarkSabongApiHandler();
////            msah.BaseUrl = _apiUrl;
////            msah.UserId = _user.UserId;
////            msah.AccessToken = "helloworld";
////            msah.MonoBehaviour = this;

////            _apiHandler = msah;
////        }

////        // Initialize Socket Handler.
////        if (_socketHandler != null &&
////            !_socketUrl.IsNullOrEmpty())
////        {
////            _socketHandler.BaseUrl = _socketUrl;
////            _socketHandler.On_Connect = Socket_SubscribeSabongEvents;
////            _socketHandler.On_Disconnect = Socket_UnsubscribeSabongEvents;

////            _socketHandler.Connect();
////        }
////        else
////        {
////            Debug.Log("Socket Handler is null. Setting value to QuobjectSocketIoClient.");

////            _socketHandler = new QuobjectSocketIoClient (_socketUrl)
////            {
////                On_Connect = Socket_SubscribeSabongEvents,
////                On_Disconnect = Socket_UnsubscribeSabongEvents
////            };

////            _socketHandler.Connect();
////        }

////        _initialized = true;
////    }

////    // Temporary
////    public void RandomizeUser ()
////    {
////        _user.UserId = UERandom.Range(1, 1000001).ToString();
////        _user.FirstName = UERandom.Choice("Andrew", "Bobson", "Charlie", "Dean", "Eric",
////            "Alice", "Betty", "Christine", "Dianne", "Elaine");
////        _user.LastName = UERandom.Choice("Atlas", "Butcher", "Cook", "Diner", "Enger",
////            "Filibuster", "Garfield", "Holland", "Il", "Jackson");
////        _user.Money = 3000;
////    }
////    #endregion

////    #region ISabongApiHandler Methods
////    public void GetAllArenas (Action<bool, Arena[]> callback)
////    {
////        if (_apiHandler == null)
////        {
////            Debug.LogError("Api not handled.");
////            return;
////        }

////        _apiHandler.GetAllArenas(callback);
////    }

////    public void GetAllDerbies (Action<bool, Derby[]> callback)
////    {
////        if (_apiHandler == null)
////        {
////            Debug.LogError("Api not handled.");
////            return;
////        }

////        _apiHandler.GetAllDerbies(callback);
////    }

////    public void GetArena (string arenaId, Action<bool, Arena> callback)
////    {
////        if (_apiHandler == null)
////        {
////            Debug.LogError("Api not handled.");
////            return;
////        }

////        _apiHandler.GetArena(arenaId, callback);
////    }

////    public void GetDerby (string derbyId, Action<bool, Derby> callback)
////    {
////        if (_apiHandler == null)
////        {
////            Debug.LogError("Api not handled.");
////            return;
////        }

////        _apiHandler.GetDerby(derbyId, callback);
////    }

////    public void GetFight (string fightId, Action<bool, GCFight> callback)
////    {
////        if (_apiHandler == null)
////        {
////            Debug.LogError("Api not handled.");
////            return;
////        }

////        _apiHandler.GetFight(fightId, callback);
////    }

////    public void GetFightResult (string fightId, Action<bool, GCFightData> callback)
////    {
////        if (_apiHandler == null)
////        {
////            Debug.LogError("Api not handled.");
////            return;
////        }

////        _apiHandler.GetFightResult(fightId, callback);
////    }

////    public void GetActiveDerby (string arenaId, Action<bool, Derby> callback)
////    {
////        if (_apiHandler == null)
////        {
////            Debug.LogError("Api not handled.");
////            return;
////        }

////        _apiHandler.GetActiveDerby(arenaId, callback);
////    }

////    public void GetDerbyFights (string derbyId, Action<bool, GCFight[]> callback)
////    {
////        if (_apiHandler == null)
////        {
////            Debug.LogError("Api not handled.");
////            return;
////        }

////        _apiHandler.GetDerbyFights(derbyId, callback);
////    }

////    public void PostFightBet (string fightId, int gamecock1Bet, int gamecock2Bet, Action<bool> callback)
////    {
////        if (_apiHandler == null)
////        {
////            Debug.LogError("Api not handled.");
////            return;
////        }

////        _apiHandler.PostFightBet(fightId, gamecock1Bet, gamecock2Bet, callback);
////    }
////    #endregion

////    #region BaseSocketIoClient Subscription Methods
////    private void Socket_SubscribeSabongEvents (object data)
////    {
////        if (_socketHandler != null)
////        {
////            _socketHandler.On("join_game", Socket_On_JoinGame);
////            _socketHandler.On("authSuccess", Socket_On_AuthSuccess);
////            _socketHandler.On("authFailed", Socket_On_AuthFailed);
////            _socketHandler.On("join_match", Socket_On_JoinMatch);
////            _socketHandler.On("joinMatchSuccess", Socket_On_JoinMatchSuccess);
////            _socketHandler.On("alreadyInMatch", Socket_On_AlreadyInMatch);
////            _socketHandler.On("new_bet", Socket_On_NewBet);

////            Socket_Emit_JoinGame(_user.UserId, (b) => _socketConnected = b);
////        }
////    }

////    private void Socket_UnsubscribeSabongEvents (object data)
////    {
////        if (_socketHandler != null)
////        {
////            _socketHandler.Off("join_game");
////            _socketHandler.Off("authSuccess");
////            _socketHandler.Off("authFailed");
////            _socketHandler.Off("join_match");
////            _socketHandler.Off("joinMatchSuccess");
////            _socketHandler.Off("alreadyInMatch");
////            _socketHandler.Off("new_bet");
////        }
////    }

////    private void Socket_On_JoinGame ()
////    {
////        Debug.Log("join_game");
////    }

////    private void Socket_On_AuthSuccess ()
////    {
////        Debug.Log("authSuccess");

////        if (_socket_On_JoinGameFinish != null)
////        {
////            _socket_On_JoinGameFinish(true);
////            _socket_On_JoinGameFinish = null;
////        }
////    }

////    private void Socket_On_AuthFailed ()
////    {
////        Debug.Log("authFailed");

////        if (_socket_On_JoinGameFinish != null)
////        {
////            _socket_On_JoinGameFinish(false);
////            _socket_On_JoinGameFinish = null;
////        }
////    }

////    private void Socket_On_JoinMatch ()
////    {
////        Debug.Log("join_match");
////    }

////    private void Socket_On_JoinMatchSuccess ()
////    {
////        Debug.Log("joinMatchSuccess");

////        _socketInMatch = true;

////        // TODO
////        MockSocket_AddMatch(_user.UserId, _socketMatch);

////        if (_socket_On_JoinMatchFinish != null)
////        {
////            _socket_On_JoinMatchFinish(true);
////            _socket_On_JoinMatchFinish = null;
////        }
////    }

////    private void Socket_On_AlreadyInMatch ()
////    {
////        Debug.Log("alreadyInMatch");

////        _socketInMatch = true;

////        if (_socket_On_JoinMatchFinish != null)
////        {
////            _socket_On_JoinMatchFinish(true);
////            _socket_On_JoinMatchFinish = null;
////        }
////    }

////    private void Socket_On_NewBet (object data)
////    {
////        SocketBet bet = JsonConvert.DeserializeObject<SocketBet>(data.ToString());

////        // TODO
////        MockSocket_UpdateBet(_socketInMatchId, bet.username, bet.betAmount, bet.cockId);

////        Debug.LogFormat("'{0}' betted ${1} on Cock #{2}.",
////            bet.username,
////            bet.betAmount,
////            bet.cockId);
////    }

////    public bool Socket_Emit_JoinGame (string username, Action<bool> callback = null)
////    {
////        if (_socketHandler == null)
////            return false;

////        bool result = _socketHandler.Emit("join_game",
////            new JObject(
////                new JProperty("username", username)));

////        if (!result)
////        {
////            if (callback != null)
////                callback(false);

////            return false;
////        }

////        if (callback != null)
////            _socket_On_JoinGameFinish = callback;

////        return true;
////    }

////    public bool Socket_Emit_JoinMatch (string eventMatchId, string eventId, string blueId, string redId, string startTime, string endTime, string duration, Action<bool> callback = null)
////    {
////        if (_socketHandler == null)
////            return false;

////        bool result = _socketHandler.Emit("join_match",
////            new JObject(
////                new JProperty("EventMatchID", eventMatchId),
////                new JProperty("EventID", eventId),
////                new JProperty("StartTime", startTime),
////                new JProperty("EndTime", endTime),
////                new JProperty("Duration", duration)));

////        if (!result)
////        {
////            if (callback != null)
////                callback(false);

////            return false;
////        }

////        _socketInMatchId = eventMatchId;
////        _socket_blueBet = UERandom.Range(0, 100001);
////        _socket_redBet = _socket_blueBet + UERandom.Range(-5000, 5001);

////        _socketMatch = new SocketMatch()
////        {
////            EventMatchID = eventMatchId,
////            EventID = eventId,
////            BlueID = blueId,
////            RedID = redId,
////            StartTime = startTime,
////            EndTime = endTime,
////            Duration = duration
////        };

////        if (callback != null)
////            _socket_On_JoinMatchFinish = callback;

////        return true;
////    }

////    public bool Socket_Emit_LeaveMatch (string eventMatchId)
////    {
////        _socketInMatch = false;
////        _socketInMatchId = string.Empty;

////        Debug.LogFormat("'{0}' left the match #{1}.",
////            _user.FullName,
////            _socketInMatchId);

////        return true;
////    }

////    public bool Socket_Emit_NewBet (string eventMatchId, string amount, string cockId)
////    {
////        if (_socketHandler == null)
////            return false;

////        // TODO
////        if (SocketDataObject.Matches.First(x => x.EventMatchID == eventMatchId) != null)
////        {
////            if (cockId == SocketDataObject.Matches.First(x => x.EventMatchID == eventMatchId).BlueID)
////                tempBet = SocketDataObject.Matches.First(x => x.EventMatchID == eventMatchId).BettingUsers.First(x => x.user.UserId == User.UserId).blueBet;
////            else if (cockId == SocketDataObject.Matches.First(x => x.EventMatchID == eventMatchId).RedID)
////                tempBet = SocketDataObject.Matches.First(x => x.EventMatchID == eventMatchId).BettingUsers.First(x => x.user.UserId == User.UserId).redBet;
////        }

////        _user.Money = _user.Money - Mathf.Abs(tempBet - int.Parse(amount));

////        //Debug.Log("User Money: " + _user.Money);

////        MockSocket_UpdateBet(eventMatchId, _user.UserId, amount, cockId);

////        Debug.LogFormat("'{0}' betted ${1} on Cock #{2}.",
////            "You",
////            amount,
////            cockId);

////        return _socketHandler.Emit("new_bet",
////            new JObject(
////                new JProperty("EventMatchID", eventMatchId),
////                new JProperty("amount", amount),
////                new JProperty("cockId", cockId)));
////    }

////    public void MockSocket_AddMatch (string userId, SocketMatch newSocketMatch)
////    {
////        if (SocketDataObject != null)
////        {
////            if (SocketDataObject.Matches == null)
////                SocketDataObject.Matches = new List<SocketMatch>();

////            if (SocketDataObject.Matches.Count == 0 ||
////                !SocketDataObject.Matches.Any(x => x.EventMatchID == newSocketMatch.EventMatchID))
////            {
////                SocketDataObject.Matches.Add(new SocketMatch()
////                {
////                    EventMatchID = newSocketMatch.EventMatchID,
////                    EventID = newSocketMatch.EventID,
////                    BlueID = newSocketMatch.BlueID,
////                    RedID = newSocketMatch.RedID,
////                    StartTime = newSocketMatch.StartTime,
////                    EndTime = newSocketMatch.EndTime,
////                    Duration = newSocketMatch.Duration
////                });
////            }

////            SocketMatch socketMatch = SocketDataObject.Matches.First(x => x.EventMatchID == newSocketMatch.EventMatchID);

////            if (socketMatch != null)
////            {
////                socketMatch.BettingUsers.Add(new SocketBettingUser()
////                {
////                    blueBet = _socket_blueBet,
////                    redBet = _socket_redBet,
////                    user = new User() { UserId = "All" }
////                });

////                socketMatch.BettingUsers.Add(new SocketBettingUser() { blueBet = 0, redBet = 0,
////                    user = new User() { UserId = userId }
////                });
////            }
////        }
////    }

////    public void MockSocket_UpdateBet (string eventMatchId, string userId, string amount, string cockId)
////    {
////        if (SocketDataObject != null)
////        {
////            SocketMatch socketMatch = SocketDataObject.Matches.First(x => x.EventMatchID == eventMatchId);

////            if (socketMatch != null)
////            {
////                if (socketMatch.BettingUsers == null)
////                    socketMatch.BettingUsers = new List<SocketBettingUser>();

////                if (socketMatch.BettingUsers.Count == 0 ||
////                    !socketMatch.BettingUsers.Any(x => x.user.UserId == userId))
////                {
////                    socketMatch.BettingUsers.Add(new SocketBettingUser()
////                    {
////                        blueBet = (socketMatch.BlueID == cockId ? int.Parse(amount) : 0),
////                        redBet = (socketMatch.RedID == cockId ? int.Parse(amount) : 0),
////                        user = new User()
////                        {
////                            UserId = userId
////                        }
////                    });
////                }
////                else
////                {
////                    SocketBettingUser socketBettingUser = socketMatch.BettingUsers.First(x => x.user.UserId == userId);

////                    if (socketBettingUser != null)
////                    {
////                        if (socketMatch.BlueID == cockId)
////                            socketBettingUser.blueBet = int.Parse(amount);

////                        else if (socketMatch.RedID == cockId)
////                            socketBettingUser.redBet = int.Parse(amount);
////                    }
////                }
////            }
////        }
////    }
////    #endregion

////    #region MonoBehaviour Methods
////    protected override void Awake ()
////    {
////        base.Awake();

////        Initialize();
////    }

////    private void Update ()
////    {
////        if (Input.GetKeyDown(KeyCode.Q))
////            Socket_Emit_JoinGame(_user.UserId);

////        if (Input.GetKeyDown(KeyCode.W))
////            Socket_Emit_JoinMatch(_matchId, _matchId, "1", "2", "0", "10", "10");

////        if (Input.GetKeyDown(KeyCode.E))
////            Socket_Emit_NewBet(_matchId, UERandom.Range(1, 101).ToString(), UERandom.Choice("1", "2"));

////        if (Input.GetKeyDown(KeyCode.R))
////            Socket_Emit_LeaveMatch(_matchId);

////        if (Input.GetKeyDown(KeyCode.Escape))
////            if (_socketHandler != null)
////                _socketHandler.Disconnect();
////    }

////    protected override void OnDestroy ()
////    {
////        base.OnDestroy();

////        if (_socketHandler != null)
////            _socketHandler.Disconnect();
////    }
////    #endregion
////}

