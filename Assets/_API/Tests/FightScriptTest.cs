﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FullInspector;
using LegendsOfThePit;

public class FightScriptTest : BaseBehavior
{
    public Gamecock gamecock1;
    public Gamecock gamecock2;

    [ContextMenu("Simulate")]
    void Simulate ()
    {
        FightResult.Simulate(gamecock1, gamecock2);
    }
}
