﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeightLocker : MonoBehaviour
{

    public Slider OpposingSlider;
    public Toggle OpposingToggle;
    public Slider OwnSlider;
    Toggle toggle;

    void Start()
    {
        toggle = GetComponent<Toggle>();
    }
    
    public void Lock()
    {
        if(toggle)
        {
            if (toggle.isOn)
            {
                OpposingSlider.interactable = false;
                OpposingToggle.interactable = false;
                OpposingSlider.value = OwnSlider.value;
            }
            else
            {
                OpposingSlider.interactable = true;
                OpposingToggle.interactable = true;
            }
        }
        
    }
}
