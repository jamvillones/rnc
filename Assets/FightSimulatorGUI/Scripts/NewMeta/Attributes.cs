﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NewMeta
{
    /// <summary>
    /// the class that contains the stats of the chicken
    /// </summary>
    [System.Serializable]
    public class Attributes
    {
        public Attributes()
        {
            Gameness = 1;
            Intellect = 1;
            Strength = 1;
            Toughness = 1;
            Agility = 1;
            Weight = 1;
            HealthPoints = TotalHealthPoints;
        }
        public const float CritDamage = 1.5f;
        public const float TarikDamage = 5.0f;

        public float Gameness = 1;
        public float Intellect = 1;
        public float Strength = 1;
        public float Toughness = 1;
        public float Agility = 1;
        public float Weight = 1;

        /// <summary>
        /// = agility + intellect
        /// </summary>
        public float Initiative { get { return Gameness + Intellect; } }
        /// <summary>
        /// = gameness + intellect
        /// </summary>
        public float Morale { get { return Gameness + Intellect; } }
        /// <summary>
        /// = strength + weight
        /// </summary>
        public float Power { get { return Strength*2 + (Weight*0.2f); } }
        /// <summary>
        /// = gameness + agility
        /// </summary>
        public float Speed { get { return Gameness + Agility; } }
        /// <summary>
        /// agility + strength
        /// </summary>
        public float Accuracy { get { return Agility + Intellect/2; } }
        /// <summary>
        /// get the lowest value between agility or intellect
        /// </summary>
        public float DefenseLow { get { return Agility < Intellect ? Agility : Intellect; } }

        public float DefenseMax { get { return Agility > Intellect ? Agility : Intellect; } }
        /// <summary>
        /// = toughness + weight + strength
        /// </summary>
        public float TotalHealthPoints { get { return Weight + Toughness; } }

        public float HealthPoints;

        #region Mapped values
        public float GetMappedInitiative()
        {
            return this.Map(Initiative, 1, 100, 1, 20);
        }
        public float GetMappedPower()
        {
            return this.Map(Power, 1, 100, 1, 25);
        }
        public float GetMappedSpeed()
        {
            return this.Map(Speed, 1, 100, 1, 25);
        }
        public float GetMappedWeight()
        {
            return this.Map(Weight, 1, 100, 1.4f, 2.7f);
        }
        public float GetMappedAccuracy()
        {
            return this.Map(Accuracy, 1, 75, 20, 80);
        }
        public float GetMappedDefenseLow()
        {
            return this.Map(DefenseLow, 1, 100, 5, 45);
        }
        public float GetMappedDefenseMax()
        {
            //return (Agility > Intellect) ? Agility / 2 : Intellect / 2;
            return this.Map(DefenseMax, 1, 100, 5, 45);
        }
        public float GetMappedDefenseAgi()
        {
            return this.Map(Agility, 1, 50, 10, 25);
        }
        public float GetMappedDefenseInt()
        {
            return this.Map(Intellect, 1, 50, 10, 25);
        }
        public static float GetMappedDamage(float Damage)
        {
            if (Damage < 0) Damage = 0;
            if (Damage > 100) Damage = 100;

            return ValueMapper.Map(Damage, 1, 100, 1, 25);
        }
        public float GetMappedCuttingPower()
        {
            //return ValueMapper.Map(GetMappedSpeed(), 1, 25, 2f, 2f);
            return 2f;
        }
        public  float GetMappedCuttingChance()
        {
            return ValueMapper.Map(GetMappedSpeed(), 1, 25, 0, 30);
        }
        #endregion

        #region Bonuses
        public float InitiativeBonus;
        public float DamageBonus { get { return GetMappedPower() / 2; } }
        public float CuttingBonus;
        public float AccuracyBonus;
        public float DefenseBonus;
        #endregion
    }
}
