﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultScreen : MonoBehaviour
{
    public Toggle VisualsToggle;
    public ScrollRect ScrollView;
    public GameObject ResultsText;
    public GameObject GraphArea;


    public void SetVisualization()
    {
        if (VisualsToggle.isOn)
        {
            ResultsText.SetActive(false);
            GraphArea.SetActive(true);
            ScrollView.content = GraphArea.GetComponent<RectTransform>();
            ScrollView.vertical = false;
            ScrollView.horizontal = true;
        }
        else
        {
            ResultsText.SetActive(true);
            GraphArea.SetActive(false);
            ScrollView.content = ResultsText.GetComponent<RectTransform>();
            ScrollView.vertical = true;
            ScrollView.horizontal = false;
        }
    }
}
