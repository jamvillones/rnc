﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DiamondValue : MonoBehaviour
{
    public GameObject MaxAnchorPoint;
    public float Value;
    public Text ValueText;
    public bool Aerial;
    public void Awake()
    {
        //ValueText = GetComponentInChildren<Text>();
        ValueText = transform.Find("Value").GetComponentInChildren<Text>();
    }
    public void UpdateValues(float maxAnchorValue, GameObject cursor, float maxValue) 
    {
        Value = ValueMapper.Map((maxAnchorValue - Vector3.Distance(MaxAnchorPoint.transform.position, cursor.transform.position)), 0, maxAnchorValue, maxValue/4, maxValue);
        Value = Mathf.Clamp(Value, 0, maxValue);
        Value = (float)Decimal.Round(Convert.ToDecimal(Value), 2);
    }
}
