﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class LiveGraph : MonoBehaviour
{
    public UI_CockToUI RedSide;
    public UI_CockToUI BlueSide;
    public LineRenderer LineGraph;
    public float MoveInterval;
    public int LineSpawnThreshold;
    public GameObject LineSprite;
    public Transform LineSpawn;
    public float Ylimit;
    List<GameObject> lines = new List<GameObject>();
    float timeElapsed;
    float bias;
    int updates;
    GameChicken redBoi;
    GameChicken blueBoi;

    // Use this for initialization
    void Start()
    {
        LineGraph.positionCount = 0;
        timeElapsed = 0;
        updates = LineSpawnThreshold;
        createInitialLines();
    }

    // Update is called once per frame
    void Update()
    {
        timeElapsed += Time.deltaTime;
        if (MoveInterval <= timeElapsed)
        {           
            lines.ForEach(l => l.transform.position = new Vector3(l.transform.position.x, l.transform.position.y - 1, l.transform.position.z));
            LineCheck();
            simulateFight();
            timeElapsed = 0;
            updates++;
        }
        if (updates >= LineSpawnThreshold)
        {
            GameObject temp = Instantiate(LineSprite, LineSpawn.position, Quaternion.identity);
            lines.Add(temp);
            updates = 0;
        }
    }

    void simulateFight()
    {
        redBoi = RedSide.SelectedChicken(true);
        blueBoi = BlueSide.SelectedChicken(true);

        redBoi.ChickenAttributes.Gameness = RedSide.CurrentSliders()[0].value;
        redBoi.ChickenAttributes.Intellect = RedSide.CurrentSliders()[1].value;
        redBoi.ChickenAttributes.Strength = RedSide.CurrentSliders()[2].value;
        redBoi.ChickenAttributes.Toughness = RedSide.CurrentSliders()[3].value;
        redBoi.ChickenAttributes.Agility = RedSide.CurrentSliders()[4].value;
        redBoi.ChickenAttributes.Weight = RedSide.CurrentSliders()[5].value;

        blueBoi.ChickenAttributes.Gameness = BlueSide.CurrentSliders()[0].value;
        blueBoi.ChickenAttributes.Intellect = BlueSide.CurrentSliders()[1].value;
        blueBoi.ChickenAttributes.Strength = BlueSide.CurrentSliders()[2].value;
        blueBoi.ChickenAttributes.Toughness = BlueSide.CurrentSliders()[3].value;
        blueBoi.ChickenAttributes.Agility = BlueSide.CurrentSliders()[4].value;
        blueBoi.ChickenAttributes.Weight = BlueSide.CurrentSliders()[5].value;

        List<Results> list = new List<Results>();
        for (int i = 0; i < 35; i++)
            list.Add(Simulator.SimulateFight(blueBoi, redBoi));

        bool blueMajority = list.SelectMany(x => x.DecisionsArray).Where(y => y.Decision == blueBoi.UniqueID).Count() > list.SelectMany(x => x.DecisionsArray).Where(y => y.Decision == redBoi.UniqueID).Count();
        int winRatio = blueMajority ? list.SelectMany(x => x.DecisionsArray).Where(y => y.Decision == blueBoi.UniqueID).Count() : list.SelectMany(x => x.DecisionsArray).Where(y => y.Decision == redBoi.UniqueID).Count();
        addLine(new Vector3(blueMajority ? winRatio : -winRatio, LineSpawn.position.y, LineSpawn.position.z));
    }

    void addLine(Vector3 position)
    {
        for (int i = 0; i < LineGraph.positionCount; i++)
        {
            Vector3 currentLinePos = LineGraph.GetPosition(i);
            currentLinePos.y -= 1;
            LineGraph.SetPosition(i, currentLinePos);
            if (LineGraph.GetPosition(i).y <= Ylimit)
            {
                List<Vector3> positions = new List<Vector3>();
                for (int j = 1; j < LineGraph.positionCount; j++)
                {
                    positions.Add(LineGraph.GetPosition(j));
                }
                LineGraph.positionCount = positions.Count;
                LineGraph.SetPositions(positions.ToArray());
            }
        }
        LineGraph.positionCount += 1;
        LineGraph.SetPosition(LineGraph.positionCount - 1, position);
    }

    void createInitialLines()
    {
        Vector3 spawnLocation = LineSpawn.position;
        spawnLocation.y -= 5;
        for (int i = 0; i < 10; i++)
        {
            GameObject temp = Instantiate(LineSprite, spawnLocation, Quaternion.identity);
            lines.Add(temp);
            spawnLocation.y -= 5;
        }
    }

    public void LineCheck()
    {
        for (int i = 0; i < lines.Count; i++)
        {
            if (lines[i].transform.position.y <= Ylimit)
            {
                Destroy(lines[i].gameObject);
                lines.RemoveAt(i);
                lines.TrimExcess();
            }
        }
    }
}
