﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MoveHolder
{
    #region InferiorMoves
    public static Move InferiorDirectAirAttack
    {
        get
        {
            return new Move("DirectAirAttack(Inferior)", MoveType.Aerial, 0, 0, 0, 0, 0, 0, 0, false, false, false, false, false, true);
        }
    }

    public static Move InferiorDirectGroundAttack
    {
        get
        {
            return new Move("DirectGroundAttack(Inferior)", MoveType.Grounded, 0, 0, 0, 0, 0, 0, 0, false, false, false, false, false, true);
        }
    }

    public static Move InferiorGroundGeneral
    {
        get
        {
            return new Move("GroundGeneral(Inferior)", MoveType.Grounded, 0, 0, 0, 0, 0, 0, 0, false, true, true, false, false, false);
        }
    }

    public static Move InferiorCounterGroundGeneral
    {
        get
        {
            return new Move("CounterGroundGeneral(Inferior)", MoveType.Grounded, 0, 0, 0, 0, 0, 0, 0, false, true, true, false, false, false);
        }
    }

    public static Move InferiorCounterGroundDirect
    {
        get
        {
            return new Move("CounterGroundDirect(Inferior)", MoveType.Grounded, 0, 0, 0, 0, 0, 0, 0, false, false, false, false, false, true);
        }
    }

    public static Move InferiorCounterAirDirect
    {
        get
        {
            return new Move("CounterAirDirect(Inferior)", MoveType.Aerial, 0, 0, 0, 0, 0, 0, 0, false, false, false,false, false, true);
        }
    }

    public static Move InferiorCounterAirGeneral
    {
        get
        {
            return new Move("CounterAirGeneral(Inferior)", MoveType.Aerial, 0, 0, 0, 0, 0, 0, 0, false, true, true, false,false, false);
        }
    }

    public static Move InferiorAirGeneral
    {
        get
        {
            return new Move("AirGeneral(Inferior)", MoveType.Aerial, 0, 0, 0, 0, 0, 0, 0, false, true, true, false, false, false);
        }
    }
    #endregion

    #region SuperiorMoves

    public static Move SuperiorDirectAirAttack
    {
        get
        {
            return new Move("DirectAirAttack(Superior)", MoveType.Aerial, 0, 0, 0, 0, 0, 0, 0, false, false, false, false ,true, true);
        }
    }

    public static Move SuperiorDirectGroundAttack
    {
        get
        {
            return new Move("DirectGroundAttack(Superior)", MoveType.Grounded, 0, 0, 0, 0, 0, 0, 0, true, false, false, true, false, true);
        }
    }

    public static Move SuperiorCounterGroundGeneral
    {
        get
        {
            return new Move("CounterGroundGeneral(Superior)", MoveType.Grounded, 0, 0, 0, 0, 0, 0, 0, false, false, true, false, false, true);
        }
    }

    public static Move SuperiorCounterGroundDirect
    {
        get
        {
            return new Move("CounterGroundDirect(Superior)", MoveType.Grounded, 0, 0, 0, 0, 0, 0, 0, false, false, true, false, false, true);
        }
    }

    public static Move SuperiorCounterAirGeneral
    {
        get
        {
            return new Move("CounterAirGeneral(Superior)", MoveType.Aerial, 0, 0, 0, 0, 0, 0, 0, false, false, true, false, false, true);
        }
    }

    public static Move SuperiorCounterAirDirect
    {
        get
        {
            return new Move("CounterAirDirect(Superior)", MoveType.Aerial, 0, 0, 0, 0, 0, 0, 0, false, false, true, false, false, true);
        }
    }

    public static Move SuperiorAirGeneral
    {
        get
        {
            return new Move("SuperiorAirGeneral(Superior)", MoveType.Aerial, 0, 0, 0, 0, 0, 0, 0, false, false, false, false, true, true);
        }
    }

    public static Move SuperiorGroundGeneral
    {
        get
        {
            return new Move("GroundGeneral(Superior)", MoveType.Grounded, 0, 0, 0, 0, 0, 0, 0, true, false, false, true, false, true);
        }
    }

    #endregion
}
