﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MoveCardHolder
{
    public static MoveCard DirectAirAttack
    {
        get
        {
            MoveCard moveCard = new MoveCard();
            moveCard.UniqueID = 0.ToString();
            moveCard.Name = "Direct Air Attack";
            moveCard.Weight = 10;
            moveCard.SuperiorMove = MoveHolder.SuperiorDirectAirAttack;
            moveCard.InferiorMove = MoveHolder.InferiorDirectAirAttack;
            return moveCard;
        }
    }

    public static MoveCard DirectGroundAttack
    {
        get
        {
            MoveCard moveCard = new MoveCard();
            moveCard.UniqueID = 1.ToString();
            moveCard.Name = "Direct Ground Attack";
            moveCard.Weight = 10;
            moveCard.SuperiorMove = MoveHolder.SuperiorDirectGroundAttack;
            moveCard.InferiorMove = MoveHolder.InferiorDirectGroundAttack;
            return moveCard;
        }
    }

    public static MoveCard CounterGroundGeneral
    {
        get
        {
            MoveCard moveCard = new MoveCard();
            moveCard.UniqueID = 2.ToString();
            moveCard.Name = "Counter Ground General";
            moveCard.Weight = 10;
            moveCard.SuperiorMove = MoveHolder.SuperiorCounterGroundGeneral;
            moveCard.InferiorMove = MoveHolder.InferiorCounterGroundGeneral;
            return moveCard;
        }
    }

    public static MoveCard CounterGroundDirect
    {
        get
        {
            MoveCard moveCard = new MoveCard();
            moveCard.UniqueID = 3.ToString();
            moveCard.Name = "Counter Ground Direct";
            moveCard.Weight = 10;
            moveCard.SuperiorMove = MoveHolder.SuperiorCounterGroundDirect;
            moveCard.InferiorMove = MoveHolder.InferiorCounterGroundDirect;
            return moveCard;
        }
    }

    public static MoveCard CounterAirGeneral
    {
        get
        {
            MoveCard moveCard = new MoveCard();
            moveCard.UniqueID = 4.ToString();
            moveCard.Name = "Counter Air General";
            moveCard.Weight = 10;
            moveCard.SuperiorMove = MoveHolder.SuperiorCounterAirGeneral;
            moveCard.InferiorMove = MoveHolder.InferiorCounterAirGeneral;
            return moveCard;
        }
    }

    public static MoveCard CounterAirDirect
    {
        get
        {
            MoveCard moveCard = new MoveCard();
            moveCard.UniqueID = 5.ToString();
            moveCard.Name = "Counter Air Direct";
            moveCard.Weight = 10;
            moveCard.SuperiorMove = MoveHolder.SuperiorCounterAirDirect;
            moveCard.InferiorMove = MoveHolder.InferiorCounterAirDirect;
            return moveCard;
        }
    }

    public static MoveCard AirGeneral
    {
        get
        {
            MoveCard moveCard = new MoveCard();
            moveCard.UniqueID = 6.ToString();
            moveCard.Name = "Air General";
            moveCard.Weight = 10;
            moveCard.SuperiorMove = MoveHolder.SuperiorAirGeneral;
            moveCard.InferiorMove = MoveHolder.InferiorAirGeneral;
            return moveCard;
        }
    }

    public static MoveCard GroundGeneral
    {
        get
        {
            MoveCard moveCard = new MoveCard();
            moveCard.UniqueID = 7.ToString();
            moveCard.Name = "Ground General";
            moveCard.Weight = 10;
            moveCard.SuperiorMove = MoveHolder.SuperiorGroundGeneral;
            moveCard.InferiorMove = MoveHolder.InferiorGroundGeneral;
            return moveCard;
        }
    }
}
