﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEssentials;
using NewMeta;

[Serializable]
public partial class GameChicken
{
    public GameChicken()
    {
        Name = "";
        UniqueID = "";
        BreederName = "";
        BreederName = "";
        ChickenAttributes = new Attributes();
        for(int i =0; i<8;i++)
        {
            MoveCards[i] = new MoveCard();
        }
    }
    public string Name;
    public string UniqueID;
    public string BreedName;
    public string BreederName;

    public Attributes ChickenAttributes;

    public MoveCard RandomMoveCard()
    {
        return UERandom.WeightedChoice(MoveCards, MoveCards.Select(mc => mc.Weight).ToArray());
    }

    [SerializeField]
    public MoveCard[] MoveCards = new MoveCard[8];
    //{
    //    MoveCardHolder.DirectAirAttack,
    //    MoveCardHolder.AirGeneral,
    //    MoveCardHolder.CounterAirDirect,
    //    MoveCardHolder.CounterAirGeneral,
    //    MoveCardHolder.DirectGroundAttack,
    //    MoveCardHolder.GroundGeneral,
    //    MoveCardHolder.CounterGroundDirect,
    //    MoveCardHolder.CounterGroundGeneral
    //};
}



