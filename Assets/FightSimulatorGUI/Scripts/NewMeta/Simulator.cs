﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEssentials;

/// <summary>
/// ////////////////  TO DO: A PROPER IMPLEMENTATION OF MOVES, INITIATIVE CALCULATION AND DAMAGE COMPUTATION
/// </summary>

public class FightIndex
{
    public int Index { get; set; }
    public string Type { get; set; }
}

public class RoundClash
{
    public int Index { get; set; }

    public bool BlueInitiative { get; set; }
    public bool BlueSuccess { get; set; }
    public int BlueHp { get; set; }
    public int BlueDamage { get; set; }
    public bool BlueCrit { get; set; }
    public int BlueInitiativeValue { get; set; }
    public string BlueMove { get; set; }

    public bool RedInitiative { get; set; }
    public bool RedSuccess { get; set; }
    public int RedHp { get; set; }
    public int RedDamage { get; set; }
    public bool RedCrit { get; set; }
    public int RedInitiativeValue { get; set; }
    public string RedMove { get; set; }
}

public class DetailedRoundClash
{
    public int Index { get; set; }

    public int BlueInitiativeRoll { get; set; }
    public bool BlueInitiative { get; set; }
    public bool BlueSuccess { get; set; }
    public int BlueHp { get; set; }
    public int BlueCuttingBonus { get; set; }
    public int BlueDefenseBonus { get; set; }
    public int BlueBonusDamage { get; set; }
    public int BlueDamage { get; set; }
    public bool BlueCrit { get; set; }
    public int BlueInitiativeValue { get; set; }
    public string BlueMove { get; set; }

    public int RedInitiativeRoll { get; set; }
    public bool RedInitiative { get; set; }
    public bool RedSuccess { get; set; }
    public int RedHp { get; set; }
    public int RedCuttingBonus { get; set; }
    public int RedDefenseBonus { get; set; }
    public int RedBonusDamage { get; set; }
    public int RedDamage { get; set; }
    public bool RedCrit { get; set; }
    public int RedInitiativeValue { get; set; }
    public string RedMove { get; set; }
}

public class FightDecision
{
    public float CritSumRed { get; set; }
    public float CritSumBlue { get; set; }
    public int Index { get; set; }
    public string Decision { get; set; }
}

public class MoveReport
{
    InitiativeType Type { get; set; }
    public int Damage { get; set; }
}

public class Results
{
    public string BlueId { get; set; }
    public string RedId { get; set; }
    public FightIndex[] Indices { get; set; }
    public RoundClash[] ClashesArray { get; set; }
    public FightDecision[] DecisionsArray { get; set; }
    public MoveReport[] RedMoveReportsArray { get; set; }
    public MoveReport[] BlueMoveReportsArray { get; set; }
}

public class DetailedResults
{
    public string BlueId { get; set; }
    public string RedId { get; set; }
    public FightIndex[] Indices { get; set; }
    public DetailedRoundClash[] ClashesArray { get; set; }
    public FightDecision[] DecisionsArray { get; set; }
    public List<string> Logs { get; set; }
}

public enum RoundType
{
    Null,
    Clash,
    Decision
}

public class Simulator
{
    private static RoundType DetermineRoundType(GameChicken blue, GameChicken red, RoundType previousRoundType = RoundType.Null)
    {
        if (blue.ChickenAttributes.HealthPoints <= 0 || red.ChickenAttributes.HealthPoints <= 0)
            return RoundType.Decision;

        return RoundType.Clash;
    }


    public static DetailedResults SimulateDetailedFight(GameChicken blueSide, GameChicken redSide, bool verbose)
    {
        #region fight
        bool isFighting = true;
        int roundsCounter = 0;
        string currentLog = string.Format("\n\n{0} >> blue  VS.  {1} >> red", blueSide.Name, redSide.Name);
        List<string> logList = new List<string>();
        RoundType roundType = RoundType.Null;
        List<FightIndex> indexes = new List<FightIndex>();
        List<DetailedRoundClash> clashes = new List<DetailedRoundClash>();
        List<FightDecision> decisions = new List<FightDecision>();

        blueSide.ChickenAttributes.HealthPoints = blueSide.ChickenAttributes.TotalHealthPoints;
        redSide.ChickenAttributes.HealthPoints = redSide.ChickenAttributes.TotalHealthPoints;

        blueSide.ChickenAttributes.InitiativeBonus = 0;
        redSide.ChickenAttributes.InitiativeBonus = 0;

        logList.Add(currentLog);

        while (isFighting)
        {
            currentLog = "";
            roundType = DetermineRoundType(blueSide, redSide, roundType);

            switch (roundType)
            {
                #region Clash
                case RoundType.Clash:

                    DetailedRoundClash clash = new DetailedRoundClash
                    {
                        Index = roundsCounter
                    };

                    currentLog += "\n====================================================================================================";
                    currentLog += string.Format("\nROUND: {0}", roundsCounter + 1);

                    MoveCard blueMoveCard = blueSide.RandomMoveCard();
                    MoveCard redMoveCard = redSide.RandomMoveCard();

                    currentLog += string.Format("\n\n{0} picked {1} move card.", blueSide.Name, blueMoveCard.Name);
                    currentLog += string.Format("\n{0} picked {1} move card.", redSide.Name, redMoveCard.Name);

                    int blueInitiativeRoll = UnityEngine.Random.Range(1, 21);
                    int redInitiativeRoll = UnityEngine.Random.Range(1, 21);

                    int bluePassiveInitiativeRoll = (int)UERandom.Range(1, blueSide.ChickenAttributes.GetMappedInitiative());
                    int redPassiveInitiativeRoll = (int)UERandom.Range(1, redSide.ChickenAttributes.GetMappedInitiative());

                    int blueBonusInitiativeRoll = (int)blueSide.ChickenAttributes.InitiativeBonus;
                    int redBonusInitiativeRoll = (int)redSide.ChickenAttributes.InitiativeBonus;

                    clash.BlueInitiativeRoll = blueInitiativeRoll;
                    clash.RedInitiativeRoll = redInitiativeRoll;

                    int blueInitiative = blueInitiativeRoll + bluePassiveInitiativeRoll + blueBonusInitiativeRoll;
                    int redInitiative = redInitiativeRoll + redPassiveInitiativeRoll + redBonusInitiativeRoll;

                    clash.BlueInitiative = (blueInitiative > redInitiative);
                    clash.RedInitiative = (blueInitiative < redInitiative);

                    clash.BlueInitiativeValue = blueInitiative;
                    clash.RedInitiativeValue = redInitiative;

                    Move blueMove = !clash.BlueInitiative ? blueMoveCard.InferiorMove : blueMoveCard.SuperiorMove;
                    Move redMove = !clash.RedInitiative ? redMoveCard.InferiorMove : redMoveCard.SuperiorMove;


                    clash.BlueMove = blueMove.Name;
                    clash.RedMove = redMove.Name;

                    blueSide.ChickenAttributes.CuttingBonus = blueMove.CuttingBonus ? blueSide.ChickenAttributes.GetMappedCuttingChance() : 0;
                    redSide.ChickenAttributes.CuttingBonus = redMove.CuttingBonus ? redSide.ChickenAttributes.GetMappedCuttingChance() : 0;

                    clash.BlueCuttingBonus = (int)blueSide.ChickenAttributes.CuttingBonus;
                    clash.RedCuttingBonus = (int)redSide.ChickenAttributes.CuttingBonus;

                    blueSide.ChickenAttributes.DefenseBonus = (blueMove.DodgeBonusAgiInt ? (blueMove.Type.Equals(MoveType.Aerial) ? blueSide.ChickenAttributes.GetMappedDefenseAgi() : blueSide.ChickenAttributes.GetMappedDefenseInt()) : 0);
                    redSide.ChickenAttributes.DefenseBonus = (redMove.DodgeBonusAgiInt ? (redMove.Type.Equals(MoveType.Aerial) ? redSide.ChickenAttributes.GetMappedDefenseAgi() : redSide.ChickenAttributes.GetMappedDefenseInt()) : 0);

                    clash.BlueDefenseBonus = (int)blueSide.ChickenAttributes.DefenseBonus;
                    clash.RedDefenseBonus = (int)redSide.ChickenAttributes.DefenseBonus;

                    int blueBonusDamage = (blueMove.Type.Equals(MoveType.Grounded) ? (int)blueMove.BonusGroundedDamage : (int)blueMove.BonusAerialDamage) + (blueMove.SuperiorDamage ? (int)blueSide.ChickenAttributes.DamageBonus : 0) + (int)blueMove.BonusDamage + 5; //5 is tarik
                    int redBonusDamage = (redMove.Type.Equals(MoveType.Grounded) ? (int)redMove.BonusGroundedDamage : (int)redMove.BonusAerialDamage) + (redMove.SuperiorDamage ? (int)redSide.ChickenAttributes.DamageBonus : 0) + (int)redMove.BonusDamage + 5;

                    clash.BlueBonusDamage = blueBonusDamage;
                    clash.RedBonusDamage = redBonusDamage;

                    //int damageRoll = UnityEngine.Random.Range((int)blueSide.ChickenAttributes.GetMappedPower(), (int)blueSide.ChickenAttributes.GetMappedPower() + (int)blueSide.ChickenAttributes.DamageBonus + 5);

                    int blueCritRoll = blueMove.DealDamage ? UERandom.Range(1, 101) : 0;
                    int redCritRoll = redMove.DealDamage ? UERandom.Range(1, 101) : 0;

                    int blueCritToBeat = (100 - ((int)blueSide.ChickenAttributes.GetMappedCuttingChance()) + (int)blueSide.ChickenAttributes.CuttingBonus);
                    int redCritToBeat = (100 - ((int)redSide.ChickenAttributes.GetMappedCuttingChance()) + (int)redSide.ChickenAttributes.CuttingBonus);

                    bool blueCrit = (blueCritRoll >= blueCritToBeat) ? true : false;
                    bool redCrit = (redCritRoll >= redCritToBeat) ? true : false;

                    clash.BlueCrit = blueCrit;
                    clash.RedCrit = redCrit;

                    int blueDamage = blueMove.DealDamage ? (int)blueSide.ChickenAttributes.GetMappedPower() + blueBonusDamage + (int)redMove.EnemyDamage : 0;
                    int redDamage = redMove.DealDamage ? (int)redSide.ChickenAttributes.GetMappedPower() + redBonusDamage + (int)blueMove.EnemyDamage : 0;
                    //int redDamage = redMove.DealDamage ? (int)redSide.ChickenAttributes.GetMappedPower() + redBonusDamage + (int)blueMove.EnemyDamage : 0;

                    int blueAccuracy = (int)blueSide.ChickenAttributes.GetMappedAccuracy() + (int)blueMove.BonusAccuracy + (int)redMove.EnemyAccuracy - (blueMove.DecreaseHigherAccuracy ? 20 : 0) - (blueMove.DecreaseLowerAccuracy ? 10 : 0);
                    int blueDodge = (blueMove.DodgeBonus ? (int)blueSide.ChickenAttributes.GetMappedDefenseMax() : (int)blueSide.ChickenAttributes.GetMappedDefenseLow()) + (int)blueSide.ChickenAttributes.DefenseBonus + (int)blueMove.BonusDodge + (int)redMove.EnemyDodge;

                    int redAccuracy = (int)redSide.ChickenAttributes.GetMappedAccuracy() + (int)redMove.BonusAccuracy + (int)blueMove.EnemyAccuracy - (redMove.DecreaseHigherAccuracy ? 20 : 0) - (redMove.DecreaseLowerAccuracy ? 10 : 0);
                    int redDodge = (redMove.DodgeBonus ? (int)redSide.ChickenAttributes.GetMappedDefenseMax() : (int)redSide.ChickenAttributes.GetMappedDefenseLow()) + (int)redSide.ChickenAttributes.DefenseBonus + (int)redMove.BonusDodge + (int)blueMove.EnemyDodge;

                    int blueRoll = UERandom.Range(1, 101);
                    int redRoll = UERandom.Range(1, 101);

                    int blueOdds = (blueAccuracy - redDodge);
                    int redOdds = (redAccuracy - blueDodge);

                    bool blueSuccess = (blueRoll <= blueOdds);
                    bool redSuccess = (redRoll <= redOdds);


                    float blueFinalDamage = blueSuccess ? (blueCrit ? blueDamage * 2 : blueDamage) : 0;
                    float redFinalDamage = redSuccess ? (redCrit ? redDamage * 2 : redDamage) : 0;


                    if (blueSuccess && (blueDamage > 0)) redSide.ChickenAttributes.HealthPoints -= blueFinalDamage;
                    if (redSuccess && (redDamage > 0)) blueSide.ChickenAttributes.HealthPoints -= redFinalDamage;


                    blueSide.ChickenAttributes.InitiativeBonus = blueMove.InitiativeBonus ? blueSide.ChickenAttributes.GetMappedInitiative() / 2 : 0;
                    redSide.ChickenAttributes.InitiativeBonus = redMove.InitiativeBonus ? redSide.ChickenAttributes.GetMappedInitiative() / 2 : 0;

                    #region TextFeedback
                    if(verbose)
                    {
                        currentLog += string.Format("\n\n{0} rolled {1} for initiative.", blueSide.Name, blueInitiativeRoll);
                        currentLog += string.Format("\n{0} rolled {1} for initiative.", redSide.Name, redInitiativeRoll);
                        currentLog += string.Format("\n\n{0} has a total of {1} initiative. ({2}(roll) + {3}(base initiative) + {4}(initiative bonus))", blueSide.Name, blueInitiative, blueInitiativeRoll, bluePassiveInitiativeRoll, blueBonusInitiativeRoll);
                        currentLog += string.Format("\n{0} has a total of {1} initiative. ({2}(roll) + {3}(base initiative) + {4}(initiative bonus))", redSide.Name, redInitiative, redInitiativeRoll, redPassiveInitiativeRoll, redBonusInitiativeRoll);
                        currentLog += string.Format("\n\n{0} will use {1} ({2}) move", blueSide.Name, blueMove.Name, clash.BlueInitiative ? "Superior" : "Inferior");
                        currentLog += string.Format("\n{0} will use {1} ({2}) move", redSide.Name, redMove.Name, clash.RedInitiative ? "Superior" : "Inferior");
                        currentLog += string.Format("\n\n{0} has a total of {1}%{2} cutting chance.", blueSide.Name, blueSide.ChickenAttributes.GetMappedCuttingChance() + blueSide.ChickenAttributes.CuttingBonus, blueMove.CuttingBonus ? string.Format("(+{0} move bonus)", blueSide.ChickenAttributes.CuttingBonus) : "");
                        currentLog += string.Format("\n{0} has a total of {1}%{2} cutting chance.", redSide.Name, redSide.ChickenAttributes.GetMappedCuttingChance() + redSide.ChickenAttributes.CuttingBonus, redMove.CuttingBonus ? string.Format("(+{0} move bonus)", redSide.ChickenAttributes.CuttingBonus) : "");
                        currentLog += blueMove.DealDamage ? string.Format("\n\n{0} has rolled {1} for critical odds.", blueSide.Name, blueCritRoll) : string.Format("\n\n{0} ", blueSide.Name) + "won't roll for crits(move deals no damage).";
                        currentLog += redMove.DealDamage ? string.Format("\n{0} has rolled {1} for critical odds.", redSide.Name, redCritRoll) : string.Format("\n{0} ", redSide.Name) + "won't roll for crits(move deals no damage).";
                        currentLog += string.Format("\n\n{0} : ", blueSide.Name) + (blueMove.DealDamage ? string.Format("{0} critRoll >= {1} (100 - {2}({3} PassiveCuttingChance + {4} Cutting Bonus = totalCritChance)) (Percent to exceed to crit) : {5}", blueCritRoll, blueCritToBeat, (100 - blueCritToBeat), blueSide.ChickenAttributes.GetMappedCuttingChance() / 2, blueSide.ChickenAttributes.CuttingBonus, blueCrit ? " Will critically hit!" : " Will not critically hit :(") : "\n\nNo chance to crit.");
                        currentLog += string.Format("\n{0} : ", redSide.Name) + (redMove.DealDamage ? string.Format("{0} critRoll >= {1} (100 - {2}({3} PassiveCuttingChance + {4} Cutting Bonus = totalCritChance)) (Percent to exceed to crit) : {5}", redCritRoll, redCritToBeat, (100 - redCritToBeat), redSide.ChickenAttributes.GetMappedCuttingChance() / 2, redSide.ChickenAttributes.CuttingBonus, redCrit ? " Will critically hit!" : " Will not critically hit :(") : "\nNo chance to crit.");
                        currentLog += string.Format("\n\n{0} has a total of {1} defense ({2},{3}).", blueSide.Name, blueDodge, blueMove.DodgeBonus ? string.Format(" +{0} max defense bonus", (int)blueSide.ChickenAttributes.GetMappedDefenseMax()) : " no defense bonus", blueMove.DodgeBonusAgiInt ? blueMove.Type.Equals(MoveType.Aerial) ? string.Format(" + {0}(max agi bonus)", blueMove.DodgeBonusAgiInt) : string.Format(" + {0}(max int bonus)", blueMove.DodgeBonusAgiInt) : " no bonus from agi/int");
                        currentLog += string.Format("\n{0} has a total of {1} defense ({2},{3}).", redSide.Name, redDodge, redMove.DodgeBonus ? string.Format(" +{0} max defense bonus", (int)redSide.ChickenAttributes.GetMappedDefenseMax()) : " no defense bonus", redMove.DodgeBonusAgiInt ? redMove.Type.Equals(MoveType.Aerial) ? string.Format(" + {0}(max agi bonus)", blueMove.DodgeBonusAgiInt) : string.Format(" + {0}(max int bonus)", blueMove.DodgeBonusAgiInt) : " no bonus from agi/int");
                        currentLog += string.Format("\n\n{0} has rolled {1} for hit chance", blueSide.Name, blueRoll);
                        currentLog += string.Format("\n{0} has rolled {1} hit chance", redSide.Name, redRoll);
                        currentLog += string.Format("\n\n{0} success: ({1}(accuracy) - {2}(enemy dodge)) = {3}", blueSide.Name, blueAccuracy, redDodge, blueOdds);
                        currentLog += string.Format("\n{0} success: ({1}(accuracy) - {2}(enemy dodge)) = {3}", redSide.Name, redAccuracy, blueDodge, redOdds);
                        currentLog += string.Format("\n\n{0} : ", blueSide.Name) + string.Format("{0}(roll) <= {1}(odds); {2}", blueRoll, blueOdds, blueSuccess ? "hit" : "miss");
                        currentLog += string.Format("\n{0} : ", redSide.Name) + string.Format("{0}(roll) <= {1}(odds); {2}", redRoll, redOdds, redSuccess ? "hit" : "miss");
                        currentLog += string.Format("\n\n{0} {1} damage =", blueSide.Name, blueFinalDamage) + (blueMove.DealDamage && blueSuccess ? string.Format(" {0} (PassiveDamage) + {1} (bonusDamage) + {2} (EnemyDamage)", (int)blueSide.ChickenAttributes.GetMappedPower(), blueBonusDamage, (int)redMove.EnemyDamage) : " 0");
                        currentLog += string.Format("\n{0} {1} damage =", redSide.Name, redFinalDamage) + (redMove.DealDamage && redSuccess ? string.Format(" {0} (PassiveDamage) + {1} (bonusDamage) + {2} (EnemyDamage)", (int)redSide.ChickenAttributes.GetMappedPower(), redBonusDamage, (int)blueMove.EnemyDamage) : " 0");
                        currentLog += string.Format("\n\n<color=#ff0000ff>Final damage of {0}: {1} ({2} x {3})", blueSide.Name, blueFinalDamage, blueSuccess ? blueDamage : 0, blueCrit && blueSuccess && blueMove.DealDamage ? string.Format("CRITICAL HIT({0}x)", blueSide.ChickenAttributes.GetMappedCuttingPower()) : "1");
                        currentLog += string.Format("\nFinal damage of {0}: {1} ({2} x {3})</color>", redSide.Name, redFinalDamage, redSuccess ? redDamage : 0, redCrit && redSuccess && redMove.DealDamage ? string.Format("CRITICAL HIT({0}x)", redSide.ChickenAttributes.GetMappedCuttingPower()) : "1");
                        currentLog += string.Format("\n\n{0} now has {1} health left.", blueSide.Name, blueSide.ChickenAttributes.HealthPoints);
                        currentLog += string.Format("\n{0} now has {1} health left.", redSide.Name, redSide.ChickenAttributes.HealthPoints);
                        currentLog += string.Format("\n\n{0} has {1} initiative bonus for the next turn.", blueSide.Name, blueSide.ChickenAttributes.InitiativeBonus);
                        currentLog += string.Format("\n{0} has {1} initiative bonus for the next turn.", redSide.Name, redSide.ChickenAttributes.InitiativeBonus);
                    }
                    else
                    {
                        currentLog += string.Format("\n\n{0}", clash.BlueInitiative ? blueSide.Name + " uses superior move!" : blueSide.Name + " uses inferior move!");
                        currentLog += string.Format("\n\n{0}", clash.RedInitiative ? redSide.Name + " uses superior move!" : redSide.Name + " uses inferior move!");
                        currentLog += string.Format("\n\n{0} {1}", blueSide.Name, blueSuccess ? "hits!" : "misses!");
                        currentLog += string.Format("\n{0} {1}", redSide.Name, redSuccess ? "hits!" : "misses!");
                        currentLog += string.Format("\n\n<color=#ff0000ff>{0} dealt {1} damage!", blueSide.Name, blueFinalDamage);
                        currentLog += string.Format("\n{0} dealt {1} damage!</color>", redSide.Name, redFinalDamage);
                        currentLog += string.Format("\n\n{0} has {1}hp remaining.", blueSide.Name, blueSide.ChickenAttributes.HealthPoints);
                        currentLog += string.Format("\n{0} has {1}hp remaining.", redSide.Name, redSide.ChickenAttributes.HealthPoints);
                        currentLog += string.Format("\n\n{0} gains {1} initiative bonus.", blueSide.Name, blueSide.ChickenAttributes.InitiativeBonus);
                        currentLog += string.Format("\n{0} gains {1} initiative bonus.", redSide.Name, redSide.ChickenAttributes.InitiativeBonus);
                    }
                    #endregion

                    clash.BlueSuccess = blueSuccess;
                    clash.BlueHp = Mathf.RoundToInt(blueSide.ChickenAttributes.HealthPoints);
                    clash.BlueDamage = (int)blueFinalDamage;

                    clash.RedSuccess = redSuccess;
                    clash.RedHp = Mathf.RoundToInt(redSide.ChickenAttributes.HealthPoints);
                    clash.RedDamage = (int)redFinalDamage;

                    clashes.Add(clash);
                    logList.Add(currentLog);

                    break;
                #endregion

                #region Decision
                case RoundType.Decision:

                    string decisionResult = string.Empty;

                    if (Mathf.RoundToInt(blueSide.ChickenAttributes.HealthPoints) <= 0 && Mathf.RoundToInt(redSide.ChickenAttributes.HealthPoints) <= 0)
                    {
                        decisionResult = "Draw";
                        currentLog += "\nXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\nXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n";
                        currentLog += "Match ended in a draw!";
                        currentLog += "\nXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\nXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n";
                    }
                    else if (Mathf.RoundToInt(blueSide.ChickenAttributes.HealthPoints) <= 0)
                    {
                        decisionResult = redSide.UniqueID;
                        currentLog += "\nXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\nXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n";
                        currentLog += string.Format("\n\n{0}({1}) >>red wins the match\n\n", redSide.Name, redSide.BreedName);
                        currentLog += "\nXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\nXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n";
                    }
                    else if (Mathf.RoundToInt(redSide.ChickenAttributes.HealthPoints) <= 0)
                    {
                        decisionResult = blueSide.UniqueID;
                        currentLog += "\nXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\nXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n";
                        currentLog += string.Format("\n\n{0}({1}) >>blue wins the match\n\n", blueSide.Name, blueSide.BreedName);
                        currentLog += "\nXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\nXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n";
                    }
                    else
                        decisionResult = "Proceed";

                    if (decisionResult != "Proceed")
                        isFighting = false;

                    logList.Add(currentLog);
                    FightDecision decision = new FightDecision
                    {
                        Index = roundsCounter,
                        Decision = decisionResult
                    };
                    decisions.Add(decision);

                    break;
                    #endregion
            }
            indexes.Add(new FightIndex { Index = roundsCounter, Type = roundType.ToString() });
            roundsCounter++;
        }
        #endregion
        DetailedResults result = new DetailedResults
        {
            BlueId = blueSide.UniqueID,
            RedId = redSide.UniqueID,
            Indices = indexes.ToArray(),
            ClashesArray = clashes.ToArray(),
            DecisionsArray = decisions.ToArray(),
            Logs = new List<string>()
        };

        logList.ForEach(log => result.Logs.Add(log));
        return result;

    }



    //  /*
    public static Results SimulateFight(GameChicken blueSide, GameChicken redSide)
    {
        bool isFighting = true;
        int roundsCounter = 0;
        RoundType roundType = RoundType.Null;
        List<FightIndex> indexes = new List<FightIndex>();
        List<RoundClash> clashes = new List<RoundClash>();
        List<FightDecision> decisions = new List<FightDecision>();
        List<MoveReport> redMovesReport = new List<MoveReport>();
        List<MoveReport> blueMovesReport = new List<MoveReport>();
        List<bool> redCrits = new List<bool>();
        List<bool> blueCrits = new List<bool>();

        blueSide.ChickenAttributes.HealthPoints = blueSide.ChickenAttributes.TotalHealthPoints;
        redSide.ChickenAttributes.HealthPoints = redSide.ChickenAttributes.TotalHealthPoints;

        blueSide.ChickenAttributes.InitiativeBonus = 0;
        redSide.ChickenAttributes.InitiativeBonus = 0;

        while (isFighting)
        {
            roundType = DetermineRoundType(blueSide, redSide, roundType);

            switch (roundType)
            {
                #region Clash
                case RoundType.Clash:

                    // Build Round Data.
                    RoundClash clash = new RoundClash
                    {
                        Index = roundsCounter
                    };

                    // GOD SAVE THE MOVE CARDS
                    // Random a move card for both gamecocks.
                    MoveCard blueMoveCard = blueSide.RandomMoveCard();
                    MoveCard redMoveCard = redSide.RandomMoveCard();

                    // Roll for initiative for both gamecocks. D20 + Bonuses.
                    //int blueInitiative = UnityEngine.Random.Range(1, 21) + Mathf.RoundToInt(blueSide.ChickenAttributes.GetMappedInitiative()) + Mathf.RoundToInt(blueSide.ChickenAttributes.InitiativeBonus);
                    //int redInitiative = UnityEngine.Random.Range(1, 21) + Mathf.RoundToInt(redSide.ChickenAttributes.GetMappedInitiative()) + Mathf.RoundToInt(redSide.ChickenAttributes.InitiativeBonus);
                    int blueInitiativeRoll = UnityEngine.Random.Range(1, 21);
                    int redInitiativeRoll = UnityEngine.Random.Range(1, 21);

                    int bluePassiveInitiativeRoll = (int)UERandom.Range(1, blueSide.ChickenAttributes.GetMappedInitiative());
                    int redPassiveInitiativeRoll = (int)UERandom.Range(1, redSide.ChickenAttributes.GetMappedInitiative());

                    int blueBonusInitiativeRoll = (int)blueSide.ChickenAttributes.InitiativeBonus;
                    int redBonusInitiativeRoll = (int)redSide.ChickenAttributes.InitiativeBonus;

                    int blueInitiative = blueInitiativeRoll + bluePassiveInitiativeRoll + blueBonusInitiativeRoll;
                    int redInitiative = redInitiativeRoll + redPassiveInitiativeRoll + redBonusInitiativeRoll;

                    clash.BlueInitiative = (blueInitiative > redInitiative);
                    clash.RedInitiative = (blueInitiative < redInitiative);
                    clash.BlueInitiativeValue = blueInitiative;
                    clash.RedInitiativeValue = redInitiative;

                    // GOD SAVE THE MOVE CARDS
                    // Determine move for both gamecocks.
                    Move blueMove = !clash.BlueInitiative ? blueMoveCard.InferiorMove : blueMoveCard.SuperiorMove;
                    Move redMove = !clash.RedInitiative ? redMoveCard.InferiorMove : redMoveCard.SuperiorMove;

                    clash.BlueMove = blueMove.Name;
                    clash.RedMove = redMove.Name;

                    blueSide.ChickenAttributes.CuttingBonus = blueMove.CuttingBonus ? blueSide.ChickenAttributes.GetMappedCuttingChance() : 0;
                    redSide.ChickenAttributes.CuttingBonus = redMove.CuttingBonus ? redSide.ChickenAttributes.GetMappedCuttingChance() : 0;

                    blueSide.ChickenAttributes.DefenseBonus = (blueMove.DodgeBonusAgiInt ? (blueMove.Type.Equals(MoveType.Aerial) ? blueSide.ChickenAttributes.GetMappedDefenseAgi() : blueSide.ChickenAttributes.GetMappedDefenseInt()) : 0);
                    redSide.ChickenAttributes.DefenseBonus = (redMove.DodgeBonusAgiInt ? (redMove.Type.Equals(MoveType.Aerial) ? redSide.ChickenAttributes.GetMappedDefenseAgi() : redSide.ChickenAttributes.GetMappedDefenseInt()) : 0);

                    //CHECK FOR BONUS DAMAGE HERE 
                    int blueBonusDamage = (blueMove.Type.Equals(MoveType.Grounded) ? (int)blueMove.BonusGroundedDamage : (int)blueMove.BonusAerialDamage) + (blueMove.SuperiorDamage ? (int)blueSide.ChickenAttributes.DamageBonus : 0) + (int)blueMove.BonusDamage + 5; //5 is tarik
                    int redBonusDamage = (redMove.Type.Equals(MoveType.Grounded) ? (int)redMove.BonusGroundedDamage : (int)redMove.BonusAerialDamage) + (redMove.SuperiorDamage ? (int)redSide.ChickenAttributes.DamageBonus : 0) + (int)redMove.BonusDamage + 5;

                    int blueCritRoll = (blueMove.DealDamage) ? UERandom.Range(1, 101) : 0;
                    int redCritRoll = (redMove.DealDamage) ? UERandom.Range(1, 101) : 0;

                    bool blueCrit = (blueCritRoll > 0 && blueCritRoll < (blueSide.ChickenAttributes.GetMappedCuttingChance() + blueSide.ChickenAttributes.CuttingBonus)) ? true : false;
                    bool redCrit = redCritRoll < redSide.ChickenAttributes.GetMappedCuttingChance() + redSide.ChickenAttributes.CuttingBonus ? true : false;

                    clash.BlueCrit = blueCrit;
                    clash.RedCrit = redCrit;

                    if (blueCrit)
                        blueCrits.Add(blueCrit);
                    if (redCrit)
                        redCrits.Add(redCrit);

                    // Calculate damage for both gamecocks. 
                    int blueDamage = blueMove.DealDamage ? (int)blueSide.ChickenAttributes.GetMappedPower() + blueBonusDamage + (int)redMove.EnemyDamage : 0;
                    int redDamage = redMove.DealDamage ? (int)redSide.ChickenAttributes.GetMappedPower() + redBonusDamage + (int)blueMove.EnemyDamage : 0;

                    // Calculate accuracy for both gamecocks.
                    int blueAccuracy = (int)blueSide.ChickenAttributes.GetMappedAccuracy() + (int)blueMove.BonusAccuracy + (int)redMove.EnemyAccuracy - (blueMove.DecreaseHigherAccuracy ? 20 : 0) - (blueMove.DecreaseLowerAccuracy ? 10 : 0);
                    int blueDodge = (blueMove.DodgeBonus ? (int)blueSide.ChickenAttributes.GetMappedDefenseMax() : (int)blueSide.ChickenAttributes.GetMappedDefenseLow()) + (int)blueSide.ChickenAttributes.DefenseBonus + (int)blueMove.BonusDodge + (int)redMove.EnemyDodge;

                    // Calculate dodge for both gamecocks.
                    int redAccuracy = (int)redSide.ChickenAttributes.GetMappedAccuracy() + (int)redMove.BonusAccuracy + (int)blueMove.EnemyAccuracy - (blueMove.DecreaseHigherAccuracy ? 20 : 0) - (redMove.DecreaseLowerAccuracy ? 10 : 0);
                    int redDodge = (redMove.DodgeBonus ? (int)redSide.ChickenAttributes.GetMappedDefenseMax() : (int)redSide.ChickenAttributes.GetMappedDefenseLow()) + (int)redSide.ChickenAttributes.DefenseBonus + (int)redMove.BonusDodge + (int)blueMove.EnemyDodge;

                    // Roll for move for both gamecocks.
                    int blueRoll = UERandom.Range(1, 101);
                    int redRoll = UERandom.Range(1, 101);

                    // Calculate move success odds.
                    int blueOdds = (blueAccuracy - redDodge);
                    int redOdds = (redAccuracy - blueDodge);

                    // Calculate move success.
                    bool blueSuccess = (blueRoll <= blueOdds);
                    bool redSuccess = (redRoll <= redOdds);

                    float redFinalDamage = redCrit ? redDamage * 2 : redDamage;
                    float blueFinalDamage = blueCrit ? blueDamage * 2 : blueDamage;

                    // Resolve damage for both gamecocks.
                    if (blueSuccess && (blueDamage > 0)) redSide.ChickenAttributes.HealthPoints -= blueFinalDamage;
                    if (redSuccess && (redDamage > 0)) blueSide.ChickenAttributes.HealthPoints -= redFinalDamage;

                    // Resolve bonus initiative for next round for both gamecocks.
                    // blue.InitiativeBonus = blueSuccess ? blueMove.BaseInitiative : 0;
                    // red.InitiativeBonus = redSuccess ? redMove.BaseInitiative : 0;

                    blueSide.ChickenAttributes.InitiativeBonus = blueMove.InitiativeBonus ? blueSide.ChickenAttributes.GetMappedInitiative() / 2 : 0;
                    redSide.ChickenAttributes.InitiativeBonus = redMove.InitiativeBonus ? redSide.ChickenAttributes.GetMappedInitiative() / 2 : 0;

                    //blueSide.ChickenAttributes.CuttingBonus = blueMove.CuttingBonus ? blueSide.ChickenAttributes.GetMappedCuttingChance() : 0;
                    //redSide.ChickenAttributes.CuttingBonus = redMove.CuttingBonus ? redSide.ChickenAttributes.GetMappedCuttingChance() : 0;

                    //blueSide.ChickenAttributes.DefenseBonus = (blueMove.DodgeBonus ? blueSide.ChickenAttributes.GetMappedDefenseMax() : 0) + (blueMove.DodgeBonusAgiInt ? (blueMove.Type.Equals(MoveType.Aerial) ? blueSide.ChickenAttributes.GetMappedDefenseAgi() : blueSide.ChickenAttributes.GetMappedDefenseInt()) : 0);
                    //redSide.ChickenAttributes.DefenseBonus = (redMove.DodgeBonus ? redSide.ChickenAttributes.GetMappedDefenseMax() : 0) + (redMove.DodgeBonusAgiInt ? (redMove.Type.Equals(MoveType.Aerial) ? redSide.ChickenAttributes.GetMappedDefenseAgi() : redSide.ChickenAttributes.GetMappedDefenseInt()) : 0);

                    clash.BlueSuccess = blueSuccess;
                    clash.BlueHp = Mathf.RoundToInt(blueSide.ChickenAttributes.HealthPoints);
                    clash.BlueDamage = (int)blueFinalDamage;

                    clash.RedSuccess = redSuccess;
                    clash.RedHp = Mathf.RoundToInt(redSide.ChickenAttributes.HealthPoints);
                    clash.RedDamage = (int)redFinalDamage;

                    // Register Round Data.
                    clashes.Add(clash);

                    break;
                #endregion

                #region Decision
                case RoundType.Decision:

                    string decisionResult = string.Empty;

                    if (Mathf.RoundToInt(blueSide.ChickenAttributes.HealthPoints) <= 0 && Mathf.RoundToInt(redSide.ChickenAttributes.HealthPoints) <= 0)
                        decisionResult = "Draw";
                    else if (Mathf.RoundToInt(blueSide.ChickenAttributes.HealthPoints) <= 0)
                        decisionResult = redSide.UniqueID;
                    else if (Mathf.RoundToInt(redSide.ChickenAttributes.HealthPoints) <= 0)
                        decisionResult = blueSide.UniqueID;
                    else
                        decisionResult = "Proceed";

                    if (decisionResult != "Proceed")
                        isFighting = false;

                    // Build Round Data.
                    FightDecision decision = new FightDecision
                    {
                        CritSumBlue = (float)blueCrits.Sum(b => Convert.ToInt32(b)),
                        CritSumRed = (float)redCrits.Sum(b => Convert.ToInt32(b)),
                        Index = roundsCounter,
                        Decision = decisionResult
                    };

                    // Register Round Data.
                    decisions.Add(decision);

                    break;
                    #endregion
            }

            // Register Rounds.
            indexes.Add(new FightIndex { Index = roundsCounter, Type = roundType.ToString() });

            // Increment rounds counter.
            roundsCounter++;
        }

        Results result = new Results
        {
            BlueId = blueSide.UniqueID,
            RedId = redSide.UniqueID,
            Indices = indexes.ToArray(),
            ClashesArray = clashes.ToArray(),
            DecisionsArray = decisions.ToArray(),
        };

        return result;
    }
    //  */
}
