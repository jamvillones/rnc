﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MoveCard
{
    public string Name;
    public string UniqueID;
    public float Weight;
    public Move InferiorMove;
    public Move SuperiorMove;
    public MoveCard()
    {
        Name = "";
        UniqueID = "";
        Weight = 0;
        Move InferiorMove = new Move();
        Move SuperiorMove = new Move();

    }
}
[System.Serializable]
public class Move
{
    public Move
        (
        string name, MoveType type,
        float dodge, float bonusInitiative, float bonusAerialDamage, float bonusGroundedDamage, float enemyDamage, float enemyAccuracy, float enemyDodge,
        bool initiativeBonus, bool dodgeBonus, bool dodgeBonusAgiInt, bool superiorDamage, bool cuttingBonus, bool dealDamage
        )
    {
        Name = name;
        Type = type;
        BonusDodge = dodge;
        BonusInitiative = bonusInitiative;
        BonusAerialDamage = bonusAerialDamage;
        BonusGroundedDamage = bonusGroundedDamage;
        EnemyDamage = enemyDamage;
        EnemyAccuracy = enemyAccuracy;
        EnemyDodge = enemyDodge;
        InitiativeBonus = initiativeBonus;
        DodgeBonus = dodgeBonus;
        DodgeBonusAgiInt = dodgeBonusAgiInt;
        SuperiorDamage = superiorDamage;
        CuttingBonus = cuttingBonus;
        DealDamage = dealDamage;
    }
    public Move()
    {

    }
    public static Move GetValues(Move m)
    {
        Move temp = new Move();
        temp.UniqueID = m.UniqueID;
        temp.Name = m.Name;
        temp.Type = m.Type;
        temp.BonusDamage = m.BonusDamage;
        temp.BonusAccuracy = m.BonusAccuracy;
        temp.BonusDodge = m.BonusDodge;
        temp.BonusInitiative = m.BonusInitiative;
        temp.BonusAerialDamage = m.BonusAerialDamage;
        temp.BonusGroundedDamage = m.BonusGroundedDamage;
        temp.EnemyDamage = m.EnemyDamage;
        temp.EnemyAccuracy = m.EnemyAccuracy;
        temp.EnemyDodge = m.EnemyDodge;
        temp.InitiativeBonus = m.InitiativeBonus;
        temp.DodgeBonus = m.DodgeBonus;
        temp.DodgeBonusAgiInt = m.DodgeBonusAgiInt;
        temp.SuperiorDamage = m.SuperiorDamage;
        temp.CuttingBonus = m.CuttingBonus;
        temp.DealDamage=m.DealDamage;
        temp.SureCrit = m.SureCrit;
        return temp;

    }
    public string UniqueID;
    public string Name;
    public MoveType Type;
    public float BonusDamage;
    public float BonusAccuracy;
    public float BonusDodge;
    public float BonusInitiative;
    public float BonusAerialDamage;
    public float BonusGroundedDamage;
    public float EnemyDamage;
    public float EnemyAccuracy;
    public float EnemyDodge;

    public bool InitiativeBonus;
    public bool DodgeBonus;
    public bool DodgeBonusAgiInt;
    public bool SuperiorDamage;
    public bool CuttingBonus;
    public bool DealDamage;
    public bool SureCrit;
    public bool DecreaseHigherAccuracy;
    public bool DecreaseLowerAccuracy;
}
[System.Serializable]
public enum MoveType
{
    Grounded = 0,
    Aerial = 1,
    length = 2,
}


[System.Serializable]
public enum InitiativeType
{
    Inferior = 0,
    Superior = 1,
    length = 2,
}
