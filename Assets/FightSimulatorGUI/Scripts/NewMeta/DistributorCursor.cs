﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class DistributorCursor : MonoBehaviour
{
    public float DiamondSize;
    public float MaxValue;
    public float MaxAnchorValue { get { return Mathf.Sqrt(Mathf.Pow(DiamondSize, 2) + Mathf.Pow(DiamondSize, 2)); } }
    public float MaxX, MaxY;
    public float MinX, MinY;
    public bool CursorReposition;
    float movesSum;
    public GameObject CursorSprite;
    public Slider AerialGrounded;
    public List<DiamondValue> Values = new List<DiamondValue>();

    private void OnEnable()
    {
        //this.AddEventListenerGlobal<MaxValueUpdate>(UpdateMaxValue);
    }

    private void OnDisable()
    {
        //this.RemoveEventListenerGlobal<MaxValueUpdate>(UpdateMaxValue);
    }

    void UpdateMaxValue(MaxValueUpdate value)
    { 

    }

    // Use this for initialization
    void Start()
    {
        CursorReposition = false;
        AerialGrounded.value = AerialGrounded.maxValue / 2;
        UpdateValues();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0) && CursorReposition)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray))
            {
                float tempX = ray.origin.x;
                float tempY = ray.origin.y;
                tempX = Mathf.Clamp(tempX, MinX, MaxX);
                tempY = Mathf.Clamp(tempY, MinY, MaxY);
                CursorSprite.transform.position = new Vector3(tempX, tempY, -5);
                UpdateValues();
            }
        }
    }

    public void MoveCursor()
    {
        CursorReposition = true;
    }

    public void FreezeCursor()
    {
        CursorReposition = false;
    }

    public void RandomPointCircle()
    {
        Vector3 randomPoint = Random.insideUnitSphere * DiamondSize;
        randomPoint.z = -5;
        CursorSprite.transform.position = randomPoint;
        UpdateValues();
    }

    public void RandomPointSquare()
    {
        CursorSprite.transform.position = new Vector3(Random.Range(-DiamondSize, DiamondSize), Random.Range(-DiamondSize, DiamondSize), -5);
        UpdateValues();
    }

    public void ResetValues()
    {
        Vector3 defaultPosition = Vector3.zero;
        defaultPosition.z = -5;
        CursorSprite.transform.position = defaultPosition;
        UpdateValues();
    }

    public void UpdateValues()
    {
        foreach (DiamondValue diamondValue in Values)
        {
            float finalMaxValue = MaxValue;
            if (diamondValue.Aerial)
            {
                finalMaxValue = ValueMapper.Map(AerialGrounded.value, AerialGrounded.minValue, AerialGrounded.maxValue, 0, MaxValue);              
                diamondValue.UpdateValues(MaxAnchorValue, CursorSprite, finalMaxValue);               
            }
            else
            {
                finalMaxValue = MaxValue - ValueMapper.Map(AerialGrounded.value, AerialGrounded.minValue, AerialGrounded.maxValue, 0, MaxValue);             
                diamondValue.UpdateValues(MaxAnchorValue, CursorSprite, finalMaxValue);
            }
        }
        movesSum = Values.Select(v => v.Value).Sum();
        //Values.ForEach(diamondValue => diamondValue.ValueText.text = diamondValue.Value.ToString() + " (" + ValueMapper.Map(diamondValue.Value, 0, movesSum, 0, 100));
        Values.ForEach(diamondValue => diamondValue.ValueText.text = diamondValue.Value.ToString() + " (" + System.Decimal.Round(System.Convert.ToDecimal(ValueMapper.Map(diamondValue.Value, 0, movesSum, 0, 100)), 2) + "%)");
    }
}
