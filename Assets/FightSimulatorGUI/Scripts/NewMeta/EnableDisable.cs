﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableDisable : MonoBehaviour
{
    public void Activate()
    {
        gameObject.SetActive(!gameObject.activeInHierarchy);
    }
}
