﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using LegendsOfThePit;

public class MatchManager : MonoBehaviour
{
    public GameObject BarGraphPrefab;
    public GameObject ConfirmScreen;
    public GameObject ResultScreen;
    //public CockSelector BlueSide;
    //public CockSelector RedSide;
    public UI_CockToUI RedSide;
    public UI_CockToUI BlueSide;
    public Dropdown ResultList;
    public Text TextField;
    public Transform DetailedTextField;
    public GameObject DetailedTextPrefab;
    public Toggle VerboseToggle;
    [HideInInspector]
    public GameChicken blue;
    [HideInInspector]
    public GameChicken red;

    MenuModeSelector blueSelector;
    MenuModeSelector redSelector;
    List<string> results = new List<string>();
    List<List<Results>> fights = new List<List<Results>>();
    List<GameObject> graphs = new List<GameObject>();

    void Start()
    {
        ResultList.ClearOptions();
    }


    void makeGraphs(List<Results> fightResultList)
    {
        if (graphs.Count > 0)
        {
            graphs.ForEach(o => Destroy(o));
            graphs.Clear();
        }

        GameObject numberOfRoundsObj = Instantiate(BarGraphPrefab, ResultScreen.transform);
        BarGraph numberOfRounds = numberOfRoundsObj.GetComponent<BarGraph>();
        numberOfRounds.MaxValue = fightResultList.Count;
        numberOfRounds.DescriptionLabel.text = "Number of Simulations";
        numberOfRounds.AddBar(fightResultList.SelectMany(x => x.DecisionsArray).Where(y => y.Decision == red.UniqueID).ToArray().Length.ToString(), "Red Wins", Color.red);
        numberOfRounds.AddBar(fightResultList.SelectMany(x => x.DecisionsArray).Where(y => y.Decision == blue.UniqueID).ToArray().Length.ToString(), "Blue Wins", Color.blue);
        numberOfRounds.AddBar(fightResultList.SelectMany(x => x.DecisionsArray).Where(y => y.Decision == "Draw").ToArray().Length.ToString(), "Draws", Color.yellow);
        graphs.Add(numberOfRoundsObj);

        GameObject clashesObj = Instantiate(BarGraphPrefab, ResultScreen.transform);
        BarGraph clashesData = clashesObj.GetComponent<BarGraph>();
        clashesData.MaxValue = fightResultList.OrderByDescending(x => x.ClashesArray.Length).First().ClashesArray.Length;
        clashesData.DescriptionLabel.text = "Number of Rounds";
        clashesData.AddBar(fightResultList.OrderByDescending(x => x.ClashesArray.Length).First().ClashesArray.Length.ToString(), "Highest No. of Rounds", Color.green);
        clashesData.AddBar(fightResultList.OrderByDescending(x => x.ClashesArray.Length).Last().ClashesArray.Length.ToString(), "Lowest No. of Rounds", Color.red);
        clashesData.AddBar(fightResultList.Average(x => x.ClashesArray.Length).ToString(), "Average Number of Rounds", Color.yellow);
        graphs.Add(clashesObj);

        GameObject initiativeObj = Instantiate(BarGraphPrefab, ResultScreen.transform);
        BarGraph initiativeData = initiativeObj.GetComponent<BarGraph>();
        initiativeData.MaxValue = fightResultList.SelectMany(x => x.ClashesArray).Where(y => y.BlueInitiative).ToArray().Length > fightResultList.SelectMany(x => x.ClashesArray).Where(y => y.RedInitiative).ToArray().Length ? fightResultList.SelectMany(x => x.ClashesArray).Where(y => y.BlueInitiative).ToArray().Length : fightResultList.SelectMany(x => x.ClashesArray).Where(y => y.RedInitiative).ToArray().Length;
        initiativeData.DescriptionLabel.text = "Number of times where cock gained initiative";
        initiativeData.AddBar(fightResultList.SelectMany(x => x.ClashesArray).Where(y => y.RedInitiative).ToArray().Length.ToString(), "Red", Color.red);
        initiativeData.AddBar(fightResultList.SelectMany(x => x.ClashesArray).Where(y => y.BlueInitiative).ToArray().Length.ToString(), "Blue", Color.blue);
        graphs.Add(initiativeObj);

        GameObject initiativeValueObj = Instantiate(BarGraphPrefab, ResultScreen.transform);
        BarGraph initiativeValueData = initiativeValueObj.GetComponent<BarGraph>();
        initiativeValueData.MaxValue = fightResultList.SelectMany(x => x.ClashesArray).OrderByDescending(y => y.BlueInitiativeValue).First().BlueInitiativeValue > fightResultList.SelectMany(x => x.ClashesArray).OrderByDescending(y => y.RedInitiativeValue).First().RedInitiativeValue ? fightResultList.SelectMany(x => x.ClashesArray).OrderByDescending(y => y.BlueInitiativeValue).First().BlueInitiativeValue : fightResultList.SelectMany(x => x.ClashesArray).OrderByDescending(y => y.RedInitiativeValue).First().RedInitiativeValue;
        initiativeValueData.DescriptionLabel.text = "Initiative value of cocks";
        initiativeValueData.AddBar(fightResultList.SelectMany(x => x.ClashesArray).OrderByDescending(y => y.RedInitiativeValue).First().RedInitiativeValue.ToString(), "Red Max", Color.red);
        initiativeValueData.AddBar(fightResultList.SelectMany(x => x.ClashesArray).OrderByDescending(y => y.RedInitiativeValue).Last().RedInitiativeValue.ToString(), "Red Min", Color.red);
        initiativeValueData.AddBar(fightResultList.SelectMany(x => x.ClashesArray).Average(y => y.RedInitiativeValue).ToString(), "Red Average", Color.red);
        initiativeValueData.AddBar(fightResultList.SelectMany(x => x.ClashesArray).OrderByDescending(y => y.BlueInitiativeValue).First().BlueInitiativeValue.ToString(), "Blue Max", Color.blue);
        initiativeValueData.AddBar(fightResultList.SelectMany(x => x.ClashesArray).OrderByDescending(y => y.BlueInitiativeValue).Last().BlueInitiativeValue.ToString(), "Blue Min", Color.blue);
        initiativeValueData.AddBar(fightResultList.SelectMany(x => x.ClashesArray).Average(y => y.BlueInitiativeValue).ToString(), "Blue Average", Color.blue);
        graphs.Add(initiativeValueObj);

        GameObject averageHitRateObj = Instantiate(BarGraphPrefab, ResultScreen.transform);
        BarGraph averageHitRateData = averageHitRateObj.GetComponent<BarGraph>();
        averageHitRateData.MaxValue = Decimal.Divide(fightResultList.SelectMany(x => x.ClashesArray).Where(y => y.BlueSuccess).ToArray().Length, fightResultList.SelectMany(x => x.ClashesArray).ToArray().Length) > Decimal.Divide(fightResultList.SelectMany(x => x.ClashesArray).Where(y => y.RedSuccess).ToArray().Length, fightResultList.SelectMany(x => x.ClashesArray).ToArray().Length) ? (float)Decimal.Divide(fightResultList.SelectMany(x => x.ClashesArray).Where(y => y.BlueSuccess).ToArray().Length, fightResultList.SelectMany(x => x.ClashesArray).ToArray().Length) * 100 : (float)Decimal.Divide(fightResultList.SelectMany(x => x.ClashesArray).Where(y => y.RedSuccess).ToArray().Length, fightResultList.SelectMany(x => x.ClashesArray).ToArray().Length) * 100;
        averageHitRateData.DescriptionLabel.text = "Average hit rate of cocks in percent";
        averageHitRateData.AddBar(((float)Decimal.Divide(fightResultList.SelectMany(x => x.ClashesArray).Where(y => y.RedSuccess).ToArray().Length, fightResultList.SelectMany(x => x.ClashesArray).ToArray().Length) * 100).ToString(), "Red", Color.red);
        averageHitRateData.AddBar(((float)Decimal.Divide(fightResultList.SelectMany(x => x.ClashesArray).Where(y => y.BlueSuccess).ToArray().Length, fightResultList.SelectMany(x => x.ClashesArray).ToArray().Length) * 100).ToString(), "Blue", Color.blue);
        graphs.Add(averageHitRateObj);

        GameObject critsObj = Instantiate(BarGraphPrefab, ResultScreen.transform);
        BarGraph critsData = critsObj.GetComponent<BarGraph>();
        critsData.MaxValue = fightResultList.SelectMany(x => x.ClashesArray).Where(y => y.RedCrit).ToArray().Length > fightResultList.SelectMany(x => x.ClashesArray).Where(y => y.BlueCrit).ToArray().Length ? fightResultList.SelectMany(x => x.ClashesArray).Where(y => y.RedCrit).ToArray().Length : fightResultList.SelectMany(x => x.ClashesArray).Where(y => y.BlueCrit).ToArray().Length;
        critsData.DescriptionLabel.text = "Times cocks proc'd a crit";
        critsData.AddBar(fightResultList.SelectMany(x => x.ClashesArray).Where(y => y.RedCrit).ToArray().Length.ToString(), "Red", Color.red);
        critsData.AddBar(fightResultList.SelectMany(x => x.ClashesArray).Where(y => y.BlueCrit).ToArray().Length.ToString(), "Blue", Color.blue);
        graphs.Add(critsObj);

        GameObject damageDealtObj = Instantiate(BarGraphPrefab, ResultScreen.transform);
        BarGraph damageDealtData = damageDealtObj.GetComponent<BarGraph>();
        damageDealtData.MaxValue = fightResultList.SelectMany(x => x.ClashesArray).Where(y => y.BlueSuccess).OrderByDescending(z => z.BlueDamage).First().BlueDamage > fightResultList.SelectMany(x => x.ClashesArray).Where(y => y.RedSuccess).OrderByDescending(z => z.RedDamage).First().RedDamage ? fightResultList.SelectMany(x => x.ClashesArray).Where(y => y.BlueSuccess).OrderByDescending(z => z.BlueDamage).First().BlueDamage : fightResultList.SelectMany(x => x.ClashesArray).Where(y => y.RedSuccess).OrderByDescending(z => z.RedDamage).First().RedDamage;
        damageDealtData.DescriptionLabel.text = "Highest Damage Dealt (+ crits)";
        damageDealtData.AddBar(fightResultList.SelectMany(x => x.ClashesArray).Where(y => y.RedSuccess).OrderByDescending(z => z.RedDamage).First().RedDamage.ToString(), string.Format("Red Max Damage: {0}", fightResultList.SelectMany(x => x.ClashesArray).Where(y => y.RedSuccess).OrderByDescending(z => z.RedDamage).First().RedMove.ToString()), Color.red);
        damageDealtData.AddBar(fightResultList.SelectMany(x => x.ClashesArray).Where(y => y.BlueSuccess).OrderByDescending(z => z.BlueDamage).First().BlueDamage.ToString(), string.Format("Blue Max Damage: {0}", fightResultList.SelectMany(x => x.ClashesArray).Where(y => y.BlueSuccess).OrderByDescending(z => z.BlueDamage).First().BlueMove.ToString()), Color.blue);
        damageDealtData.AddBar(fightResultList.SelectMany(x => x.ClashesArray).Where(y => y.RedSuccess).Average(z => z.RedDamage).ToString(), "Red Average Damage", Color.red);
        damageDealtData.AddBar(fightResultList.SelectMany(x => x.ClashesArray).Where(y => y.BlueSuccess).Average(z => z.BlueDamage).ToString(), "Blue Average Damage", Color.blue);
        graphs.Add(damageDealtObj);

        GameObject totalDamageDealtObj = Instantiate(BarGraphPrefab, ResultScreen.transform);
        BarGraph totalDamageDealtData = totalDamageDealtObj.GetComponent<BarGraph>();
        totalDamageDealtData.MaxValue = fightResultList.SelectMany(x => x.ClashesArray).Where(y => y.BlueSuccess).Sum(z => z.BlueDamage) > fightResultList.SelectMany(x => x.ClashesArray).Where(y => y.RedSuccess).Sum(z => z.RedDamage) ? fightResultList.SelectMany(x => x.ClashesArray).Where(y => y.BlueSuccess).Sum(z => z.BlueDamage) : fightResultList.SelectMany(x => x.ClashesArray).Where(y => y.RedSuccess).Sum(z => z.RedDamage);
        totalDamageDealtData.DescriptionLabel.text = "Total Damage of Cocks";
        totalDamageDealtData.AddBar(fightResultList.SelectMany(x => x.ClashesArray).Where(y => y.RedSuccess).Sum(z => z.RedDamage).ToString(), "Red", Color.red);
        totalDamageDealtData.AddBar(fightResultList.SelectMany(x => x.ClashesArray).Where(y => y.BlueSuccess).Sum(z => z.BlueDamage).ToString(), "Blue", Color.blue);
        graphs.Add(totalDamageDealtObj);

        GameObject moveChanceRedObj = Instantiate(BarGraphPrefab, ResultScreen.transform);
        BarGraph moveChanceRedData = moveChanceRedObj.GetComponent<BarGraph>();
        moveChanceRedData.MaxValue = 100;
        moveChanceRedData.DescriptionLabel.text = "Chance To Use Move of Red in Percentage";
        MoveCard[] redMoveCard = red.MoveCards.Where(x => x.Weight > 0).Select(y => y).ToArray();
        string[] redMoves = red.MoveCards.Select(x => x.SuperiorMove.Name).Union(red.MoveCards.Select(x => x.InferiorMove.Name)).ToArray();
        foreach (string move in redMoves)
        {
            if (Math.Round(Decimal.Divide(fightResultList.SelectMany(x => x.ClashesArray).Where(y => y.RedMove == move).ToArray().Length, fightResultList.SelectMany(x => x.ClashesArray).ToArray().Length) * 100, 2) > 0)
                moveChanceRedData.AddBar(Math.Round(Decimal.Divide(fightResultList.SelectMany(x => x.ClashesArray).Where(y => y.RedMove == move).ToArray().Length, fightResultList.SelectMany(x => x.ClashesArray).ToArray().Length) * 100, 2).ToString(), move, Color.red);
        }
        graphs.Add(moveChanceRedObj);

        GameObject moveChanceBlueObj = Instantiate(BarGraphPrefab, ResultScreen.transform);
        BarGraph moveChanceBlueData = moveChanceBlueObj.GetComponent<BarGraph>();
        moveChanceBlueData.MaxValue = 100;
        moveChanceBlueData.DescriptionLabel.text = "Chance To Use Move of Blue in Percentage";
        string[] blueMoves = blue.MoveCards.Select(x => x.SuperiorMove.Name).Union(blue.MoveCards.Select(x => x.InferiorMove.Name)).ToArray();
        foreach (string move in blueMoves)
        {
            if (Math.Round(Decimal.Divide(fightResultList.SelectMany(x => x.ClashesArray).Where(y => y.BlueMove == move).ToArray().Length, fightResultList.SelectMany(x => x.ClashesArray).ToArray().Length) * 100, 2) > 0)
                moveChanceBlueData.AddBar(Math.Round(Decimal.Divide(fightResultList.SelectMany(x => x.ClashesArray).Where(y => y.BlueMove == move).ToArray().Length, fightResultList.SelectMany(x => x.ClashesArray).ToArray().Length) * 100, 2).ToString(), move, Color.blue);
        }
        graphs.Add(moveChanceBlueObj);

        GameObject moveSuccessRateRedObj = Instantiate(BarGraphPrefab, ResultScreen.transform);
        BarGraph moveSuccessRateRedData = moveSuccessRateRedObj.GetComponent<BarGraph>();
        moveSuccessRateRedData.MaxValue = 100;
        moveSuccessRateRedData.DescriptionLabel.text = "Success Rate of Moves by Red in Percentage";
        foreach (string move in redMoves)
        {
            if (fightResultList.SelectMany(x => x.ClashesArray).Where(y => y.RedMove == move).ToArray().Length > 0)
                moveSuccessRateRedData.AddBar(Math.Round(Decimal.Divide(fightResultList.SelectMany(x => x.ClashesArray).Where(y => y.RedMove == move && y.RedSuccess).ToArray().Length, fightResultList.SelectMany(x => x.ClashesArray).Where(y => y.RedMove == move).ToArray().Length) * 100, 2).ToString(), move, Color.red);
        }
        graphs.Add(moveSuccessRateRedObj);

        GameObject moveSuccessRateBlueObj = Instantiate(BarGraphPrefab, ResultScreen.transform);
        BarGraph moveSuccessRateBlueData = moveSuccessRateBlueObj.GetComponent<BarGraph>();
        moveSuccessRateBlueData.MaxValue = 100;
        moveSuccessRateBlueData.DescriptionLabel.text = "Success Rate of Moves by Blue in Percentage";
        foreach (string move in blueMoves)
        {
            if (fightResultList.SelectMany(x => x.ClashesArray).Where(y => y.BlueMove == move).ToArray().Length > 0)
                moveSuccessRateBlueData.AddBar(Math.Round(Decimal.Divide(fightResultList.SelectMany(x => x.ClashesArray).Where(y => y.BlueMove == move && y.BlueSuccess).ToArray().Length, fightResultList.SelectMany(x => x.ClashesArray).Where(y => y.BlueMove == move).ToArray().Length) * 100, 2).ToString(), move, Color.blue);
        }
        graphs.Add(moveSuccessRateBlueObj);

        graphs.ForEach(g => g.transform.parent = ResultScreen.GetComponent<ResultScreen>().GraphArea.transform);
    }

    void assignFromList()
    {
        blue = BlueSide.SelectedChicken(true);
        red = RedSide.SelectedChicken(true);
    }

    public void DetailedFight()
    {
        assignFromList();
        foreach (Transform child in DetailedTextField)
            Destroy(child.gameObject);
        foreach (string log in Simulator.SimulateDetailedFight(blue, red, VerboseToggle.isOn).Logs)
        {
            GameObject temp = Instantiate(DetailedTextPrefab, DetailedTextField);
            temp.GetComponent<Text>().text = log;
        }

    }

    public void MultipleFights(GameObject textField)
    {
        assignFromList();

        List<Results> list = new List<Results>();

        for (int i = 0; i < Int32.Parse(textField.GetComponent<InputField>().text); i++)
            list.Add(Simulator.SimulateFight(blue, red));

        List<string> statistics = new List<string> { };

        statistics.Add("\n");
        statistics.Add(string.Format("Match between <color=#aa3333ff>{0}({1})</color> and <color=#3333aaff>{2}({3})</color> simulated <color=#ffff00ff>{4}</color> times.", red.BreedName, red.Name, blue.BreedName, blue.Name, textField.GetComponent<InputField>().text));
        statistics.Add(string.Format("Average number of Rounds: <color=#ffff00ff>{0}</color>", list.Average(x => x.ClashesArray.Length)));
        statistics.Add(string.Format("<color=#3333aaff>Number of times {0} Won:</color> <color=#00ffffff>{1}</color>", blue.Name, list.SelectMany(x => x.DecisionsArray).Where(y => y.Decision == blue.UniqueID).ToArray().Length));
        statistics.Add(string.Format("<color=#aa3333ff>Number of times {0} Won:</color> <color=#00ffffff>{1}</color>", red.Name, list.SelectMany(x => x.DecisionsArray).Where(y => y.Decision == red.UniqueID).ToArray().Length));
        statistics.Add(string.Format("Number of times fight ended with a Draw: <color=#00ffffff>{0}</color>", list.SelectMany(x => x.DecisionsArray).Where(y => y.Decision == "Draw").ToArray().Length));
        statistics.Add(string.Format("<color=#3333aaff>Number of times {0} gained Initiative:</color> <color=#00ffffff>{1}</color>", blue.Name, list.SelectMany(x => x.ClashesArray).Where(y => y.BlueInitiative).ToArray().Length));
        statistics.Add(string.Format("<color=#aa3333ff>Number of times {0} gained Initiative:</color> <color=#00ffffff>{1}</color>", red.Name, list.SelectMany(x => x.ClashesArray).Where(y => y.RedInitiative).ToArray().Length));
        statistics.Add(string.Format("<color=#3333aaff>Average Initiative Value of {0}: </color><color=#00ffffff>{1}</color>", blue.Name, Math.Round(list.SelectMany(x => x.ClashesArray).Average(y => y.BlueInitiativeValue), 3)));
        statistics.Add(string.Format("<color=#aa3333ff>Average Initiative Value of {0}:</color> <color=#00ffffff>{1}</color>", red.Name, Math.Round(list.SelectMany(x => x.ClashesArray).Average(y => y.RedInitiativeValue), 3)));
        statistics.Add(string.Format("<color=#3333aaff>Average Hit Rate of {0}:</color> <color=#00ffffff>{1}</color>", blue.Name, Math.Round((float)list.SelectMany(x => x.ClashesArray).Where(y => y.BlueSuccess).ToArray().Length / list.SelectMany(x => x.ClashesArray).ToArray().Length, 3)));
        statistics.Add(string.Format("<color=#aa3333ff>Average Hit Rate of {0}:</color> <color=#00ffffff>{1}</color>", red.Name, Math.Round((float)list.SelectMany(x => x.ClashesArray).Where(y => y.RedSuccess).ToArray().Length / list.SelectMany(x => x.ClashesArray).ToArray().Length, 3)));
        statistics.Add(string.Format("<color=#3333aaff>Average Damage Dealt by {0}:</color> <color=#00ffffff>{1}</color>", blue.Name, Math.Round(list.SelectMany(x => x.ClashesArray).Where(y => y.BlueSuccess).ToList().Count > 0 ? list.SelectMany(x => x.ClashesArray).Where(y => y.BlueSuccess).Average(z => z.BlueDamage) : 0, 3)));
        statistics.Add(string.Format("<color=#aa3333ff>Average Damage Dealt by {0}:</color> <color=#00ffffff>{1}</color>", red.Name, Math.Round(list.SelectMany(x => x.ClashesArray).Where(y => y.RedSuccess).ToList().Count > 0 ? list.SelectMany(x => x.ClashesArray).Where(y => y.RedSuccess).Average(z => z.RedDamage) : 0, 3)));
        statistics.Add(string.Format("<color=#3333aaff>Average number of times {0} proc'd a critical:</color> <color=#00ffffff>{1}</color>", blue.Name, list.SelectMany(x => x.DecisionsArray).Average(y => y.CritSumBlue)));
        statistics.Add(string.Format("<color=#aa3333ff>Average number of times {0} proc'd a critical:</color> <color=#00ffffff>{1}</color>", red.Name, list.SelectMany(x => x.DecisionsArray).Average(y => y.CritSumRed)));
        statistics.Add(string.Format("<color=#3333aaff>Total number of times {0} proc'd a critical:</color> <color=#00ffffff>{1}</color>", blue.Name, list.SelectMany(x => x.ClashesArray).Where(y => y.BlueCrit).ToArray().Length));
        statistics.Add(string.Format("<color=#aa3333ff>Total number of times {0} proc'd a critical:</color> <color=#00ffffff>{1}</color>", red.Name, list.SelectMany(x => x.ClashesArray).Where(y => y.RedCrit).ToArray().Length));
        statistics.Add(string.Format("<color=#3333aaff>Total Damage Dealt by {0}:</color> <color=#00ffffff>{1}</color>", blue.Name, list.SelectMany(x => x.ClashesArray).Where(y => y.BlueSuccess).Sum(z => z.BlueDamage)));
        statistics.Add(string.Format("<color=#aa3333ff>Total Damage Dealt by {0}:</color> <color=#00ffffff>{1}</color>", red.Name, list.SelectMany(x => x.ClashesArray).Where(y => y.RedSuccess).Sum(z => z.RedDamage)));

        RoundClash highestBlueDamage = list.SelectMany(x => x.ClashesArray).Where(y => y.BlueSuccess).ToList().Count > 0 ? list.SelectMany(x => x.ClashesArray).Where(y => y.BlueSuccess).OrderByDescending(z => z.BlueDamage).First() : null;
        statistics.Add(string.Format("<color=#3333aaff>Highest Damaging Move of {0} (+ crit):</color> <color=#00ffffff>{1}</color> Damage '<color=#ffffffff>{2}</color>'", blue.Name, highestBlueDamage != null ? highestBlueDamage.BlueDamage : 0, highestBlueDamage != null ? highestBlueDamage.BlueMove : null));

        RoundClash highestRedDamage = list.SelectMany(x => x.ClashesArray).Where(y => y.RedSuccess).ToList().Count > 0 ? list.SelectMany(x => x.ClashesArray).Where(y => y.RedSuccess).OrderByDescending(z => z.RedDamage).First() : null;
        statistics.Add(string.Format("<color=#aa3333ff>Highest Damaging Move of {0} (+ crit):</color> <color=#00ffffff>{1}</color> Damage '<color=#ffffffff>{2}</color>'", red.Name, highestRedDamage != null ? highestRedDamage.RedDamage : 0, highestRedDamage != null ? highestRedDamage.RedMove : null));

        int blueMovesCount = list.SelectMany(x => x.ClashesArray).ToArray().Length;
        string blueMC = string.Format("Moves by <color=#3333aaff>{0}:</color>", blue.Name);
        string[] blueMoves = blue.MoveCards.Select(x => x.SuperiorMove.Name).Union(blue.MoveCards.Select(y => y.InferiorMove.Name)).OrderBy(z => z.Length).ToArray();
        foreach (string move in blueMoves)
        {
            int moveUsage = list.SelectMany(x => x.ClashesArray).Where(y => y.BlueMove == move).ToArray().Length;
            int moveUsageSuccess = list.SelectMany(x => x.ClashesArray).Where(y => y.BlueMove == move && y.BlueSuccess).ToArray().Length;
            blueMC += string.Format("\n\t<color=#ffffffff>{0}</color>: <color=#ffff66ff>{1}%</color> chance to use with Success Rate of: <color=#00ffffff>{2}%</color>",
                move,
                (float)Math.Round((blueMovesCount > 0 ? Decimal.Divide(moveUsage, blueMovesCount) : 0) * 100, 2),
                (float)Math.Round((moveUsage > 0 ? Decimal.Divide(moveUsageSuccess, moveUsage) : 0) * 100, 2));

            if (move.Contains("Superior"))
            {
                blueMC += string.Format("\n\t Number of times the move Used: {0} out of {1}", list.SelectMany(x => x.ClashesArray).Where(y => y.BlueInitiative && y.BlueMove == move).ToList().Count, list.SelectMany(x => x.ClashesArray).ToList().Count);
                blueMC += string.Format("\n\t Average number of times the move Used in {0} fights: {1}", list.Count, list.SelectMany(x => x.ClashesArray).Where(y => y.BlueInitiative && y.BlueMove == move).ToList().Count > 0 ? Math.Round((float)Decimal.Divide(list.SelectMany(x => x.ClashesArray).ToList().Count, list.SelectMany(x => x.ClashesArray).Where(y => y.BlueInitiative && y.BlueMove == move).ToList().Count), 2) : 0);
                blueMC += string.Format("\n\t Percent to use move in {0} fight/s : {1}%", list.Count, (float)Math.Round(Decimal.Divide(list.SelectMany(x => x.ClashesArray).Where(y => y.BlueInitiative && y.BlueMove == move).ToList().Count, list.SelectMany(x => x.ClashesArray).ToList().Count) * 100, 2));
                blueMC += string.Format("\n\t Number of times the move used in a fight: {0} out of {1}, Average Damage: {2}", Mathf.RoundToInt((float)Decimal.Divide(list.SelectMany(x => x.ClashesArray).Where(y => y.BlueInitiative && y.BlueMove == move).ToList().Count, list.SelectMany(x => x.ClashesArray).ToList().Count) * Mathf.RoundToInt((float)Decimal.Divide(list.SelectMany(x => x.ClashesArray).ToList().Count, list.Count))), Mathf.RoundToInt((float)Decimal.Divide(list.SelectMany(x => x.ClashesArray).ToList().Count, list.Count)), Math.Round(list.SelectMany(x => x.ClashesArray).Where(y => y.BlueInitiative && y.BlueMove == move && y.BlueSuccess && y.BlueDamage > 0).ToList().Count > 0 ? list.SelectMany(x => x.ClashesArray).Where(y => y.BlueInitiative && y.BlueMove == move && y.BlueSuccess && y.BlueDamage > 0).Average(z => z.BlueDamage) : 0, 2));
                blueMC += string.Format("\n");
            } else if (move.Contains("Inferior"))
            {
                blueMC += string.Format("\n\t Number of times the move Used: {0} out of {1}", list.SelectMany(x => x.ClashesArray).Where(y => !y.BlueInitiative && y.BlueMove == move).ToList().Count, list.SelectMany(x => x.ClashesArray).ToList().Count);
                blueMC += string.Format("\n\t Average number of times the move Used in {0} fights: {1}", list.Count, list.SelectMany(x => x.ClashesArray).Where(y => !y.BlueInitiative && y.BlueMove == move).ToList().Count > 0 ? Math.Round((float)Decimal.Divide(list.SelectMany(x => x.ClashesArray).ToList().Count, list.SelectMany(x => x.ClashesArray).Where(y => !y.BlueInitiative && y.BlueMove == move).ToList().Count), 2) : 0);
                blueMC += string.Format("\n\t Percent to use move in {0} fight/s : {1}%", list.Count, (float)Math.Round(Decimal.Divide(list.SelectMany(x => x.ClashesArray).Where(y => !y.BlueInitiative && y.BlueMove == move).ToList().Count, list.SelectMany(x => x.ClashesArray).ToList().Count) * 100, 2));
                blueMC += string.Format("\n\t Number of times the move used in a fight: {0} out of {1}, Average Damage: {2}", Mathf.RoundToInt((float)Decimal.Divide(list.SelectMany(x => x.ClashesArray).Where(y => !y.BlueInitiative && y.BlueMove == move).ToList().Count, list.SelectMany(x => x.ClashesArray).ToList().Count) * Mathf.RoundToInt((float)Decimal.Divide(list.SelectMany(x => x.ClashesArray).ToList().Count, list.Count))), Mathf.RoundToInt((float)Decimal.Divide(list.SelectMany(x => x.ClashesArray).ToList().Count, list.Count)), Math.Round(list.SelectMany(x => x.ClashesArray).Where(y => !y.BlueInitiative && y.BlueMove == move && y.BlueSuccess && y.BlueDamage > 0).ToList().Count > 0 ? list.SelectMany(x => x.ClashesArray).Where(y => !y.BlueInitiative && y.BlueMove == move && y.BlueSuccess && y.BlueDamage > 0).Average(z => z.BlueDamage) : 0, 2));
                blueMC += string.Format("\n");
            }
            
        }
        statistics.Add(blueMC);

        int redMovesCount = list.SelectMany(x => x.ClashesArray).ToArray().Length;
        string redMC = string.Format("Moves by <color=#aa3333ff>{0}:</color>", red.Name);
        string[] redMoves = red.MoveCards.Select(x => x.SuperiorMove.Name).Union(red.MoveCards.Select(y => y.InferiorMove.Name)).OrderBy(z => z.Length).ToArray();
        foreach (string move in redMoves)
        {
            int moveUsage = list.SelectMany(x => x.ClashesArray).Where(y => y.RedMove == move).ToArray().Length;
            int moveUsageSuccess = list.SelectMany(x => x.ClashesArray).Where(y => y.RedMove == move && y.RedSuccess).ToArray().Length;
            redMC += string.Format("\n\t<color=#ffffffff>{0}</color>: <color=#ffff66ff>{1}%</color> chance to use with Success Rate of: <color=#00ffffff>{2}%</color>",
                move,
                (float)Math.Round((redMovesCount > 0 ? Decimal.Divide(moveUsage, redMovesCount) : 0) * 100, 2),
                (float)Math.Round((moveUsage > 0 ? Decimal.Divide(moveUsageSuccess, moveUsage) : 0) * 100, 2));

            if (move.Contains("Superior"))
            {
                redMC += string.Format("\n\t Number of times the move Used: {0} out of {1}", list.SelectMany(x => x.ClashesArray).Where(y => y.RedInitiative && y.RedMove == move).ToList().Count, list.SelectMany(x => x.ClashesArray).ToList().Count);
                redMC += string.Format("\n\t Average number of times the move Used in {0} fights: {1}", list.Count, list.SelectMany(x => x.ClashesArray).Where(y => y.RedInitiative && y.RedMove == move).ToList().Count > 0 ? Math.Round((float)Decimal.Divide(list.SelectMany(x => x.ClashesArray).ToList().Count, list.SelectMany(x => x.ClashesArray).Where(y => y.RedInitiative && y.RedMove == move).ToList().Count), 2) : 0);
                redMC += string.Format("\n\t Percent to use move in {0} fight/s : {1}%", list.Count, (float)Math.Round(Decimal.Divide(list.SelectMany(x => x.ClashesArray).Where(y => y.RedInitiative && y.RedMove == move).ToList().Count, list.SelectMany(x => x.ClashesArray).ToList().Count) * 100, 2));
                redMC += string.Format("\n\t Number of times the move used in a fight: {0} out of {1}, Average Damage: {2}", Mathf.RoundToInt((float)Decimal.Divide(list.SelectMany(x => x.ClashesArray).Where(y => y.RedInitiative && y.RedMove == move).ToList().Count, list.SelectMany(x => x.ClashesArray).ToList().Count) * Mathf.RoundToInt((float)Decimal.Divide(list.SelectMany(x => x.ClashesArray).ToList().Count, list.Count))), Mathf.RoundToInt((float)Decimal.Divide(list.SelectMany(x => x.ClashesArray).ToList().Count, list.Count)), Math.Round(list.SelectMany(x => x.ClashesArray).Where(y => y.RedInitiative && y.RedMove == move && y.RedSuccess && y.RedDamage > 0).ToList().Count > 0 ? list.SelectMany(x => x.ClashesArray).Where(y => y.RedInitiative && y.RedMove == move && y.RedSuccess && y.RedDamage > 0).Average(z => z.RedDamage) : 0, 2));
                redMC += string.Format("\n");
            }
            else if (move.Contains("Inferior"))
            {
                redMC += string.Format("\n\t Number of times the move Used: {0} out of {1}", list.SelectMany(x => x.ClashesArray).Where(y => !y.RedInitiative && y.RedMove == move).ToList().Count, list.SelectMany(x => x.ClashesArray).ToList().Count);
                redMC += string.Format("\n\t Average number of times the move Used in {0} fights: {1}", list.Count, list.SelectMany(x => x.ClashesArray).Where(y => !y.RedInitiative && y.RedMove == move).ToList().Count > 0 ? Math.Round((float)Decimal.Divide(list.SelectMany(x => x.ClashesArray).ToList().Count, list.SelectMany(x => x.ClashesArray).Where(y => !y.RedInitiative && y.RedMove == move).ToList().Count), 2) : 0);
                redMC += string.Format("\n\t Percent to use move in {0} fight/s : {1}%", list.Count, (float)Math.Round(Decimal.Divide(list.SelectMany(x => x.ClashesArray).Where(y => !y.RedInitiative && y.RedMove == move).ToList().Count, list.SelectMany(x => x.ClashesArray).ToList().Count) * 100, 2));
                redMC += string.Format("\n\t Number of times the move used in a fight: {0} out of {1}, Average Damage: {2}", Mathf.RoundToInt((float)Decimal.Divide(list.SelectMany(x => x.ClashesArray).Where(y => !y.RedInitiative && y.RedMove == move).ToList().Count, list.SelectMany(x => x.ClashesArray).ToList().Count) * Mathf.RoundToInt((float)Decimal.Divide(list.SelectMany(x => x.ClashesArray).ToList().Count, list.Count))), Mathf.RoundToInt((float)Decimal.Divide(list.SelectMany(x => x.ClashesArray).ToList().Count, list.Count)), Math.Round(list.SelectMany(x => x.ClashesArray).Where(y => !y.RedInitiative && y.RedMove == move && y.RedSuccess && y.RedDamage > 0).ToList().Count > 0 ? list.SelectMany(x => x.ClashesArray).Where(y => !y.RedInitiative && y.RedMove == move && y.RedSuccess && y.RedDamage > 0).Average(z => z.RedDamage) : 0, 2));
                redMC += string.Format("\n");
            }
        }
        statistics.Add(redMC);
            
        statistics.Add("\n\n\n");

        string DamageResult = string.Format("\n\t<color=#3333aaff>{0}</color>", blue.Name);
        Debug.Log(string.Join("\n", statistics.ToArray()));

        results.Add(string.Join("\n", statistics.ToArray()));
        fights.Add(list);

        Dropdown.OptionData option = new Dropdown.OptionData();
        option.text = results.Count.ToString();
        ResultList.options.Add(option);
        ResultList.captionText.text = ResultList.options.Last().text;
        ResultList.value = ResultList.options.Count - 1;
        ResultList.RefreshShownValue();
        SelectFightResult(ResultList.options.Count - 1);
        TextField.text = string.Join("\n", statistics.ToArray());
    }

    public void Cleanup()
    {
        ResultList.ClearOptions();
        results.Clear();
        fights.Clear();
        results.TrimExcess();
        fights.TrimExcess();
        if (graphs.Count > 0)
        {
            graphs.ForEach(o => Destroy(o));
            graphs.Clear();
            graphs.TrimExcess();
        }
        TextField.text = "No Fights Simulated Yet . . . .";
    }

    public void SelectFightResult(int index)
    {
        TextField.text = results[index];
        //makeGraphs(fights[index]);
    }

    //public void Fight()
    //{
    //    if (blueSelector.ChangesMade() || redSelector.ChangesMade())
    //        ConfirmScreen.SetActive(true);

    //    else
    //    {
    //        MultipleFights();
    //        ResultScreen.SetActive(true);
    //    }
    //}
}
