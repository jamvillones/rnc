﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiamondCameraZoom : MonoBehaviour
{
    public Vector3[] Positions;
    public float[] RespectiveSizes;
    Camera mainCamera;

    private void Start()
    {
        mainCamera = GetComponent<Camera>();
    }

    public void ChangeCamera(int index)
    {
        mainCamera.transform.position = Positions[index];
        mainCamera.orthographicSize = RespectiveSizes[index];
    }
}
