﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using LegendsOfThePit;

public class MatchHandler : MonoBehaviour
{
    public GameObject ConfirmScreen;
    public GameObject ResultScreen;
    public Text FightAmount;
    public CockSelector BlueSide;
    public CockSelector RedSide;
    public Dropdown ResultList;
    public Text TextField;
    [HideInInspector]
    public GamecockObject blue;
    [HideInInspector]
    public GamecockObject red;

    MenuModeSelector blueSelector;
    MenuModeSelector redSelector;
    List<string> results = new List<string>();

    void Start()
    {
        ResultList.ClearOptions();
        if (BlueSide)
            blueSelector = BlueSide.transform.parent.transform.parent.GetComponentInChildren<MenuModeSelector>();
        if (RedSide)
            redSelector = RedSide.transform.parent.transform.parent.GetComponentInChildren<MenuModeSelector>();
    }

    void assignFromList()
    {
        //if (blueSelector.ChangesMade())
        //    BlueSide.SaveChanges();

        //if (redSelector.ChangesMade())
        //    RedSide.SaveChanges();

        //BlueSide.RevertToDefault();
        //RedSide.RevertToDefault();
        blue = BlueSide.CurrentGameCock();
        red = RedSide.CurrentGameCock();
        List<GCMoveCard> blueMoves = new List<GCMoveCard>();
        List<GCMoveCard> redMoves = new List<GCMoveCard>();
        KeyData[] keyData = KeyHolder.GetKeysByExtension("-movecard");
        foreach (KeyData kd in keyData)
        {
            if (kd.UniqueId.Contains(blue.Gamecock.UniqueId))
                blueMoves.Add(GCMoveCard.FromJson(kd.JsonString));

            if (kd.UniqueId.Contains(red.Gamecock.UniqueId))
                redMoves.Add(GCMoveCard.FromJson(kd.JsonString));
        }
        Array.Clear(blue.Gamecock.MoveCards, 0, blue.Gamecock.MoveCards.Length);
        Array.Clear(red.Gamecock.MoveCards, 0, red.Gamecock.MoveCards.Length);
        blue.Gamecock.MoveCards = blueMoves.ToArray();
        red.Gamecock.MoveCards = redMoves.ToArray();
    }

    public void SingleFight()
    {

    }

    public void MultipleFights()
    {
        assignFromList();
        List<FightResult> list = new List<FightResult>();

        for (int i = 0; i < Int32.Parse(FightAmount.text); i++)
            list.Add(FightResult.Simulate_(blue.Gamecock, red.Gamecock));

        List<string> statistics = new List<string> { };

        statistics.Add("\n");
        statistics.Add(string.Format("Match between <color=#aa3333ff>{0}</color> and <color=#3333aaff>{1}</color> simulated <color=#ffff00ff>{2}</color> times.", red.Gamecock.BreedName, blue.Gamecock.BreedName, FightAmount.text));
        statistics.Add(string.Format("Average number of Rounds: <color=#ffff00ff>{0}</color>", list.Average(x => x.Clashes.Length)));
        statistics.Add(string.Format("<color=#3333aaff>Number of times Blue Won:</color> <color=#00ffffff>{0}</color>", list.SelectMany(x => x.Decisions).Where(y => y.Decision == blue.Gamecock.UniqueId).ToArray().Length));
        statistics.Add(string.Format("<color=#aa3333ff>Number of times Red Won:</color> <color=#00ffffff>{0}</color>", list.SelectMany(x => x.Decisions).Where(y => y.Decision == red.Gamecock.UniqueId).ToArray().Length));
        statistics.Add(string.Format("Number of times fight ended with a Draw: <color=#00ffffff>{0}</color>", list.SelectMany(x => x.Decisions).Where(y => y.Decision == "Draw").ToArray().Length));
        statistics.Add(string.Format("<color=#3333aaff>Number of times Blue gained Initiative:</color> <color=#00ffffff>{0}</color>", list.SelectMany(x => x.Clashes).Where(y => y.BlueInitiative).ToArray().Length));
        statistics.Add(string.Format("<color=#aa3333ff>Number of times Red gained Initiative:</color> <color=#00ffffff>{0}</color>", list.SelectMany(x => x.Clashes).Where(y => y.RedInitiative).ToArray().Length));
        statistics.Add(string.Format("<color=#3333aaff>Average Initiative Value of Blue: </color><color=#00ffffff>{0}</color>", Math.Round(list.SelectMany(x => x.Clashes).Average(y => y.BlueInitiativeValue), 3)));
        statistics.Add(string.Format("<color=#aa3333ff>Average Initiative Value of Red:</color> <color=#00ffffff>{0}</color>", Math.Round(list.SelectMany(x => x.Clashes).Average(y => y.RedInitiativeValue), 3)));
        statistics.Add(string.Format("<color=#3333aaff>Average Hit Rate of Blue:</color> <color=#00ffffff>{0}</color>", Math.Round((float)list.SelectMany(x => x.Clashes).Where(y => y.BlueSuccess).ToArray().Length / list.SelectMany(x => x.Clashes).ToArray().Length, 3)));
        statistics.Add(string.Format("<color=#aa3333ff>Average Hit Rate of Red:</color> <color=#00ffffff>{0}</color>", Math.Round((float)list.SelectMany(x => x.Clashes).Where(y => y.RedSuccess).ToArray().Length / list.SelectMany(x => x.Clashes).ToArray().Length, 3)));
        statistics.Add(string.Format("<color=#3333aaff>Average Damage Dealt by Blue:</color> <color=#00ffffff>{0}</color>", Math.Round(list.SelectMany(x => x.Clashes).Where(y => y.BlueSuccess).Average(z => z.BlueDamage), 3)));
        statistics.Add(string.Format("<color=#aa3333ff>Average Damage Dealt by Red:</color> <color=#00ffffff>{0}</color>", Math.Round(list.SelectMany(x => x.Clashes).Where(y => y.RedSuccess).Average(z => z.RedDamage), 3)));
        statistics.Add(string.Format("<color=#3333aaff>Total Damage Dealt by Blue:</color> <color=#00ffffff>{0}</color>", list.SelectMany(x => x.Clashes).Where(y => y.BlueSuccess).Sum(z => z.BlueDamage)));
        statistics.Add(string.Format("<color=#aa3333ff>Total Damage Dealt by Red:</color> <color=#00ffffff>{0}</color>", list.SelectMany(x => x.Clashes).Where(y => y.RedSuccess).Sum(z => z.RedDamage)));

        FightRound_Clash highestBlueDamage = list.SelectMany(x => x.Clashes).Where(y => y.BlueSuccess).OrderByDescending(z => z.BlueDamage).First();
        statistics.Add(string.Format("<color=#3333aaff>Highest Damaging Move of Blue:</color> <color=#00ffffff>{0}</color> Damage '<color=#ffffffff>{1}</color>'", highestBlueDamage.BlueDamage, highestBlueDamage.BlueMove));

        FightRound_Clash highestRedDamage = list.SelectMany(x => x.Clashes).Where(y => y.RedSuccess).OrderByDescending(z => z.RedDamage).First();
        statistics.Add(string.Format("<color=#aa3333ff>Highest Damaging Move of Red:</color> <color=#00ffffff>{0}</color> Damage '<color=#ffffffff>{1}</color>'", highestRedDamage.RedDamage, highestRedDamage.RedMove));

        statistics.Add(string.Format("<color=#3333aaff>Total Damage Mitigated by Blue:</color> <color=#00ffffff>{0}</color>", list.SelectMany(x => x.Clashes).Where(y => y.RedSuccess).Sum(z => z.BlueDamageMitigated)));

        statistics.Add(string.Format("<color=#aa3333ff>Total Damage Mitigated by Red:</color> <color=#00ffffff>{0}</color>", list.SelectMany(x => x.Clashes).Where(y => y.BlueSuccess).Sum(z => z.RedDamageMitigated)));

        FightRound_Clash highestBlueDamageMitigated = list.SelectMany(x => x.Clashes).Where(y => y.RedSuccess).OrderByDescending(z => z.BlueDamageMitigated).First();
        statistics.Add(string.Format("<color=#3333aaff>Highest Damage Mitigating Move of Blue:</color> <color=#00ffffff>{0}</color> Damage Mitigated '<color=#ffffffff>{1}</color>'", highestBlueDamageMitigated.BlueDamageMitigated, highestBlueDamageMitigated.BlueMove));

        FightRound_Clash highestRedDamageMitigated = list.SelectMany(x => x.Clashes).Where(y => y.BlueSuccess).OrderByDescending(z => z.RedDamageMitigated).First();
        statistics.Add(string.Format("<color=#aa3333ff>Highest Damage Mitigating Move of Red:</color> <color=#00ffffff>{0}</color> Damage Mitigated '<color=#ffffffff>{1}</color>'", highestRedDamageMitigated.RedDamageMitigated, highestRedDamageMitigated.RedMove));

        int blueMovesCount = list.SelectMany(x => x.Clashes).ToArray().Length;
        string blueMC = "Moves by <color=#3333aaff>Blue:</color>";
        string[] blueMoves = blue.Gamecock.MoveCards.Select(x => x.Move1.Name).Union(blue.Gamecock.MoveCards.Select(x => x.Move2.Name)).ToArray();
        foreach (string move in blueMoves)
        {
            int moveUsage = list.SelectMany(x => x.Clashes).Where(y => y.BlueMove == move).ToArray().Length;
            int moveUsageSuccess = list.SelectMany(x => x.Clashes).Where(y => y.BlueMove == move && y.BlueSuccess).ToArray().Length;
            blueMC += string.Format("\n\t<color=#ffffffff>{0}</color>: <color=#ffff66ff>{1}%</color> chance to use with Success Rate of: <color=#00ffffff>{2}%</color>",
                move,
                (float)Math.Round(Decimal.Divide(moveUsage, blueMovesCount) * 100, 2),
                (float)Math.Round(Decimal.Divide(moveUsageSuccess, moveUsage) * 100, 2));
        }
        statistics.Add(blueMC);

        int redMovesCount = list.SelectMany(x => x.Clashes).ToArray().Length;
        string redMC = "Moves by <color=#aa3333ff>Red:</color>";
        string[] redMoves = red.Gamecock.MoveCards.Select(x => x.Move1.Name).Union(red.Gamecock.MoveCards.Select(x => x.Move2.Name)).ToArray();
        foreach (string move in redMoves)
        {
            int moveUsage = list.SelectMany(x => x.Clashes).Where(y => y.RedMove == move).ToArray().Length;
            int moveUsageSuccess = list.SelectMany(x => x.Clashes).Where(y => y.RedMove == move && y.RedSuccess).ToArray().Length;
            redMC += string.Format("\n\t<color=#ffffffff>{0}</color>: <color=#ffff66ff>{1}%</color> chance to use with Success Rate of: <color=#00ffffff>{2}%</color>",
                move,
                (float)Math.Round(Decimal.Divide(moveUsage, redMovesCount) * 100, 2),
                (float)Math.Round(Decimal.Divide(moveUsageSuccess, moveUsage) * 100, 2));
        }
        statistics.Add(redMC);

        statistics.Add("\n\n\n");

        //Debug.Log(string.Join("\n", statistics.ToArray()));

        results.Add(string.Join("\n", statistics.ToArray()));

        Dropdown.OptionData option = new Dropdown.OptionData();
        option.text = results.Count.ToString();
        ResultList.options.Add(option);
        ResultList.captionText.text = ResultList.options.Last().text;
        ResultList.value = ResultList.options.Count - 1;
        ResultList.RefreshShownValue();
        SelectFightResult(ResultList.options.Count - 1);
        //TextField.text = string.Join("\n", statistics.ToArray());
    }

    public void SelectFightResult(int index)
    {
        TextField.text = results[index];
    }

    public void Fight()
    {
        if (blueSelector.ChangesMade() || redSelector.ChangesMade())
            ConfirmScreen.SetActive(true);

        else
        {
            MultipleFights();
            ResultScreen.SetActive(true);
        }
    }
}
