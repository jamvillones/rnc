﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputChangeDetector : MonoBehaviour
{
    public bool Changed;
    InputField field;

    void Start()
    {
        Changed = false;
        field = GetComponent<InputField>();
        field.onValueChanged.AddListener(delegate { Changed = true; });
    }
}
