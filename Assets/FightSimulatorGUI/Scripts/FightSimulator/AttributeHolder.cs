﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class AttributeHolder : MonoBehaviour
{
    [HideInInspector]
    public InputField AttributeField;
    // Use this for initialization
    void Start()
    {
        AttributeField = GetComponentInChildren<InputField>();
    }
}
