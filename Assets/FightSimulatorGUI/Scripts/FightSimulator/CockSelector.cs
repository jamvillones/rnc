﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class CockSelector : MonoBehaviour
{
    GamecockObject selectedCock;
    List<GamecockObject> cocks = new List<GamecockObject>();
    public Dropdown NameLabel;
    [HideInInspector]
    public List<InputField> AttributeFields = new List<InputField>();

    void Start()
    {       
        NameLabel.ClearOptions();
        if (!PlayerPrefs.HasKey("firstStart"))
        {
            KeyHolder.SaveGameCockResourcesToPrefs();
            PlayerPrefs.SetInt("firstStart", 1);
        }
        KeyData[] cockData = KeyHolder.GetKeysByExtension("-gamecock");
        foreach (KeyData cock in cockData)
        {
            GamecockObject temp = new GamecockObject();
            temp.Gamecock = Gamecock.FromJson(cock.JsonString);
            cocks.Add(temp);
        }

        foreach (Transform child in transform)
        {
            if (child.GetComponent<AttributeHolder>())
                AttributeFields.Add(child.GetComponent<AttributeHolder>().GetComponentInChildren<InputField>());
        }

        if (cocks != null)
        {
            selectedCock = cocks[0];
            foreach (GamecockObject c in cocks)
            {
                Dropdown.OptionData option = new Dropdown.OptionData();
                option.text = c.Gamecock.BreedName;
                NameLabel.options.Add(option);
            }
            NameLabel.captionText.text = cocks[0].Gamecock.BreedName;
            RevertToDefault();
        }
    }

    void OnEnable()
    {
        UpdateList();
    }

    public void SaveChanges()
    {
        if (NameLabel.captionText.text == selectedCock.Gamecock.BreedName)
        {
            selectedCock.Gamecock.Attributes.FightingPeak = Int32.Parse(AttributeFields[0].text);
            selectedCock.Gamecock.Attributes.Gameness = Int32.Parse(AttributeFields[1].text);
            selectedCock.Gamecock.Attributes.Intellect = Int32.Parse(AttributeFields[2].text);
            selectedCock.Gamecock.Attributes.Power = Int32.Parse(AttributeFields[3].text);
            selectedCock.Gamecock.Attributes.Cut = Int32.Parse(AttributeFields[4].text);
            selectedCock.Gamecock.Attributes.SpeedAerial = Int32.Parse(AttributeFields[5].text);
            selectedCock.Gamecock.Attributes.SpeedGround = Int32.Parse(AttributeFields[6].text);
            selectedCock.Gamecock.Attributes.Pedigree = Int32.Parse(AttributeFields[7].text);
            selectedCock.Gamecock.Attributes.Instinct = Int32.Parse(AttributeFields[8].text);
            selectedCock.Gamecock.Attributes.BaseWeight = Int32.Parse(AttributeFields[9].text);
        }
        string currentCockSave = Newtonsoft.Json.JsonConvert.SerializeObject(selectedCock.Gamecock);
        KeyData saveCockData = new KeyData(selectedCock.Gamecock.UniqueId + "-gamecock", currentCockSave);
        KeyHolder.AddThenSaveKey(saveCockData);
        AttributeFields.ForEach(i => i.GetComponent<InputChangeDetector>().Changed = false);
    }

    public void RevertToDefault()
    {
        if (KeyHolder.KeyExists(selectedCock.Gamecock.UniqueId + "-gamecock"))
            selectedCock.Gamecock = Gamecock.FromJson(KeyHolder.GetKey(selectedCock.Gamecock.UniqueId + "-gamecock").JsonString);
        AttributeFields[0].text = selectedCock.Gamecock.Attributes.FightingPeak.ToString();
        AttributeFields[1].text = selectedCock.Gamecock.Attributes.Gameness.ToString();
        AttributeFields[2].text = selectedCock.Gamecock.Attributes.Intellect.ToString();
        AttributeFields[3].text = selectedCock.Gamecock.Attributes.Power.ToString();
        AttributeFields[4].text = selectedCock.Gamecock.Attributes.Cut.ToString();
        AttributeFields[5].text = selectedCock.Gamecock.Attributes.SpeedAerial.ToString();
        AttributeFields[6].text = selectedCock.Gamecock.Attributes.SpeedGround.ToString();
        AttributeFields[7].text = selectedCock.Gamecock.Attributes.Pedigree.ToString();
        AttributeFields[8].text = selectedCock.Gamecock.Attributes.Instinct.ToString();
        AttributeFields[9].text = selectedCock.Gamecock.Attributes.BaseWeight.ToString();
        AttributeFields.ForEach(i => i.GetComponent<InputChangeDetector>().Changed = false);
    }


    public void UpdateList()
    {
        if (cocks.Count > 0 && AttributeFields.Count > 0)
        {
            selectedCock = cocks.Find(c => c.Gamecock.BreedName == NameLabel.captionText.text);
            RevertToDefault();
        }
    }

    public GamecockObject CurrentGameCock()
    {
        return selectedCock;
    }
}
