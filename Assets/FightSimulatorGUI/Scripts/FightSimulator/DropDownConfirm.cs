﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DropDownConfirm : MonoBehaviour
{
    public GameObject ConfirmPanel;
    public List<InputField> InputFields = new List<InputField>();
    Dropdown dropdown;

    void Start()
    {
        dropdown = GetComponent<Dropdown>();
        EventTrigger eventTrigger = gameObject.AddComponent<EventTrigger>();
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerDown;
        entry.callback.AddListener(delegate { CheckForChanges(); });
        eventTrigger.triggers.Add(entry);
    }

    public void CheckForChanges()
    {
        if (InputFields.Exists(i => i.GetComponent<InputChangeDetector>().Changed))
        {
            ConfirmPanel.SetActive(true);
        }
    }
}
