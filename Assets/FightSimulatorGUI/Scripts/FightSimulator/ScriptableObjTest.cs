﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEditor;

public class ScriptableObjTest : MonoBehaviour
{
    public GamecockObject gamecockObject;
    string fileName = "test";
    public void CreateSO()
    {
        fileName = "test";     
        ScriptableObjectUtility.CreateAsset<GamecockObject>("Assets/Resources/Gamecocks", fileName);
        //temp = AssetDatabase.LoadAssetAtPath<GamecockObject>("Assets/Resources/Gamecocks/test.asset");
    }

    public void AssignCock()
    {
       // gamecockObject = AssetDatabase.LoadAssetAtPath<GamecockObject>("Assets/Resources/Gamecocks/" + fileName + ".asset");
        gamecockObject.Autofill();
    }
}
