﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MenuModeSelector : MonoBehaviour
{
    Dropdown selectorDropdown;
    public List<GameObject> MenuSections = new List<GameObject>();
    List<GameObject> activeConfirmScreens = new List<GameObject>();

    void Start()
    {
        selectorDropdown = GetComponent<Dropdown>();

        if (MenuSections != null)
            ShowMenuPage(0);

        selectorDropdown.ClearOptions();
        foreach (GameObject menuItem in MenuSections)
        {
            Dropdown.OptionData option = new Dropdown.OptionData();
            option.text = menuItem.name;
            selectorDropdown.options.Add(option);
        }
        selectorDropdown.captionText.text = MenuSections[0].name;
        MenuSections.ForEach(o => activeConfirmScreens.Add(o.GetComponentInChildren<ConfirmScreen>(true).gameObject));

        EventTrigger eventTrigger = gameObject.AddComponent<EventTrigger>();
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerDown;
        entry.callback.AddListener(delegate { checkForChange(); });
        eventTrigger.triggers.Add(entry);

        StartCoroutine(checkForConfirmScreen());
    }

    
    IEnumerator checkForConfirmScreen()
    {
        yield return new WaitUntil(() => MenuSections != null);
        yield return new WaitUntil(() => activeConfirmScreens.Exists(o => o.activeInHierarchy));
        GameObject temp = activeConfirmScreens.Find(o => o.activeInHierarchy);
        selectorDropdown.interactable = false;
        yield return new WaitUntil(() => !temp.activeInHierarchy);
        selectorDropdown.interactable = true;
        StartCoroutine(checkForConfirmScreen());
    }

    void checkForChange()
    {
        if (MenuSections.Find(o => o.activeInHierarchy).GetComponentInChildren<DropDownConfirm>().InputFields.Exists(i => i.GetComponent<InputChangeDetector>().Changed))
        {
            MenuSections.Find(o => o.activeInHierarchy).GetComponentInChildren<ConfirmScreen>(true).gameObject.SetActive(true);
        }
    }

    public bool ChangesMade()
    {
        if (MenuSections.Find(o => o.activeInHierarchy).GetComponentInChildren<DropDownConfirm>().InputFields.Exists(i => i.GetComponent<InputChangeDetector>().Changed))
            return true;

        return false;
    }

    public void ShowMenuPage(int index)
    {
        MenuSections.ForEach(o => o.SetActive(false));
        MenuSections[index].SetActive(true);
    }



}
