﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class MoveCardSelector : MonoBehaviour
{
    public CockSelector MainCockSelector;
    public Dropdown MoveCardDropdown;
    public Dropdown MovesDropDown;
    public List<InputField> SkillAttributes = new List<InputField>();
    List<GCMoveCard> moves = new List<GCMoveCard>();
    InputField weight;
    GCMoveCard currentMoveCard;

    // Use this for initialization
    void Awake()
    {
        weight = MoveCardDropdown.GetComponentInChildren<AttributeHolder>().GetComponentInChildren<InputField>();
        foreach (Transform child in transform)
        {
            if (child.GetComponent<AttributeHolder>())
                SkillAttributes.Add(child.GetComponent<AttributeHolder>().GetComponentInChildren<InputField>());
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnEnable()
    {
        //currentMoveCard = MainCockSelector.CurrentGameCock().Gamecock.MoveCards[0];
        populateMoveCards();
        populateMoves();
        UpdateFields();
    }

    void populateMoveCards()
    {
        moves.Clear();
        MoveCardDropdown.ClearOptions();       

        KeyData[] moveData = KeyHolder.GetKeysByExtension("-movecard");

        foreach (KeyData kd in moveData)
        {
            if (kd.UniqueId.Contains(MainCockSelector.CurrentGameCock().Gamecock.UniqueId))
            {
                GCMoveCard temp = new GCMoveCard();
                temp = GCMoveCard.FromJson(kd.JsonString);
                moves.Add(temp);
            }
        }

        if (moves.Count <= 0)
            moves = MainCockSelector.CurrentGameCock().Gamecock.MoveCards.ToList();

        foreach (GCMoveCard moveCard in moves)
        {
            Dropdown.OptionData option = new Dropdown.OptionData();
            option.text = moveCard.Name;
            MoveCardDropdown.options.Add(option);
        }

        if(currentMoveCard != null)
            MoveCardDropdown.value = MoveCardDropdown.options.FindIndex(o => o.text == currentMoveCard.Name);

        else
        {
            currentMoveCard = moves[0];
            MoveCardDropdown.value = 0;
        }

        MoveCardDropdown.captionText.text = MoveCardDropdown.options[0].text;
    }

    GCMoveCard searchForMove(GCMoveCard[] moveCards, string key)
    {
        foreach (GCMoveCard card in moveCards)
        {
            if (key == card.Name)
                return card;
        }
        return null;
    }

    void populateMoves()
    {
        MovesDropDown.ClearOptions();
        Dropdown.OptionData option = new Dropdown.OptionData();
        option.text = currentMoveCard.Move1.Name;
        MovesDropDown.options.Add(option);
        option = new Dropdown.OptionData();
        option.text = currentMoveCard.Move2.Name;
        MovesDropDown.options.Add(option);
        MovesDropDown.value = 0;
        MovesDropDown.captionText.text = currentMoveCard.Move1.Name;
    }

    public void UpdateFields()
    {
        foreach (GCMoveCard moveCard in moves)
        {
            if (moveCard.Name == MoveCardDropdown.captionText.text)
                currentMoveCard = moveCard;
        }
        populateMoves();
        RevertToDefault();
    }

    public void RevertToDefault()
    {
        if (KeyHolder.KeyExists(MainCockSelector.CurrentGameCock().Gamecock.UniqueId + currentMoveCard.UniqueId + "-movecard"))
            currentMoveCard = GCMoveCard.FromJson(KeyHolder.GetKey(MainCockSelector.CurrentGameCock().Gamecock.UniqueId + currentMoveCard.UniqueId + "-movecard").JsonString);

        if (MovesDropDown.captionText.text == currentMoveCard.Move1.Name)
        {
            SkillAttributes[0].text = currentMoveCard.Move1.BaseInitiative.ToString();
            SkillAttributes[1].text = currentMoveCard.Move1.BaseDamage.ToString();
            SkillAttributes[2].text = currentMoveCard.Move1.BaseAccuracy.ToString();
            SkillAttributes[3].text = currentMoveCard.Move1.BaseDodge.ToString();
            SkillAttributes[4].text = currentMoveCard.Move1.EnemyInitiative.ToString();
            SkillAttributes[5].text = currentMoveCard.Move1.EnemyDamage.ToString();
            SkillAttributes[6].text = currentMoveCard.Move1.EnemyAccuracy.ToString();
            SkillAttributes[7].text = currentMoveCard.Move1.EnemyDodge.ToString();
        }
        else if (MovesDropDown.captionText.text == currentMoveCard.Move2.Name)
        {
            SkillAttributes[0].text = currentMoveCard.Move2.BaseInitiative.ToString();
            SkillAttributes[1].text = currentMoveCard.Move2.BaseDamage.ToString();
            SkillAttributes[2].text = currentMoveCard.Move2.BaseAccuracy.ToString();
            SkillAttributes[3].text = currentMoveCard.Move2.BaseDodge.ToString();
            SkillAttributes[4].text = currentMoveCard.Move2.EnemyInitiative.ToString();
            SkillAttributes[5].text = currentMoveCard.Move2.EnemyDamage.ToString();
            SkillAttributes[6].text = currentMoveCard.Move2.EnemyAccuracy.ToString();
            SkillAttributes[7].text = currentMoveCard.Move2.EnemyDodge.ToString();
        }
        weight.text = currentMoveCard.Weight.ToString();
        weight.GetComponent<InputChangeDetector>().Changed = false;
        SkillAttributes.ForEach(i => i.GetComponent<InputChangeDetector>().Changed = false);
    }

    public void SaveValues()
    {
        if (MovesDropDown.captionText.text == currentMoveCard.Move1.Name)
        {
            currentMoveCard.Move1.BaseInitiative = Int32.Parse(SkillAttributes[0].text);
            currentMoveCard.Move1.BaseDamage = Int32.Parse(SkillAttributes[1].text);
            currentMoveCard.Move1.BaseAccuracy = Int32.Parse(SkillAttributes[2].text);
            currentMoveCard.Move1.BaseDodge = Int32.Parse(SkillAttributes[3].text);
            currentMoveCard.Move1.EnemyInitiative = Int32.Parse(SkillAttributes[4].text);
            currentMoveCard.Move1.EnemyDamage = Int32.Parse(SkillAttributes[5].text);
            currentMoveCard.Move1.EnemyAccuracy = Int32.Parse(SkillAttributes[6].text);
            currentMoveCard.Move1.EnemyDodge = Int32.Parse(SkillAttributes[7].text);
        }
        else if (MovesDropDown.captionText.text == currentMoveCard.Move2.Name)
        {
            currentMoveCard.Move2.BaseInitiative = Int32.Parse(SkillAttributes[0].text);
            currentMoveCard.Move2.BaseDamage = Int32.Parse(SkillAttributes[1].text);
            currentMoveCard.Move2.BaseAccuracy = Int32.Parse(SkillAttributes[2].text);
            currentMoveCard.Move2.BaseDodge = Int32.Parse(SkillAttributes[3].text);
            currentMoveCard.Move2.EnemyInitiative = Int32.Parse(SkillAttributes[4].text);
            currentMoveCard.Move2.EnemyDamage = Int32.Parse(SkillAttributes[5].text);
            currentMoveCard.Move2.EnemyAccuracy = Int32.Parse(SkillAttributes[6].text);
            currentMoveCard.Move2.EnemyDodge = Int32.Parse(SkillAttributes[7].text);
        }
        currentMoveCard.Weight = Int32.Parse(weight.text);
        weight.GetComponent<InputChangeDetector>().Changed = false;
        string currentMoveCardSave = Newtonsoft.Json.JsonConvert.SerializeObject(currentMoveCard);
        KeyData saveMoveData = new KeyData(MainCockSelector.CurrentGameCock().Gamecock.UniqueId + currentMoveCard.UniqueId + "-movecard", currentMoveCardSave);
        KeyHolder.AddThenSaveKey(saveMoveData);
        MainCockSelector.CurrentGameCock().Gamecock.MoveCards[MainCockSelector.CurrentGameCock().Gamecock.MoveCards.ToList().FindIndex(mc => mc.UniqueId == currentMoveCard.UniqueId)] = currentMoveCard;
        SkillAttributes.ForEach(i => i.GetComponent<InputChangeDetector>().Changed = false);
    }

}
