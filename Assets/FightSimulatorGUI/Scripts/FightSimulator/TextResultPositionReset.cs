﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextResultPositionReset : MonoBehaviour
{
    //public RectTransform Content;
    void OnEnable()
    {
        StartCoroutine(reposition());
    }

    IEnumerator reposition()
    {
        yield return new WaitForSecondsRealtime(0.1f);
        RectTransform Content = GetComponent<RectTransform>();
        Content.localPosition = new Vector3(Content.localPosition.x, -(Content.sizeDelta.y / 2), Content.localPosition.z);
    }
}
