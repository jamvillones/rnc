﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DataBar : MonoBehaviour
{
    Image barImage;

    public Text Value;
    public Text Description;

    void Start()
    {
        barImage = GetComponent<Image>();
    }


    public void UpdateData(float maxValue)
    {
        float floatResult;
        if (!barImage)
            barImage = GetComponent<Image>();
        barImage.fillAmount = ValueMapper.Map(float.TryParse(Value.text, out floatResult) ? floatResult : Int32.Parse(Value.text), 0, maxValue, 0, 1);
        Value.text = Math.Round(floatResult, 2).ToString();
        RectTransform valueTransform = Value.GetComponent<RectTransform>();
        valueTransform.localPosition = new Vector3(valueTransform.localPosition.x, ((barImage.rectTransform.rect.height * barImage.fillAmount) - barImage.rectTransform.rect.height / 2) + 10, valueTransform.localPosition.z);
    }
}
