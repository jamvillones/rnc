﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEssentials;

public class BarGraph : MonoBehaviour
{
    public GameObject BarPrefab;
    public float MaxValue;
    public Transform VerticalTextValueParent;
    public Transform BarAreaParent;
    public Text DescriptionLabel;
    List<Text> verticalValues = new List<Text>();
    public List<DataBar> bars = new List<DataBar>();

    void adjustValues()
    {
        float percentDecrease = 1;
        for (int i = 0; i < verticalValues.Count; i++)
        {
            verticalValues[i].text = Math.Round(MaxValue * percentDecrease, 2).ToString();
            percentDecrease -= 0.25f;
        }

    }

    void Start()
    {
        foreach (Transform child in VerticalTextValueParent)
        {
            if (child.GetComponent<Text>())
                verticalValues.Add(child.GetComponent<Text>());
        }
        foreach (Transform child in BarAreaParent)
        {
            if (child.GetComponent<DataBar>())
                bars.Add(child.GetComponent<DataBar>());
        }

        adjustValues();
    }

    public void RefreshBars()
    {
        bars.ForEach(b => b.UpdateData(MaxValue));
    }

    public void AddBar(string value, string description, Color color)
    {
        GameObject temp = Instantiate(BarPrefab, BarAreaParent) as GameObject;
        temp.GetComponent<Image>().color = color;
        DataBar tempData = temp.GetComponent<DataBar>();
        tempData.Value.text = value;
        tempData.Description.text = description;
        bars.Add(tempData);
        RefreshBars();
    }

    public void AddTest()
    {
        AddBar(UERandom.Range(0, MaxValue).ToString(), "some test move", Color.blue);
    }
}
