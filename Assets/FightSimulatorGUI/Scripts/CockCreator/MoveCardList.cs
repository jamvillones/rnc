﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class MoveCardList : MonoBehaviour
{
    [HideInInspector]
    public Dropdown MoveList;
    public GameObject SuperiorParent;
    public GameObject InferiorParent;
    public InputField Name;
    public InputField Weight;
    public Dropdown MoveCopies;
    [HideInInspector]
    public List<InputField> superiorAttributes = new List<InputField>();
    [HideInInspector]
    public List<InputField> inferiorAttributes = new List<InputField>();

    List<GCMoveCard> moves = new List<GCMoveCard>();
    List<GCMoveCard> allMoveCopies = new List<GCMoveCard>();
    GamecockObject currentCock;
    GCMoveCard currentMoveCard;


    void Start()
    {
        MoveList = GetComponent<Dropdown>();
        MoveList.ClearOptions();
        foreach (Transform child in SuperiorParent.transform)
        {
            if (child.GetComponentInChildren<InputField>())
                superiorAttributes.Add(child.GetComponentInChildren<InputField>());
        }
        foreach (Transform child in InferiorParent.transform)
        {
            if (child.GetComponentInChildren<InputField>())
                inferiorAttributes.Add(child.GetComponentInChildren<InputField>());
        }
    }

    void fillMoveList()
    {
        MoveList.ClearOptions();
        moves.Clear();

        KeyData[] moveData = KeyHolder.GetKeysByExtension("-movecard");

        foreach (KeyData md in moveData)
        {
            if (md.UniqueId.Contains(currentCock.Gamecock.UniqueId))
            {
                GCMoveCard temp = new GCMoveCard();
                temp = GCMoveCard.FromJson(md.JsonString);
                moves.Add(temp);
            }
        }

        if (moves.Count <= 0)
            moves = currentCock.Gamecock.MoveCards.ToList();

        foreach (GCMoveCard moveCard in moves)
        {
            Dropdown.OptionData option = new Dropdown.OptionData();
            option.text = moveCard.Name;
            MoveList.options.Add(option);
        }

        if (currentMoveCard != null)
            MoveList.value = MoveList.options.FindIndex(o => o.text == currentMoveCard.Name);

        else
        {
            currentMoveCard = moves[0];
            MoveList.value = 0;
        }
        MoveList.captionText.text = currentMoveCard.Name;
    }

    public void CreateCopyCard()
    {
        GCMoveCard referenceCard = allMoveCopies.Find(mc => mc.Name == MoveCopies.captionText.text);

        GCMoveCard newMoveCard = new GCMoveCard();
        newMoveCard.UniqueId = Guid.NewGuid().ToString();
        newMoveCard.Name = referenceCard.Name + "-copy";
        newMoveCard.Weight = referenceCard.Weight;
        GCMove tempSuperior = GCMove.DeepCopy(referenceCard.Move1);
        GCMove tempInferior = GCMove.DeepCopy(referenceCard.Move2);
        newMoveCard.Move1 = tempSuperior;
        newMoveCard.Move2 = tempInferior;

        List<GCMoveCard> temp = currentCock.Gamecock.MoveCards.ToList();
        temp.Add(newMoveCard);
        currentCock.Gamecock.MoveCards = temp.ToArray();

        string currentMoveCardSave = Newtonsoft.Json.JsonConvert.SerializeObject(newMoveCard);
        KeyData saveMoveData = new KeyData(currentCock.Gamecock.UniqueId + newMoveCard.UniqueId + "-movecard", currentMoveCardSave);
        KeyHolder.AddThenSaveKey(saveMoveData);

        currentMoveCard = newMoveCard;
        Init(currentCock);
    }

    public void FillCopyDropDown()
    {
        allMoveCopies.Clear();
        MoveCopies.ClearOptions();
        KeyData[] availableMovesData = KeyHolder.GetKeysByExtension("-movecard");

        foreach (KeyData moveData in availableMovesData)
        {
            allMoveCopies.Add(GCMoveCard.FromJson(moveData.JsonString));
        }

        foreach (GCMoveCard moveCard in allMoveCopies)
        {
            Dropdown.OptionData option = new Dropdown.OptionData();
            option.text = moveCard.Name;
            MoveCopies.options.Add(option);
        }
        MoveCopies.value = 0;
        MoveCopies.captionText.text = MoveCopies.options[0].text;
    }

    public void AssignCock(GamecockObject cock)
    {
        currentCock = cock;
    }

    public void Init(GamecockObject selectedCock)
    {
        currentCock = selectedCock;
        fillMoveList();
        ReloadValues();
    }

    public void ReloadValues()
    {
        foreach (GCMoveCard mc in moves)
        {
            if (mc.Name == MoveList.captionText.text)
                currentMoveCard = mc;
        }
        if (KeyHolder.KeyExists(currentCock.Gamecock.UniqueId + currentMoveCard.UniqueId + "-movecard"))
            currentMoveCard = GCMoveCard.FromJson(KeyHolder.GetKey(currentCock.Gamecock.UniqueId + currentMoveCard.UniqueId + "-movecard").JsonString);

        Name.text = currentMoveCard.Name.ToString();
        Weight.text = currentMoveCard.Weight.ToString();
        superiorAttributes[0].text = currentMoveCard.Move1.Name;
        superiorAttributes[1].text = currentMoveCard.Move1.Verticality.ToString();
        superiorAttributes[2].text = currentMoveCard.Move1.BaseInitiative.ToString();
        superiorAttributes[3].text = currentMoveCard.Move1.BaseDamage.ToString();
        superiorAttributes[4].text = currentMoveCard.Move1.BaseAccuracy.ToString();
        superiorAttributes[5].text = currentMoveCard.Move1.BaseDodge.ToString();
        superiorAttributes[6].text = currentMoveCard.Move1.EnemyInitiative.ToString();
        superiorAttributes[7].text = currentMoveCard.Move1.EnemyDamage.ToString();
        superiorAttributes[8].text = currentMoveCard.Move1.EnemyAccuracy.ToString();
        superiorAttributes[9].text = currentMoveCard.Move1.EnemyDodge.ToString();

        inferiorAttributes[0].text = currentMoveCard.Move2.Name;
        inferiorAttributes[1].text = currentMoveCard.Move2.Verticality.ToString();
        inferiorAttributes[2].text = currentMoveCard.Move2.BaseInitiative.ToString();
        inferiorAttributes[3].text = currentMoveCard.Move2.BaseDamage.ToString();
        inferiorAttributes[4].text = currentMoveCard.Move2.BaseAccuracy.ToString();
        inferiorAttributes[5].text = currentMoveCard.Move2.BaseDodge.ToString();
        inferiorAttributes[6].text = currentMoveCard.Move2.EnemyInitiative.ToString();
        inferiorAttributes[7].text = currentMoveCard.Move2.EnemyDamage.ToString();
        inferiorAttributes[8].text = currentMoveCard.Move2.EnemyAccuracy.ToString();
        inferiorAttributes[9].text = currentMoveCard.Move2.EnemyDodge.ToString();
        Name.GetComponent<InputChangeDetector>().Changed = false;
        Weight.GetComponent<InputChangeDetector>().Changed = false;
        superiorAttributes.ForEach(i => i.GetComponent<InputChangeDetector>().Changed = false);
        inferiorAttributes.ForEach(i => i.GetComponent<InputChangeDetector>().Changed = false);
    }

    public void SaveValues()
    {
        currentMoveCard.Name = Name.text;
        currentMoveCard.Weight = Int32.Parse(Weight.text);
        currentMoveCard.Move1.Name = superiorAttributes[0].text;
        currentMoveCard.Move1.Verticality = Int32.Parse(superiorAttributes[1].text);
        currentMoveCard.Move1.BaseInitiative = Int32.Parse(superiorAttributes[2].text);
        currentMoveCard.Move1.BaseDamage = Int32.Parse(superiorAttributes[3].text);
        currentMoveCard.Move1.BaseAccuracy = Int32.Parse(superiorAttributes[4].text);
        currentMoveCard.Move1.BaseDodge = Int32.Parse(superiorAttributes[5].text);
        currentMoveCard.Move1.EnemyInitiative = Int32.Parse(superiorAttributes[6].text);
        currentMoveCard.Move1.EnemyDamage = Int32.Parse(superiorAttributes[7].text);
        currentMoveCard.Move1.EnemyAccuracy = Int32.Parse(superiorAttributes[8].text);
        currentMoveCard.Move1.EnemyDodge = Int32.Parse(superiorAttributes[9].text);

        currentMoveCard.Move2.Name = inferiorAttributes[0].text;
        currentMoveCard.Move2.Verticality = Int32.Parse(inferiorAttributes[1].text);
        currentMoveCard.Move2.BaseInitiative = Int32.Parse(inferiorAttributes[2].text);
        currentMoveCard.Move2.BaseDamage = Int32.Parse(inferiorAttributes[3].text);
        currentMoveCard.Move2.BaseAccuracy = Int32.Parse(inferiorAttributes[4].text);
        currentMoveCard.Move2.BaseDodge = Int32.Parse(inferiorAttributes[5].text);
        currentMoveCard.Move2.EnemyInitiative = Int32.Parse(inferiorAttributes[6].text);
        currentMoveCard.Move2.EnemyDamage = Int32.Parse(inferiorAttributes[7].text);
        currentMoveCard.Move2.EnemyAccuracy = Int32.Parse(inferiorAttributes[8].text);
        currentMoveCard.Move2.EnemyDodge = Int32.Parse(inferiorAttributes[9].text);

        string currentMoveCardSave = Newtonsoft.Json.JsonConvert.SerializeObject(currentMoveCard);
        KeyData saveMoveData = new KeyData(currentCock.Gamecock.UniqueId + currentMoveCard.UniqueId + "-movecard", currentMoveCardSave);
        KeyHolder.AddThenSaveKey(saveMoveData);
        Init(currentCock);
    }

    public void CreateNew()
    {
        GCMoveCard newMoveCard = new GCMoveCard();
        newMoveCard.UniqueId = Guid.NewGuid().ToString();
        newMoveCard.Name = "New Move Card";
        newMoveCard.Weight = 10;
        GCMove tempSuperior = new GCMove();
        GCMove tempInferior = new GCMove();

        tempSuperior.UniqueId = 1;
        tempSuperior.Name = "Superior Move";
        tempSuperior.AnimationId = 1;
        tempSuperior.Verticality = 5;
        tempSuperior.BaseInitiative = 5;
        tempSuperior.BaseDamage = 25;
        tempSuperior.BaseAccuracy = 60;
        tempSuperior.BaseDodge = 10;

        tempInferior.UniqueId = 2;
        tempInferior.Name = "Inferior Move";
        tempInferior.AnimationId = 2;
        tempInferior.Verticality = 4;
        tempInferior.BaseInitiative = 0;
        tempInferior.BaseDamage = 20;
        tempInferior.BaseAccuracy = 60;
        tempInferior.BaseDodge = 10;

        newMoveCard.Move1 = tempSuperior;
        newMoveCard.Move2 = tempInferior;

        List<GCMoveCard> temp = currentCock.Gamecock.MoveCards.ToList();
        temp.Add(newMoveCard);
        currentCock.Gamecock.MoveCards = temp.ToArray();

        string currentMoveCardSave = Newtonsoft.Json.JsonConvert.SerializeObject(newMoveCard);
        KeyData saveMoveData = new KeyData(currentCock.Gamecock.UniqueId + newMoveCard.UniqueId + "-movecard", currentMoveCardSave);
        KeyHolder.AddThenSaveKey(saveMoveData);

        currentMoveCard = newMoveCard;
        Init(currentCock);
    }

    public void DeleteMove()
    {
        KeyHolder.RemoveKey(currentCock.Gamecock.UniqueId + currentMoveCard.UniqueId + "-movecard");
        Init(currentCock);
    }
}
