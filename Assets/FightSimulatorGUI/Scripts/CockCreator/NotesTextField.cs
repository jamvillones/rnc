﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NotesTextField : MonoBehaviour
{
    public Text TextField;
    public InputField EditField;

    public void SetActiveText(bool active)
    {


        if (active)
            EditField.textComponent.color = Color.clear;
        else
            EditField.textComponent.color = Color.black;

        EditField.interactable = !active;
        EditField.ActivateInputField();

        TextField.text = EditField.text;
        TextField.gameObject.SetActive(active);
    }
}
