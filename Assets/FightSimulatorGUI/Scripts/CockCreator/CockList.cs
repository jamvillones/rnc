﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class CockList : MonoBehaviour
{
    [HideInInspector]
    public Dropdown CockListDropDown;
    public GameObject GeneralInfoParent;
    public GameObject AttributeParents;
    public MoveCardList MoveCards;
    public Dropdown CopyFromExisting;
    public InputField NotesField;
    [HideInInspector]
    public List<InputField> GeneralInfos = new List<InputField>();
    [HideInInspector]
    public List<InputField> Attributes = new List<InputField>();
    List<GamecockObject> cocks = new List<GamecockObject>();
    GamecockObject selectedCock;

    void Start()
    {
        CockListDropDown = GetComponent<Dropdown>();
        CockListDropDown.ClearOptions();
        CopyFromExisting.ClearOptions();

        if (!PlayerPrefs.HasKey("firstStart"))
        {
            KeyHolder.SaveGameCockResourcesToPrefs();
            PlayerPrefs.SetInt("firstStart", 1);
        }
        KeyData[] cockData = KeyHolder.GetKeysByExtension("-gamecock");
        foreach (KeyData cock in cockData)
        {
            GamecockObject temp = new GamecockObject();
            temp.Gamecock = Gamecock.FromJson(cock.JsonString);
            cocks.Add(temp);
        }

        foreach (Transform child in GeneralInfoParent.transform)
        {
            if (child.GetComponentInChildren<InputField>())
                GeneralInfos.Add(child.GetComponentInChildren<InputField>());
        }

        foreach (Transform child in AttributeParents.transform)
        {
            if (child.GetComponentInChildren<InputField>())
                Attributes.Add(child.GetComponentInChildren<InputField>());
        }

        GeneralInfos.ForEach(i => i.gameObject.AddComponent<InputChangeDetector>());
        Attributes.ForEach(i => i.gameObject.AddComponent<InputChangeDetector>());
        selectedCock = cocks[0];
        updateDropDown(CockListDropDown);
        CockListDropDown.captionText.text = cocks[0].Gamecock.BreedName;
        RevertToDefault();
    }

    void updateDropDown(Dropdown dropdown)
    {
        dropdown.ClearOptions();
        if (cocks != null)
        {
            foreach (GamecockObject c in cocks)
            {
                Dropdown.OptionData option = new Dropdown.OptionData();
                option.text = c.Gamecock.BreedName;
                dropdown.options.Add(option);
            } 
        }
    }

    public void RevertToDefault()
    {
        selectedCock = cocks.Find(gc => gc.Gamecock.BreedName == CockListDropDown.captionText.text);
        if (KeyHolder.KeyExists(selectedCock.Gamecock.UniqueId + "-gamecock"))
            selectedCock.Gamecock = Gamecock.FromJson(KeyHolder.GetKey(selectedCock.Gamecock.UniqueId + "-gamecock").JsonString);

        if (KeyHolder.KeyExists(selectedCock.Gamecock.UniqueId + "-notes"))
            NotesField.text = KeyHolder.GetKey(selectedCock.Gamecock.UniqueId + "-notes").JsonString;
        else
            NotesField.text = string.Empty;

        MoveCards.AssignCock(selectedCock);
        GeneralInfos[0].text = selectedCock.Gamecock.Name;
        GeneralInfos[1].text = selectedCock.Gamecock.BreedName;
        GeneralInfos[2].text = selectedCock.Gamecock.BreederName;

        Attributes[0].text = selectedCock.Gamecock.Attributes.FightingPeak.ToString();
        Attributes[1].text = selectedCock.Gamecock.Attributes.Gameness.ToString();
        Attributes[2].text = selectedCock.Gamecock.Attributes.Intellect.ToString();
        Attributes[3].text = selectedCock.Gamecock.Attributes.Power.ToString();
        Attributes[4].text = selectedCock.Gamecock.Attributes.Cut.ToString();
        Attributes[5].text = selectedCock.Gamecock.Attributes.SpeedAerial.ToString();
        Attributes[6].text = selectedCock.Gamecock.Attributes.SpeedGround.ToString();
        Attributes[7].text = selectedCock.Gamecock.Attributes.Pedigree.ToString();
        Attributes[8].text = selectedCock.Gamecock.Attributes.Instinct.ToString();
        Attributes[9].text = selectedCock.Gamecock.Attributes.BaseWeight.ToString();
        Attributes.ForEach(i => i.GetComponent<InputChangeDetector>().Changed = false);
        MoveCards.Init(selectedCock);
    }

    public GamecockObject SelectedCock()
    {
        return selectedCock;
    }

    public void CreateNewCock()
    {
        GamecockObject newCock = new GamecockObject();
        newCock.Gamecock = new Gamecock();
        newCock.Gamecock.Style = new GCStyle();
        newCock.Gamecock.Attributes = new GCAttributes();
        newCock.Autofill();
        cocks.Add(newCock);

        Dropdown.OptionData option = new Dropdown.OptionData();
        option.text = newCock.Gamecock.BreedName;
        CockListDropDown.options.Add(option);
        CockListDropDown.captionText.text = newCock.Gamecock.BreedName;

        RevertToDefault();

        string currentCockSave = Newtonsoft.Json.JsonConvert.SerializeObject(newCock.Gamecock);
        KeyData saveCockData = new KeyData(newCock.Gamecock.UniqueId + "-gamecock", currentCockSave);
        KeyHolder.AddThenSaveKey(saveCockData);
    }

    public void RemoveCock()
    {
        KeyHolder.RemoveKey(selectedCock.Gamecock.UniqueId + "-gamecock");
        CockListDropDown.options.RemoveAt(CockListDropDown.options.FindIndex(o => o.text == selectedCock.Gamecock.BreedName));
        cocks.RemoveAt(cocks.FindIndex(c => c.Gamecock.UniqueId == selectedCock.Gamecock.UniqueId));
        if (cocks != null)
        {
            selectedCock = cocks[0];
            CockListDropDown.captionText.text = cocks[0].Gamecock.BreedName;
            RevertToDefault();
        }
    }

    public void SaveNotes()
    {
        KeyHolder.AddThenSaveKey(new KeyData(selectedCock.Gamecock.UniqueId + "-notes", NotesField.text));
    }

    public void Save()
    {
        if (CockListDropDown.captionText.text == selectedCock.Gamecock.BreedName)
        {
            selectedCock.Gamecock.Name = GeneralInfos[0].text;
            selectedCock.Gamecock.BreedName = GeneralInfos[1].text;
            selectedCock.Gamecock.BreederName = GeneralInfos[2].text;

            selectedCock.Gamecock.Attributes.FightingPeak = Int32.Parse(Attributes[0].text);
            selectedCock.Gamecock.Attributes.Gameness = Int32.Parse(Attributes[1].text);
            selectedCock.Gamecock.Attributes.Intellect = Int32.Parse(Attributes[2].text);
            selectedCock.Gamecock.Attributes.Power = Int32.Parse(Attributes[3].text);
            selectedCock.Gamecock.Attributes.Cut = Int32.Parse(Attributes[4].text);
            selectedCock.Gamecock.Attributes.SpeedAerial = Int32.Parse(Attributes[5].text);
            selectedCock.Gamecock.Attributes.SpeedGround = Int32.Parse(Attributes[6].text);
            selectedCock.Gamecock.Attributes.Pedigree = Int32.Parse(Attributes[7].text);
            selectedCock.Gamecock.Attributes.Instinct = Int32.Parse(Attributes[8].text);
            selectedCock.Gamecock.Attributes.BaseWeight = Int32.Parse(Attributes[9].text);
        }
        string currentCockSave = Newtonsoft.Json.JsonConvert.SerializeObject(selectedCock.Gamecock);
        KeyData saveCockData = new KeyData(selectedCock.Gamecock.UniqueId + "-gamecock", currentCockSave);
        KeyHolder.AddThenSaveKey(saveCockData);
        MoveCards.SaveValues();
        updateDropDown(CockListDropDown);
        CockListDropDown.captionText.text = selectedCock.Gamecock.BreedName;
    }

    public void UpdateCopyList()
    {
        CopyFromExisting.ClearOptions();
        updateDropDown(CopyFromExisting);
        CopyFromExisting.captionText.text = CopyFromExisting.options[0].text;
    }

    public void CopyAndCreate()
    {
        GamecockObject currentCock = cocks.Find(c => c.Gamecock.BreedName == CopyFromExisting.captionText.text);
        GamecockObject newCock = new GamecockObject();
        newCock.Gamecock = new Gamecock();
        newCock.Gamecock.Style = new GCStyle();
        newCock.Gamecock.Attributes = new GCAttributes();
        newCock.Autofill();
        newCock.Gamecock.Name = currentCock.Gamecock.Name + "-copy";
        newCock.Gamecock.BreedName = currentCock.Gamecock.BreedName + "-copy";
        newCock.Gamecock.BreederName = currentCock.Gamecock.BreederName + "-copy";

        newCock.Gamecock.Attributes.FightingPeak = currentCock.Gamecock.Attributes.FightingPeak;
        newCock.Gamecock.Attributes.Gameness = currentCock.Gamecock.Attributes.Gameness;
        newCock.Gamecock.Attributes.Intellect = currentCock.Gamecock.Attributes.Intellect;
        newCock.Gamecock.Attributes.Power = currentCock.Gamecock.Attributes.Power;
        newCock.Gamecock.Attributes.Cut = currentCock.Gamecock.Attributes.Cut;
        newCock.Gamecock.Attributes.SpeedAerial = currentCock.Gamecock.Attributes.SpeedAerial;
        newCock.Gamecock.Attributes.SpeedGround = currentCock.Gamecock.Attributes.SpeedGround;
        newCock.Gamecock.Attributes.Pedigree = currentCock.Gamecock.Attributes.Pedigree;
        newCock.Gamecock.Attributes.Instinct = currentCock.Gamecock.Attributes.Instinct;
        newCock.Gamecock.Attributes.BaseWeight = currentCock.Gamecock.Attributes.BaseWeight;

        List<GCMoveCard> moveCards = new List<GCMoveCard> { };

        foreach (GCMoveCard mco in currentCock.Gamecock.MoveCards)
        {
            moveCards.Add(new GCMoveCard
            {
                Name = mco.Name + "-copy",
                Weight = mco.Weight,
                Move1 = GCMove.DeepCopy(mco.Move1),
                Move2 = GCMove.DeepCopy(mco.Move2)
            });
        }

        newCock.Gamecock.MoveCards = moveCards.ToArray();
        cocks.Add(newCock);

        Dropdown.OptionData option = new Dropdown.OptionData();
        option.text = newCock.Gamecock.BreedName;
        CockListDropDown.options.Add(option);
        CockListDropDown.captionText.text = newCock.Gamecock.BreedName;

        RevertToDefault();

        string currentCockSave = Newtonsoft.Json.JsonConvert.SerializeObject(newCock.Gamecock);
        KeyData saveCockData = new KeyData(newCock.Gamecock.UniqueId + "-gamecock", currentCockSave);
        KeyHolder.AddThenSaveKey(saveCockData);
    }
}
