﻿using System.Collections;
using NewMeta;

public static class ValueMapper
{
    public static float Map(this Attributes a, float value, float fromlow, float fromhigh, float tolow, float tohigh)
    {
        return tolow + (value - fromlow) * (tohigh - tolow) / (fromhigh - fromlow);
    }
    
    public static float Map(float value, float fromlow, float fromhigh, float tolow, float tohigh)
    {
        return tolow + (value - fromlow) * (tohigh - tolow) / (fromhigh - fromlow);
    }
}
