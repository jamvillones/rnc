﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FullInspector;
using System.Linq;

public class UpdateFromResources : BaseScriptableObject
{
    [InspectorButton, InspectorOrder(1.0)]
    public void SaveResourcesToPrefs()
    {
        List<GamecockObject> cocks = new List<GamecockObject>();
        cocks = Resources.LoadAll("Gamecocks", typeof(GamecockObject)).Cast<GamecockObject>().ToList();
        foreach (GamecockObject cock in cocks)
        {
            string currentCockSave = Newtonsoft.Json.JsonConvert.SerializeObject(cock.Gamecock);
            KeyData saveCockData = new KeyData(cock.Gamecock.UniqueId + "-gamecock", currentCockSave);
            KeyHolder.AddThenSaveKey(saveCockData);

            foreach (GCMoveCard mc in cock.Gamecock.MoveCards)
            {
                string currentMoveCardSave = Newtonsoft.Json.JsonConvert.SerializeObject(mc);
                KeyData saveMoveData = new KeyData(cock.Gamecock.UniqueId + mc.UniqueId + "-movecard", currentMoveCardSave);
                KeyHolder.AddThenSaveKey(saveMoveData);
            }
        }
    }
}
