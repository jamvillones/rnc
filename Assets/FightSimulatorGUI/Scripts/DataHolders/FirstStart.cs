﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstStart : MonoBehaviour
{
    void Start()
    {
        if (!PlayerPrefs.HasKey("firstStart"))
        {
            PlayerPrefs.DeleteAll();
            KeyHolder.SaveGameCockResourcesToPrefs();
            PlayerPrefs.SetInt("firstStart", 1);
        }
    }
}
