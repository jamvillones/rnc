﻿using FullSerializer;
using Newtonsoft.Json;
using J = Newtonsoft.Json.JsonPropertyAttribute;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public static class KeyHolder
{
    public static List<KeyData> SavedKeys = new List<KeyData>();

    public static void AddThenSaveKey(KeyData key)
    {
        if (!SavedKeys.Exists(s => s.UniqueId == key.UniqueId))
        {
            if (key.UniqueId == "")
                key.UniqueId = Guid.NewGuid().ToString();
            SavedKeys.Add(key);
        }
        else
            OverwriteKey(key);
        PlayerPrefs.SetString("MasterSaveList", JsonHelper.ToJson(SavedKeys.ToArray()));
    }

    public static void OverwriteKey(KeyData key)
    {
        if (SavedKeys.Exists(s => s.UniqueId == key.UniqueId))
            SavedKeys[SavedKeys.FindIndex(s => s.UniqueId == key.UniqueId)] = key;

        PlayerPrefs.SetString("MasterSaveList", JsonHelper.ToJson(SavedKeys.ToArray()));
    }

    public static KeyData GetKey(string id)
    {
        if (PlayerPrefs.HasKey("MasterSaveList"))
            SavedKeys = JsonHelper.FromJson<KeyData>(PlayerPrefs.GetString("MasterSaveList")).ToList();

        return SavedKeys.Exists(s => s.UniqueId == id) ? SavedKeys[SavedKeys.FindIndex(s => s.UniqueId == id)] : null;
    }

    public static KeyData[] GetKeysByExtension(string extension)
    {
        if (PlayerPrefs.HasKey("MasterSaveList"))
            SavedKeys = JsonHelper.FromJson<KeyData>(PlayerPrefs.GetString("MasterSaveList")).ToList();

        List<KeyData> matchedObjects = new List<KeyData>();

        foreach (KeyData key in SavedKeys)
        {
            int counter = 0;
            int j = 0;
            for (int i = (key.UniqueId.Length - extension.Length); i < key.UniqueId.Length; i++)
            {
                if (key.UniqueId[i] == extension[j])
                    counter++;
                j++;
            }
            if (counter >= extension.Length)
                matchedObjects.Add(key);
        }
        return matchedObjects.ToArray();
    }

    public static KeyData[] GetKeys(string searchTerm)
    {
        if (PlayerPrefs.HasKey("MasterSaveList"))
            SavedKeys = JsonHelper.FromJson<KeyData>(PlayerPrefs.GetString("MasterSaveList")).ToList();

        return SavedKeys.Where(k => k.UniqueId == searchTerm).ToArray();
    }

    public static bool KeyExists(string id)
    {
        return SavedKeys.Exists(s => s.UniqueId == id);
    }

    public static void SaveGameCockResourcesToPrefs()
    {
        List<GamecockObject> cocks = new List<GamecockObject>();
        cocks = Resources.LoadAll("Gamecocks", typeof(GamecockObject)).Cast<GamecockObject>().ToList();

        if (cocks == null)
        {
            throw new Exception("Where are the cocks tho");
        }
        foreach (GamecockObject cock in cocks)
        {
            string currentCockSave = Newtonsoft.Json.JsonConvert.SerializeObject(cock.Gamecock);
            KeyData saveCockData = new KeyData(cock.Gamecock.UniqueId + "-gamecock", currentCockSave);
            KeyHolder.AddThenSaveKey(saveCockData);

            foreach (GCMoveCard mc in cock.Gamecock.MoveCards)
            {
                string currentMoveCardSave = Newtonsoft.Json.JsonConvert.SerializeObject(mc);
                KeyData saveMoveData = new KeyData(cock.Gamecock.UniqueId + mc.UniqueId + "-movecard", currentMoveCardSave);
                KeyHolder.AddThenSaveKey(saveMoveData);
            }
        }

        if (KeyHolder.GetKeysByExtension("-movecard").Length <= 0)
        {
            foreach (GamecockObject cock in cocks)
            {
                cock.Autofill();
            }
        }
    }

    public static void RemoveKey(string id)
    {
        SavedKeys.RemoveAt(SavedKeys.FindIndex(k => k.UniqueId == id));
        PlayerPrefs.SetString("MasterSaveList", JsonHelper.ToJson(SavedKeys.ToArray()));
    }
}

[Serializable]
public class KeyData
{
    public KeyData(string id, string jsonString)
    {
        UniqueId = id;
        JsonString = jsonString;
    }
    public string UniqueId;
    public string JsonString;
}
