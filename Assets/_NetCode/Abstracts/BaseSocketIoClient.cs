﻿using System;

/// <summary>
/// Socket.IO base class.
/// </summary>
public abstract class BaseSocketIoClient : ISocketIoClient
{
    #region Delegates
    public delegate void SocketIoClientEventHandler (object data);
    #endregion

    #region Fields
    private string _baseUrl;
    private string _username;
    public SocketIoClientEventHandler On_Connect;
    public SocketIoClientEventHandler On_ConnectError;
    public SocketIoClientEventHandler On_ConnectTimeout;
    public SocketIoClientEventHandler On_Disconnect;
    public SocketIoClientEventHandler On_Error;
    public SocketIoClientEventHandler On_Message;
    public SocketIoClientEventHandler On_Reconnect;
    public SocketIoClientEventHandler On_ReconnectAttempt;
    public SocketIoClientEventHandler On_ReconnectError;
    public SocketIoClientEventHandler On_ReconnectFailed;
    public SocketIoClientEventHandler On_Reconnecting;
    #endregion

    #region Properties
    public string BaseUrl
    {
        get { return _baseUrl; }
        set { _baseUrl = value; }
    }

    public string Username
    {
        get { return _username; }
        set { _username = value; }
    }

    public abstract bool Initialized { get; }
    #endregion

    #region Constructor
    public BaseSocketIoClient() { }

    public BaseSocketIoClient (string baseUrl)
    {
        _baseUrl = baseUrl;
    }
    #endregion

    #region Abstract Methods
    public abstract bool Validate ();
    public abstract bool Connect ();
    public abstract void Disconnect ();
    public abstract bool Emit (string eventString);
    public abstract bool Emit (string eventString, params object[] args);
    public abstract void Off (string eventString);
    public abstract bool On (string eventString, Action action);
    public abstract bool On (string eventString, Action<object> action);
    #endregion

    #region Virtual Methods
    protected virtual void OnConnect (object data)
    {
        if (On_Connect != null)
            On_Connect(data);
    }

    protected virtual void OnConnectError (object data)
    {
        if (On_ConnectError != null)
            On_ConnectError(data);
    }

    protected virtual void OnConnectTimeout (object data)
    {
        if (On_ConnectTimeout != null)
            On_ConnectTimeout(data);
    }

    protected virtual void OnDisconnect (object data)
    {
        if (On_Disconnect != null)
            On_Disconnect(data);
    }

    protected virtual void OnError (object data)
    {
        if (On_Error != null)
            On_Error(data);
    }

    protected virtual void OnMessage (object data)
    {
        if (On_Message != null)
            On_Message(data);
    }

    protected virtual void OnReconnect (object data)
    {
        if (On_Reconnect != null)
            On_Reconnect(data);
    }

    protected virtual void OnReconnectAttempt (object data)
    {
        if (On_ReconnectAttempt != null)
            On_ReconnectAttempt(data);
    }

    protected virtual void OnReconnectError (object data)
    {
        if (On_ReconnectError != null)
            On_ReconnectError(data);
    }

    protected virtual void OnReconnectFailed (object data)
    {
        if (On_ReconnectFailed != null)
            On_ReconnectFailed(data);
    }

    protected virtual void OnReconnecting (object data)
    {
        if (On_Reconnecting != null)
            On_Reconnecting(data);
    }
    #endregion
}
