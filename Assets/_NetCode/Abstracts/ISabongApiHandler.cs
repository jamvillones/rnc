﻿using System;
using UnityEngine;

public interface ISabongApiHandler
{
    #region Properties
    string BaseUrl { get; set; }
    string UserId { get; set; }
    string AccessToken { get; set; }
    #endregion

    #region Methods
    bool Validate ();
    void DownloadTexture (string url, Action<bool, Texture> callback);
    void Authenticate (string username, string password);
    void GetAllArenas (Action<bool, Arena[], XSimpleResponse> callback);
    void GetAllDerbies (Action<bool, Derby[], XSimpleResponse> callback);
    void GetArena (string arenaId, Action<bool, Arena, XSimpleResponse> callback);
    void GetDerby (string derbyId, Action<bool, Derby, XSimpleResponse> callback);
    void GetFight (string fightId, Action<bool, GCFight, XSimpleResponse> callback);
    void GetFightResult (string fightId, Action<bool, GCFightData, XSimpleResponse> callback);
    void GetActiveDerby (string arenaId, Action<bool, Derby, XSimpleResponse> callback);
    void GetDerbyFights (string derbyId, Action<bool, GCFight[], XSimpleResponse> callback);
    void GetPlayerPayouts(string playerId, Action<bool, PlayerPayout[], XSimpleResponse> callback);
    void GetEventMatchJoined (string eventId, string playerId, Action<bool, XSimpleResponse> callback);
    void GetMatchFinished (string eventMatchId, Action<bool, XSimpleResponse> callback);
    void GetVerifyToken(string playerId, Action<bool, XSimpleResponse> callback);
    void GetAllMatchHistory(string playerId, Action<bool, PlayerHistory[], XSimpleResponse> callback);
    void GetArenaSetting (Action<bool, ArenaSetting[], XSimpleResponse> callback);
    void GetAdvertisement(Action<bool, Advertisement[], XSimpleResponse> callback);
    void GetVersion(string version, Action<bool, XSimpleResponse> callback);
    void PostLogin(string username, string password, bool isFacebook, Action<bool, LoginResponse> callback);
    void PostFBLogin(string fbID, Action<bool, LoginResponse, XSimpleResponse> callback);
    void PostRegister(string username, string password, string email, string firstName, string middleName, string lastName, string phoneNumber, string birthday, Action<bool, XSimpleResponse> callback);
    void PostFacebookRegister (string facebookId, string facebookToken, string firstName, string middleName, string lastName, string phoneNumber, string birthday, string languages, Action<bool, XSimpleResponse> callback);
    void PostFightBet (string fightId, int gamecock1Bet, int gamecock2Bet, Action<bool, XSimpleResponse> callback);
    void PostGoldEarnings (int amount, string playerId, Action<bool, XSimpleResponse> callback);
    void PostGoldLosses (int amount, string playerId, Action<bool, XSimpleResponse> callback);
    void PostExperienceGain (int experience, string playerId, Action<bool, XSimpleResponse> callback);
    void PostIncrementLevel (int levels, string playerId, Action<bool, XSimpleResponse> callback);
    void PostUpdateCoin (int amount, string playerId, bool isAdd, Action<bool, XSimpleResponse> callback);
    void PostJoinEvent (string eventId, string playerId, Action<bool, XSimpleResponse> callback);
    void PostUpdatePayout (string eventMatchId, string playerId, int payout, Action<bool, XPayoutResponse, XSimpleResponse> callback);
    void PostClaimPayout (string payoutId, string playerId, Action<bool, XSimpleResponse> callback);
    void PostClaimLoginReward (string playerId, int amount, string date, Action<bool, XSimpleResponse> callback);
    void PostMatchHistory (string playerId, string eventMatchId, int blueOdds, int redOdds, int blueBet, int redBet, int blueWinnings, int redWinnings, int experienceGained, Action<bool, XSimpleResponse> callback);
    void PostFinishMatch (string eventMatchId, Action<bool, XSimpleResponse> callback);
    void PostFinishEvent (string eventId, Action<bool, XSimpleResponse> callback);
    void PostRefreshToken (Action<bool, XToken, XSimpleResponse> callback);
    #endregion
}
