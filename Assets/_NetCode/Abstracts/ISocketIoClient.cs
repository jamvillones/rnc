﻿using System;

public interface ISocketIoClient
{
    #region Properties
    string BaseUrl { get; set; }
    string Username { get; set; }
    bool Initialized { get; }
    #endregion

    #region Methods
    bool Validate ();
    bool Connect ();
    void Disconnect ();
    bool Emit (string eventString);
    bool Emit (string eventString, params object[] args);
    void Off (string eventString);
    bool On (string eventString, Action action);
    bool On (string eventString, Action<object> action);
    #endregion
}
