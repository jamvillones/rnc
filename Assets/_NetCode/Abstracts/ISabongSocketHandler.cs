﻿using System;

public delegate void OnMatchUpdateEvent (SocketIOMatchUpdate matchUpdate);
public delegate void OnUnautheticatedPlayerEvent ();

public interface ISabongSocketHandler : ISocketIoClient
{
    #region Properties
    bool IsConnected { get; set; }
    SocketIOUser CurrentUser { get; set; }
    SocketIOMatch CurrentMatch { get; set; }
    SocketIOBet LastBet { get; set; }
    OnMatchUpdateEvent OnMatchUpdateEvent { get; set; }
    OnUnautheticatedPlayerEvent OnUnautheticatedPlayerEvent { get; set; }
    #endregion

    #region Methods
    void OnAuthSuccess (object data);
    void OnAuthFailed (object data);
    void OnErrorCreatingMatch (object data);
    void OnErrorJoiningMatch (object data);
    void OnJoinMatchSuccess (object data);
    void OnUnauthenticatedPlayer (object data);
    void OnBetErrorDatabase (object data);
    void OnBetErrorPlayerNotInMatch (object data);
    void OnBetErrorUnknownMatch (object data);
    void OnTotalPlayerBet (object data);
    void OnBetErrorBettingEnded(object data);

    bool EmitJoinGame (string username, string accessToken,
        Action<bool, string> callback = null);

    bool EmitJoinMatch (string eventMatchID, string eventID, string matchName,
        string firstRoosterID, string secondRoosterID,
        string startTime, string endTime, string duration,
        string matchWinner,
        string createdDate, string modifiedDate,
        bool active, bool deleted, bool isFinished,
        Action<bool, string> callback = null,
        Action<SocketIOMatchUpdate> continuousCallback = null);

    bool EmitNewBet (string eventMatchID,
        int firstRooster, int secondRooster, int phase,
        Action<bool, string> callback = null);

    bool EmitGetPayout (string eventMatchID);

   // bool EmitMatchBets(string eventMatchID);
    #endregion
}
