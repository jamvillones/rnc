﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;

public class SabongSocketIoClient : QuobjectSocketIoClient, ISabongSocketHandler
{
    #region Fields
    private Action<bool, string> _onJoinGameFinished;
    private Action<bool, string> _onJoinMatchFinished;
    private Action<bool, string> _onNewBetFinished;
    private Action<SocketIOMatchUpdate> _onMatchUpdate;
    private OnMatchUpdateEvent _onMatchUpdateEvent = null;
    private OnUnautheticatedPlayerEvent _onUnautheticatedPlayerEvent = null;
    #endregion

    #region Properties
    public bool IsConnected { get; set; }
    public bool Authenticated { get; set; }
    public SocketIOUser CurrentUser { get; set; }
    public SocketIOMatch CurrentMatch { get; set; }
    public SocketIOBet LastBet { get; set; }
    public OnMatchUpdateEvent OnMatchUpdateEvent { get { return _onMatchUpdateEvent; } set { _onMatchUpdateEvent = value; } }
    public OnUnautheticatedPlayerEvent OnUnautheticatedPlayerEvent { get { return _onUnautheticatedPlayerEvent; } set { _onUnautheticatedPlayerEvent = value; } }

    public SocketIOUser PendingUser { get; set; }
    public SocketIOMatch PendingMatch { get; set; }
    public SocketIOBet PendingBet { get; set; }
    #endregion

    #region Constructor
    public SabongSocketIoClient (string baseUrl) : base(baseUrl) { }
    #endregion

    #region Override Methods
    public override bool Connect ()
    {
        bool result = base.Connect();

        if (result)
        {
            On("authSuccess", OnAuthSuccess);
            On("authFailed", OnAuthFailed);
            On("errorCreatingMatch", OnErrorCreatingMatch);
            On("errorJoiningMatch", OnErrorJoiningMatch);
            On("joinMatchSuccess", OnJoinMatchSuccess);
            On("unauthenticatedPlayer", OnUnauthenticatedPlayer);
            On("betError_DBError", OnBetErrorDatabase);
            On("betError_PlayerNotInMatch", OnBetErrorPlayerNotInMatch);
            On("betError_UnknownMatch", OnBetErrorUnknownMatch);
            On("betError_BettingEnded", OnBetErrorBettingEnded);
            On("total_player_bet", OnTotalPlayerBet);
            //On("playerMatchBets", OnPlayerMatchBets);
            On_Connect = OnConnected;
        }

        return result;
    }
    #endregion

    #region Methods
    private void MainThreadDispatch (Action action)
    {
        UnityMainThreadDispatcher.Instance().Enqueue(action);
    }

    public void OnConnected (object data)
    {
        IsConnected = true;

        Debug.Log("Connected");

        //if (UIManager.Instance != null)
        //    UIManager.Instance.isConnectedToSocket = true;

        // Auto-login the user.
        //if (!Username.IsNullOrEmpty())
        //    EmitJoinGame(Username);
    }

    public void OnAuthSuccess (object data)
    {
        Authenticated = true;
        CurrentUser = PendingUser;
        PendingUser = null;
        Username = CurrentUser.username;

        if (_onJoinGameFinished != null)
        {
            MainThreadDispatch(() =>
            {
                _onJoinGameFinished(true, "OnAuthSuccess");
                _onJoinGameFinished = null;
            });
            
        }
    }

    public void OnAuthFailed (object data)
    {
        Authenticated = false;
        CurrentUser = null;
        PendingUser = null;

        if (_onJoinGameFinished != null)
        {
            MainThreadDispatch(() =>
            {
                _onJoinGameFinished(false, "OnAuthFailed");
                _onJoinGameFinished = null;
            });
        }
    }

    public void OnErrorCreatingMatch (object data)
    {
        CurrentMatch = null;
        PendingMatch = null;

        if (_onJoinMatchFinished != null)
        {
            MainThreadDispatch(() =>
            {
                _onJoinMatchFinished(false, "Error Creating Match");
                _onJoinMatchFinished = null;
            });
        }

        if (_onMatchUpdate != null)
        {
            MainThreadDispatch(() => Debug.LogWarning("Error Creating Match"));

            _onMatchUpdate = null;
        }
    }

    public void OnErrorJoiningMatch (object data)
    {
        CurrentMatch = null;
        PendingMatch = null;

        if (_onJoinMatchFinished != null)
        {
            MainThreadDispatch(() =>
            {
                _onJoinMatchFinished(false, "Error Joining Match");
                _onJoinMatchFinished = null;
            });
        }

        if (_onMatchUpdate != null)
        {
            MainThreadDispatch(() => Debug.LogWarning("Error Joining Match"));

            _onMatchUpdate = null;
        }
    }

    public void OnJoinMatchSuccess (object data)
    {
        CurrentMatch = PendingMatch;
        PendingMatch = null;

        if (_onJoinMatchFinished != null)
        {
            MainThreadDispatch(() =>
            {
                _onJoinMatchFinished(true, "OnJoinMatchSuccess");
                _onJoinMatchFinished = null;
            });
        }
    }

    public void OnUnauthenticatedPlayer (object data)
    {
        PendingBet = null;

        if (_onNewBetFinished != null)
        {
            MainThreadDispatch(() =>
            {
                _onNewBetFinished(false, "Unauthenticated Player");
                _onNewBetFinished = null;
            });
        }

        if (_onMatchUpdate != null)
        {
            MainThreadDispatch(() => Debug.LogWarning("Unauthenticated Player"));
            _onMatchUpdate = null;
        }

        if (_onUnautheticatedPlayerEvent != null)
        {
            MainThreadDispatch(() =>
            {
                _onUnautheticatedPlayerEvent();
            });
        }
    }

    public void OnBetErrorDatabase (object data)
    {
        PendingBet = null;

        if (_onNewBetFinished != null)
        {
            MainThreadDispatch(() =>
            {
                _onNewBetFinished(false, "Database Error");
                _onNewBetFinished = null;
            });
        }

        if (_onMatchUpdate != null)
        {
            MainThreadDispatch(() => Debug.LogWarning("Database Error"));

            _onMatchUpdate = null;
        }
    }

    public void OnBetErrorPlayerNotInMatch (object data)
    {
        PendingBet = null;

        if (_onNewBetFinished != null)
        {
            MainThreadDispatch(() =>
            {
                _onNewBetFinished(false, "Player Not In Match");
                _onNewBetFinished = null;
            });
        }

        if (_onMatchUpdate != null)
        {
            MainThreadDispatch(() => Debug.LogWarning("Player Not In Match"));

            _onMatchUpdate = null;
        }
    }

    public void OnBetErrorUnknownMatch (object data)
    {
        PendingBet = null;

        if (_onNewBetFinished != null)
        {
            MainThreadDispatch(() =>
            {
                _onNewBetFinished(false, "Unknown Match");
                _onNewBetFinished = null;
            });
        }

        if (_onMatchUpdate != null)
        {
            MainThreadDispatch(() => Debug.LogWarning("Unknown Match"));

            _onMatchUpdate = null;
        }
    }

    public void OnBetErrorBettingEnded(object data)
    {
        PendingBet = null;

        if (_onNewBetFinished != null)
        {
            MainThreadDispatch(() =>
            {
                _onNewBetFinished(false, "Betting Ended");
                _onNewBetFinished = null;
            });
        }

        if (_onMatchUpdate != null)
        {
            MainThreadDispatch(() => Debug.LogWarning("Betting Ended"));

            _onMatchUpdate = null;
        }
    }

    public void OnTotalPlayerBet (object data)
    {
        // HACK:
        if (PendingBet != null)
        {
            LastBet = PendingBet;
            PendingBet = null;
        }

        if (_onNewBetFinished != null)
        {
            MainThreadDispatch(() =>
            {
                _onNewBetFinished(true, "OnTotalPlayerBet");
                _onNewBetFinished = null;
            });
        }

        SocketIOMatchUpdate matchUpdate = JsonConvert.DeserializeObject<SocketIOMatchUpdate>(data.ToString());

        if (_onMatchUpdate != null)
        {
            MainThreadDispatch(() =>
            {
                _onMatchUpdate(matchUpdate);
            });
        }

        if (_onMatchUpdateEvent != null)
        {
            MainThreadDispatch(() =>
            {
                _onMatchUpdateEvent(matchUpdate);
            });
        }
    }

    //public void OnPlayerMatchBets(object data)
    //{
    //    SocketIOMatchUpdate matchUpdate = JsonConvert.DeserializeObject<SocketIOMatchUpdate>(data.ToString());

    //    if (_onMatchUpdate != null)
    //    {
    //        MainThreadDispatch(() =>
    //        {
    //            _onMatchUpdate(matchUpdate);
    //        });
    //    }

    //    if (_onMatchUpdateEvent != null)
    //    {
    //        MainThreadDispatch(() =>
    //        {
    //            _onMatchUpdateEvent(matchUpdate);
    //        });
    //    }
    //}

    public bool EmitJoinGame (string username, string accessToken,
        Action<bool, string> callback = null)
    {
        string emit = "join_game";
        SocketIOUser user = new SocketIOUser { username = username, Token = accessToken };

        bool result = Emit(emit, JObject.FromObject(user));

        if (result)
        {
            PendingUser = user;

            Debug.LogFormat("Joining Game: {0} : {1}.", user.username, user.Token);

            if (callback != null)
                _onJoinGameFinished = callback;

            return result;
        }

        if (callback != null)
            callback(false, "Socket.IO Error");

        return false;
    }

    public bool EmitJoinMatch (string eventMatchID, string eventID, string matchName,
        string firstRoosterID, string secondRoosterID, string startTime, string endTime,
        string duration, string matchWinner, string createdDate, string modifiedDate,
        bool active, bool deleted, bool isFinished,
        Action<bool, string> callback = null,
        Action<SocketIOMatchUpdate> continuousCallback = null)
    {
        string startTimeNew = string.Format("{0:yyyy-MM-dd HH:mm:ss}", DateTime.Parse(startTime));
        string endTimeNew = string.Format("{0:yyyy-MM-dd HH:mm:ss}", DateTime.Parse(endTime));

        string emit = "join_match";
        SocketIOMatch match = new SocketIOMatch
        {
            EventMatchID = eventMatchID,
            EventID = eventID,
            MatchName = matchName,
            FirstRoosterID = firstRoosterID,
            SecondRoosterID = secondRoosterID,
            StartTime = startTimeNew,
            EndTime = endTimeNew,
            Duration = duration,
            MatchWinner = matchWinner,
            CreatedDate = createdDate,
            ModifiedDate = modifiedDate,
            Active = (active ? 1 : 0),
            Deleted = (deleted ? 1 : 0),
            isFinished = (isFinished ? 1 : 0)
        };

        bool result = Emit(emit, JObject.FromObject(match));

        if (result)
        {
            PendingMatch = match;

            //Debug.LogFormat("Joining Match: {0}.", match.EventMatchID);

            if (callback != null)
                _onJoinMatchFinished = callback;

            if (continuousCallback != null)
                _onMatchUpdate = continuousCallback;

            return result;
        }

        if (callback != null)
            callback(false, "Socket.IO Error");

        return false;
    }

    public bool EmitNewBet (string eventMatchID, int firstRooster, int secondRooster, int phase,
        Action<bool, string> callback = null)
    {
        string emit = "new_bet";
        SocketIOBet bet = new SocketIOBet
        {
            EventMatchID = eventMatchID,
            FirstRooster = firstRooster,
            SecondRooster = secondRooster,
            Phase = phase
        };

        bool result = Emit(emit, JObject.FromObject(bet));

        if (result)
        {
            PendingBet = bet;

            Debug.LogFormat("Betting Match: {0}.", bet.EventMatchID);

            if (callback != null)
                _onNewBetFinished = callback;

            return result;
        }

        if (callback != null)
            callback(false, "Socket.IO Error");
        return false;
    }

    public bool EmitGetPayout (string eventMatchID)
    {
        string emit = "get_payout";

        bool result = Emit(emit, new JObject
        {
            new JProperty("EventMatchID", eventMatchID)
        });

        return result;
    }

    //public bool EmitMatchBets (string eventMatchID)
    //{
    //    string emit = "get_match_bets";

    //    bool result = Emit(emit, new JObject
    //    {
    //        new JProperty("EventMatchID", eventMatchID)
    //    });

    //    return result;
    //}
    #endregion
}
