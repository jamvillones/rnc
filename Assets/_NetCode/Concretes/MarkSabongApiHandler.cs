﻿using Newtonsoft.Json;
using System;
using System.Linq;
using UnityEngine;
using UnityEssentials;
using Newtonsoft.Json.Linq;

public class MarkSabongApiHandler : ISabongApiHandler
{
    #region Fields
    private string _baseUrl = "";
    private string _userId = "";
    private string _accessToken = "";
    private MonoBehaviour _monoBehaviour;
    #endregion

    #region Properties
    public string BaseUrl
    {
        get { return _baseUrl; }
        set { _baseUrl = value; }
    }

    public string UserId
    {
        get { return _userId; }
        set { _userId = value; }
    }

    public string AccessToken
    {
        get { return _accessToken; }
        set { _accessToken = value; }
    }

    public MonoBehaviour MonoBehaviour
    {
        get { return _monoBehaviour; }
        set { _monoBehaviour = value; }
    }
    #endregion

    #region Constructor
    public MarkSabongApiHandler () { }

    public MarkSabongApiHandler (string baseUrl, MonoBehaviour monoBehaviour)
    {
        BaseUrl = baseUrl;
        MonoBehaviour = monoBehaviour;
    }
    #endregion

    #region Methods
    private void MainThreadDispatch (Action action)
    {
        UnityMainThreadDispatcher.Instance().Enqueue(action);
    }

    public bool Validate ()
    {
        if (string.IsNullOrEmpty(BaseUrl))
        {
            Debug.LogWarning("BaseUrl missing.");
            return false;
        }

        if (MonoBehaviour == null)
        {
            Debug.LogWarning("MonoBehaviour missing.");
            return false;
        }

        return true;
    }

    public void DownloadTexture (string url, Action<bool, Texture> callback)
    {
        WWWRequest wwwRequest = new WWWRequest(url);

        if (callback != null)
        {
            wwwRequest.OnSuccessful = (www) => callback(true, www.texture);
            wwwRequest.OnFailed = (s) => callback(false, null);
        }

        wwwRequest.Request(MonoBehaviour);
    }

    public void Authenticate (string username, string password)
    {
        _userId = username;
        _accessToken = password;
    }

    public void GetAllArenas (Action<bool, Arena[], XSimpleResponse> callback)
    {
        if (!Validate())
            return;

        string url = string.Format("{0}/v1/arenas?token={1}", _baseUrl, _accessToken);

        WWWRequest wwwRequest = new WWWRequest(url);

        if (callback != null)
        {
            wwwRequest.OnSuccessful = (www) =>
            {
                try { callback(true, JsonConvert.DeserializeObject<Arena[]>(www.text), null); }
                catch (Exception ex) { callback(false, null, JsonConvert.DeserializeObject<XSimpleResponse>(www.text)); }
            };

            wwwRequest.OnFailed = (s) => callback(false, null, null);
        }

        wwwRequest.Request(MonoBehaviour);
    }

    public void GetAllDerbies (Action<bool, Derby[], XSimpleResponse> callback)
    {
        if (!Validate())
            return;

        string url = string.Format("{0}/v1/events?&token={1}", _baseUrl, _accessToken);
        //string url = string.Format("{0}/v1/events", _baseUrl);
        WWWRequest wwwRequest = new WWWRequest(url);

        if (callback != null)
        {
            wwwRequest.OnSuccessful = (www) =>
            {
                //Debug.Log(www.text);
                //callback(true, JsonConvert.DeserializeObject<GCFight[]>(www.text), null);

                bool successful = false;
                Derby[] events = null;
                XSimpleResponse simpleResponse = null;

                try
                {
                    events = JsonConvert.DeserializeObject<Derby[]>(www.text);
                    successful = true;
                }
                catch (Exception ex)
                {
                    simpleResponse = JsonConvert.DeserializeObject<XSimpleResponse>(www.text);
                }

                callback(successful, events, simpleResponse);
            };

            wwwRequest.OnFailed = (s) => callback(false, null, null);
        }
            wwwRequest.Request(MonoBehaviour);
    }

    public void GetArena (string arenaId, Action<bool, Arena, XSimpleResponse> callback)
    {
        if (!Validate())
            return;

        string url = string.Format("{0}/v1/getArena?arenaId={1}&_userId={2}&_accessToken={3}", _baseUrl, arenaId, _userId, _accessToken);
        WWWRequest wwwRequest = new WWWRequest(url);

        if (callback != null)
        {
            wwwRequest.OnSuccessful = (www) =>
            {
                try { callback(true, JsonConvert.DeserializeObject<Arena>(www.text), null); }
                catch (Exception ex) { callback(false, null, JsonConvert.DeserializeObject<XSimpleResponse>(www.text)); }
            };
            wwwRequest.OnFailed = (s) => callback(false, null, null);
        }

        wwwRequest.Request(MonoBehaviour);
    }

    public void GetDerby (string derbyId, Action<bool, Derby, XSimpleResponse> callback)
    {
        if (!Validate())
            return;

        string url = string.Format("{0}/v1/getDerby?derbyId={1}&_userId={2}&_accessToken={3}", _baseUrl, derbyId, _userId, _accessToken);
        WWWRequest wwwRequest = new WWWRequest(url);

        if (callback != null)
        {
            wwwRequest.OnSuccessful = (www) =>
            {
                try { callback(true, JsonConvert.DeserializeObject<Derby>(www.text), null); }
                catch (Exception ex) { callback(false, null, JsonConvert.DeserializeObject<XSimpleResponse>(www.text)); }
            };
            wwwRequest.OnFailed = (s) => callback(false, null, null);
        }

        wwwRequest.Request(MonoBehaviour);
    }

    public void GetFight (string fightId, Action<bool, GCFight, XSimpleResponse> callback)
    {
        if (!Validate())
            return;

        string url = string.Format("{0}/v1/getFight?fightId={1}&_userId={2}&_accessToken={3}", _baseUrl, fightId, _userId, _accessToken);
        WWWRequest wwwRequest = new WWWRequest(url);

        if (callback != null)
        {
            wwwRequest.OnSuccessful = (www) =>
            {
                try { callback(true, JsonConvert.DeserializeObject<GCFight>(www.text), null); }
                catch (Exception ex) { callback(false, null, JsonConvert.DeserializeObject<XSimpleResponse>(www.text)); }
            };
            wwwRequest.OnFailed = (s) => callback(false, null, null);
        }

        wwwRequest.Request(MonoBehaviour);
    }

    public void GetFightResult (string fightId, Action<bool, GCFightData, XSimpleResponse> callback)
    {
        if (!Validate())
            return;

        string url = string.Format("{0}/v1/match-result?matchId={1}&token={2}", _baseUrl, fightId, _accessToken);

        Debug.Log(url);
        WWWRequest wwwRequest = new WWWRequest(url);

        if (callback != null)
        {
            wwwRequest.OnSuccessful = (www) =>
            {
                try { callback(true, JsonConvert.DeserializeObject<GCFightData>(www.text), null); }
                catch (Exception ex) { callback(false, null, JsonConvert.DeserializeObject<XSimpleResponse>(www.text)); }
            };
            wwwRequest.OnFailed = (s) => callback(false, null, null);
        }

        wwwRequest.Request(MonoBehaviour);
    }

    public void GetActiveDerby (string arenaId, Action<bool, Derby, XSimpleResponse> callback)
    {
        if (!Validate())
            return;

        string url = string.Format("{0}/v1/getActiveDerby?arenaId={1}&_userId={2}&_accessToken={3}", _baseUrl, arenaId, _userId, _accessToken);
        WWWRequest wwwRequest = new WWWRequest(url);

        if (callback != null)
        {
            wwwRequest.OnSuccessful = (www) =>
            {
                try { callback(true, JsonConvert.DeserializeObject<Derby>(www.text), null); }
                catch (Exception ex) { callback(false, null, JsonConvert.DeserializeObject<XSimpleResponse>(www.text)); }
            };
            wwwRequest.OnFailed = (s) => callback(false, null, null);
        }

        wwwRequest.Request(MonoBehaviour);
    }

    public void GetDerbyFights (string derbyId, Action<bool, GCFight[], XSimpleResponse> callback)
    {
        if (!Validate())
            return;

        string url = string.Format("{0}/v1/event-matches?eventId={1}&token={2}", _baseUrl, derbyId, _accessToken);

        WWWRequest wwwRequest = new WWWRequest(url);

        if (callback != null)
        {
            wwwRequest.OnSuccessful = (www) =>
            {
                //Debug.Log(www.text);
                //callback(true, JsonConvert.DeserializeObject<GCFight[]>(www.text), null);

                bool successful = false;
                GCFight[] fights = null;
                XSimpleResponse simpleResponse = null;

                try
                {
                    fights = JsonConvert.DeserializeObject<GCFight[]>(www.text);
                    successful = true;
                }
                catch (Exception ex)
                {
                    simpleResponse = JsonConvert.DeserializeObject<XSimpleResponse>(www.text);
                }

                callback(successful, fights, simpleResponse);

                //try { callback(true, JsonConvert.DeserializeObject<GCFight[]>(www.text), null); }
                //catch (Exception ex) { Debug.Log(ex.Message); callback(false, null, JsonConvert.DeserializeObject<XSimpleResponse>(www.text)); }

                //MainThreadDispatch(() =>
                //{

                //});
            };
            wwwRequest.OnFailed = (s) => callback(false, null, null);
        }

        wwwRequest.Request(MonoBehaviour);
    }

    public void GetPlayerPayouts(string playerId, Action<bool, PlayerPayout[], XSimpleResponse> callback)
    {
        if (!Validate())
            return;

        string url = string.Format("{0}/v1/player/player-payouts?pid={1}&token={2}",
            _baseUrl,
            playerId,
            _accessToken);

        Debug.Log(url);

        WWWRequest wwwRequest = new WWWRequest(url);

        if (callback != null)
        {
            wwwRequest.OnSuccessful = (www) =>
            {
                try { callback(true, JsonConvert.DeserializeObject<PlayerPayout[]>(www.text), null); }
                catch (Exception ex) { callback(false, null, JsonConvert.DeserializeObject<XSimpleResponse>(www.text)); }
            };
            wwwRequest.OnFailed = (s) => callback(false, null, null);
        }

        wwwRequest.Request(MonoBehaviour);
    }

    public void GetEventMatchJoined (string eventId, string playerId, Action<bool, XSimpleResponse> callback)
    {
        if (!Validate())
            return;

        string url = string.Format("{0}/v1/player/check-event-joined?eid={1}&pid={2}&token={3}",
            _baseUrl,
            eventId,
            playerId, _accessToken);

        Debug.Log(url);

        WWWRequest wwwRequest = new WWWRequest(url);

        if (callback != null)
        {
            wwwRequest.OnSuccessful = (www) => callback(true, JsonConvert.DeserializeObject<XSimpleResponse>(www.text));
            wwwRequest.OnFailed = (s) => callback(false, null);
        }

        wwwRequest.Request(MonoBehaviour);
    }

    public void GetMatchFinished (string eventMatchId, Action<bool, XSimpleResponse> callback)
    {
        if (!Validate())
            return;

        string url = string.Format("{0}/v1/match/match-finished?mid={1}&token={2}",
            _baseUrl,
            eventMatchId, _accessToken);

        WWWRequest wwwRequest = new WWWRequest(url);

        if (callback != null)
        {
            wwwRequest.OnSuccessful = (www) => callback(true, JsonConvert.DeserializeObject<XSimpleResponse>(www.text));
            wwwRequest.OnFailed = (s) => callback(false, null);
        }

        wwwRequest.Request(MonoBehaviour);
    }

    public void GetVerifyToken (string playerId, Action<bool, XSimpleResponse> callback)
    {
        if (!Validate())
            return;

        string url = string.Format("{0}/v1/verify-token?token={1}&acctid={2}",
            _baseUrl,
            _accessToken,
            playerId);

        WWWRequest wwwRequest = new WWWRequest(url);

        if (callback != null)
        {
            wwwRequest.OnSuccessful = (www) => callback(true, JsonConvert.DeserializeObject<XSimpleResponse>(www.text));
            wwwRequest.OnFailed = (s) => callback(false, null);
        }

        wwwRequest.Request(MonoBehaviour);
    }

    public void GetAllMatchHistory(string playerId, Action<bool, PlayerHistory[], XSimpleResponse> callback)
    {
        if (!Validate())
            return;

        string url = string.Format("{0}/v1/player/all-match-history?pid={1}&token={2}",
            _baseUrl,
            playerId, _accessToken);

        WWWRequest wwwRequest = new WWWRequest(url);

        if (callback != null)
        {
            wwwRequest.OnSuccessful = (www) =>
            {

                bool successful = false;
                PlayerHistory[] player = null;
                XSimpleResponse simpleResponse = null;

                try
                {
                    player = JsonConvert.DeserializeObject<PlayerHistory[]>(www.text);
                    successful = true;
                }
                catch (Exception ex)
                {
                    simpleResponse = JsonConvert.DeserializeObject<XSimpleResponse>(www.text);
                }

                callback(successful, player, simpleResponse);
            };

            wwwRequest.OnFailed = (s) => callback(false, null, null);
        }


        //if (callback != null)
        //{
        //    wwwRequest.OnSuccessful = (www) =>
        //    {
        //        try { callback(true, JsonConvert.DeserializeObject<PlayerHistory[]>(www.text), null); }
        //        catch (Exception ex) { callback(false, null, JsonConvert.DeserializeObject<XSimpleResponse>(www.text)); }
        //    };
        //    wwwRequest.OnFailed = (s) => callback(false, null, null);
        //}

        wwwRequest.Request(MonoBehaviour);
    }

    public void GetArenaSetting (Action<bool, ArenaSetting[], XSimpleResponse> callback)
    {
        if (!Validate())
            return;

        string url = string.Format("{0}/v1/arena-setting?token={1}",
           _baseUrl,
           _accessToken);

        WWWRequest wwwRequest = new WWWRequest(url);

        if (callback != null)
        {
            wwwRequest.OnSuccessful = (www) =>
            {
                try { callback(true, JsonConvert.DeserializeObject<ArenaSetting[]>(www.text), null); }
                catch (Exception ex) { callback(false, null, JsonConvert.DeserializeObject<XSimpleResponse>(www.text)); }
            };
            wwwRequest.OnFailed = (s) => callback(false, null, null);
        }

        wwwRequest.Request(MonoBehaviour);
    }

    public void GetAdvertisement(Action<bool, Advertisement[], XSimpleResponse> callback)
    {
        if (!Validate())
            return;

        string url = string.Format("{0}/v1/advertisements?token={1}",
            _baseUrl,
            _accessToken);

        WWWRequest wwwRequest = new WWWRequest(url);

        if (callback != null)
        {
            wwwRequest.OnSuccessful = (www) =>
            {
                try { callback(true, JsonConvert.DeserializeObject<Advertisement[]>(www.text), null); }
                catch (Exception ex) { callback(false, null, JsonConvert.DeserializeObject<XSimpleResponse>(www.text)); }
            };
            wwwRequest.OnFailed = (s) => callback(false, null, null);
        }

        wwwRequest.Request(MonoBehaviour);
    }

    public void GetVersion(string version, Action<bool, XSimpleResponse> callback)
    {
        if (!Validate())
            return;

        string url = string.Format("{0}/check-android-version?ver={1}",
            _baseUrl,
            version);

        WWWRequest wwwRequest = new WWWRequest(url);

        if (callback != null)
        {
            wwwRequest.OnSuccessful = (www) => callback(true, JsonConvert.DeserializeObject<XSimpleResponse>(www.text));
            wwwRequest.OnFailed = (s) => callback(false, null);
        }

        wwwRequest.Request(MonoBehaviour);
    }

    public void PostLogin (string username, string password, bool isFacebook, Action<bool, LoginResponse> callback)
    {
        string url = string.Format("{0}/login?username={1}&password={2}&isFB={3}", _baseUrl, username, password, (isFacebook ? "1" : "0"));

        WWWRequest wwwRequest = new WWWRequest(url);
        wwwRequest.AddField("post", "true");

        if (callback != null)
        {
            wwwRequest.OnSuccessful = (www) => callback(true, JsonConvert.DeserializeObject<LoginResponse>(www.text)); 
            wwwRequest.OnFailed = (s) => callback(false, null);
        }

        wwwRequest.Request(MonoBehaviour);
    }

    public void PostFBLogin(string fbID, Action<bool, LoginResponse, XSimpleResponse> callback)
    {
        string url = string.Format("{0}/login?fbid={1}&isFB=1", _baseUrl, fbID);

        Debug.Log(url);

        WWWRequest wwwRequest = new WWWRequest(url);
        wwwRequest.AddField("post", "true");

        if (callback != null)
        {
            wwwRequest.OnSuccessful = (www) =>
            {
                try { callback(true, JsonConvert.DeserializeObject<LoginResponse>(www.text), null); }
                catch (Exception ex) { callback(false, null, JsonConvert.DeserializeObject<XSimpleResponse>(www.text)); }
            };
            wwwRequest.OnFailed = (s) => callback(false, null, null);
        }

        wwwRequest.Request(MonoBehaviour);
    }

    public void PostRegister (string username, string password, string email, string firstName, string middleName, string lastName, string phoneNumber, string birthday, Action<bool, XSimpleResponse> callback)
    {
        string url = string.Format("{0}/register?username={1}&email={2}&password={3}&fname={4}&mname={5}&lname={6}&phone={7}&bday={8}&isFB={9}&lang=English",
            _baseUrl, username, email, password, firstName, middleName, lastName, phoneNumber, birthday, "0");

        WWWRequest wwwRequest = new WWWRequest(url);
        wwwRequest.AddField("post", "true");

        if (callback != null)
        {
            wwwRequest.OnSuccessful = (www) => callback(true, JsonConvert.DeserializeObject<XSimpleResponse>(www.text));
            wwwRequest.OnFailed = (s) => callback(false, null);
        }

        wwwRequest.Request(MonoBehaviour);
    }

    public void PostFacebookRegister (string facebookId, string facebookToken, string firstName, string middleName, string lastName, string phoneNumber, string birthday, string languages, Action<bool, XSimpleResponse> callback)
    {
        if (languages == null || languages == string.Empty)
            languages = "English";

        string url = string.Format("{0}/register?fname={1}&fbid={2}&mname={3}&lname={4}&isFB={5}&fbtoken={6}&phone={7}&bday={8}&lang={9}",
            _baseUrl, firstName, facebookId, middleName, lastName, "1", facebookToken, phoneNumber, birthday, languages);
        
        WWWRequest wwwRequest = new WWWRequest(url);
        wwwRequest.AddField("post", "true");

        if (callback != null)
        {
            wwwRequest.OnSuccessful = (www) => callback(true, JsonConvert.DeserializeObject<XSimpleResponse>(www.text));
            wwwRequest.OnFailed = (s) => callback(false, null);
        }

        wwwRequest.Request(MonoBehaviour);
    }

    public void PostFightBet (string fightId, int gamecock1Bet, int gamecock2Bet, Action<bool, XSimpleResponse> callback)
    {
        if (!Validate())
            return;

        string url = string.Format("{0}/v1/postFightBet?fightId={1}&_userId={2}&_accessToken={3}", _baseUrl, fightId, _userId, _accessToken);
        WWWRequest wwwRequest = new WWWRequest(url);
        wwwRequest.AddField("deviceId", SystemInfo.deviceUniqueIdentifier);
        wwwRequest.AddField("timeStamp", DateTime.UtcNow.ToShortDateString());
        wwwRequest.AddField("gamecock1Bet", gamecock1Bet);
        wwwRequest.AddField("gamecock2Bet", gamecock2Bet);

        if (callback != null)
        {
            wwwRequest.OnSuccessful = (www) => callback(true, JsonConvert.DeserializeObject<XSimpleResponse>(www.text));
            wwwRequest.OnFailed = (s) => callback(false, null);
        }

        wwwRequest.Request(MonoBehaviour);
    }

    public void PostGoldEarnings (int amount, string playerId, Action<bool, XSimpleResponse> callback)
    {
        if (!Validate())
            return;

        string url = string.Format("{0}/v1/player/add-earning?pid={1}&coin={2}&token={3}",
            _baseUrl,
            playerId,
            amount, _accessToken);

        WWWRequest wwwRequest = new WWWRequest(url);
        wwwRequest.AddField("post", "true");

        if (callback != null)
        {
            wwwRequest.OnSuccessful = (www) => callback(true, JsonConvert.DeserializeObject<XSimpleResponse>(www.text));
            wwwRequest.OnFailed = (s) => callback(false, null);
        }

        wwwRequest.Request(MonoBehaviour);
    }

    public void PostGoldLosses (int amount, string playerId, Action<bool, XSimpleResponse> callback)
    {
        if (!Validate())
            return;

        string url = string.Format("{0}/v1/player/add-loss?pid={1}&coin={2}&token={3}",
            _baseUrl,
            playerId,
            amount, _accessToken);

        WWWRequest wwwRequest = new WWWRequest(url);
        wwwRequest.AddField("post", "true");

        if (callback != null)
        {
            wwwRequest.OnSuccessful = (www) => callback(true, JsonConvert.DeserializeObject<XSimpleResponse>(www.text));
            wwwRequest.OnFailed = (s) => callback(false, null);
        }

        wwwRequest.Request(MonoBehaviour);
    }

    public void PostExperienceGain (int experience, string playerId, Action<bool, XSimpleResponse> callback)
    {
        if (!Validate())
            return;

        string url = string.Format("{0}/v1/player/add-experience?pid={1}&exp={2}&token={3}",
            _baseUrl,
            playerId,
            experience, _accessToken);

        WWWRequest wwwRequest = new WWWRequest(url);
        wwwRequest.AddField("post", "true");

        if (callback != null)
        {
            wwwRequest.OnSuccessful = (www) => callback(true, JsonConvert.DeserializeObject<XSimpleResponse>(www.text));
            wwwRequest.OnFailed = (s) => callback(false, null);
        }

        wwwRequest.Request(MonoBehaviour);
    }

    public void PostIncrementLevel (int levels, string playerId, Action<bool, XSimpleResponse> callback)
    {
        if (!Validate())
            return;

        string url = string.Format("{0}/v1/player/add-level?pid={1}&lvl={2}&token={3}",
            _baseUrl,
            playerId,
            levels, _accessToken);

        WWWRequest wwwRequest = new WWWRequest(url);
        wwwRequest.AddField("post", "true");

        if (callback != null)
        {
            wwwRequest.OnSuccessful = (www) => callback(true, JsonConvert.DeserializeObject<XSimpleResponse>(www.text));
            wwwRequest.OnFailed = (s) => callback(false, null);
        }

        wwwRequest.Request(MonoBehaviour);
    }

    public void PostUpdateCoin (int amount, string playerId, bool isAdd, Action<bool, XSimpleResponse> callback)
    {
        if (!Validate())
            return;

        string url = string.Format("{0}/v1/player/update-coin?pid={1}&coin={2}&action={3}&token={4}",
            _baseUrl,
            playerId,
            amount,
            (isAdd ? "add" : "deduct"), _accessToken);
        
        WWWRequest wwwRequest = new WWWRequest(url);
        wwwRequest.AddField("post", "true");

        if (callback != null)
        {
            wwwRequest.OnSuccessful = (www) => callback(true, JsonConvert.DeserializeObject<XSimpleResponse>(www.text));
            wwwRequest.OnFailed = (s) => callback(false, null);
        }

        wwwRequest.Request(MonoBehaviour);
    }

    public void PostJoinEvent (string eventId, string playerId, Action<bool, XSimpleResponse> callback)
    {
        if (!Validate())
            return;

        string url = string.Format("{0}/v1/player/player-join-event?eid={1}&pid={2}&token={3}",
            _baseUrl,
            eventId,
            playerId, _accessToken);

        Debug.Log(url);

        WWWRequest wwwRequest = new WWWRequest(url);
        wwwRequest.AddField("post", "true");

        if (callback != null)
        {
            wwwRequest.OnSuccessful = (www) => callback(true, JsonConvert.DeserializeObject<XSimpleResponse>(www.text));
            wwwRequest.OnFailed = (s) => callback(false, null);
        }

        wwwRequest.Request(MonoBehaviour);
    }

    public void PostUpdatePayout (string eventMatchId, string playerId, int payout, Action<bool, XPayoutResponse, XSimpleResponse> callback)
    {
        if (!Validate())
            return;

        string url = string.Format("{0}/v1/player/payout?pid={1}&mid={2}&amt={3}&token={4}",
            _baseUrl,
            playerId,
            eventMatchId,
            payout, _accessToken);

        WWWRequest wwwRequest = new WWWRequest(url);
        wwwRequest.AddField("post", "true");

        if (callback != null)
        {
            wwwRequest.OnSuccessful = (www) =>
            {
                try { callback(true, JsonConvert.DeserializeObject<XPayoutResponse>(www.text), null); }
                catch (Exception ex) { callback(false, null, JsonConvert.DeserializeObject<XSimpleResponse>(www.text)); }
            };
            wwwRequest.OnFailed = (s) => callback(false, null, null);
        }

        wwwRequest.Request(MonoBehaviour);
    }

    public void PostClaimPayout (string payoutId, string playerId, Action<bool, XSimpleResponse> callback)
    {
        if (!Validate())
            return;

        string url = string.Format("{0}/v1/player/claim-payout?payid={1}&token={2}",
            _baseUrl,
            payoutId,
            _accessToken);

        WWWRequest wwwRequest = new WWWRequest(url);
        wwwRequest.AddField("post", "true");

        if (callback != null)
        {
            wwwRequest.OnSuccessful = (www) => callback(true, JsonConvert.DeserializeObject<XSimpleResponse>(www.text));
            wwwRequest.OnFailed = (s) => callback(false, null);
        }

        wwwRequest.Request(MonoBehaviour);
    }

    public void PostClaimLoginReward (string playerId, int amount, string date, Action<bool, XSimpleResponse> callback)
    {
        if (!Validate())
            return;

        string url = string.Format("{0}/v1/player/claim-login-reward?pid={1}&amt={2}&rdate={3}&token={4}",
            _baseUrl,
            playerId,
            amount,
            date,
            _accessToken);

        Debug.Log(url);

        WWWRequest wwwRequest = new WWWRequest(url);
        wwwRequest.AddField("post", "true");

        if (callback != null)
        {
            wwwRequest.OnSuccessful = (www) => callback(true, JsonConvert.DeserializeObject<XSimpleResponse>(www.text));
            wwwRequest.OnFailed = (s) => callback(false, null);
        }

        wwwRequest.Request(MonoBehaviour);
    }

    public void PostMatchHistory (string playerId, string eventMatchId,
        int blueOdds, int redOdds,
        int blueBet, int redBet,
        int blueWinnings, int redWinnings,
        int experienceGained, Action<bool, XSimpleResponse> callback)
    {
        if (!Validate())
            return;

        string url = string.Format("{0}/v1/player/match-history?pid={1}&mid={2}&bodds={3}&rodds={4}&bbet={5}&rbet={6}&bwin={7}&rwin={8}&exp={9}&token={10}",
            _baseUrl,
            playerId,
            eventMatchId,
            blueOdds,
            redOdds,
            blueBet,
            redBet,
            blueWinnings,
            redWinnings,
            experienceGained,
            _accessToken);

        WWWRequest wwwRequest = new WWWRequest(url);
        wwwRequest.AddField("post", "true");

        if (callback != null)
        {
            wwwRequest.OnSuccessful = (www) => callback(true, JsonConvert.DeserializeObject<XSimpleResponse>(www.text));
            wwwRequest.OnFailed = (s) => callback(false, null);
        }

        wwwRequest.Request(MonoBehaviour);
    }

    public void PostFinishMatch (string eventMatchId, Action<bool, XSimpleResponse> callback)
    {
        if (!Validate())
            return;

        string url = string.Format("{0}/v1/match/finish-match?mid={1}&token={2}",
            _baseUrl,
            eventMatchId, _accessToken);

        Debug.Log(url);

        WWWRequest wwwRequest = new WWWRequest(url);
        wwwRequest.AddField("post", "true");

        if (callback != null)
        {
            wwwRequest.OnSuccessful = (www) => callback(true, JsonConvert.DeserializeObject<XSimpleResponse>(www.text));
            wwwRequest.OnFailed = (s) => callback(false, null);
        }

        wwwRequest.Request(MonoBehaviour);
    }

    public void PostFinishEvent (string eventId, Action<bool, XSimpleResponse> callback)
    {
        if (!Validate())
            return;

        string url = string.Format("{0}/v1/event/finish-event?eid={1}&token={2}",
            _baseUrl,
            eventId,
            _accessToken);

        Debug.Log(url);

        WWWRequest wwwRequest = new WWWRequest(url);
        wwwRequest.AddField("post", "true");

        if (callback != null)
        {
            wwwRequest.OnSuccessful = (www) => callback(true, JsonConvert.DeserializeObject<XSimpleResponse>(www.text));
            wwwRequest.OnFailed = (s) => callback(false, null);
        }

        wwwRequest.Request(MonoBehaviour);
    }

    public void PostRefreshToken (Action<bool, XToken, XSimpleResponse> callback)
    {
        if (!Validate())
            return;

        string url = string.Format("{0}/refresh-token?token={1}",
            _baseUrl,
            _accessToken);

        WWWRequest wwwRequest = new WWWRequest(url);
        wwwRequest.AddField("post", "true");

        if (callback != null)
        {
            wwwRequest.OnSuccessful = (www) =>
            {
                try { callback(true, JsonConvert.DeserializeObject<XToken>(www.text), null); }
                catch (Exception ex) { callback(false, null, JsonConvert.DeserializeObject<XSimpleResponse>(www.text)); }
            };
            wwwRequest.OnFailed = (s) => callback(false, null, null);
        }

        wwwRequest.Request(MonoBehaviour);
    }
    #endregion
}
