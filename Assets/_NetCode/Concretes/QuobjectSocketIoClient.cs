﻿using Quobject.SocketIoClientDotNet.Client;
using System;
using UnityEngine;

/// <summary>
/// Socket.IO implementation of Quobject.
/// </summary>
public class QuobjectSocketIoClient : BaseSocketIoClient
{
    #region Fields
    protected Socket _socket;
    #endregion

    #region Properties
    public override bool Initialized
    {
        get { return (_socket != null); }
    }
    #endregion

    #region Constructor
    public QuobjectSocketIoClient (string baseUrl) : base(baseUrl) { }
    #endregion

    #region ISocketIoClient Methods
    public override bool Validate ()
    {
        if (string.IsNullOrEmpty(BaseUrl))
        {
            Debug.LogWarning("BaseUrl missing.");
            return false;
        }

        return true;
    }

    public override bool Connect ()
    {
        if (!Initialized && !string.IsNullOrEmpty(BaseUrl))
        {
            // Create Socket object.
            _socket = IO.Socket(BaseUrl);

            // Subscribe default Socket.IO events.
            _socket.On(Socket.EVENT_CONNECT, OnConnect);
            _socket.On(Socket.EVENT_CONNECT_ERROR, OnConnectError);
            _socket.On(Socket.EVENT_CONNECT_TIMEOUT, OnConnectTimeout);
            _socket.On(Socket.EVENT_DISCONNECT, OnDisconnect);
            _socket.On(Socket.EVENT_ERROR, OnError);
            _socket.On(Socket.EVENT_MESSAGE, OnMessage);
            _socket.On(Socket.EVENT_RECONNECT, OnReconnect);
            _socket.On(Socket.EVENT_RECONNECTING, OnReconnecting);
            _socket.On(Socket.EVENT_RECONNECT_ATTEMPT, OnReconnectAttempt);
            _socket.On(Socket.EVENT_RECONNECT_ERROR, OnReconnectError);
            _socket.On(Socket.EVENT_RECONNECT_FAILED, OnReconnectFailed);

            return true;
        }

        return false;
    }

    public override void Disconnect ()
    {
        if (Initialized)
            _socket.Disconnect();

        _socket = null;

        On_Connect = null;
        On_ConnectError = null;
        On_ConnectTimeout = null;
        On_Disconnect = null;
        On_Error = null;
        On_Message = null;
        On_Reconnect = null;
        On_ReconnectAttempt = null;
        On_ReconnectError = null;
        On_ReconnectFailed = null;
        On_Reconnecting = null;
    }

    public override bool Emit (string eventString)
    {
        if (!Initialized)
            return false;

        _socket.Emit(eventString);

        return true;
    }
    public override bool Emit (string eventString, params object[] args)
    {
        if (!Initialized)
            return false;

        _socket.Emit(eventString, args);

        return true;
    }

    public override void Off (string eventString)
    {
        if (Initialized)
            _socket.Off(eventString);
    }

    public override bool On (string eventString, Action action)
    {
        if (!Initialized)
            return false;

        _socket.On(eventString, action);

        return true;
    }
    public override bool On (string eventString, Action<object> action)
    {
        if (!Initialized)
            return false;

        _socket.On(eventString, action);

        return true;
    }
    #endregion
}
