﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TooltipManager : Core.Utilities.Singleton<TooltipManager>
{
    public Transform TooltipPrefab;
    public float Delay = 1;
    Transform tooltip;

    IEnumerator coroutine;

    private void OnEnable()
    {
        this.AddEventListenerGlobal<CursorEvent>(CursorEnteredCallback);
    }
    private void OnDisable()
    {
        this.RemoveEventListenerGlobal<CursorEvent>(CursorEnteredCallback);
    }

    private void CursorEnteredCallback(CursorEvent c)
    {
        Debug.Log("hey");
        if (c.IsOn)
        {
            coroutine = ShowTooltip(Delay, c);
            StartCoroutine(coroutine);
        }
        else if(!c.IsOn)
        {
            if (coroutine != null)
                StopCoroutine(coroutine);
            if (tooltip != null)
                Destroy(tooltip.gameObject);
            tooltip = null;
        }
    }
    IEnumerator ShowTooltip(float time, CursorEvent c)
    {
        yield return new WaitForSeconds(time);
        tooltip = Instantiate(TooltipPrefab);
        tooltip.GetComponentInChildren<UnityEngine.UI.Text>().text = c.message;
        tooltip.SetParent(c.transform);
        tooltip.position = c.transform.position;
        tooltip.localScale = new Vector3(1, 1, 1);
    }
}
