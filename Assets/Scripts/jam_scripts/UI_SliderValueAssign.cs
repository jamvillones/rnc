﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UI_SliderValueAssign : MonoBehaviour, IPointerUpHandler
{
    [SerializeField]
    Slider slider;
    [SerializeField]
    Text text;
    public bool Touched;
    float oldValue;

    // Use this for initialization
    void Start()
    {
        slider = GetComponent<Slider>();
        if (slider == null)
        {
            Debug.Log("assign slider first");
            return;
        }
        slider.onValueChanged.AddListener(delegate { SliderCallback(); });
        text.text = slider.value.ToString();
        oldValue = slider.value;
    }
    private void OnDisable()
    {
        slider.onValueChanged.RemoveListener(delegate { SliderCallback(); });
    }

    // Update is called once per frame
    void Update()
    {

    }
    void SliderCallback()
    {
        UI_CockToUI c = GetComponentInParent<UI_CockToUI>();
        if (c)
            c.UpdateSecondaryStats();

        text.text = slider.value.ToString();
        Touched = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if(slider.value != oldValue)
        {
            this.transform.GetComponentInParent<UI_CockToUI>().Save();
            oldValue = slider.value;
        }
    }
}
