﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using NewMeta;
using UnityEngine.UI;
using UnityEngine.EventSystems;
[System.Serializable]
public class ChickenSaved : UnityEngine.Events.UnityEvent<int>
{

}

public class UI_CockToUI : MonoBehaviour
{
    public ChickenSaved savedEvent;
    //public CockListHolder instance;
    public bool PendingChanges;
    public GameObject ConfirmPanel;
    public Text TotalText;
    public Text[] SecondaryStats;
    [SerializeField]
    GameChicken Cock;
    [SerializeField]
    Slider[] sliders;
    // [SerializeField]
    Dropdown dd;
    GameChicken[] getCocks
    {
        get { return CockListHolder.instance.data.ToArray(); }
        //set { CockListHolder.instance.cocks = value; }
    }

    //GameChicken[] cocks;

    [SerializeField]
    Text cockName, breedername, breedname, id;
    private void Awake()
    {
        dd = GetComponentInChildren<Dropdown>();
        sliders = GetComponentsInChildren<Slider>();
        sliders.ToList().ForEach(s => s.onValueChanged.AddListener(delegate { setPending(true); }));
    }

    void setPending(bool boolean)
    {
        PendingChanges = boolean;
    }

    // Use this for initialization
    void Start()
    {

        dd.onValueChanged.AddListener(delegate { DropdownCallback(); });
        EventTrigger eventTrigger = dd.gameObject.AddComponent<EventTrigger>();
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerDown;
        entry.callback.AddListener(delegate { checkForChanges(); });
        eventTrigger.triggers.Add(entry);
    }

    void checkForChanges()
    {
        if (sliders.ToList().Exists(s => s.GetComponent<UI_SliderValueAssign>().Touched))
            ConfirmPanel.SetActive(true);
    }

    private void OnEnable()
    {
        this.AddEventListenerGlobal<CockUpdated>(CockLoadedCallback);
    }
    private void OnDisable()
    {
        dd.onValueChanged.RemoveListener(delegate { DropdownCallback(); });
        this.RemoveEventListenerGlobal<CockUpdated>(CockLoadedCallback);

    }
    void CockLoadedCallback(CockUpdated c)
    {
        // if (dd.value == c.ID)
        if(c.type == DataUpdateType.Load)
        AssignCock(dd.value);
    }
    public void SaveBtnCallback(int ddValue)
    {
        if (ddValue == dd.value)
            AssignCock(ddValue);
    }
    public void AssignCock(int value)
    {
        //if (value != EventID)
        // return;

        if (getCocks == null)
        {
            Debug.Log("null");
            return;
        }
        if (getCocks.Length == 0)
            return;
        Cock = getCocks[value];

        if (Cock != null)
            AssignValue();
    }
    void AssignValue()
    {
        sliders[0].value = Cock.ChickenAttributes.Gameness;
        sliders[1].value = Cock.ChickenAttributes.Intellect;
        sliders[2].value = Cock.ChickenAttributes.Strength;
        sliders[3].value = Cock.ChickenAttributes.Toughness;
        sliders[4].value = Cock.ChickenAttributes.Agility;
        sliders[5].value = Cock.ChickenAttributes.Weight;

        SecondaryStats[0].text = Cock.ChickenAttributes.GetMappedInitiative().ToString();
        SecondaryStats[1].text = Cock.ChickenAttributes.Morale.ToString();
        SecondaryStats[2].text = Cock.ChickenAttributes.GetMappedPower().ToString();
        SecondaryStats[3].text = Cock.ChickenAttributes.GetMappedSpeed().ToString();
        SecondaryStats[4].text = Cock.ChickenAttributes.GetMappedAccuracy().ToString();
        SecondaryStats[5].text = Cock.ChickenAttributes.GetMappedDefenseLow().ToString();
        SecondaryStats[6].text = Cock.ChickenAttributes.TotalHealthPoints.ToString();

        TotalText.text = sliders.ToList().Select(x => x.value).Sum().ToString();

        cockName.text = Cock.Name;
        breedername.text = Cock.BreederName;
        breedname.text = Cock.BreedName;
        id.text = Cock.UniqueID;
        sliders.ToList().ForEach(s => s.GetComponent<UI_SliderValueAssign>().Touched = false);
        PendingChanges = false;
    }
    public void UpdateSecondaryStats()
    {
        TotalText.text = sliders.ToList().Select(x => x.value).Sum().ToString();
        SecondaryStats[0].text = ValueMapper.Map(sliders[4].value + sliders[1].value, 1, 100, 1, 15).ToString();   //Cock.ChickenAttributes.GetMappedInitiative().ToString();
        SecondaryStats[1].text = ValueMapper.Map(sliders[0].value + sliders[1].value, 1, 100, 1, 100).ToString(); //Cock.ChickenAttributes.Morale.ToString();
        SecondaryStats[2].text = ValueMapper.Map(sliders[2].value + sliders[5].value, 1, 100, 1, 25).ToString(); //Cock.ChickenAttributes.GetMappedPower().ToString();
        SecondaryStats[3].text = ValueMapper.Map(sliders[0].value + sliders[4].value, 1, 100, 1, 25).ToString();//Cock.ChickenAttributes.GetMappedSpeed().ToString();
        SecondaryStats[4].text = ValueMapper.Map(sliders[4].value + sliders[2].value, 1, 100, 60, 80).ToString(); //Cock.ChickenAttributes.GetMappedAccuracy().ToString();
        SecondaryStats[5].text = ValueMapper.Map(sliders[4].value < sliders[1].value ? sliders[4].value : sliders[1].value, 1, 50, 10, 25).ToString();//Cock.ChickenAttributes.GetMappedDefense().ToString();
        SecondaryStats[6].text = (sliders[3].value + sliders[5].value + sliders[2].value).ToString();
    }
    public void DropdownCallback()
    {
        AssignCock(dd.value);

    }
    public void Save()
    {
        Cock.ChickenAttributes.Gameness = sliders[0].value;
        Cock.ChickenAttributes.Intellect = sliders[1].value;
        Cock.ChickenAttributes.Strength = sliders[2].value;
        Cock.ChickenAttributes.Toughness = sliders[3].value;
        Cock.ChickenAttributes.Agility = sliders[4].value;
        Cock.ChickenAttributes.Weight = sliders[5].value;
        Cock.ChickenAttributes.HealthPoints = Cock.ChickenAttributes.TotalHealthPoints;

        for (int i = 0; i < getCocks.Length; i++)
        {
            if (getCocks[i].Name == Cock.Name)
                getCocks[i] = Cock;

        }
        CockListHolder.instance.SaveData(true);
        if (savedEvent != null)
            savedEvent.Invoke(dd.value);
        sliders.ToList().ForEach(s => s.GetComponent<UI_SliderValueAssign>().Touched = false);
        PendingChanges = false;

    }

    public GameChicken SelectedChicken(bool deepCopy)
    {
        GameChicken temp = new GameChicken();
        temp.UniqueID = System.Guid.NewGuid().ToString();
        temp.Name = Cock.Name;
        temp.BreedName = Cock.BreedName;
        temp.BreederName = Cock.BreederName;

        temp.ChickenAttributes.Gameness = Cock.ChickenAttributes.Gameness;
        temp.ChickenAttributes.Intellect = Cock.ChickenAttributes.Intellect;
        temp.ChickenAttributes.Strength = Cock.ChickenAttributes.Strength;
        temp.ChickenAttributes.Toughness = Cock.ChickenAttributes.Toughness;
        temp.ChickenAttributes.Agility = Cock.ChickenAttributes.Agility;
        temp.ChickenAttributes.Weight = Cock.ChickenAttributes.Weight;

        for (int i = 0; i < 8; i++)
        {
            temp.MoveCards[i].Name = Prototypes.MoveCardDataHolder.instance.data[i].Name;
            temp.MoveCards[i].UniqueID = Prototypes.MoveCardDataHolder.instance.data[i].UniqueID;
            temp.MoveCards[i].Weight = Prototypes.MoveCardDataHolder.instance.data[i].Weight;
            temp.MoveCards[i].InferiorMove = Move.GetValues(Prototypes.MoveCardDataHolder.instance.data[i].InferiorMove);
            temp.MoveCards[i].SuperiorMove = Move.GetValues(Prototypes.MoveCardDataHolder.instance.data[i].SuperiorMove);
        }

        for (int i = 0; i < temp.MoveCards.Length; i++)
            temp.MoveCards[i].Weight = Cock.MoveCards[i].Weight;

        return deepCopy ? temp : Cock;
    }

    public Slider[] CurrentSliders()
    {
        return sliders;
    }

}
