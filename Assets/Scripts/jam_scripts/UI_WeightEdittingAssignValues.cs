﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_WeightEdittingAssignValues : MonoBehaviour
{
    [SerializeField]
    Dropdown dd;
    [SerializeField]
    InputField[] inputF;
    GameChicken[] getCocks { get { return CockListHolder.instance.data.ToArray(); } }
    GameChicken cock;
    [SerializeField]

    private void Awake()
    {
        inputF = GetComponentsInChildren<InputField>();
    }
    private void Start()
    {
    }
    private void OnEnable()
    {
        this.AddEventListenerGlobal<CockUpdated>(CockLoadCallback);

        dd.onValueChanged.AddListener(delegate { DropdownCallback(); });
    }
    private void OnDisable()
    {
        this.RemoveEventListenerGlobal<CockUpdated>(CockLoadCallback);
        dd.onValueChanged.RemoveListener(delegate { DropdownCallback(); });
    }
    void Setvalues()
    {
        if (getCocks.Length == 0)
            return;
        cock = getCocks[dd.value];
        for (int i = 0; i < inputF.Length; i++)
        {
            inputF[i].placeholder.GetComponent<Text>().text = cock.MoveCards[i].Weight.ToString();
        }
    }
    void DropdownCallback()
    {

        Setvalues();

    }
    void CockLoadCallback(CockUpdated c)
    {
        Setvalues();
    }
    public void SaveWeight()
    {
        for (int i = 0; i < getCocks.Length; i++)
        {
            if (getCocks[i].UniqueID == cock.UniqueID)
            {
                for (int j = 0; j < cock.MoveCards.Length; j++)
                {
                    if (inputF[j].text == "")
                    {

                    }
                    else
                        cock.MoveCards[j].Weight = int.Parse(inputF[j].text);

                  
                }
                Setvalues();
                ClearFields();
                break;
            }
        }
    }
    public void ClearFields()
    {
        foreach(InputField i in inputF)
        {
            i.text = "";
        }
    }
}
