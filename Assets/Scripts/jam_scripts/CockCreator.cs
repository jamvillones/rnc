﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class CockCreator : MonoBehaviour {
    
    public List<GameChicken> cockList { get { return CockListHolder.instance.data; } }
    public DistributorCursor DC;
    public GameChicken cock;

   [Header("UI components")]
    public Slider[] sliders;
    public InputField[] IF;
  
    private void Awake()
    {
        sliders = GetComponentsInChildren<Slider>();
        IF = GetComponentsInChildren<InputField>();
    }
    /// <summary>
    /// renew the inputfield and slider value 
    /// </summary>
    void RefreshFields()
    {
        foreach (InputField i in IF)
            i.text = "";

        foreach (Slider s in sliders)
            s.value = 1;
    }

    public bool CanCreate()
    {
        if (IF[0].text == "" || IF[1].text == "" || IF[2].text == "")
            return false;
        return true;
    }
    public void CreateCock()
    {
        if(!CanCreate())
        {
            ///fields are empty
            return;
        }
      
        cock = new GameChicken();
        //cock.MoveCards = Prototypes.MoveCardDataHolder.instance.data.ToArray();
        for(int i = 0; i < 8; i++)
        {

            cock.MoveCards[i].Name = Prototypes.MoveCardDataHolder.instance.data[i].Name;
            cock.MoveCards[i].UniqueID = Prototypes.MoveCardDataHolder.instance.data[i].UniqueID;
            cock.MoveCards[i].Weight = Prototypes.MoveCardDataHolder.instance.data[i].Weight;
            cock.MoveCards[i].InferiorMove = Move.GetValues(Prototypes.MoveCardDataHolder.instance.data[i].InferiorMove);
            cock.MoveCards[i].SuperiorMove= Move.GetValues(Prototypes.MoveCardDataHolder.instance.data[i].SuperiorMove);

        }
      
        Position p = new Position();
        p.UniqueID = cock.UniqueID;

        cock.UniqueID = System.Guid.NewGuid().ToString();
       



        cock.BreedName = IF[1].text;
        cock.BreederName = IF[2].text;
       
        cock.ChickenAttributes.Gameness = sliders[0].value;
        cock.ChickenAttributes.Intellect = sliders[1].value;
        cock.ChickenAttributes.Strength = sliders[2].value;
        cock.ChickenAttributes.Toughness = sliders[3].value;
        cock.ChickenAttributes.Agility = sliders[4].value;
        cock.ChickenAttributes.Weight = sliders[5].value;

        cock.ChickenAttributes.HealthPoints = cock.ChickenAttributes.TotalHealthPoints;
        /// apply to chicken weight when saving
        for(int i =0; i < cock.MoveCards.Length;i++)
        {
            cock.MoveCards[i].Weight = DC.Values[i].Value;
        }

        if(CockNameTaken(IF[0].text))
        {
            Debug.Log("Creation failed! Name Already taken");
            return;
        }
        // p.x=DC.CursorSprite.transform.position.x;
        //p.x = DC.CursorSprite.transform.position.y;
        //p.x = DC.CursorSprite.transform.position.z;
        p = DC.CursorSprite.transform.position;
        p.UniqueID = cock.UniqueID;
        p.SliderFillAmount = DC.AerialGrounded.value;
        CursorPositionHolder.instance.data.Add(p);

        cock.Name = IF[0].text;

        cockList.Add(cock);
        CockListHolder.instance.SaveData(true);
        RefreshFields();
        //cock = null;
    }
    bool CockNameTaken(string name)
    {
       
        foreach(GameChicken c in cockList)
        {
            if (c.Name == name)
                return true;
        }
        
        return false;
    }
}


