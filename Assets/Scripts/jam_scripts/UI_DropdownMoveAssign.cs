﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_DropdownMoveAssign : MonoBehaviour {
    Dropdown dd;
    List<Move> Moves { get { return MoveDataHolder.instance.Moves; } }
    private void Awake()
    {
        dd = GetComponent<Dropdown>();
    }
    private void OnEnable()
    {
        this.AddEventListenerGlobal<MoveDataUpdated>(MoveDataChangedCB);
    }
    private void OnDisable()
    {
        this.RemoveEventListenerGlobal<MoveDataUpdated>(MoveDataChangedCB);
    }
    private void MoveDataChangedCB(MoveDataUpdated mdu)
    {
        List<string> names = new List<string>();
        if (Moves.Count != 0)
        {

            for (int i = 0; i < Moves.Count; i++)
            {
                names.Add(Moves[i].Name);
            }
        }
        dd.RefreshDropdown(names,true);
    }
}

