﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Prototypes
{
    public class MoveCardDataHolder : GenericDataHolder<MoveCard>
    {
        private void OnEnable()
        {
            LoadData(true);
        }
        private void OnDisable()
        {
            SaveData(true);
        }
        protected override void RaiseEvent(DataUpdateType type)
        {
            MoveCardUpdated_Event e = new MoveCardUpdated_Event();
            e.type = type;
            this.RaiseEventGlobal<MoveCardUpdated_Event>(e);
        }

    }
}
public class MoveCardUpdated_Event: GameEvent
{
   public DataUpdateType type;
}

