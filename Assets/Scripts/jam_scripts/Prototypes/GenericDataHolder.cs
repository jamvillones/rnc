﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Core.Utilities;

public enum DataUpdateType { Save, Load }

public class GenericDataHolder<T> : Singleton<GenericDataHolder<T>>
{
    
    public string FileName;
    public List<T> data;

    public void SaveData(bool shouldRaiseEvent)
    {
        this.SaveArray<T>(data.ToArray(), FileName);
        if (shouldRaiseEvent)
        {
           RaiseEvent(DataUpdateType.Save);
        }
    }
    public void LoadData(bool shouldRaiseEvent)
    {
        data = this.LoadArray<T>(FileName).ToList();
        if (shouldRaiseEvent)
        {
            RaiseEvent(DataUpdateType.Load);
        }
    }
    protected virtual void RaiseEvent( DataUpdateType type)
    {
       
    }
}