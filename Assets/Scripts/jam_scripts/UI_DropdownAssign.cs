﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_DropdownAssign : MonoBehaviour
{

    public Dropdown dd;
    private void Awake()
    {
        dd = GetComponent<Dropdown>();

    }
    void OnEnable()
    {
        this.AddEventListenerGlobal<CockUpdated>(CockLoadedCallback);
    }
    private void OnDisable()
    {
        this.RemoveEventListenerGlobal<CockUpdated>(CockLoadedCallback);
    }
    private void CockLoadedCallback(CockUpdated c)
    {

        List<string> names = new List<string>();
        if (CockListHolder.instance.data != null)
        {

            for (int i = 0; i < CockListHolder.instance.data.Count; i++)
            {
                names.Add(CockListHolder.instance.data[i].BreedName);
            }
        }
        dd.RefreshDropdown(names, false);

    }

}
static class UIExtension
{
    #region dropdown
    public static void RefreshDropdown(this Dropdown dd, List<string> names, bool gotoFirst)
    {
        dd.ClearOptions();
        dd.AddOptions(names);
        if (gotoFirst)
            dd.value = 0;
        
    }
    #endregion
}

