﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class CockDeleter : MonoBehaviour {
    public Dropdown dd;
    public List<GameChicken> cockList { get { return CockListHolder.instance.data; } }

    private void Awake()
    {
        dd = GetComponentInChildren<Dropdown>();
       
    }
    private void Start()
    {
        SetDropdown();
    }

    private void OnEnable()
    {
        Debug.Log("subscribe");
        this.AddEventListenerGlobal<CockUpdated>(CockLoadCallback);

    }
    private void OnDisable()
    {
        this.RemoveEventListenerGlobal<CockUpdated>(CockLoadCallback);
    }
   
    void CockLoadCallback(CockUpdated c)
    {
      
        SetDropdown();  
    }
   
    public void DeleteCock()
    {
        GameChicken temp = new GameChicken();
        foreach(GameChicken g in cockList)
        {
            if(g.Name == dd.captionText.text)
            {
                cockList.Remove(g);
                temp = g;
                break;
            }
        }
        for(int i = 0;i < CursorPositionHolder.instance.data.Count;i++)
        {
            if(temp.UniqueID == CursorPositionHolder.instance.data[i].UniqueID)
            {
                CursorPositionHolder.instance.data.Remove(CursorPositionHolder.instance.data[i]);
                break;
            }
        }
       
        //cockList.Remove(cockList[dd.value]);
        CockListHolder.instance.SaveData(true);
        CursorPositionHolder.instance.SaveData(false);
        //update dropdown
        SetDropdown();
    }
    public void SetDropdown()
    {

        List<string> dropdownNames = new List<string>();
       
        for (int i = 0; i < cockList.Count; i++)
            dropdownNames.Add(cockList[i].Name);
        dd.RefreshDropdown(dropdownNames, true);
        //dd.ClearOptions();
        //dd.AddOptions(dropdownNames);
        //dd.value = 0;
    }
}
