﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class Position
{
    public string UniqueID;
    public float x, y, z;
    public float SliderFillAmount;
    public Position(float X, float Y, float Z)
    {
        x = X;
        y = Y;
        z = Z;
    }
    public Position()
    {

    }

    public static implicit operator Position(Vector3 vector)
    {
        return new Position(vector.x,vector.y,vector.z);
    }
}
/// <summary>
/// this class is specially used in reversing diamond distributor
/// </summary>
public class CursorPositionHolder : GenericDataHolder<Position> {

    private void OnEnable()
    {
        LoadData(false);
    }
    private void OnDisable()
    {
        SaveData(false);
    }
}
