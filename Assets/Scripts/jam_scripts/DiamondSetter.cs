﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiamondSetter : MonoBehaviour
{
    DistributorCursor dc;
    public UnityEngine.UI.Dropdown DD;
    public GameChicken gc;
    Position p;
    public UnityEngine.UI.Slider slider;
    private void Awake()
    {
        dc = GetComponent<DistributorCursor>();
    }

    private void OnEnable()
    {
        DD.onValueChanged.AddListener(delegate { set(); });
    }
    private void OnDisable()
    {
        DD.onValueChanged.RemoveListener(delegate { set(); });
    }
    private void Start()
    {
        set();
    }
    public void set()
    {
        // dc.CursorSprite.transform.position.x = CursorPositionHolder.instance.data[0].x;
        ///gc = new GameChicken();
        if (CockListHolder.instance.data == null || CockListHolder.instance.data.Count == 0)
            return;
        gc = CockListHolder.instance.data[DD.value];
       
        p = Find(gc.UniqueID);
        if (p == null)
            return;
        Vector3 temp;
        temp.x = p.x; temp.y = p.y; temp.z = p.z;
        dc.CursorSprite.transform.position = temp;
        dc.AerialGrounded.value = p.SliderFillAmount;
        dc.UpdateValues();

    }
    public Position Find(string ID)
    {
        if (CursorPositionHolder.instance.data.Count == 0 || CursorPositionHolder.instance.data == null)
        {
            return null;
        }
        for (int i = 0; i < CursorPositionHolder.instance.data.Count; i++)
        {
            if (ID == CursorPositionHolder.instance.data[i].UniqueID)
            {
                Debug.Log("Position found");
                return CursorPositionHolder.instance.data[i];
            }
        }
        return new Position();
    }
    public void Save()
    {
        //save the weight of the chicken and the position of the cursor
        for (int i = 0; i < gc.MoveCards.Length; i++)
            gc.MoveCards[i].Weight = dc.Values[i].Value;

        p.x = dc.CursorSprite.transform.position.x;
        p.y = dc.CursorSprite.transform.position.y;
        p.z = dc.CursorSprite.transform.position.z;
        p.SliderFillAmount = slider.value;
    }
}
