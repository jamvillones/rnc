﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class CursorEvent: GameEvent
{
    public Transform transform;
    public bool IsOn;
    public string message;
}
//[RequireComponent()]
public class UI_ToolTipMouseHover : MonoBehaviour {
    public string Message;
    public void Enter()
    {
        //Debug.Log("enter");
        CursorEvent c = new CursorEvent();
        c.transform = transform;
        c.IsOn = true;
        c.message = Message;
        this.RaiseEventGlobal<CursorEvent>(c);

    }
    public void Exit()
    {
        //Debug.Log("exit");
        CursorEvent c = new CursorEvent();
        c.IsOn = false;
        this.RaiseEventGlobal<CursorEvent>(c);
    }
}
