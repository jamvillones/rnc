﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Core.Utilities;

public class MoveDataHolder : Singleton<MoveDataHolder>
{
    public string FileName = "/MoveData.dat";
    public List<Move> Moves;
    private void Start()
    {
        Load(true);
    }
    private void OnDisable()
    {
        Save(false);
    }

    public bool ListEmpty()
    {
        return Moves.Count == 0;
    }
    public void Save(bool raiseEvent)
    {
        this.SaveArray<Move>(Moves.ToArray(), FileName);
        if (raiseEvent)
            raiseDataChangedEvent();

    }
    public void Load(bool raiseEvent)
    {
        Moves = this.LoadArray<Move>(FileName).ToList();
        if (raiseEvent)
            raiseDataChangedEvent();
    }
    void raiseDataChangedEvent()
    {
        MoveDataUpdated mds = new MoveDataUpdated();
        this.RaiseEventGlobal<MoveDataUpdated>(mds);
    }
}

/// <summary>
/// class event used to fire up an event whenever the data is changed or to be loaded in init
/// </summary>
public class MoveDataUpdated : GameEvent
{
}

