﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class MoveEdit : MonoBehaviour
{
    InputField[] inputF;
    Toggle[] toggles;
    Dropdown dd;
    Dropdown childDD;
    Button saveBtn;
    List<Move> Moves { get { return MoveDataHolder.instance.Moves; } }
    Move move { get { return Moves[dd.value]; } }

    private void Awake()
    {
        saveBtn = transform.parent.GetComponentInChildren<Button>();
        dd = transform.parent.GetComponentInChildren<Dropdown>();
        inputF = GetComponentsInChildren<InputField>();
        childDD = GetComponentInChildren<Dropdown>();
        toggles = GetComponentsInChildren<Toggle>();
    }
    // Use this for initialization
    void Start()
    {

    }
    // Update is called once per frame
    void Update()
    {

    }

    private void OnEnable()
    {
        saveBtn.onClick.AddListener(delegate { SaveButtonCallback(); });
        dd.onValueChanged.AddListener(delegate { DropdownCB(); });
        this.AddEventListenerGlobal<MoveDataUpdated>(MoveDataChangedCB);
    }
    private void OnDisable()
    {
        saveBtn.onClick.RemoveListener(delegate { SaveButtonCallback(); });
        dd.onValueChanged.RemoveListener(delegate { DropdownCB(); });
        this.RemoveEventListenerGlobal<MoveDataUpdated>(MoveDataChangedCB);
    }

    #region Callbacks
    private void MoveDataChangedCB(MoveDataUpdated mdu)
    {
        AssignValues();
    }

    void DropdownCB()
    {
        AssignValues();
    }
    void SaveButtonCallback()
    {
        //Debug.Log("save");
        if (inputF[0].text != "")
            move.Name = inputF[0].text;
        if (inputF[1].text != "")
            move.BonusDamage = Int32.Parse(inputF[1].text);
        if (inputF[2].text != "")
            move.BonusAccuracy = Int32.Parse(inputF[2].text);
        if (inputF[3].text != "")
            move.BonusDodge = Int32.Parse(inputF[3].text);
        if (inputF[4].text != "")
            move.BonusInitiative = Int32.Parse(inputF[4].text);
        if (inputF[5].text != "")
            move.BonusAerialDamage = Int32.Parse(inputF[5].text);
        if (inputF[6].text != "")
            move.BonusGroundedDamage = Int32.Parse(inputF[6].text);

        if (inputF[7].text != "")
            move.EnemyDamage = Int32.Parse(inputF[7].text);
        if (inputF[8].text != "")
            move.EnemyAccuracy = Int32.Parse(inputF[8].text);
        if (inputF[9].text != "")
            move.EnemyDodge = Int32.Parse(inputF[9].text);
       

        move.InitiativeBonus = toggles[0].isOn;
        move.DodgeBonus = toggles[1].isOn;
        move.SuperiorDamage = toggles[2].isOn;


        MoveDataHolder.instance.Save(true);

        ClearInputValues();
    }
    #endregion

    void ClearInputValues()
    {
        foreach (InputField i in inputF)
        {
            i.text = "";
        }
    }

    void AssignValues()
    {
        inputF[0].placeholder.GetComponent<Text>().text = move.Name;
        inputF[1].placeholder.GetComponent<Text>().text = move.BonusDamage.ToString();
        inputF[2].placeholder.GetComponent<Text>().text = move.BonusAccuracy.ToString();
        inputF[3].placeholder.GetComponent<Text>().text = move.BonusDodge.ToString();
        inputF[4].placeholder.GetComponent<Text>().text = move.BonusInitiative.ToString();
        inputF[5].placeholder.GetComponent<Text>().text = move.BonusAerialDamage.ToString();
        inputF[6].placeholder.GetComponent<Text>().text = move.BonusGroundedDamage.ToString();
        inputF[7].placeholder.GetComponent<Text>().text = move.EnemyDamage.ToString();
        inputF[8].placeholder.GetComponent<Text>().text = move.EnemyAccuracy.ToString();
        inputF[9].placeholder.GetComponent<Text>().text = move.EnemyDodge.ToString();
        toggles[0].isOn = move.InitiativeBonus;
        toggles[1].isOn = move.DodgeBonus;
        toggles[2].isOn = move.SuperiorDamage;
        List<string> names = new List<string>();
        for (int i = 0; i < (int)MoveType.length; i++)
        {
            names.Add(((MoveType)i).ToString());
        }
        childDD.RefreshDropdown(names, false);
        childDD.value = (int)move.Type;
    }

}
