﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Prototypes;
using System;
using System.Linq;

public class UI_MoveCardElementsAssign : MonoBehaviour
{
    [Header("UI References")]
    public Dropdown MainDropdown;
    public Dropdown MoveTypeDropdown;
    public Dropdown MoveAttackTypeDropdown;
    public InputField[] InputF;
    public Toggle[] Toggles;
    Button SaveButton;


    // public MoveCard currentMoveCard;
    MoveCard GetCurrentMoveCard { get { return MoveCardDataHolder.instance.data[MainDropdown.value]; } }
    private void Awake()
    {
        SaveButton = transform.parent.GetComponentInChildren<Button>();
        InputF = GetComponentsInChildren<InputField>();
        Toggles = GetComponentsInChildren<Toggle>();
    }

    void Start()
    {
        SetMainDropdown();
        SetMoveTypeDropdown();
        SetValues();
    }

    void SetMoveTypeDropdown()
    {
        if (MoveTypeDropdown == null)
            return;
        string[] tempName = new string[2] { "Superior", "Inferior" };
        MoveTypeDropdown.RefreshDropdown(tempName.ToList(), true);
    }
    void SetMainDropdown()
    {
        if (MainDropdown == null)
            return;
        List<string> names = new List<string>();
        foreach (MoveCard mc in MoveCardDataHolder.instance.data)
        {
            names.Add(mc.Name);
        }
        MainDropdown.RefreshDropdown(names, true);
        //currentMoveCard = GetCurrentMoveCard;
    }
    /// <summary>
    /// this is where you set field values
    /// </summary>
    void SetValues()
    {
        //Debug.Log(GetCurrentMoveCard);
        if (GetCurrentMoveCard == null)
            return;
        //if superior
        if (MoveTypeDropdown.value == 0)
        {
            InputF[0].placeholder.GetComponent<Text>().text = GetCurrentMoveCard.SuperiorMove.Name;
            InputF[1].placeholder.GetComponent<Text>().text = GetCurrentMoveCard.SuperiorMove.BonusDamage.ToString();
            InputF[2].placeholder.GetComponent<Text>().text = GetCurrentMoveCard.SuperiorMove.BonusAccuracy.ToString();
            InputF[3].placeholder.GetComponent<Text>().text = GetCurrentMoveCard.SuperiorMove.BonusDodge.ToString();
            InputF[4].placeholder.GetComponent<Text>().text = GetCurrentMoveCard.SuperiorMove.BonusInitiative.ToString();
            InputF[5].placeholder.GetComponent<Text>().text = GetCurrentMoveCard.SuperiorMove.BonusAerialDamage.ToString();
            InputF[6].placeholder.GetComponent<Text>().text = GetCurrentMoveCard.SuperiorMove.BonusGroundedDamage.ToString();
            InputF[7].placeholder.GetComponent<Text>().text = GetCurrentMoveCard.SuperiorMove.EnemyDamage.ToString();
            InputF[8].placeholder.GetComponent<Text>().text = GetCurrentMoveCard.SuperiorMove.EnemyAccuracy.ToString();
            InputF[9].placeholder.GetComponent<Text>().text = GetCurrentMoveCard.SuperiorMove.EnemyDodge.ToString();
            Toggles[0].isOn = GetCurrentMoveCard.SuperiorMove.InitiativeBonus;
            Toggles[1].isOn = GetCurrentMoveCard.SuperiorMove.DodgeBonus;
            Toggles[2].isOn = GetCurrentMoveCard.SuperiorMove.SuperiorDamage;
            Toggles[3].isOn = GetCurrentMoveCard.SuperiorMove.CuttingBonus;
            Toggles[4].isOn = GetCurrentMoveCard.SuperiorMove.DodgeBonusAgiInt;
            Toggles[5].isOn = GetCurrentMoveCard.SuperiorMove.DealDamage;
            Toggles[6].isOn = GetCurrentMoveCard.SuperiorMove.DecreaseHigherAccuracy;
            Toggles[7].isOn = GetCurrentMoveCard.SuperiorMove.DecreaseLowerAccuracy;
            List<string> names = new List<string>();
            for (int i = 0; i < (int)MoveType.length; i++)
            {
                names.Add(((MoveType)i).ToString());
            }
            MoveAttackTypeDropdown.RefreshDropdown(names, false);
            MoveAttackTypeDropdown.value = (int)GetCurrentMoveCard.SuperiorMove.Type;
        }
        //if inferior
        else if (MoveTypeDropdown.value == 1)
        {
            InputF[0].placeholder.GetComponent<Text>().text = GetCurrentMoveCard.InferiorMove.Name;
            InputF[1].placeholder.GetComponent<Text>().text = GetCurrentMoveCard.InferiorMove.BonusDamage.ToString();
            InputF[2].placeholder.GetComponent<Text>().text = GetCurrentMoveCard.InferiorMove.BonusAccuracy.ToString();
            InputF[3].placeholder.GetComponent<Text>().text = GetCurrentMoveCard.InferiorMove.BonusDodge.ToString();
            InputF[4].placeholder.GetComponent<Text>().text = GetCurrentMoveCard.InferiorMove.BonusInitiative.ToString();
            InputF[5].placeholder.GetComponent<Text>().text = GetCurrentMoveCard.InferiorMove.BonusAerialDamage.ToString();
            InputF[6].placeholder.GetComponent<Text>().text = GetCurrentMoveCard.InferiorMove.BonusGroundedDamage.ToString();
            InputF[7].placeholder.GetComponent<Text>().text = GetCurrentMoveCard.InferiorMove.EnemyDamage.ToString();
            InputF[8].placeholder.GetComponent<Text>().text = GetCurrentMoveCard.InferiorMove.EnemyAccuracy.ToString();
            InputF[9].placeholder.GetComponent<Text>().text = GetCurrentMoveCard.InferiorMove.EnemyDodge.ToString();
            Toggles[0].isOn = GetCurrentMoveCard.InferiorMove.InitiativeBonus;
            Toggles[1].isOn = GetCurrentMoveCard.InferiorMove.DodgeBonus;
            Toggles[2].isOn = GetCurrentMoveCard.InferiorMove.SuperiorDamage;
            Toggles[3].isOn = GetCurrentMoveCard.InferiorMove.CuttingBonus;
            Toggles[4].isOn = GetCurrentMoveCard.InferiorMove.DodgeBonusAgiInt;
            Toggles[5].isOn = GetCurrentMoveCard.InferiorMove.DealDamage;
            Toggles[6].isOn = GetCurrentMoveCard.InferiorMove.DecreaseHigherAccuracy;
            Toggles[7].isOn = GetCurrentMoveCard.InferiorMove.DecreaseLowerAccuracy;

            List<string> names = new List<string>();
            for (int i = 0; i < (int)MoveType.length; i++)
            {
                names.Add(((MoveType)i).ToString());
            }
            MoveAttackTypeDropdown.RefreshDropdown(names, false);
            MoveAttackTypeDropdown.value = (int)GetCurrentMoveCard.InferiorMove.Type;
        }
    }
   public void SaveButtonCallback()
    {
        if (MoveTypeDropdown.value == 0)
        {
            //Debug.Log("save");
            if (InputF[0].text != "")
                GetCurrentMoveCard.SuperiorMove.Name = InputF[0].text;
            if (InputF[1].text != "")
                GetCurrentMoveCard.SuperiorMove.BonusDamage = Int32.Parse(InputF[1].text);
            if (InputF[2].text != "")
                GetCurrentMoveCard.SuperiorMove.BonusAccuracy = Int32.Parse(InputF[2].text);
            if (InputF[3].text != "")
                GetCurrentMoveCard.SuperiorMove.BonusDodge = Int32.Parse(InputF[3].text);
            if (InputF[4].text != "")
                GetCurrentMoveCard.SuperiorMove.BonusInitiative = Int32.Parse(InputF[4].text);
            if (InputF[5].text != "")
                GetCurrentMoveCard.SuperiorMove.BonusAerialDamage = Int32.Parse(InputF[5].text);
            if (InputF[6].text != "")
                GetCurrentMoveCard.SuperiorMove.BonusGroundedDamage = Int32.Parse(InputF[6].text);
            if (InputF[7].text != "")
                GetCurrentMoveCard.SuperiorMove.EnemyDamage = Int32.Parse(InputF[7].text);
            if (InputF[8].text != "")
                GetCurrentMoveCard.SuperiorMove.EnemyAccuracy = Int32.Parse(InputF[8].text);
            if (InputF[9].text != "")
                GetCurrentMoveCard.SuperiorMove.EnemyDodge = Int32.Parse(InputF[9].text);

            GetCurrentMoveCard.SuperiorMove.InitiativeBonus = Toggles[0].isOn;
            GetCurrentMoveCard.SuperiorMove.DodgeBonus = Toggles[1].isOn;
            GetCurrentMoveCard.SuperiorMove.SuperiorDamage = Toggles[2].isOn;
            GetCurrentMoveCard.SuperiorMove.CuttingBonus = Toggles[3].isOn;
            GetCurrentMoveCard.SuperiorMove.DodgeBonusAgiInt = Toggles[4].isOn;
            GetCurrentMoveCard.SuperiorMove.DealDamage = Toggles[5].isOn;
            GetCurrentMoveCard.SuperiorMove.DecreaseHigherAccuracy = Toggles[6].isOn;
            GetCurrentMoveCard.SuperiorMove.DecreaseLowerAccuracy = Toggles[7].isOn;
        }
        else if (MoveTypeDropdown.value == 1)
        {
            //Debug.Log("save");
            if (InputF[0].text != "")
                GetCurrentMoveCard.InferiorMove.Name = InputF[0].text;
            if (InputF[1].text != "")
                GetCurrentMoveCard.InferiorMove.BonusDamage = Int32.Parse(InputF[1].text);
            if (InputF[2].text != "")
                GetCurrentMoveCard.InferiorMove.BonusAccuracy = Int32.Parse(InputF[2].text);
            if (InputF[3].text != "")
                GetCurrentMoveCard.InferiorMove.BonusDodge = Int32.Parse(InputF[3].text);
            if (InputF[4].text != "")
                GetCurrentMoveCard.InferiorMove.BonusInitiative = Int32.Parse(InputF[4].text);
            if (InputF[5].text != "")
                GetCurrentMoveCard.InferiorMove.BonusAerialDamage = Int32.Parse(InputF[5].text);
            if (InputF[6].text != "")
                GetCurrentMoveCard.InferiorMove.BonusGroundedDamage = Int32.Parse(InputF[6].text);
            if (InputF[7].text != "")
                GetCurrentMoveCard.InferiorMove.EnemyDamage = Int32.Parse(InputF[7].text);
            if (InputF[8].text != "")
                GetCurrentMoveCard.InferiorMove.EnemyAccuracy = Int32.Parse(InputF[8].text);
            if (InputF[9].text != "")
                GetCurrentMoveCard.InferiorMove.EnemyDodge = Int32.Parse(InputF[9].text);


            GetCurrentMoveCard.InferiorMove.InitiativeBonus = Toggles[0].isOn;
            GetCurrentMoveCard.InferiorMove.DodgeBonus = Toggles[1].isOn;
            GetCurrentMoveCard.InferiorMove.SuperiorDamage = Toggles[2].isOn;
            GetCurrentMoveCard.InferiorMove.CuttingBonus = Toggles[3].isOn;
            GetCurrentMoveCard.InferiorMove.DodgeBonusAgiInt = Toggles[4].isOn;
            GetCurrentMoveCard.InferiorMove.DealDamage = Toggles[5].isOn;
            GetCurrentMoveCard.InferiorMove.DecreaseHigherAccuracy = Toggles[6].isOn;
            GetCurrentMoveCard.InferiorMove.DecreaseLowerAccuracy = Toggles[7].isOn;
        }
        Prototypes.MoveCardDataHolder.instance.SaveData(true);

        ClearInputValues();
    }

    void ClearInputValues()
    {
        foreach (InputField i in InputF)
        {
            i.text = "";
        }
    }
    #region register/deregisterevents
    private void OnEnable()
    {
        MainDropdown.onValueChanged.AddListener(delegate { DropDownchanged_CB(); });
        MoveTypeDropdown.onValueChanged.AddListener(delegate { DropDownchanged_CB(); });
        this.AddEventListenerGlobal<MoveCardUpdated_Event>(MoveCardUpdated_Event_CB);
    }
    private void OnDisable()
    {
        MainDropdown.onValueChanged.RemoveListener(delegate { DropDownchanged_CB(); });
        MoveTypeDropdown.onValueChanged.RemoveListener(delegate { DropDownchanged_CB(); });
        this.RemoveEventListenerGlobal<MoveCardUpdated_Event>(MoveCardUpdated_Event_CB);

    }
    #endregion

    #region callbacks
    void DropDownchanged_CB()
    {
        Debug.Log("UpdateDropDownAndValues");
        //currentMoveCard = GetCurrentMoveCard;
        SetValues();
    }
    void MoveCardUpdated_Event_CB(MoveCardUpdated_Event e)
    {
        SetValues();
    }
    #endregion
}
