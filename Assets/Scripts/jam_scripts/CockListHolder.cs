﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Core.Utilities;
using System.Linq;
/// <summary>
/// singleton class that holds the list of Cocks
/// </summary>
public class CockListHolder : GenericDataHolder<GameChicken>
{
    private void OnEnable()
    {
        LoadData(true);
    }
    private void OnDisable()
    {
        SaveData(true);
    }
    protected override void RaiseEvent(DataUpdateType type)
    {
        CockUpdated e = new CockUpdated();
        e.type = type;
        this.RaiseEventGlobal<CockUpdated>(e);
    }
    
}
public class CockUpdated : GameEvent
{
    //public int ID;
    public DataUpdateType type;
}

