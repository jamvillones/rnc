﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class MoveWeightManager : MonoBehaviour {

    public Dropdown dd;
    public GameChicken gc;
    public Text Name, NameID, BreedName, BreederName;
    public float MaxValue;

    public List<MoveWeightEditor> list;

    private void Start()
    {
        Set();
    }

    private void OnEnable()
    {
        dd.onValueChanged.AddListener(delegate { Set(); });
        this.AddEventListenerGlobal<MaxValueUpdate>(MaxUpdate);
    }

    private void OnDisable()
    {
        dd.onValueChanged.RemoveListener(delegate { Set(); });
        this.RemoveEventListenerGlobal<MaxValueUpdate>(MaxUpdate);
    }

    void MaxUpdate(MaxValueUpdate mv)
    {
        MaxValue = gc.MoveCards.Select(x => x.Weight).Sum();
        for (int i = 0; i < list.Count; i++)
        {
            list[i].UpdateValues(MaxValue);
        }
    }

    void Set()
    {
        if (CockListHolder.instance.data == null || CockListHolder.instance.data.Count == 0)
            return;
        gc = CockListHolder.instance.data[dd.value];
        MaxValue = gc.MoveCards.Select(x => x.Weight).Sum();
        SetMoveCards();
        Name.text = string.Format("Name: {0}", gc.Name);
        NameID.text = string.Format("UniqueID: {0}", gc.UniqueID);
        BreedName.text = string.Format("Breed Name: {0}", gc.BreedName);
        BreederName.text = string.Format("Breeder Name: {0}", gc.BreederName);

        //dc.UpdateValues();
    }

    void SetMoveCards()
    {
        for (int i = 0; i < list.Count; i++)
        {
            list[i].moveCard = gc.MoveCards[i];
            list[i].UpdateValues(MaxValue);
        }
    }
}
