﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoveWeightEditor : MonoBehaviour {

    public InputField input;
    public float value;
    public Text textValue;
    public MoveCard moveCard;

    private void Awake()
    {
        input = transform.Find("InputField").GetComponentInChildren<InputField>();
    }

    public void OnClickButton()
    {
        int tmpValue;
        if (!string.IsNullOrEmpty(input.text) && int.TryParse(input.text, out tmpValue))
        {
            float valueChange = tmpValue - moveCard.Weight;
            moveCard.Weight = tmpValue;
            MaxValueUpdate max = new MaxValueUpdate();
            this.RaiseEventGlobal(max);
        }
        else Debug.Log("Invalid Input");

        input.text = "";
    }

    public void UpdateValues(float maxValue)
    {
        value = moveCard.Weight;
        value = Mathf.Clamp(value, 0, maxValue);
        value = (float)Decimal.Round(Convert.ToDecimal(value), 2);
        textValue.text = string.Format("{0} ({1}%)", value, Decimal.Round(Convert.ToDecimal((value/maxValue) * 100), 2));
    }
}

public class MaxValueUpdate : GameEvent
{
    
}