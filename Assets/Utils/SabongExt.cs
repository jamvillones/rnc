﻿using FullInspector;
using Newtonsoft.Json;

public class UpdateFullInspectorRootDirectory : fiSettingsProcessor
{
    public void Process ()
    {
        fiSettings.RootDirectory = "Assets/Plugins/FullInspector2/";
    }
}

public static class SabongExt
{
    public static string ToJson (this object self, bool formatted = false)
    {
        return JsonConvert.SerializeObject(self, formatted ? Formatting.Indented : Formatting.None);
    }
}
