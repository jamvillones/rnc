﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace UnityEssentials
{
    /// <summary>
    /// A helper class for generating WWW requests.
    /// </summary>
    public class WWWRequest
    {
        #region Delegates
        /// <summary>
        /// 
        /// </summary>
        /// <param name="www"></param>
        public delegate void OnFinish (WWW www);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="www"></param>
        public delegate void OnSuccess (WWW www);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="error"></param>
        public delegate void OnFail (string error);

        /// <summary>
        /// 
        /// </summary>
        public delegate void OnTimeOut ();

        /// <summary>
        /// 
        /// </summary>
        public delegate void OnDispose ();
        #endregion

        #region Fields
        private bool m_isPost = false;
        private bool m_isCreated = false;
        private bool m_isActive = false;
        private bool m_isTimedOut = false;
        private bool m_isDisposed = false;
        private float m_timeOutDuration = -1.0f;
        private string m_url;
        private WWW m_www;
        private WWWForm m_wwwForm;
        private Dictionary<string, string> m_headers;
        private MonoBehaviour m_monoBehaviour;
        private OnFinish m_onFinish = null;
        private OnSuccess m_onSuccess = null;
        private OnFail m_onFail = null;
        private OnTimeOut m_onTimeOut = null;
        private OnDispose m_onDispose = null;
        #endregion

        #region Properties
        /// <summary>
        /// Is WWWRequest Post?
        /// </summary>
        public bool IsPost
        {
            get { return m_isPost; }
        }

        /// <summary>
        /// Is WWWRequest Created?
        /// </summary>
        public bool IsCreated
        {
            get { return m_isCreated; }
        }

        /// <summary>
        /// Is WWWRequest Active?
        /// </summary>
        public bool IsActive
        {
            get { return m_isActive; }
        }

        /// <summary>
        /// Is WWWRequest Timed Out?
        /// </summary>
        public bool IsTimedOut
        {
            get { return m_isTimedOut; }
        }

        /// <summary>
        /// Is WWWRequest Disposed?
        /// </summary>
        public bool IsDisposed
        {
            get { return m_isDisposed; }
        }

        /// <summary>
        /// WWWRequest Time Out Duration.
        /// </summary>
        public float TimeOutDuration
        {
            get { return m_timeOutDuration; }
        }

        /// <summary>
        /// WWWRequest Url.
        /// </summary>
        public string Url
        {
            get { return m_url; }
            set
            {
                if (string.IsNullOrEmpty(value))
                    throw new ArgumentNullException("Cannot be null or empty.", "Url");

                m_url = value;
                m_isCreated = false;
            }
        }

        /// <summary>
        /// WWWRequest Headers.
        /// </summary>
        public ReadOnlyDictionary<string, string> Headers
        {
            get
            {
                return new ReadOnlyDictionary<string, string>(m_headers);
            }
        }

        /// <summary>
        /// WWWRequest MonoBehaviour.
        /// </summary>
        public MonoBehaviour MonoBehaviour
        {
            get { return m_monoBehaviour; }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("Cannot be null.", "MonoBehaviour");

                if (!m_isActive)
                    m_monoBehaviour = value;
            }
        }

        /// <summary>
        /// On Finised.
        /// </summary>
        public OnFinish OnFinished
        {
            get { return m_onFinish; }
            set { m_onFinish = value; }
        }

        /// <summary>
        /// On Successful.
        /// </summary>
        public OnSuccess OnSuccessful
        {
            get { return m_onSuccess; }
            set { m_onSuccess = value; }
        }

        /// <summary>
        /// On Failed.
        /// </summary>
        public OnFail OnFailed
        {
            get { return m_onFail; }
            set { m_onFail = value; }
        }

        /// <summary>
        /// On Timed Out.
        /// </summary>
        public OnTimeOut OnTimedOut
        {
            get { return m_onTimeOut; }
            set { m_onTimeOut = value; }
        }

        /// <summary>
        /// On Disposed.
        /// </summary>
        public OnDispose OnDisposed
        {
            get { return m_onDispose; }
            set { m_onDispose = value; }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Creates a WWWRequest with url of /url/.
        /// </summary>
        /// <param name="url"></param>
        public WWWRequest (string url)
        {
            if (string.IsNullOrEmpty(url))
                throw new ArgumentNullException("Cannot be null or empty.", "url");

            m_url = url;
            m_wwwForm = new WWWForm();
            m_headers = new Dictionary<string, string>();
        }
        #endregion

        #region Methods
        private IEnumerator HandleRequest ()
        {
            if (!m_isCreated)
                Create();

            Debug.Log(m_www.url);

            yield return m_monoBehaviour.StartCoroutine(ProcessRequest());

            if (!m_isTimedOut && !m_isDisposed)
            {
                if (m_onFinish != null)
                    m_onFinish(m_www);

                if (string.IsNullOrEmpty(m_www.error))
                {
                    Debug.Log("Successful: " + m_www.text);

                    if (m_onSuccess != null)
                        m_onSuccess(m_www);
                }
                //else if (!string.IsNullOrEmpty(m_www.error) && !string.IsNullOrEmpty(m_www.text))
                //{
                //    Debug.LogError("Warning: " + m_www.text);

                //    if (m_onSuccess != null)
                //        m_onSuccess(m_www);

                //}
                else
                {
                    Debug.LogError("Error: " + m_www.text);

                    if (m_onFail != null)
                        m_onFail(m_www.error);

                    Dispose();
                }
            }
        }

        private IEnumerator ProcessRequest ()
        {
            if (!m_isActive)
            {
                m_isActive = true;

                float startTime = Time.time;

                while (!m_isTimedOut && !m_isDisposed && m_www != null && !m_www.isDone)
                {
                    if ((m_timeOutDuration > 0.0f) && ((Time.time - startTime) >= m_timeOutDuration))
                    {
                        TimeOut();
                        Dispose();
                        break;
                    }
                    else
                        yield return null;
                }

                m_isActive = false;
            }
        }

        /// <summary>
        /// Add binary data to the form.
        /// </summary>
        /// <param name="fieldName"></param>
        /// <param name="contents"></param>
        public void AddBinaryData (string fieldName, byte[] contents)
        {
            m_wwwForm.AddBinaryData(fieldName, contents);

            m_isPost = true;
            m_isCreated = false;
        }

        /// <summary>
        /// Add binary data to the form.
        /// </summary>
        /// <param name="fieldName"></param>
        /// <param name="contents"></param>
        /// <param name="fileName"></param>
        public void AddBinaryData (string fieldName, byte[] contents, string fileName)
        {
            m_wwwForm.AddBinaryData(fieldName, contents, fileName);

            m_isPost = true;
            m_isCreated = false;
        }

        /// <summary>
        /// Add binary data to the form.
        /// </summary>
        /// <param name="fieldName"></param>
        /// <param name="contents"></param>
        /// <param name="fileName"></param>
        /// <param name="mimeType"></param>
        public void AddBinaryData (string fieldName, byte[] contents, string fileName, string mimeType)
        {
            m_wwwForm.AddBinaryData(fieldName, contents, fileName, mimeType);

            m_isPost = true;
            m_isCreated = false;
        }

        /// <summary>
        /// Adds a simple field to the form.
        /// </summary>
        /// <param name="fieldName"></param>
        /// <param name="i"></param>
        public void AddField (string fieldName, int i)
        {
            m_wwwForm.AddField(fieldName, i);

            m_isPost = true;
            m_isCreated = false;
        }

        /// <summary>
        /// Adds a simple field to the form.
        /// </summary>
        /// <param name="fieldName"></param>
        /// <param name="value"></param>
        public void AddField (string fieldName, string value)
        {
            m_wwwForm.AddField(fieldName, value);

            m_isPost = true;
            m_isCreated = false;
        }

        /// <summary>
        /// Adds a simple field to the form.
        /// </summary>
        /// <param name="fieldName"></param>
        /// <param name="value"></param>
        /// <param name="e"></param>
        public void AddField (string fieldName, string value, Encoding e)
        {
            m_wwwForm.AddField(fieldName, value, e);

            m_isPost = true;
            m_isCreated = false;
        }

        /// <summary>
        /// Adds a header to the form.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void AddHeader (string key, string value)
        {
            if (!m_headers.ContainsKey(key))
                m_headers.Add(key, value);
            else
                m_headers[key] = value;

            m_isCreated = false;
        }

        /// <summary>
        /// Forces the WWWRequest to be a POST call.
        /// </summary>
        public void ForceAsPost ()
        {
            m_isPost = true;
        }

        /// <summary>
        /// Initialize the WWWRequest.
        /// </summary>
        public void Create ()
        {
            if (!m_isPost)
                m_www = new WWW(m_url);
            else
            {
                foreach (KeyValuePair<string, string> header in m_wwwForm.headers)
                    m_headers[header.Key] = header.Value;

                m_www = new WWW(m_url, m_wwwForm.data, m_headers);
            }

            m_isCreated = true;
        }

        /// <summary>
        /// Dispose the WWWRequest.
        /// </summary>
        public void Dispose ()
        {
            if (m_isDisposed || !m_isActive || m_www == null)
                return;

            m_www.Dispose();

            m_isDisposed = true;

            if (m_onDispose != null)
                m_onDispose();
        }

        /// <summary>
        /// Start the WWWRequest.
        /// </summary>
        /// <param name="monoBehaviour"></param>
        public void Request (MonoBehaviour monoBehaviour)
        {
            if (monoBehaviour == null)
                throw new ArgumentNullException("Cannot be null.", "monoBehaviour");

            if (m_isActive)
                throw new InvalidOperationException("Cannot perform request as it is already active.");

            m_monoBehaviour = monoBehaviour;
            m_monoBehaviour.StartCoroutine(HandleRequest());
        }

        /// <summary>
        /// Force TimeOut the WWWRequest.
        /// </summary>
        public void TimeOut ()
        {
            if (m_isTimedOut || !m_isActive || m_www == null)
                return;

            m_isTimedOut = true;

            if (m_onTimeOut != null)
                m_onTimeOut();
        }
        #endregion
    }
}
