﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace UnityEssentials
{
    /// <summary>
    /// A pool of dice that can store its results.
    /// </summary>
    public class DicePool
    {
        #region Enums
        /// <summary>
        /// 
        /// </summary>
        public enum DiceType
        {
            /// <summary>
            /// A die with an 'n' number of faces.
            /// </summary>
            Custom = 1,

            /// <summary>
            /// A coin.
            /// </summary>
            D2 = 2,

            /// <summary>
            /// A tetrahedon.
            /// </summary>
            D4 = 4,

            /// <summary>
            /// A cube.
            /// </summary>
            D6 = 6,

            /// <summary>
            /// An octahedron.
            /// </summary>
            D8 = 8,

            /// <summary>
            /// A deltohedron.
            /// </summary>
            D10 = 10,

            /// <summary>
            /// A dodecahedron.
            /// </summary>
            D12 = 12,

            /// <summary>
            /// An icosahedron.
            /// </summary>
            D20 = 20,

            /// <summary>
            /// A zocchihedron.
            /// </summary>
            D100 = 100
        }
        #endregion

        #region Fields
        [SerializeField]
        private DiceType m_type = DiceType.D20;

        [SerializeField, HideInInspector]
        private int m_faces = 20;

        [SerializeField]
        private int m_size = 0;

        [SerializeField, HideInInspector]
        private List<int> m_rolls = new List<int>();
        #endregion

        #region Properties
        /// <summary>
        /// The Dice Type used by the Dice Pool.
        /// </summary>
        public DiceType Type
        {
            get { return m_type; }
            set { SetType(value); }
        }

        /// <summary>
        /// Number of Faces of the Dice Type used by the Dice Pool.
        /// </summary>
        public int Faces
        {
            get { return m_faces; }
            set { SetFaces(value); }
        }

        /// <summary>
        /// Size of the Dice Pool.
        /// </summary>
        public int Size
        {
            get { return m_size; }
            set { m_size = value; }
        }

        /// <summary>
        /// Rolls of the Dice Pool.
        /// </summary>
        public int[] Rolls
        {
            get { return m_rolls.ToArray(); }
        }

        /// <summary>
        /// Highest Roll of the Dice Pool.
        /// </summary>
        public int HighestRoll
        {
            get { return m_rolls.Max(); }
        }

        /// <summary>
        /// Lowest Roll of the Dice Pool.
        /// </summary>
        public int LowestRoll
        {
            get { return m_rolls.Min(); }
        }

        /// <summary>
        /// Total Roll of the Dice Pool.
        /// </summary>
        public int TotalRoll
        {
            get { return m_rolls.Sum(); }
        }

        /// <summary>
        /// Average of the Rolls of the Dice Pool.
        /// </summary>
        public float AverageRoll
        {
            get { return (float) m_rolls.Average(); }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Creates a Dice Pool with /size/ and Dice Type of /type/.
        /// </summary>
        /// <param name="size"></param>
        /// <param name="type"></param>
        public DicePool (int size, DiceType type = DiceType.D20)
        {
            Size = size;
            Type = type;

            Reroll();
        }

        /// <summary>
        /// Creates a Dice Pool with /size/ and D/faces/.
        /// </summary>
        /// <param name="size"></param>
        /// <param name="faces"></param>
        public DicePool (int size, int faces)
        {
            Size = size;
            Faces = faces;

            Reroll();
        }
        #endregion

        #region Methods
        /// <summary>
        /// Sets the Dice Type and Rerolls the Dice Pool.
        /// </summary>
        /// <param name="value"></param>
        public void SetType (DiceType value)
        {
            m_type = value;

            m_faces = (int) value;

            Reroll();
        }

        /// <summary>
        /// Sets the Faces (Dice Type) and Rerolls the Dice Pool.
        /// </summary>
        /// <param name="value"></param>
        public void SetFaces (int value)
        {
            m_faces = value;

            if (Enum.IsDefined(typeof(DiceType), value))
                m_type = (DiceType) value;
            else
                m_type = DiceType.Custom;

            Reroll();
        }

        /// <summary>
        /// Rerolls the Dice Pool.
        /// </summary>
        public void Reroll ()
        {
            m_rolls.Clear();

            for (int i = 0; i < m_size; i++)
                m_rolls.Add(UnityEngine.Random.Range(1, m_faces + 1));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString ()
        {
            return string.Format("{0}D{1}", m_size, m_faces);
        }
        #endregion
    }
}
