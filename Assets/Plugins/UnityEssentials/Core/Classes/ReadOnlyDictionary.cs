﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace UnityEssentials
{
    /// <summary>
    /// Represents a generic collection of read-only key/value pairs.
    /// </summary>
    /// <typeparam name="TKey">The type of keys in the dictionary.</typeparam>
    /// <typeparam name="TValue">The type of values in the dictionary.</typeparam>
    public sealed class ReadOnlyDictionary<TKey, TValue> : IDictionary<TKey, TValue>
    {
        #region Fields
        private readonly IDictionary<TKey, TValue> m_dictionary;
        #endregion

        #region Properties
        /// <summary>
        /// Gets the value associated with the specified key.
        /// </summary>
        /// <param name="key">The key whose value to get.</param>
        /// <returns></returns>
        public TValue this [TKey key]
        {
            get { return m_dictionary[key]; }
            set { throw new Exception("Read only."); }
        }

        /// <summary>
        /// Gets the number of elements contained in the ICollection&lt;T&gt;.
        /// </summary>
        public int Count
        {
            get { return m_dictionary.Count; }
        }

        /// <summary>
        /// Gets a value indicating whether the ICollection&lt;T&gt; is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return true; }
        }

        /// <summary>
        /// Gets an ICollection&lt;T&gt; containing the keys of the IDictionary&lt;TKey, TValue&gt;.
        /// </summary>
        public ICollection<TKey> Keys
        {
            get
            {
                return m_dictionary.Keys;
            }
        }

        /// <summary>
        /// Gets an ICollection&lt;T&gt; containing the values of the IDictionary&lt;TKey, TValue&gt;.
        /// </summary>
        public ICollection<TValue> Values
        {
            get
            {
                return m_dictionary.Values;
            }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Represents a generic collection of read-only key/value pairs.
        /// </summary>
        public ReadOnlyDictionary ()
        {
            m_dictionary = new Dictionary<TKey, TValue>();
        }

        /// <summary>
        /// Represents a generic collection of read-only key/value pairs.
        /// </summary>
        /// <param name="dictionary">A generic collection of key/value pairs</param>
        public ReadOnlyDictionary (IDictionary<TKey, TValue> dictionary)
        {
            m_dictionary = dictionary;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Adds an item to the ICollection&lt;T&gt;.
        /// </summary>
        /// <param name="item">The object to add to the ICollection&lt;T&gt;.</param>
        public void Add (KeyValuePair<TKey, TValue> item)
        {
            throw new Exception("Read only.");
        }

        /// <summary>
        /// Adds an element with the provided key and value to the IDictionary&lt;TKey, TValue&gt;. (Read Only)
        /// </summary>
        /// <param name="key">The object to use as the key of the element to add.</param>
        /// <param name="value">The object to use as the value of the element to add.</param>
        public void Add (TKey key, TValue value)
        {
            throw new Exception("Read only.");
        }

        /// <summary>
        /// Removes all items from the ICollection&lt;T&gt;. (Read only)
        /// </summary>
        public void Clear ()
        {
            throw new Exception("Read only.");
        }

        /// <summary>
        /// Determines whether the ICollection&lt;T&gt; contains a specific value.
        /// </summary>
        /// <param name="item">The object to locate in the ICollection&lt;T&gt;.</param>
        /// <returns></returns>
        public bool Contains (KeyValuePair<TKey, TValue> item)
        {
            return m_dictionary.Contains(item);
        }

        /// <summary>
        /// Determines whether the IDictionary&lt;TKey, TValue&gt; contains an element with the specified key.
        /// </summary>
        /// <param name="key">The key to locate in the IDictionary&lt;TKey, TValue&gt;.</param>
        /// <returns></returns>
        public bool ContainsKey (TKey key)
        {
            return m_dictionary.ContainsKey(key);
        }

        /// <summary>
        /// Copies the elements of the ICollection&lt;T&gt; to an Array, starting at a particular Array index.
        /// </summary>
        /// <param name="array">The one-dimensional Array that is the destination of the elements copied from ICollection&lt;T&gt;. The Array must have zero-based indexing.</param>
        /// <param name="arrayIndex">The zero-based index in array at which copying begins.</param>
        public void CopyTo (KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            m_dictionary.CopyTo(array, arrayIndex);
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns></returns>
        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator ()
        {
            return m_dictionary.GetEnumerator();
        }

        /// <summary>
        /// Removes the first occurence of a specific object from the ICollection&lt;T&gt;. (Read only)
        /// </summary>
        /// <param name="item">The object to remove from the ICollection&lt;T&gt;.</param>
        /// <returns></returns>
        public bool Remove (KeyValuePair<TKey, TValue> item)
        {
            throw new Exception("Read only.");
        }

        /// <summary>
        /// Removes the element with the specified key from the IDictionary&lt;TKey, TValue&gt;. (Read only)
        /// </summary>
        /// <param name="key">The key of the element to remove.</param>
        /// <returns></returns>
        public bool Remove (TKey key)
        {
            throw new Exception("Read only.");
        }

        /// <summary>
        /// Gets the value associated with the specified key.
        /// </summary>
        /// <param name="key">The key whose value to get.</param>
        /// <param name="value">When this methods returns, the value associated with the specific key, if the key is found; otherwise, the default value for the type of the value parameter. This parameter is passed uninitialized.</param>
        /// <returns></returns>
        public bool TryGetValue (TKey key, out TValue value)
        {
            return m_dictionary.TryGetValue(key, out value);
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns></returns>
        IEnumerator IEnumerable.GetEnumerator ()
        {
            return m_dictionary.GetEnumerator();
        }
        #endregion
    }
}
