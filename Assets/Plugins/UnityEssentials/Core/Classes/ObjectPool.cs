﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace UnityEssentials
{
    /// <summary>
    /// Creates a pool for a ObjectPoolMember to be reused.
    /// </summary>
    public class ObjectPool
    {
        #region Fields
        private bool m_isInitialized = false;
        private int m_poolSizeInitial = 1;
        private int m_poolSizeMax = 100;
        private ObjectPoolMember m_pooledMember;
        private List<ObjectPoolMember> m_pooledMembers;
        #endregion

        #region Properties
        /// <summary>
        /// Initial Pool Size.
        /// </summary>
        public int PoolSizeInitial
        {
            get { return m_poolSizeInitial; }
        }

        /// <summary>
        /// Max Pool Size.
        /// </summary>
        public int PoolSizeMax
        {
            get { return m_poolSizeMax; }
        }

        /// <summary>
        /// Pooled Member.
        /// </summary>
        public ObjectPoolMember PooledMember
        {
            get { return m_pooledMember; }
        }

        /// <summary>
        /// Pooled Members.
        /// </summary>
        public IList<ObjectPoolMember> PooledMembers
        {
            get { return m_pooledMembers.AsReadOnly(); }
        }

        /// <summary>
        /// Active Pooled Members.
        /// </summary>
        public IList<ObjectPoolMember> ActivePooledMembers
        {
            get { return m_pooledMembers.Where(x => x.IsActive).ToList().AsReadOnly(); }
        }

        /// <summary>
        /// Inactive Pooled Members.
        /// </summary>
        public IList<ObjectPoolMember> InactivePooledMembers
        {
            get { return m_pooledMembers.Where(x => !x.IsActive).ToList().AsReadOnly(); }
        }

        /// <summary>
        /// Pooled Members Count.
        /// </summary>
        public int PooledMembersCount
        {
            get { return m_pooledMembers.Count; }
        }

        /// <summary>
        /// Active Pooled Members Count.
        /// </summary>
        public int ActivePooledMembersCount
        {
            get { return m_pooledMembers.Where(x => x.IsActive).Count(); }
        }

        /// <summary>
        /// Inactive Pooled Members Count.
        /// </summary>
        public int InactivePooledMembersCount
        {
            get { return m_pooledMembers.Where(x => !x.IsActive).Count(); }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Creates an pool for /pooledMember/ with an initial size of /poolSizeInitial/ and a max size of /poolSizeMax/.
        /// </summary>
        /// <param name="pooledMember"></param>
        /// <param name="poolSizeInitial"></param>
        /// <param name="poolSizeMax"></param>
        public ObjectPool (ObjectPoolMember pooledMember, int poolSizeInitial, int poolSizeMax)
        {
            if (pooledMember == null)
                throw new ArgumentNullException("Cannot be null.", "pooledMember");

            if (poolSizeInitial < 0)
                throw new ArgumentException("Cannot be less than zero.", "poolSizeInitial");

            if (poolSizeMax < poolSizeInitial)
                throw new ArgumentException("Cannot be less than " + "poolSizeInitial" + ".", "poolSizeMax");

            m_poolSizeInitial = poolSizeInitial;
            m_poolSizeMax = poolSizeMax;
            m_pooledMember = pooledMember;
            m_pooledMembers = new List<ObjectPoolMember>();

            InitializePool();
        }
        #endregion

        #region Methods
        private ObjectPoolMember CreatePoolMember ()
        {
            if (PooledMembersCount >= PoolSizeMax)
                return null;

            GameObject newPooledGameObject = UnityEngine.Object.Instantiate(m_pooledMember.gameObject, Vector3.zero, Quaternion.identity) as GameObject;

            if (newPooledGameObject == null)
                throw new NullReferenceException("Failed to instantiate " + "m_pooledMember" + ".");

            ObjectPoolMember newPooledMember = newPooledGameObject.GetComponent<ObjectPoolMember>();

            if (newPooledMember == null)
                newPooledMember = newPooledGameObject.AddComponent<ObjectPoolMember>();

            if (newPooledMember == null)
                throw new MissingComponentException("Failed to get component " + "ObjectPoolMember" + ".");

            newPooledMember.RegisterMember(m_pooledMember.GetInstanceID());
            m_pooledMembers.Add(newPooledMember);

            return newPooledMember;
        }

        private void InitializePool ()
        {
            if (m_isInitialized)
                return;

            m_isInitialized = true;

            for (int i = 0; i < m_poolSizeInitial; i++)
                CreatePoolMember();
        }

        /// <summary>
        /// Tries to remove inactive pooled members until the initial pool size is reached.
        /// </summary>
        public void Shrink ()
        {
            int membersToRemove = m_pooledMembers.Count - m_poolSizeInitial;

            if (membersToRemove <= 0)
                return;

            ObjectPoolMember memberToRemove = null;

            for (int i = m_pooledMembers.Count - 1; i >= 0; i--)
            {
                if (membersToRemove <= 0)
                    break;

                if (m_pooledMembers[i] == null)
                {
                    m_pooledMembers.RemoveAt(i);
                    membersToRemove--;
                    continue;
                }

                memberToRemove = m_pooledMembers[i];

                if (memberToRemove != null && !memberToRemove.IsActive)
                {
                    m_pooledMembers.Remove(memberToRemove);
                    membersToRemove--;
                    UnityEngine.Object.Destroy(memberToRemove.gameObject);
                }

                memberToRemove = null;
            }
        }

        /// <summary>
        /// Gets an inactive pooled member to use.
        /// </summary>
        /// <param name="position"></param>
        /// <param name="rotation"></param>
        /// <returns></returns>
        public GameObject GetPoolMember (Vector3 position, Quaternion rotation)
        {
            for (int i = 0; i < m_pooledMembers.Count; i++)
            {
                if (m_pooledMembers[i] != null && !m_pooledMembers[i].IsActive)
                {
                    m_pooledMembers[i].Acquire();
                    return m_pooledMembers[i].gameObject;
                }
            }

            ObjectPoolMember newPooledMember = CreatePoolMember();

            newPooledMember.Acquire();
            return newPooledMember.gameObject;
        }
        #endregion
    }
}
