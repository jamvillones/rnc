﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace UnityEssentials
{
    /// <summary>
    /// A modified version of UnityEngine.PlayerPrefs.
    /// Stores and accesses player preferences between game sessions.
    /// </summary>
    public static class UEPlayerPrefs
    {
        private const string UEPLAYERPREFS_KEYLIST_KEY = "UEPlayerPrefs.KeyList";

        /// <summary>
        /// Saved UnityEngine.Playerprefs Keys.
        /// </summary>
        public static List<string> Keys = new List<string>();

        /// <summary>
        /// Removes all keys and values from the preferences. Use with caution.
        /// </summary>
        public static void DeleteAll()
        {
            PlayerPrefs.DeleteAll();

            Keys.Clear();
        }

        /// <summary>
        /// Removes key and its corresponding value from the preferences.
        /// </summary>
        /// <param name="key"></param>
        public static void DeleteKey(string key)
        {
            PlayerPrefs.DeleteKey(key);

            if (Keys.Contains(key))
                Keys.Remove(key);
        }

        /// <summary>
        /// Returns the value corresponding to key in the preferences file if it exists; if not then set the default value in the preferences file and return it.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static double GetDouble(string key, double defaultValue)
        {
            string defaultValueString = defaultValue.ToString();

            if (!PlayerPrefs.HasKey(key))
                SetString(key, defaultValueString);

            string doubleValueString = PlayerPrefs.GetString(key, defaultValueString);
            double doubleValue = defaultValue;

            if (double.TryParse(doubleValueString, out doubleValue))
                return doubleValue;
            else
                return defaultValue;
        }

        /// <summary>
        /// Returns the value corresponding to key in the preferences file if it exists; if not then set the default value in the preferences file and return it.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static float GetFloat(string key, float defaultValue)
        {
            if (!PlayerPrefs.HasKey(key))
                SetFloat(key, defaultValue);

            return PlayerPrefs.GetFloat(key, defaultValue);
        }

        /// <summary>
        /// Returns the value corresponding to key in the preferences file if it exists; if not then set the default value in the preferences file and return it.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static int GetInt(string key, int defaultValue)
        {
            if (!PlayerPrefs.HasKey(key))
                SetInt(key, defaultValue);

            return PlayerPrefs.GetInt(key, defaultValue);
        }

        /// <summary>
        /// Returns the value corresponding to key in the preferences file if it exists; if not then set the default value in the preferences file and return it.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static string GetString(string key, string defaultValue)
        {
            if (!PlayerPrefs.HasKey(key))
                SetString(key, defaultValue);

            return PlayerPrefs.GetString(key, defaultValue);
        }

        /// <summary>
        /// Returns true if key exists in the preferences.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool HasKey(string key)
        {
            bool result = PlayerPrefs.HasKey(key);

            if (result && !Keys.Contains(key))
                Keys.Add(key);
            else if (!result && Keys.Contains(key))
                Keys.Remove(key);

            return result;
        }

        /// <summary>
        /// Reads all saved keys in the preferences.
        /// Needs to be called outside.
        /// </summary>
        public static void Load()
        {
            Keys.Clear();

            if (PlayerPrefs.HasKey(UEPLAYERPREFS_KEYLIST_KEY))
                Keys = PlayerPrefs.GetString(UEPLAYERPREFS_KEYLIST_KEY).Split(',').ToList();
        }

        /// <summary>
        /// Writes all modified preferences to disk.
        /// </summary>
        public static void Save()
        {
            PlayerPrefs.Save();

            PlayerPrefs.SetString(UEPLAYERPREFS_KEYLIST_KEY, string.Join(",", Keys.ToArray()));
        }

        /// <summary>
        /// Sets the value of the preference identified by key.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public static void SetDouble(string key, double value)
        {
            PlayerPrefs.SetString(key, value.ToString());

            if (!Keys.Contains(key))
                Keys.Add(key);
        }

        /// <summary>
        /// Sets the value of the preference identified by key.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public static void SetFloat(string key, float value)
        {
            PlayerPrefs.SetFloat(key, value);

            if (!Keys.Contains(key))
                Keys.Add(key);
        }

        /// <summary>
        /// Sets the value of the preference identified by key.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public static void SetInt(string key, int value)
        {
            PlayerPrefs.SetInt(key, value);

            if (!Keys.Contains(key))
                Keys.Add(key);
        }

        /// <summary>
        /// Sets the value of the preference identified by key.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public static void SetString(string key, string value)
        {
            PlayerPrefs.SetString(key, value);

            if (!Keys.Contains(key))
                Keys.Add(key);
        }
    }
}