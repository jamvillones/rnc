﻿using UnityEngine;

namespace UnityEssentials
{
    /// <summary>
    /// A helper class that quits and/or cancels quitting.
    /// </summary>
    public class ApplicationQuitter : MonoBehaviour
    {
        #region Methods
        /// <summary>
        /// Quits the player application.
        /// </summary>
        [ContextMenu("Quit")]
        public void Quit ()
        {
            Application.Quit();
        }

        /// <summary>
        /// Cancels quitting the application. This is useful for showing a splash screen at the end of a game.
        /// </summary>
        [ContextMenu("Cancel Quit")]
        public void CancelQuit ()
        {
            Application.CancelQuit();
        }
        #endregion
    }
}
