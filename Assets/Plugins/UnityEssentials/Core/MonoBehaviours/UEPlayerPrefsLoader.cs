﻿namespace UnityEssentials
{
    public class UEPlayerPrefsLoader : Singleton<UEPlayerPrefsLoader>
    {
        protected override void Awake()
        {
            base.Awake();

            UEPlayerPrefs.SetDouble("test", 0.001123313);
            UEPlayerPrefs.Save();

            UEPlayerPrefs.Load();
        }
    }
}
