﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace UnityEssentials
{
    /// <summary>
    /// A class that executes UnityEvents when triggered.
    /// </summary>
    public class UEEvent : MonoBehaviour
    {
        #region Enums
        /// <summary>
        /// Trigger Type of UEEvents.
        /// </summary>
        public enum UEEventType
        {
            /// <summary>
            /// Triggers only when another UnityEvent triggers this UEEvent through the use of 'TriggerEvent' method.
            /// </summary>
            Event,

            /// <summary>
            /// Triggers only when a key is pressed, held down, or released.
            /// </summary>
            Key,

            /// <summary>
            /// Triggers only after an initial delay is finished.
            /// </summary>
            TimerOnce,

            /// <summary>
            /// Triggers once after an initial delay and repeats for /x/ times after another delay.
            /// </summary>
            TimerRepeat
        }

        /// <summary>
        /// Key Press Type of UEEvents.
        /// </summary>
        public enum UEEventKeyPressType
        {
            /// <summary>
            /// Triggers once when a key is pressed.
            /// </summary>
            Start,

            /// <summary>
            /// Triggers continuously when a key is held down.
            /// </summary>
            Down,

            /// <summary>
            /// Triggers once when a key is released.
            /// </summary>
            End
        }
        #endregion

        #region Fields
        [SerializeField, HideInInspector]
        private UEEventType m_eventType = UEEventType.Event;

        [SerializeField, HideInInspector]
        private UEEventKeyPressType m_keyPressType = UEEventKeyPressType.Start;

        [SerializeField, HideInInspector]
        private KeyCode m_keyCode = KeyCode.None;

        [SerializeField, HideInInspector]
        private float m_timerDelay = 0.0f;

        [SerializeField, HideInInspector]
        private float m_timerRepeatDelay = 0.0f;

        [SerializeField, HideInInspector]
        private int m_timerRepeatsMax = 1;

        [SerializeField, HideInInspector]
        private int m_invokesMax = 1;

        [SerializeField, HideInInspector]
        private UnityEvent m_onInvoke;

        [SerializeField, HideInInspector]
        private bool m_timerStarted = false;

        [SerializeField, HideInInspector]
        private bool m_timerActive = false;

        [SerializeField, HideInInspector]
        private bool m_timerEnded = false;

        [SerializeField, HideInInspector]
        private int m_timerRepeats = 0;

        [SerializeField, HideInInspector]
        private int m_invokes = 0;
        #endregion

        #region Properties
        /// <summary>
        /// UEEventType of this UEEvent.
        /// </summary>
        public UEEventType EventType
        {
            get { return m_eventType; }
        }

        /// <summary>
        /// UEEventKeyPressType of this UEEvent.
        /// </summary>
        public UEEventKeyPressType KeyPressType
        {
            get { return m_keyPressType; }
        }

        /// <summary>
        /// KeyCode used to trigger this UEEvent.
        /// </summary>
        public KeyCode KeyCode
        {
            get { return m_keyCode; }
        }

        /// <summary>
        /// Initial Delay used to trigger this UEEvent.
        /// </summary>
        public float TimerDelay
        {
            get { return m_timerDelay; }
        }

        /// <summary>
        /// Repeat Delay used to trigger this UEEvent.
        /// </summary>
        public float TimerRepeatDelay
        {
            get { return m_timerRepeatDelay; }
        }

        /// <summary>
        /// Max Timer Repeats of this UEEvent.
        /// </summary>
        public int TimerRepeatsMax
        {
            get { return m_timerRepeatsMax; }
        }

        /// <summary>
        /// Max Invokes of this UEEvent.
        /// </summary>
        public int InvokesMax
        {
            get { return m_invokesMax; }
        }

        /// <summary>
        /// UnityEvent that triggers when this UEEvent is invoked.
        /// </summary>
        public UnityEvent OnInvoke
        {
            get { return m_onInvoke; }
        }

        /// <summary>
        /// Is Timer Started?
        /// </summary>
        public bool TimerStarted
        {
            get { return m_timerStarted; }
        }

        /// <summary>
        /// Is Timer Active?
        /// </summary>
        public bool TimerActive
        {
            get { return m_timerActive; }
        }

        /// <summary>
        /// Is Timer Ended?
        /// </summary>
        public bool TimerEnded
        {
            get { return m_timerEnded; }
        }

        /// <summary>
        /// Number of times this Timer repeated.
        /// </summary>
        public int TimerRepeats
        {
            get { return m_timerRepeats; }
        }

        /// <summary>
        /// Number of times this UEEvent was invoked.
        /// </summary>
        public int Invokes
        {
            get { return m_invokes; }
        }
        #endregion

        #region Methods
        private void Initialize ()
        {
            switch (m_eventType)
            {
                case UEEventType.Event:
                    break;

                case UEEventType.Key:
                    break;

                case UEEventType.TimerOnce:
                case UEEventType.TimerRepeat:
                    StartTimer();
                    break;

                default:
                    break;
            }
        }

        private void Execute ()
        {
            if (m_invokesMax > 0 && m_invokes >= m_invokesMax)
                return;

            m_invokes++;

            if (m_onInvoke != null)
                m_onInvoke.Invoke();
        }

        private void GetKeyInput ()
        {
            if (m_eventType.Equals(UEEventType.Key) &&
                ((m_keyPressType.Equals(UEEventKeyPressType.Start) && Input.GetKeyDown(m_keyCode)) ||
                 (m_keyPressType.Equals(UEEventKeyPressType.Down) && Input.GetKey(m_keyCode)) ||
                 (m_keyPressType.Equals(UEEventKeyPressType.End) && Input.GetKeyUp(m_keyCode))))
                Execute();
        }

        private IEnumerator InvokeTimerOnce (float time)
        {
            if (!m_timerStarted && !m_timerActive && !m_timerEnded)
            {
                m_timerStarted = true;
                m_timerActive = true;

                yield return new WaitForSeconds(time);

                Execute();

                m_timerActive = false;
                m_timerEnded = true;
            }
        }

        private IEnumerator InvokeTimerRepeat (float time, float repeatRate)
        {
            if (!m_timerStarted && !m_timerActive && !m_timerEnded)
            {
                m_timerStarted = true;
                m_timerActive = true;

                yield return new WaitForSeconds(time);

                Execute();

                while (m_timerRepeatsMax < 0 || m_timerRepeats < m_timerRepeatsMax)
                {
                    yield return new WaitForSeconds(repeatRate);

                    m_timerRepeats++;
                    Execute();
                }

                m_timerActive = false;
                m_timerEnded = true;
            }
        }

        /// <summary>
        /// Trigger this UEEvent.
        /// </summary>
        [ContextMenu("Trigger Event")]
        public void TriggerEvent ()
        {
            Execute();
        }

        /// <summary>
        /// Reset this UEEvent invoked counter.
        /// </summary>
        [ContextMenu("Reset Event Counter")]
        public void ResetEventCounter ()
        {
            m_invokes = 0;
        }

        /// <summary>
        /// Start the Timer of this UEEvent.
        /// </summary>
        [ContextMenu("Start Timer")]
        public void StartTimer ()
        {
            switch (m_eventType)
            {
                case UEEventType.TimerOnce:
                    StartCoroutine(InvokeTimerOnce(m_timerDelay));
                    break;

                case UEEventType.TimerRepeat:
                    StartCoroutine(InvokeTimerRepeat(m_timerDelay, m_timerRepeatDelay));
                    break;
            }
        }

        /// <summary>
        /// Stop the Timer of this UEEvent.
        /// </summary>
        [ContextMenu("Stop Timer")]
        public void StopTimer ()
        {
            StopAllCoroutines();

            m_timerActive = false;
            m_timerEnded = true;
        }

        /// <summary>
        /// Reset the Timer of this UEEvent.
        /// </summary>
        [ContextMenu("Reset Timer")]
        public void ResetTimer ()
        {
            StopTimer();

            m_timerStarted = false;
            m_timerActive = false;
            m_timerEnded = false;
            m_timerRepeats = 0;
        }

        /// <summary>
        /// Reset the this UEEvent.
        /// </summary>
        [ContextMenu("Reset Event")]
        public void ResetEvent ()
        {
            ResetEventCounter();
            ResetTimer();
        }
        #endregion

        #region MonoBehaviour Messages
        /// <summary>
        /// 
        /// </summary>
        protected virtual void Start ()
        {
            Initialize();
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual void Update ()
        {
            GetKeyInput();
        }
        #endregion
    }
}
