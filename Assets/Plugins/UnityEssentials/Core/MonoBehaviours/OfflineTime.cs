﻿using System;
using UnityEngine;

namespace UnityEssentials
{
    /// <summary>
    /// A singleton that stores the DateTime since last online saved in PlayerPrefs.
    /// </summary>
    public class OfflineTime : Singleton<OfflineTime>
    {
        #region Constants
        /// <summary>
        /// PlayerPrefs Key used to store the LastOnline data.
        /// </summary>
        public const string PLAYERPREFS_KEY = "OfflineTime.LastOnline";
        #endregion

        #region Fields
        [SerializeField, HideInInspector]
        private DateTime m_lastOnlineUTC = new DateTime();

        [SerializeField, HideInInspector]
        private TimeSpan m_timePassed = new TimeSpan();
        #endregion

        #region Properties
        /// <summary>
        /// Last Online represented in Local.
        /// </summary>
        public DateTime LastOnlineLocal
        {
            get { return DateTime.SpecifyKind(m_lastOnlineUTC, DateTimeKind.Local); }
        }

        /// <summary>
        /// Last Online represented in UTC.
        /// </summary>
        public DateTime LasOnlineUTC
        {
            get { return DateTime.SpecifyKind(m_lastOnlineUTC, DateTimeKind.Utc); }
        }

        /// <summary>
        /// Time Passed since Last Online.
        /// </summary>
        public TimeSpan TimePassed
        {
            get { return m_timePassed; }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        protected OfflineTime () { }
        #endregion

        #region Methods
        private void Initialize ()
        {
            if (!PlayerPrefs.HasKey(PLAYERPREFS_KEY))
                UpdateLastOnline();

            m_lastOnlineUTC = DateTime.Parse(PlayerPrefs.GetString(PLAYERPREFS_KEY));
            m_timePassed = DateTime.UtcNow - m_lastOnlineUTC;
        }

        private void UpdateLastOnline ()
        {
            PlayerPrefs.SetString(PLAYERPREFS_KEY, DateTime.UtcNow.ToString("G"));
        }

        private void Clear ()
        {
            m_lastOnlineUTC = new DateTime();
            m_timePassed = new TimeSpan();

            PlayerPrefs.DeleteKey(PLAYERPREFS_KEY);
        }
        #endregion

        #region MonoBehaviour Messages
        /// <summary>
        /// 
        /// </summary>
        protected override void Awake ()
        {
            base.Awake();

            if (IsInstance)
                Initialize();
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void OnDestroy ()
        {
            base.OnDestroy();

            if (IsInstance)
                UpdateLastOnline();
        }
        #endregion
    }
}
