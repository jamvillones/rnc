﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace UnityEssentials
{
    /// <summary>
    /// A factory class for ObjectPool.
    /// </summary>
    public class ObjectPoolFactory : Singleton<ObjectPoolFactory>
    {
        #region Fields
        private bool m_isInitialized = false;
        private Dictionary<int, ObjectPool> m_objectPools;
        #endregion

        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        protected ObjectPoolFactory () { }
        #endregion

        #region Methods
        private void Initialize ()
        {
            if (m_isInitialized)
                return;

            m_isInitialized = true;

            m_objectPools = new Dictionary<int, ObjectPool>();
        }

        /// <summary>
        /// Creates an ObjectPool for a ObjectPoolMember with an initial size of /poolSizeInitial/ and a max size of /poolSizeMax/.
        /// </summary>
        /// <param name="pooledMember"></param>
        /// <param name="poolSizeInitial"></param>
        /// <param name="poolSizeMax"></param>
        /// <returns></returns>
        public bool CreateObjectPool (ObjectPoolMember pooledMember, int poolSizeInitial, int poolSizeMax)
        {
            if (m_objectPools.ContainsKey(pooledMember.GetInstanceID()))
                return false;
            else
            {
                ObjectPool newObjectPool = new ObjectPool(pooledMember, poolSizeInitial, poolSizeMax);

                if (newObjectPool == null)
                    throw new NullReferenceException("Failed to create object pool of " + "pooledMember" + ".");

                m_objectPools.Add(pooledMember.GetInstanceID(), newObjectPool);
                return true;
            }
        }

        /// <summary>
        /// Gets the proper ObjectPool for /pooledMember/.
        /// </summary>
        /// <param name="pooledMember"></param>
        /// <returns></returns>
        public ObjectPool GetObjectPool (ObjectPoolMember pooledMember)
        {
            if (m_objectPools.ContainsKey(pooledMember.GetInstanceID()))
                return m_objectPools[pooledMember.GetInstanceID()];
            else
                return null;
        }

        /// <summary>
        /// Gets an inactive pooled member to use from the proper ObjectPool for /pooledMember/.
        /// </summary>
        /// <param name="pooledMember"></param>
        /// <param name="position"></param>
        /// <param name="rotation"></param>
        /// <returns></returns>
        public GameObject GetPoolMember (ObjectPoolMember pooledMember, Vector3 position, Quaternion rotation)
        {
            if (m_objectPools.ContainsKey(pooledMember.GetInstanceID()))
                return m_objectPools[pooledMember.GetInstanceID()].GetPoolMember(position, rotation);
            return null;
        }
        #endregion

        #region MonoBehaviour Messages
        /// <summary>
        /// 
        /// </summary>
        protected override void Awake ()
        {
            base.Awake();

            if (IsInstance)
                Initialize();
        }
        #endregion
    }
}
