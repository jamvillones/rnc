﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace UnityEssentials
{
    /// <summary>
    /// A helper class that loads scenes.
    /// </summary>
    public class SceneLoader : MonoBehaviour
    {
        #region Fields
        //[SerializeField]
        //private string m_name = string.Empty;
        #endregion

        #region Methods
        /// <summary>
        /// Loads the scene by its name.
        /// </summary>
        [ContextMenu("Load Scene")]
        public void LoadScene (string m_name)
        {
            if (!string.IsNullOrEmpty(m_name))
                SceneManager.LoadScene(m_name);
        }
        #endregion
    }
}
