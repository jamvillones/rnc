﻿using UnityEngine;
using UnityEngine.Events;

namespace UnityEssentials
{
    /// <summary>
    /// An object member to an ObjectPool.
    /// </summary>
    public class ObjectPoolMember : MonoBehaviour
    {
        #region Fields
        private bool m_isInitialized = false;
        private bool m_isActive = false;
        private int m_rootInstanceId = 0;

        [SerializeField]
        private bool m_releaseOnDisable = false;

        [SerializeField]
        private UnityEvent m_onAcquire = null;

        [SerializeField]
        private UnityEvent m_onRelease = null;
        #endregion

        #region Properties
        /// <summary>
        /// Is Active?
        /// </summary>
        public bool IsActive
        {
            get { return m_isActive; }
        }

        /// <summary>
        /// Instance ID of the root instance.
        /// </summary>
        public int RootInstanceId
        {
            get { return m_rootInstanceId; }
        }

        /// <summary>
        /// UnityEvent that triggers when an ObjectPoolMember is used.
        /// </summary>
        public UnityEvent OnAcquire
        {
            get { return m_onAcquire; }
        }

        /// <summary>
        /// UnityEvent that triggers when an ObjectPoolMember is disposed.
        /// </summary>
        public UnityEvent OnRelease
        {
            get { return m_onRelease; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Registers the ID of the root instance.
        /// </summary>
        /// <param name="rootInstanceId"></param>
        public void RegisterMember (int rootInstanceId)
        {
            if (m_isInitialized)
                return;

            m_isInitialized = true;

            m_rootInstanceId = rootInstanceId;
        }

        /// <summary>
        /// Try to use this ObjectPoolMember.
        /// </summary>
        public void Acquire ()
        {
            if (!m_isInitialized)
                return;

            if (m_isActive)
                return;

            m_isActive = true;

            if (m_onAcquire != null)
                m_onAcquire.Invoke();
        }

        /// <summary>
        /// Try to dispose this ObjectPoolMember.
        /// </summary>
        public void Release ()
        {
            if (!m_isInitialized)
                return;

            if (!m_isActive)
                return;

            m_isActive = false;

            if (m_onRelease != null)
                m_onRelease.Invoke();
        }
        #endregion

        #region MonoBehaviour Messages
        /// <summary>
        /// 
        /// </summary>
        protected virtual void OnDisable ()
        {
            if (m_releaseOnDisable)
                Release();
        }
        #endregion
    }
}
