﻿using UnityEngine;

namespace UnityEssentials.ExtensionMethods
{
    /// <summary>
    /// Extension methods to UnityEngine.Rigidbody.
    /// </summary>
    public static class EMRigidbody
    {
        #region Static Methods
        /// <summary>
        /// Shorthand method for 'rigidbody.velocity = new Vector3(rigidbody.velocity.x + /x/, rigidbody.velocity.y, rigidbody.velocity.z)'.
        /// </summary>
        /// <param name="rigidbody"></param>
        /// <param name="x"></param>
        public static void AddVelocityX (this Rigidbody rigidbody, float x)
        {
            rigidbody.velocity = new Vector3(rigidbody.velocity.x + x, rigidbody.velocity.y, rigidbody.velocity.z);
        }

        /// <summary>
        /// Shorthand method for 'rigidbody.velocity = new Vector3(rigidbody.velocity.x + /x/, rigidbody.velocity.y + /y/, rigidbody.velocity.z)'.
        /// </summary>
        /// <param name="rigidbody"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public static void AddVelocityXY (this Rigidbody rigidbody, float x, float y)
        {
            rigidbody.velocity = new Vector3(rigidbody.velocity.x + x, rigidbody.velocity.y + y, rigidbody.velocity.z);
        }

        /// <summary>
        /// Shorthand method for 'rigidbody.velocity = new Vector3(rigidbody.velocity.x + /x/, rigidbody.velocity.y, rigidbody.velocity.z + /z/)'.
        /// </summary>
        /// <param name="rigidbody"></param>
        /// <param name="x"></param>
        /// <param name="z"></param>
        public static void AddVelocityXZ (this Rigidbody rigidbody, float x, float z)
        {
            rigidbody.velocity = new Vector3(rigidbody.velocity.x + x, rigidbody.velocity.y, rigidbody.velocity.z + z);
        }

        /// <summary>
        /// Shorthand method for 'rigidbody.velocity = new Vector3(rigidbody.velocity.x, rigidbody.velocity.y + /y/, rigidbody.velocity.z)'.
        /// </summary>
        /// <param name="rigidbody"></param>
        /// <param name="y"></param>
        public static void AddVelocityY (this Rigidbody rigidbody, float y)
        {
            rigidbody.velocity = new Vector3(rigidbody.velocity.x, rigidbody.velocity.y + y, rigidbody.velocity.z);
        }

        /// <summary>
        /// Shorthand method for 'rigidbody.velocity = new Vector3(rigidbody.velocity.x, rigidbody.velocity.y + /y/, rigidbody.velocity.z + /z/)'.
        /// </summary>
        /// <param name="rigidbody"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public static void AddVelocityYZ (this Rigidbody rigidbody, float y, float z)
        {
            rigidbody.velocity = new Vector3(rigidbody.velocity.x, rigidbody.velocity.y + y, rigidbody.velocity.z + z);
        }

        /// <summary>
        /// Shorthand method for 'rigidbody.velocity = new Vector3(rigidbody.velocity.x, rigidbody.velocity.y, rigidbody.velocity.z + /z/)'.
        /// </summary>
        /// <param name="rigidbody"></param>
        /// <param name="z"></param>
        public static void AddVelocityZ (this Rigidbody rigidbody, float z)
        {
            rigidbody.velocity = new Vector3(rigidbody.velocity.x, rigidbody.velocity.y, rigidbody.velocity.z + z);
        }

        /// <summary>
        /// Shorthand method for 'if (rigidbody.velocity.magnitude > limit) rigidbody.velocity = rigidbody.velocity.normalized * /limit/'.
        /// </summary>
        /// <param name="rigidbody"></param>
        /// <param name="limit"></param>
        public static void LimitVelocity (this Rigidbody rigidbody, float limit)
        {
            if (rigidbody.velocity.magnitude > limit)
                rigidbody.velocity = rigidbody.velocity.normalized * limit;
        }

        /// <summary>
        /// Shorthand method for 'rigidbody.velocity = new Vector3(/x/, rigidbody.velocity.y, rigidbody.velocity.z)'.
        /// </summary>
        /// <param name="rigidbody"></param>
        /// <param name="x"></param>
        public static void SetVelocityX (this Rigidbody rigidbody, float x)
        {
            rigidbody.velocity = new Vector3(x, rigidbody.velocity.y, rigidbody.velocity.z);
        }

        /// <summary>
        /// Shorthand method for 'rigidbody.velocity = new Vector3(/x/, /y/, rigidbody.velocity.z)'.
        /// </summary>
        /// <param name="rigidbody"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public static void SetVelocityXY (this Rigidbody rigidbody, float x, float y)
        {
            rigidbody.velocity = new Vector3(x, y, rigidbody.velocity.z);
        }

        /// <summary>
        /// Shorthand method for 'rigidbody.velocity = new Vector3(/x/, rigidbody.velocity.y, /z/)'.
        /// </summary>
        /// <param name="rigidbody"></param>
        /// <param name="x"></param>
        /// <param name="z"></param>
        public static void SetVelocityXZ (this Rigidbody rigidbody, float x, float z)
        {
            rigidbody.velocity = new Vector3(x, rigidbody.velocity.y, z);
        }

        /// <summary>
        /// Shorthand method for 'rigidbody.velocity = new Vector3(rigidbody.velocity.x, /y/, rigidbody.velocity.z)'.
        /// </summary>
        /// <param name="rigidbody"></param>
        /// <param name="y"></param>
        public static void SetVelocityY (this Rigidbody rigidbody, float y)
        {
            rigidbody.velocity = new Vector3(rigidbody.velocity.x, y, rigidbody.velocity.z);
        }

        /// <summary>
        /// Shorthand method for 'rigidbody.velocity = new Vector3(rigidbody.velocity.x, /y/, /z/)'.
        /// </summary>
        /// <param name="rigidbody"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public static void SetVelocityYZ (this Rigidbody rigidbody, float y, float z)
        {
            rigidbody.velocity = new Vector3(rigidbody.velocity.x, y, z);
        }

        /// <summary>
        /// Shorthand method for 'rigidbody.velocity = new Vector3(rigidbody.velocity.x, rigidbody.velocity.y, /z/)'.
        /// </summary>
        /// <param name="rigidbody"></param>
        /// <param name="z"></param>
        public static void SetVelocityZ (this Rigidbody rigidbody, float z)
        {
            rigidbody.velocity = new Vector3(rigidbody.velocity.x, rigidbody.velocity.y, z);
        }
        #endregion
    }
}
