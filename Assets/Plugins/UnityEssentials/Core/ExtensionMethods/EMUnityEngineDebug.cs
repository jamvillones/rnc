﻿using UnityEngine;

namespace UnityEssentials.ExtensionMethods
{
    /// <summary>
    /// Extension methods to UnityEngine.Debug.
    /// </summary>
    public static class EMUnityEngineDebug
    {
        #region Static Methods
        /// <summary>
        /// Logs /message/ to the Unity Console.
        /// </summary>
        /// <param name="message">String or object to be converted to string representation for display.</param>
        public static void DebugLog (this object message)
        {
            Debug.Log(message);
        }

        /// <summary>
        /// A variant of Debug.Log that logs a warning message Console.
        /// </summary>
        /// <param name="message">String or object to be converted to string representation for display.</param>
        public static void DebugLogWarning (this object message)
        {
            Debug.LogWarning(message);
        }

        /// <summary>
        /// A variant of Debug.Log that logs an error message Console.
        /// </summary>
        /// <param name="message">String or object to be converted to string representation for display.</param>
        public static void DebugLogError (this object message)
        {
            Debug.LogError(message);
        }
        #endregion
    }
}
