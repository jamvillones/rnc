﻿using System;
using UnityEngine;

namespace UnityEssentials.ExtensionMethods
{
    /// <summary>
    /// Extension methods related to UnityEngine Rendering.
    /// </summary>
    public static class EMRendering
    {
        #region Static Methods
        /// <summary>
        /// Is renderer visible from /camera/.
        /// </summary>
        /// <param name="renderer"></param>
        /// <param name="camera"></param>
        /// <returns></returns>
        public static bool IsVisibleFrom (this Renderer renderer, Camera camera)
        {
            if (camera == null)
                throw new ArgumentNullException("Cannot be null.", "camera");

            Plane[] planes = GeometryUtility.CalculateFrustumPlanes(camera);

            return GeometryUtility.TestPlanesAABB(planes, renderer.bounds);
        }
        #endregion
    }
}
