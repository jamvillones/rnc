﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace UnityEssentials
{
    public abstract class DrawableDictionary { }

    /// <summary>
    /// Serializable Dictionary of TKey with TValue.
    /// </summary>
    [Serializable]
    public abstract class SerializableDictionary<TKey, TValue> : DrawableDictionary, IDictionary<TKey, TValue>, ISerializationCallbackReceiver
    {
        #region Fields
        [NonSerialized]
        private Dictionary<TKey, TValue> _dictionary;

        [SerializeField]
        private TKey[] _keys;

        [SerializeField]
        private TValue[] _values;
        #endregion

        #region IDictionary Properties
        public TValue this[TKey key]
        {
            get { if (_dictionary == null) throw new KeyNotFoundException(); return _dictionary[key]; }
            set { if (_dictionary == null) _dictionary = new Dictionary<TKey, TValue>(); _dictionary[key] = value; }
        }

        public int Count
        {
            get { return (_dictionary != null) ? _dictionary.Count : 0; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public ICollection<TKey> Keys
        {
            get { if (_dictionary == null) _dictionary = new Dictionary<TKey, TValue>(); return _dictionary.Keys; }
        }

        public ICollection<TValue> Values
        {
            get { if (_dictionary == null) _dictionary = new Dictionary<TKey, TValue>(); return _dictionary.Values; }
        }
        #endregion

        #region IDictionary Methods
        public void Add (KeyValuePair<TKey, TValue> item)
        {
            if (_dictionary == null)
                _dictionary = new Dictionary<TKey, TValue>();

            ICollection<KeyValuePair<TKey, TValue>> collection = _dictionary as ICollection<KeyValuePair<TKey, TValue>>;
            if (collection != null) collection.Add(item);
        }

        public void Add (TKey key, TValue value)
        {
            if (_dictionary == null)
                _dictionary = new Dictionary<TKey, TValue>();

            _dictionary.Add(key, value);
        }

        public void Clear ()
        {
            if (_dictionary != null)
                _dictionary.Clear();
        }

        public bool Contains (KeyValuePair<TKey, TValue> item)
        {
            if (_dictionary == null)
                return false;

            ICollection<KeyValuePair<TKey, TValue>> collection = _dictionary as ICollection<KeyValuePair<TKey, TValue>>;
            if (collection != null) return collection.Contains(item);
            else return false;
        }

        public bool ContainsKey (TKey key)
        {
            if (_dictionary == null)
                return false;

            return _dictionary.ContainsKey(key);
        }

        public void CopyTo (KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            if (_dictionary == null)
                _dictionary = new Dictionary<TKey, TValue>();

            ICollection<KeyValuePair<TKey, TValue>> collection = _dictionary as ICollection<KeyValuePair<TKey, TValue>>;
            if (collection != null) collection.CopyTo(array, arrayIndex);
        }

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            if (_dictionary == null)
                return Enumerable.Empty<KeyValuePair<TKey, TValue>>().GetEnumerator();

            return _dictionary.GetEnumerator();
        }

        public bool Remove (KeyValuePair<TKey, TValue> item)
        {
            if (_dictionary == null)
                return false;

            ICollection<KeyValuePair<TKey, TValue>> collection = _dictionary as ICollection<KeyValuePair<TKey, TValue>>;
            if (collection != null) return collection.Remove(item);
            else return false;
        }

        public bool Remove (TKey key)
        {
            if (_dictionary == null)
                return false;

            return _dictionary.Remove(key);
        }

        public bool TryGetValue (TKey key, out TValue value)
        {
            if (_dictionary == null)
            {
                value = default(TValue);
                return false;
            }

            return _dictionary.TryGetValue(key, out value);
        }

        IEnumerator IEnumerable.GetEnumerator ()
        {
            if (_dictionary == null)
                return default(Dictionary<TKey, TValue>.Enumerator);

            return _dictionary.GetEnumerator();
        }
        #endregion

        #region ISerializationCallbackReceiver Methods
        public void OnAfterDeserialize ()
        {
            if (_keys != null && _values != null)
            {
                if (_dictionary == null)
                    _dictionary = new Dictionary<TKey, TValue>();
                else
                    _dictionary.Clear();

                for (int i = 0; i < _keys.Length; i++)
                {
                    if (i < _values.Length)
                        _dictionary[_keys[i]] = _values[i];
                    else
                        _dictionary[_keys[i]] = default(TValue);
                }
            }

            _keys = null;
            _values = null;
        }

        public void OnBeforeSerialize ()
        {
            if (_dictionary == null || _dictionary.Count == 0)
            {
                _keys = null;
                _values = null;
            }
            else
            {
                int counter = 0;
                int count = _dictionary.Count;
                Dictionary<TKey, TValue>.Enumerator enumerator = _dictionary.GetEnumerator();

                _keys = new TKey[count];
                _values = new TValue[count];

                while (enumerator.MoveNext())
                {
                    _keys[counter] = enumerator.Current.Key;
                    _values[counter] = enumerator.Current.Value;

                    counter++;
                }
            }
        }
        #endregion
    }
}
