﻿using UnityEngine;

namespace UnityEssentials
{
    /// <summary>
    /// Singleton of T. Ensures that class of T has only one instance and has a global access point of that instance.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class Singleton <T> : MonoBehaviour where T : MonoBehaviour
    {
        #region Constants
        private const string LOG_CREATEDINSTANCE = "An instance was needed in the scene, creating a new one.";
        private const string LOG_FOUNDINSTANCE = "An instance was found in the scene, using it.";
        private const string LOG_PERSISTENTINSTACEDESTROYED_TRUE = "The scene persistent instance has already been destroyed. Would not create a new instance anymore.";
        private const string LOG_AUTODELETEDUPLICATES_TRUE = "A duplicate was found in the scene. Deleting it.";
        private const string LOG_AUTODELETEDUPLICATES_FALSE = "A duplicate was found in the scene. Disabling it.";
        #endregion

        #region Statics
        private static T instance = null;
        private static bool scenePersistent = true;
        private static bool autoDeleteDuplicates = false;

        private static bool persistentInstanceDestroyed = false;
        private static object threadLock = new object();

        private static string Name
        {
            get { return typeof(T).ToString(); }
        }

        /// <summary>
        /// Is Singleton of T Scene Persistent?
        /// </summary>
        public static bool ScenePersistent
        {
            get { return scenePersistent; }
        }

        /// <summary>
        /// Is Singleton of T Auto Deletes Duplicates?
        /// </summary>
        public static bool AutoDeleteDuplicates
        {
            get { return autoDeleteDuplicates; }
        }

        /// <summary>
        /// Singleton of T's Instance?
        /// </summary>
        public static T Instance
        {
            get
            {
                if (persistentInstanceDestroyed)
                {
                    Debug.LogWarning(Name + ": " + LOG_PERSISTENTINSTACEDESTROYED_TRUE);
                    return null;
                }

                lock (threadLock)
                {
                    if (instance == null)
                    {
                        T[] instances = FindObjectsOfType(typeof(T)) as T[];

                        if (instances != null && instances.Length > 0 && instances[0] != null)
                        {
                            if (instances.Length > 1)
                            {
                                T instanceDuplicate = null;

                                for (int i = instances.Length - 1; i <= 1; i--)
                                {
                                    instanceDuplicate = instances[i];

                                    if (instanceDuplicate == null)
                                        continue;

                                    if (autoDeleteDuplicates)
                                    {
                                        Debug.LogWarning(Name + ": " + LOG_AUTODELETEDUPLICATES_TRUE);
                                        Destroy(instanceDuplicate);
                                    }
                                    else
                                    {
                                        Debug.LogWarning(Name + ": " + LOG_AUTODELETEDUPLICATES_FALSE);
                                        instanceDuplicate.enabled = false;
                                    }

                                    instanceDuplicate = null;
                                }
                            }

                            instance = instances[0];
                        }
                        else
                        {
                            GameObject instanceGameObject = new GameObject(Name + " " + "Singleton", typeof(T));
                            instance = instanceGameObject.GetComponent<T>();

                            if (scenePersistent)
                                DontDestroyOnLoad(instanceGameObject);

                            Debug.Log(Name + ": " + LOG_CREATEDINSTANCE);
                        }
                    }

                    return instance;
                }
            }
        }
        #endregion

        #region Fields
        [SerializeField]
        private bool m_isScenePersistent = true;

        [SerializeField]
        private bool m_isAutoDeleteDuplicates = false;
        #endregion

        #region Properties
        /// <summary>
        /// Is instance Scene Persistent?
        /// </summary>
        public bool IsScenePersistent
        {
            get { return m_isScenePersistent; }
        }

        /// <summary>
        /// Is instance Auto Deletes Duplicates?
        /// </summary>
        public bool IsAutoDeleteDuplicates
        {
            get { return m_isAutoDeleteDuplicates; }
        }

        /// <summary>
        /// Is instance the Singleton of T Instance?
        /// </summary>
        public bool IsInstance
        {
            get { return (instance == GetComponent<T>()); }
        }
        #endregion

        #region MonoBehaviour Messages
        /// <summary>
        /// 
        /// </summary>
        protected virtual void Awake ()
        {
            if (instance == null)
            {
                instance = GetComponent<T>();

                scenePersistent = m_isScenePersistent;
                autoDeleteDuplicates = m_isAutoDeleteDuplicates;

                if (scenePersistent)
                    DontDestroyOnLoad(gameObject);

                Debug.Log(Name + ": " + LOG_FOUNDINSTANCE);
            }
            else
            {
                T instanceThis = GetComponent<T>();

                if (instance != instanceThis)
                {
                    if (autoDeleteDuplicates)
                    {
                        Debug.LogWarning(Name + ": " + LOG_AUTODELETEDUPLICATES_TRUE);
                        Destroy(instanceThis);
                    }
                    else
                    {
                        Debug.LogWarning(Name + ": " + LOG_AUTODELETEDUPLICATES_FALSE);
                        instanceThis.enabled = false;
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual void OnDestroy ()
        {
            if (scenePersistent && instance != null && instance == GetComponent<T>())
            {
                persistentInstanceDestroyed = true;
            }
        }
        #endregion
    }
}
