﻿using System.IO;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace UnityEssentialsEditor.Windows
{
    /// <summary>
    /// 
    /// </summary>
    public class ScenesInBuildWindow : EditorWindow
    {
        #region Statics
        [MenuItem("Window/Scenes in Build %`", false, 1998)]
        private static void Initialize ()
        {
            ScenesInBuildWindow scenesInBuildWindow = GetWindow<ScenesInBuildWindow>(false, "Scenes in Build", true);
            scenesInBuildWindow.Repaint();
        }
        #endregion

        #region Fields
        private int m_enabledIndex = -1;
        private Vector2 m_scrollPosition = Vector2.zero;
        #endregion

        #region EditorWindow Messages
        /// <summary>
        /// 
        /// </summary>
        protected virtual void OnGUI ()
        {
            GUILayout.Label(PlayerSettings.productName + " (" + PlayerSettings.bundleVersion + ")", EditorStyles.boldLabel);

            m_enabledIndex = -1;
            m_scrollPosition = EditorGUILayout.BeginScrollView(m_scrollPosition);

            for (int i = 0; i < EditorBuildSettings.scenes.Length; i++)
            {
                EditorBuildSettingsScene scene = EditorBuildSettings.scenes[i];

                if (scene != null)
                {
                    if (scene.enabled)
                        m_enabledIndex += 1;

                    string sceneName = Path.GetFileNameWithoutExtension(scene.path);
                    string buttonText = ((scene.enabled) ? ("☑ " + m_enabledIndex) : ("☒ #")) + ": " + sceneName;

                    if (GUILayout.Button(buttonText, new GUIStyle(GUI.skin.GetStyle("Button")) { alignment = TextAnchor.MiddleLeft}))
                    {
                        EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
                        EditorSceneManager.OpenScene(scene.path);
                    }
                }
            }

            EditorGUILayout.EndScrollView();
        }
        #endregion
    }
}
