﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEditor.ProjectWindowCallback;
using UnityEngine;

namespace UnityEssentialsEditor.Windows
{
    /// <summary>
    /// 
    /// </summary>
    public class ScriptableObjectWindow : EditorWindow
    {
        #region Statics
        private static string assemblyUnityProjectName = "Assembly-CSharp";
        private static string assemblyUnityEssentialsName = "UnityEssentials";
        private static Rect windowSize = new Rect(0.0f, 0.0f, 500.0f, 50.0f);

        [MenuItem("Assets/Create/ScriptableObject", false, 201)]
        private static void Initialize ()
        {
            Assembly assemblyUnityProject = GetAssembly(assemblyUnityProjectName);
            Assembly assemblyUnityEssentials = GetAssembly(assemblyUnityEssentialsName);
            List<Type> allScriptableObjects = new List<Type>();

            if (assemblyUnityProject != null)
            {
                Type[] types = assemblyUnityProject.GetTypes();

                if (types != null && types.Length > 0)
                    allScriptableObjects.AddRange(types.Where(x => x.IsSubclassOf(typeof(ScriptableObject))).ToArray());
            }

            if (assemblyUnityEssentials != null)
            {
                Type[] types = assemblyUnityEssentials.GetTypes();

                if (types != null && types.Length > 0)
                    allScriptableObjects.AddRange(types.Where(x => x.IsSubclassOf(typeof(ScriptableObject))).ToArray());
            }

            ScriptableObjectWindow scriptableObjectWindow = GetWindowWithRect<ScriptableObjectWindow>(windowSize, true, "Create a new ScriptableObject", true);
            scriptableObjectWindow.SetTypes(allScriptableObjects.ToArray());
            scriptableObjectWindow.ShowPopup();
        }

        private static Assembly GetAssembly (string assemblyName)
        {
            try { return Assembly.Load(new AssemblyName(assemblyName)); }
            catch (Exception ex)
            {
                Debug.LogWarning(ex.Message);
                return null;
            }
        }
        #endregion

        #region Properties
        private int m_selectedIndex = 0;
        private List<string> m_typeNames = new List<string>();
        private List<Type> m_types = new List<Type>();
        #endregion

        #region Methods
        private void SetTypes (Type[] types)
        {
            if (types != null && types.Length > 0)
            {
                m_typeNames.Clear();
                m_types.Clear();

                m_typeNames.AddRange(types.Select(x => x.FullName));
                m_types.AddRange(types);
            }
        }
        #endregion

        #region EditorWindow Messages
        /// <summary>
        /// 
        /// </summary>
        protected virtual void OnGUI ()
        {
            GUILayout.Label("ScriptableObject Class");

            GUILayout.BeginHorizontal();

            m_selectedIndex = EditorGUILayout.Popup(m_selectedIndex, m_typeNames.ToArray());

            if (GUILayout.Button("Create", GUILayout.Height(14.0f), GUILayout.Width(75.0f)))
            {
                if (m_types.Count > m_selectedIndex && m_types[m_selectedIndex] != null)
                {
                    ScriptableObject scriptableObject = CreateInstance(m_types[m_selectedIndex]);
                    ProjectWindowUtil.StartNameEditingIfProjectWindowExists(scriptableObject.GetInstanceID(),
                                                                            CreateInstance<ScriptableObjectWindowEndNameEditAction>(),
                                                                            string.Format("{0}.asset", m_typeNames[m_selectedIndex]),
                                                                            AssetPreview.GetMiniThumbnail(scriptableObject),
                                                                            null);
                    Close();
                }
            }

            GUILayout.EndHorizontal();
        }
        #endregion
    }

    internal class ScriptableObjectWindowEndNameEditAction : EndNameEditAction
    {
        #region EndNameEditAction Methods
        public override void Action (int instanceId, string pathName, string resourceFile)
        {
            AssetDatabase.CreateAsset(EditorUtility.InstanceIDToObject(instanceId), AssetDatabase.GenerateUniqueAssetPath(pathName));
        }
        #endregion
    }
}
