﻿using System;
using System.IO;
using System.Linq;
using UnityEditor;

namespace UnityEssentialsEditor
{
    /// <summary>
    /// Unity Builder.
    /// </summary>
    public class UnityBuilder
    {
        #region Fields
        private static readonly string _versionNumber;
        private static readonly string _buildNumber;
        private static readonly string _keystorePath;
        private static readonly string _keyaliasName;
        private static readonly string _keyaliasPass;

        private const string AndroidBuildPath = "build/android/UnityBuild.apk";
        private const string XcodeBuildPath = "Scratch/Xcode/";
        #endregion

        #region Constructor
        static UnityBuilder ()
        {
            _versionNumber = Environment.GetEnvironmentVariable("VERSION_NUMBER");
            if (string.IsNullOrEmpty(_versionNumber)) _versionNumber = "1.0.0.0";

            _buildNumber = Environment.GetEnvironmentVariable("BUILD_NUMBER");
            if (string.IsNullOrEmpty(_buildNumber)) _buildNumber = "1";

            _keystorePath = Environment.GetEnvironmentVariable("KEYSTORE_PATH");
            if (string.IsNullOrEmpty(_keystorePath)) _keystorePath = string.Empty;

            _keyaliasName = Environment.GetEnvironmentVariable("KEYALIAS_NAME");
            if (string.IsNullOrEmpty(_keyaliasName)) _keyaliasName = string.Empty;

            _keyaliasPass = Environment.GetEnvironmentVariable("KEYALIAS_PASS");
            if (string.IsNullOrEmpty(_keyaliasPass)) _keyaliasPass = string.Empty;

            PlayerSettings.bundleVersion = _versionNumber;
        }
        #endregion

        #region Methods
        static void Android ()
        {
            int bundleVersionCode;

            int.TryParse(_buildNumber, out bundleVersionCode);
            PlayerSettings.Android.bundleVersionCode = bundleVersionCode;

            if (!string.IsNullOrEmpty(_keystorePath) && !string.IsNullOrEmpty(_keyaliasName) && !string.IsNullOrEmpty(_keyaliasPass))
            {
                PlayerSettings.Android.keystoreName = Path.GetFullPath(_keystorePath).Replace('\\', '/');
                PlayerSettings.Android.keyaliasName = _keyaliasName;
                PlayerSettings.Android.keyaliasPass = _keyaliasPass;
            }

            string s = BuildPipeline.BuildPlayer(GetScenes(), AndroidBuildPath, BuildTarget.Android, BuildOptions.None);

            EditorUtility.DisplayDialog("Test", s, "OK");
        }

        static void iOS ()
        {
            CheckDirectory(XcodeBuildPath);
            PlayerSettings.SetScriptingBackend(BuildTargetGroup.iOS, ScriptingImplementation.IL2CPP);
            PlayerSettings.SetArchitecture(BuildTargetGroup.iOS, 2);
            BuildPipeline.BuildPlayer(GetScenes(), XcodeBuildPath, BuildTarget.iOS, BuildOptions.None);
        }

        static void CheckDirectory (string path)
        {
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
        }

        static string[] GetScenes ()
        {
            return EditorBuildSettings.scenes.Where(scene => scene.enabled).Select(scene => scene.path).ToArray();
        }
        #endregion
    }
}
