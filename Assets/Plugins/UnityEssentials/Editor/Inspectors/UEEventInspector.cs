﻿using UnityEditor;
using UnityEngine;
using UnityEssentials;

namespace UnityEssentialsEditor.Inspectors
{
    /// <summary>
    /// 
    /// </summary>
    [CustomEditor(typeof(UEEvent))]
    public class UEEventInspector : Editor
    {
        #region Fields
        private UEEvent m_ueEvent;

        private SerializedProperty m_eventType;
        private SerializedProperty m_keyPressType;
        private SerializedProperty m_keyCode;
        private SerializedProperty m_timerDelay;
        private SerializedProperty m_timerRepeatDelay;
        private SerializedProperty m_timerRepeatsMax;
        private SerializedProperty m_invokesMax;
        private SerializedProperty m_onInvoke;
        #endregion

        #region Methods
        private void ApplyLimits ()
        {
            if (m_timerDelay != null)
                m_timerDelay.floatValue = Mathf.Clamp(m_timerDelay.floatValue, 0.0f, float.MaxValue);

            if (m_timerRepeatDelay != null)
                m_timerRepeatDelay.floatValue = Mathf.Clamp(m_timerRepeatDelay.floatValue, 0.0f, float.MaxValue);

            if (m_timerRepeatsMax != null)
                m_timerRepeatsMax.intValue = Mathf.Clamp(m_timerRepeatsMax.intValue, -1, int.MaxValue);

            if (m_invokesMax != null)
                m_invokesMax.intValue = Mathf.Clamp(m_invokesMax.intValue, -1, int.MaxValue);
        }
        #endregion

        #region Editor Methods
        /// <summary>
        /// 
        /// </summary>
        public override void OnInspectorGUI ()
        {
            //base.OnInspectorGUI();

            if (m_ueEvent != null)
            {
                if (m_eventType != null)
                {
                    string infoText = string.Empty;
                    string warningText = string.Empty;

                    EditorGUILayout.PropertyField(m_eventType, new GUIContent("Event Type"));

                    switch (m_ueEvent.EventType)
                    {
                        case UEEvent.UEEventType.Event:
                            break;

                        case UEEvent.UEEventType.Key:
                            if (m_keyPressType != null)
                                EditorGUILayout.PropertyField(m_keyPressType, new GUIContent("Key Press Type"));

                            if (m_keyCode != null)
                                EditorGUILayout.PropertyField(m_keyCode, new GUIContent("Key Code"));

                            break;

                        case UEEvent.UEEventType.TimerOnce:
                            if (m_timerDelay != null)
                                EditorGUILayout.PropertyField(m_timerDelay, new GUIContent("Delay"));

                            infoText += "\n" + "Timer Started: " + m_ueEvent.TimerStarted;
                            infoText += "\n" + "Timer Active: " + m_ueEvent.TimerActive;
                            infoText += "\n" + "Timer Ended: " + m_ueEvent.TimerEnded;

                            break;

                        case UEEvent.UEEventType.TimerRepeat:
                            if (m_timerDelay != null)
                                EditorGUILayout.PropertyField(m_timerDelay, new GUIContent("Initial Delay"));

                            if (m_timerRepeatDelay != null)
                                EditorGUILayout.PropertyField(m_timerRepeatDelay, new GUIContent("Repeat Delay"));

                            if (m_timerRepeatsMax != null)
                                EditorGUILayout.PropertyField(m_timerRepeatsMax, new GUIContent("Max Repeats", "-1 is infinite."));

                            infoText += "\n" + "Timer Started: " + m_ueEvent.TimerStarted;
                            infoText += "\n" + "Timer Active: " + m_ueEvent.TimerActive;
                            infoText += "\n" + "Timer Ended: " + m_ueEvent.TimerEnded;
                            infoText += "\n" + "Timer Repeats: " + m_ueEvent.TimerRepeats;

                            break;

                        default:
                            break;
                    }

                    if (m_invokesMax != null)
                        EditorGUILayout.PropertyField(m_invokesMax, new GUIContent("Max Invokes", "-1 is infinite."));

                    if (m_onInvoke != null)
                        EditorGUILayout.PropertyField(m_onInvoke);

                    infoText += "\n" + "Invokes: " + m_ueEvent.Invokes;

                    infoText += "\n";
                    EditorGUILayout.HelpBox(infoText, MessageType.Info);

                    if (m_ueEvent.EventType.Equals(UEEvent.UEEventType.TimerRepeat) &&
                        m_ueEvent.InvokesMax >= 0 &&
                        (m_ueEvent.TimerRepeatsMax < 0 || m_ueEvent.InvokesMax < m_ueEvent.TimerRepeatsMax))
                    {
                        warningText += "\n" + "Max Invokes is less than Max Repeats.";
                    }

                    if (!string.IsNullOrEmpty(warningText))
                    {
                        warningText += "\n";
                        EditorGUILayout.HelpBox(warningText, MessageType.Warning);
                    }

                    ApplyLimits();
                }
            }

            if (GUI.changed)
                serializedObject.ApplyModifiedProperties();
        }
        #endregion

        #region Editor Messages
        /// <summary>
        /// 
        /// </summary>
        protected virtual void OnEnable ()
        {
            m_ueEvent = target as UEEvent;
            m_eventType = serializedObject.FindProperty("m_eventType");
            m_keyPressType = serializedObject.FindProperty("m_keyPressType");
            m_keyCode = serializedObject.FindProperty("m_keyCode");
            m_timerDelay = serializedObject.FindProperty("m_timerDelay");
            m_timerRepeatDelay = serializedObject.FindProperty("m_timerRepeatDelay");
            m_timerRepeatsMax = serializedObject.FindProperty("m_timerRepeatsMax");
            m_invokesMax = serializedObject.FindProperty("m_invokesMax");
            m_onInvoke = serializedObject.FindProperty("m_onInvoke");
        }
        #endregion
    }
}
