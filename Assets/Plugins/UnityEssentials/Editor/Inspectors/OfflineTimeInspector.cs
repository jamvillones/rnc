﻿using UnityEditor;
using UnityEngine;
using UnityEssentials;

namespace UnityEssentialsEditor.Inspectors
{
    /// <summary>
    /// 
    /// </summary>
    [CustomEditor(typeof(OfflineTime))]
    public class OfflineTimeInspector : Editor
    {
        #region Fields
        private OfflineTime m_offlineTime;
        #endregion

        #region Editor Methods
        /// <summary>
        /// 
        /// </summary>
        public override void OnInspectorGUI ()
        {
            base.OnInspectorGUI();

            if (m_offlineTime != null)
            {
                string lastOnline = "\n" + "Last Online (UTC):" + "\n" +
                                    "\t" + PlayerPrefs.GetString(OfflineTime.PLAYERPREFS_KEY) + "\n";

                string timePassed = "\n" + "Time Passed since Last Online:" + "\n" +
                                    "\t" + m_offlineTime.TimePassed.ToString() + "\n";

                string warningNotScenePersistent = "\n" + "Offline Time may not work as expected when it is not scene persistent." + "\n";

                EditorGUILayout.Space();

                EditorGUILayout.HelpBox(lastOnline, MessageType.Info);
                EditorGUILayout.HelpBox(timePassed, MessageType.Info);

                if (!m_offlineTime.IsScenePersistent)
                    EditorGUILayout.HelpBox(warningNotScenePersistent, MessageType.Warning);

                EditorGUILayout.Space();
            }
        }
        #endregion

        #region Editor Messages
        /// <summary>
        /// 
        /// </summary>
        protected virtual void OnEnable ()
        {
            m_offlineTime = target as OfflineTime;
        }
        #endregion
    }
}
