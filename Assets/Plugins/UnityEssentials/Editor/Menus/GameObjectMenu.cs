﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace UnityEssentialsEditor.Menus
{
    /// <summary>
    /// 
    /// </summary>
    public class GameObjectMenu : Editor
    {
        #region Constants
        private const int priority = 0;
        #endregion

        #region Static Methods
        [MenuItem("Unity Essentials/Create Empty Parent &#M", false, (priority + 0))]
        private static void CreateParent ()
        {
            Transform[] transforms = Selection.GetTransforms(SelectionMode.TopLevel | SelectionMode.OnlyUserModifiable);

            GameObject parent = null;

            foreach (Transform t in transforms)
            {
                parent = new GameObject("GameObject");
                parent.transform.position = t.position;

                if (t.parent != null)
                    parent.transform.parent = t.parent;

                t.parent = parent.transform;
            }
        }

        [MenuItem("Unity Essentials/Select Main Camera &#C", false, (priority + 1))]
        private static void SelectMainCamera ()
        {
            if (Camera.main != null)
                Selection.activeGameObject = Camera.main.gameObject;
        }

        [MenuItem("Unity Essentials/Show Mesh Info &#I", false, (priority + 2))]
        private static void ShowMeshInfo ()
        {
            int vertexCount = 0, triangleCount = 0, meshCount = 0, vertexCountAverage = 0, triangeCountAverage = 0;

            GameObject[] gameObjects = Selection.GetFiltered(typeof(GameObject), SelectionMode.TopLevel).Select(x => x as GameObject).ToArray();

            foreach (GameObject g in gameObjects)
            {
                if (g == null)
                    continue;

                SkinnedMeshRenderer[] skinnedMeshRenderers = g.GetComponentsInChildren<SkinnedMeshRenderer>();
                MeshFilter[] meshFilters = g.GetComponentsInChildren<MeshFilter>();
                List<Mesh> meshes = new List<Mesh>();

                meshes.AddRange(skinnedMeshRenderers.Where(x => x != null).Select(x => x.sharedMesh));
                meshes.AddRange(meshFilters.Where(x => x != null).Select(x => x.sharedMesh));

                foreach (Mesh m in meshes)
                {
                    if (m == null)
                    {
                        Debug.LogWarning("You have a missing mesh in your scene.");
                        continue;
                    }

                    vertexCount += m.vertexCount;
                    triangleCount += m.triangles.Length / 3;
                    meshCount += 1;
                }
            }

            if (meshCount > 0)
            {
                vertexCountAverage = vertexCount / meshCount;
                triangeCountAverage = triangleCount / meshCount;
            }

            EditorUtility.DisplayDialog("Mesh Info",
                                        vertexCount + ((vertexCount == 1) ? (" vertex") : (" vertices")) + " in selection." + "\n" +
                                        triangleCount + ((triangleCount == 1) ? (" triangle") : (" triangles")) + " in selection." + "\n" +
                                        meshCount + ((meshCount == 1) ? (" mesh") : (" meshes")) + " in selection." +
                                        ((meshCount > 0) ? ("\n" + "Average of " + vertexCountAverage + " vertices and " + triangeCountAverage + " triangles per mesh.") : ("")),
                                        "OK");
        }
        #endregion
    }
}
