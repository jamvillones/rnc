﻿using System;
using System.Reflection;
using UnityEditor;

namespace UnityEssentialsEditor.Menus
{
    /// <summary>
    /// 
    /// </summary>
    public class MiscellaneousMenu : Editor
    {
        #region Constants
        private const int priority = 1000;
        #endregion

        #region Static Methods
        [MenuItem("Unity Essentials/Get Mono Version", false, (priority + 0))]
        private static void GetMonoVersion ()
        {
            Type monoRuntime = Type.GetType("Mono.Runtime");

            if (monoRuntime != null)
            {
                MethodInfo getDisplayName = monoRuntime.GetMethod("GetDisplayName", BindingFlags.NonPublic | BindingFlags.Static);

                if (getDisplayName != null)
                    EditorUtility.DisplayDialog("Mono Version",
                                                getDisplayName.Invoke(null, null).ToString(),
                                                "OK");
            }
        }
        #endregion
    }
}
