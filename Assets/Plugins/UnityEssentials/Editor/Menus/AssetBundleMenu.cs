﻿using System.Linq;
using UnityEditor;
using UnityEngine;

namespace UnityEssentialsEditor.Menus
{
    /// <summary>
    /// 
    /// </summary>
    public class AssetBundleMenu : Editor
    {
        #region Constants
        private const int priority = 2000;
        #endregion

        #region Static Methods
        [MenuItem("Unity Essentials/AssetBundles/Get All AssetBundle Names", false, (priority + 0))]
        private static void GetAssetBundleNames ()
        {
            string[] getAllAssetBundleNames = AssetDatabase.GetAllAssetBundleNames();
            string allAssetBundleNames = string.Join("\n\t", getAllAssetBundleNames);
            Debug.Log("AssetBundles: " + ((string.IsNullOrEmpty(allAssetBundleNames)) ? ("None") : ("(" + getAllAssetBundleNames.Length + ")" + "\n\t" + allAssetBundleNames)));
        }

        [MenuItem("Unity Essentials/AssetBundles/Get Used AssetBundle Names", false, (priority + 1))]
        private static void GetUsedAssetBundleNames ()
        {
            string[] getAllAssetBundleNames = AssetDatabase.GetAllAssetBundleNames();
            string[] getAllUnusedAssetBundleNames = AssetDatabase.GetUnusedAssetBundleNames();
            string[] getAllUsedAssetBundleNames = getAllAssetBundleNames.Where(x => !getAllUnusedAssetBundleNames.Contains(x)).ToArray();
            string allUsedAssetBundleNames = string.Join("\n\t", getAllUsedAssetBundleNames);
            Debug.Log("Used AssetBundles: " + ((string.IsNullOrEmpty(allUsedAssetBundleNames)) ? ("None") : ("(" + getAllUsedAssetBundleNames.Length + ")" + "\n\t" + allUsedAssetBundleNames)));
        }

        [MenuItem("Unity Essentials/AssetBundles/Get Unused AssetBundle Names", false, (priority + 2))]
        private static void GetUnusedAssetBundleNames ()
        {
            string[] getAllUnusedAssetBundleNames = AssetDatabase.GetUnusedAssetBundleNames();
            string allUnusedAssetBundleNames = string.Join("\n\t", getAllUnusedAssetBundleNames);
            Debug.Log("Unused AssetBundles: " + ((string.IsNullOrEmpty(allUnusedAssetBundleNames)) ? ("None") : ("(" + getAllUnusedAssetBundleNames.Length + ")" + "\n\t" + allUnusedAssetBundleNames)));
        }

        [MenuItem("Unity Essentials/AssetBundles/Build for Android", false, (priority + 100))]
        private static void BuildAssetBundles_Android ()
        {
            BuildAssetBundles(BuildTarget.Android);
        }

        [MenuItem("Unity Essentials/AssetBundles/Build for iOS", false, (priority + 101))]
        private static void BuildAssetBundles_iOS ()
        {
            BuildAssetBundles(BuildTarget.iOS);
        }

        [MenuItem("Unity Essentials/AssetBundles/Build for Standalone Linux Universal", false, (priority + 102))]
        private static void BuildAssetBundles_StandaloneLinuxUniversal ()
        {
            BuildAssetBundles(BuildTarget.StandaloneLinuxUniversal);
        }

        [MenuItem("Unity Essentials/AssetBundles/Build for Standalone OSX Universal", false, (priority + 103))]
        private static void BuildAssetBundles_StandaloneOSXUniversal( )
        {
            //BuildAssetBundles(BuildTarget.StandaloneOSX);
        }

        [MenuItem("Unity Essentials/AssetBundles/Build for Standalone Windows", false, (priority + 104))]
        private static void BuildAssetBundles_StandaloneWindows ()
        {
            BuildAssetBundles(BuildTarget.StandaloneWindows);
        }

        [MenuItem("Unity Essentials/AssetBundles/Build for Standalone Windows 64", false, (priority + 105))]
        private static void BuildAssetBundles_StandaloneWindows64 ()
        {
            BuildAssetBundles(BuildTarget.StandaloneWindows64);
        }

        private static void BuildAssetBundles (BuildTarget buildTarget)
        {
            BuildPipeline.BuildAssetBundles("Assets/AssetBundles", BuildAssetBundleOptions.None, buildTarget);
        }
        #endregion
    }
}
