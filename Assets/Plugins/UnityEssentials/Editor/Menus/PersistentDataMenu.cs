﻿using System.IO;
using UnityEditor;
using UnityEngine;

namespace UnityEssentialsEditor.Menus
{
    /// <summary>
    /// 
    /// </summary>
    public class PersistentDataMenu : Editor
    {
        #region Constants
        private const int priority = 100;
        #endregion

        #region Static Methods
        [MenuItem("Unity Essentials/Delete All PlayerPrefs", false, (priority + 0))]
        private static void DeleteAllPlayerPrefs ()
        {
            PlayerPrefs.DeleteAll();

            EditorUtility.DisplayDialog("PlayerPrefs", "All PlayerPrefs has been deleted!", "OK");
        }

        [MenuItem("Unity Essentials/Delete All PersistentData", false, (priority + 1))]
        private static void DeleteAllPersistentData ()
        {
            Directory.Delete(Application.persistentDataPath, true);

            EditorUtility.DisplayDialog("PersistentData", "All PersistentData has been deleted!", "OK");
        }

        [MenuItem("Unity Essentials/Clear Cache", false, (priority + 2))]
        private static void CleanCache ()
        {
            if (Caching.ClearCache())
                EditorUtility.DisplayDialog("Clear Cache", "Cache clearing has succeeded!", "OK");
            else
                EditorUtility.DisplayDialog("Clear Cache", "Cache clearing has failed!", "OK");
        }
        #endregion
    }
}
